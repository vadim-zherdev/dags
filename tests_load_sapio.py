from unittest import TestCase
from unittest.mock import patch, MagicMock

from extensions.sapio_utils import (
    populate_sapio_tables,
    SCHEMA,
    TABLE_PREFIX,
    populate_rhe_plate_well,
    populate_rhe_exp,
    populate_rhe_tables_from_egnyte,
    EGNYTE_FILES_DATA,
    RHE_PLATE_WELL_TMP_TABLE_NAME,
)
from extensions.mocks import (
    MockPostgresHookWithSetResponse,
    MockSapioAPIHelper,
    MockAirflowTaskInstance,
    MockEgnyte,
)
from load_sapio_dag import merge_output, SUCCESS_STATUS, call_instrument_tool


@patch('extensions.sapio_utils.load_dataframe')
@patch('extensions.sapio_utils.get_sapio_helper')
class SapioTablesPopulationTestCase(TestCase):
    def setUp(self):
        mock_data_types = [
            {
                'datatype': 'TestType1',
                'datatype_id': 1,
            },
            {
                'datatype': 'TestType2',
                'datatype_id': 2,
            },
        ]
        mock_report = {
            'results': [[1, 2], [3, 4]],
            'columns': [
                {'datafield_name': 'TestField1', 'datafield_type': 1},
                {'datafield_name': 'TestField2', 'datafield_type': 2},
            ],
            'total': 2,
        }
        self.mock_sapio_helper = MockSapioAPIHelper(
            data_types=mock_data_types, report=mock_report)

    def test_sapio_tables_populated(self, nock_sapio, mock_load):
        nock_sapio.return_value = self.mock_sapio_helper
        mock_hook = MockPostgresHookWithSetResponse(
            response=[
                (TABLE_PREFIX + 'testtype1',),
                (TABLE_PREFIX + 'testtype2',)
            ]
        )
        result = populate_sapio_tables(mock_hook, 'OkiDoki')
        self.assertEqual(
            {'testtype1': 'OkiDoki', 'testtype2': 'OkiDoki'},
            result)
        self.assertTrue(nock_sapio.called)
        self.assertEqual(2, mock_load.call_count)
        df = mock_load.call_args_list[0][0][0]
        self.assertEqual(['testfield1', 'testfield2'], list(df))
        self.assertEqual([[1, 2], [3, 4]], df.values.tolist())
        self.assertEqual(
            TABLE_PREFIX + 'testtype1', mock_load.call_args_list[0][0][2])
        df = mock_load.call_args_list[1][0][0]
        self.assertEqual(['testfield1', 'testfield2'], list(df))
        self.assertEqual([[1, 2], [3, 4]], df.values.tolist())
        self.assertEqual(
            TABLE_PREFIX + 'testtype2', mock_load.call_args_list[1][0][2])

    def test_sapio_tables_populated_by_arg(self, nock_sapio, mock_load):
        nock_sapio.return_value = self.mock_sapio_helper
        mock_hook = MockPostgresHookWithSetResponse()
        result = populate_sapio_tables(
            mock_hook, 'DaDa', tables_data=[('testtype1',)])
        self.assertEqual({'testtype1': 'DaDa'}, result)
        self.assertTrue(nock_sapio.called)
        self.assertEqual(1, mock_load.call_count)
        df = mock_load.call_args_list[0][0][0]
        self.assertEqual(['testfield1', 'testfield2'], list(df))
        self.assertEqual([[1, 2], [3, 4]], df.values.tolist())
        self.assertEqual(
            TABLE_PREFIX + 'testtype1', mock_load.call_args_list[0][0][2])

    def test_sapio_tables_populated_with_debug(self, nock_sapio, mock_load):
        nock_sapio.return_value = self.mock_sapio_helper
        mock_hook = MockPostgresHookWithSetResponse()
        result = populate_sapio_tables(
            mock_hook, 'DaDa', tables_data=[('testtype1',)], debug=True)
        self.assertEqual({'testtype1': 'DaDa'}, result)
        self.assertTrue(nock_sapio.called)
        self.assertEqual(1, mock_load.call_count)
        df = mock_load.call_args_list[0][0][0]
        self.assertEqual(['testfield1', 'testfield2'], list(df))
        self.assertEqual([[1, 2]], df.values.tolist())
        self.assertEqual(
            TABLE_PREFIX + 'testtype1', mock_load.call_args_list[0][0][2])


class RhePlateWellPopulationTestCase(TestCase):
    @patch('extensions.sapio_utils.get_sapio_helper')
    def test_rhe_plate_well_populated(self, mock_sapio):
        mock_report = {
            'results': [
                [
                    'P-34_F16', 'check_it', 'Test.RecordId',
                    'TestElement.TestElementDataType',
                    'TestElement.TestElementSubType', 'TestElement.Layer',
                    'TestElement.Amount', 'TestElement.UnitOfMeasure',
                    'TestElement.Notes', 'TestElement.TimePoint',
                    'Test.TestId', 'TestElement.RecordId_1'
                ],
                [
                    'P-2_E4', 'Test.OtherTestId', 'Test.RecordId',
                    '1',
                    'P-34_F16', 'TestElement.Layer',
                    'TestElement.Amount', 'TestElement.UnitOfMeasure',
                    'TestElement.Notes', 'TestElement.TimePoint',
                    'Test.TestId', 'RepeatingRecordId'
                ],
                [
                    'P-3_E5', 'Test.OtherTestId', 'Test.RecordId',
                    '1', 'P-34_F16', 'TestElement.Layer', 'TestElement.Amount',
                    'TestElement.UnitOfMeasure', 'TestElement.Notes',
                    'TestElement.TimePoint', 'Test.TestId',
                    'RepeatingRecordId'
                ]
            ],
        }
        mock_sapio_helper = MockSapioAPIHelper(report=mock_report)
        mock_sapio.return_value = mock_sapio_helper
        mock_db_hook = MockPostgresHookWithSetResponse(response=(('test',),))
        result = populate_rhe_plate_well(mock_db_hook, 'Norm')
        self.assertEqual({TABLE_PREFIX + 'rhe_plate_well': 'Norm'}, result)
        self.assertEqual(1, mock_sapio.call_count)
        self.assertEqual(1, mock_db_hook.conn.cur.copy_from_call_count)
        self.assertEqual(
            RHE_PLATE_WELL_TMP_TABLE_NAME,
            mock_db_hook.conn.cur.copy_from_table)
        buffer = mock_db_hook.conn.cur.copy_from_buffer.read()
        self.assertEqual(
            'P-2_E4\tTest.OtherTestId\tTest.RecordId\t1\tP-34_F16\t'
            'TestElement.Layer\tTestElement.Amount\tTestElement.UnitOfMeasure'
            '\tTestElement.Notes\tTestElement.TimePoint\tRepeatingRecordId\t'
            '0\tP-2\tE\t4\n'
            'P-34_F16\tcheck_it\tTest.RecordId\t'
            'TestElement.TestElementDataType\tTestElement.TestElementSubType\t'
            'TestElement.Layer\tTestElement.Amount\t'
            'TestElement.UnitOfMeasure\tTestElement.Notes\t'
            'TestElement.TimePoint\tTestElement.RecordId_1\t0\tP-34\tF\t16\n',
            buffer)


class RheExpPopulationTestCase(TestCase):
    @patch('extensions.sapio_utils.load_dataframe')
    @patch('extensions.sapio_utils.get_sapio_helper')
    def test_rhe_plate_well_populated(self, mock_sapio, mock_load):
        mock_records = [{'fields': {'ELNExperiment.RecordId': 13}}]
        mock_parents = [[{
            'record_id': 14,
            'fields': {'DirectoryName': 'SomeFakeDir'},
        }]]
        mock_sapio_helper = MockSapioAPIHelper(
            data_records=mock_records,
            data_records_parents=mock_parents)
        mock_sapio.return_value = mock_sapio_helper
        result = populate_rhe_exp(
            MockPostgresHookWithSetResponse(), 'Super')
        self.assertEqual({TABLE_PREFIX + 'rhe_exp': 'Super'}, result)
        df = mock_load.call_args[0][0]
        self.assertEqual((1, 7), df.shape)
        for column in [
                'fk_study_recordid',
                'fk_project_recordid',
                'fk_veloxdepartment_recordid',
                'fk_veloxlocation_recordid',
                'fk_directory_recordid',
                'recordid',
                'parent_path']:
            self.assertIn(column, df)
        self.assertEqual(13, df['recordid'][0])
        self.assertEqual('/', df['parent_path'][0])


class RheEgnytePopulationTestCase(TestCase):
    @patch('extensions.sapio_utils.get_egnyte_client')
    @patch('extensions.sapio_utils.load_dataframe')
    @patch('extensions.sapio_utils.read_csv')
    @patch('extensions.sapio_utils.get_sapio_helper')
    def test_rhe_plate_well_populated(self, mock_sapio, mock_read,
                                      mock_load, mock_egnyte):
        mock_sapio.return_value = MockSapioAPIHelper(
            report={'results': [['fake_egnyte_path']]})
        mock_egnyte.return_value = MockEgnyte()
        result = populate_rhe_tables_from_egnyte(
            MockPostgresHookWithSetResponse(), 'Voila')
        self.assertEqual({
            table_name: 'Voila' for table_name in [
                TABLE_PREFIX + item[0] for item in EGNYTE_FILES_DATA]},
            result)
        for counter in range(len(EGNYTE_FILES_DATA)):
            self.assertEqual(
                TABLE_PREFIX + EGNYTE_FILES_DATA[counter][0],
                mock_load.call_args_list[counter][0][2])
        self.assertEqual(1, mock_sapio.call_count)
        self.assertEqual(7, mock_read.call_count)
        self.assertEqual(7, mock_load.call_count)


class ResultMergingTestCase(TestCase):
    @patch('load_sapio_dag.Variable.get', return_value=True)
    def test_results_merged(self, mock_var):
        mock_task_results = {
            'import_sapio_data': {
                'output': {'test1': SUCCESS_STATUS, 'test2': 'some error'}
            },
            'import_sapio_plate_well': {
                'output': {'test1': SUCCESS_STATUS, 'test2': 'some error'},
            },
            'import_sapio_exp': {
                'output': {'test1': SUCCESS_STATUS, 'test2': 'some error'}
            },
            'import_sapio_egnyte_data': {
                'output': {'test1': SUCCESS_STATUS, 'test2': 'some error'},
            },
            'fill_sapio_sample_lineage': {'output': 666},
        }
        mock_task_instance = MockAirflowTaskInstance(mock_task_results)
        result = merge_output(task_instance=mock_task_instance)
        self.assertEqual(1, mock_var.call_count)
        self.assertEqual('slack_report', result)
        self.assertEqual(
            f'*{SUCCESS_STATUS}*: `test1`\n*Error*: `test2` (some error)\n'
            f'*{SUCCESS_STATUS}*: `{SCHEMA}.{TABLE_PREFIX}rhe_sample_lineage`',
            mock_task_instance.xcom_value['output'])
        mock_task_results['fill_sapio_sample_lineage']['output'] = 0
        mock_task_instance = MockAirflowTaskInstance(mock_task_results)
        result = merge_output(task_instance=mock_task_instance)
        self.assertEqual(2, mock_var.call_count)
        self.assertEqual('slack_report', result)
        self.assertEqual(
            f'*{SUCCESS_STATUS}*: `test1`\n*Error*: `test2` (some error)\n'
            f'*Error*: `{SCHEMA}.{TABLE_PREFIX}rhe_sample_lineage` '
            '(0 rows inserted)',
            mock_task_instance.xcom_value['output'])
        mock_var.return_value = None
        result = merge_output(task_instance=mock_task_instance)
        self.assertEqual('finish', result)


class CallInstrumentTestCase(TestCase):
    @patch('airflow.models.Variable.get', return_value='test_var_val')
    @patch('load_sapio_dag.TransactPostgresHook')
    def test_instrument_called(self, mk_hook, mk_get):
        mock_tool = MagicMock()
        mock_tool.return_value = 'test return value'
        call_instrument_tool(
            mock_tool, variables=['var1', 'var2'],
            task_instance=MockAirflowTaskInstance({}))
        self.assertTrue(mock_tool.called)
        self.assertTrue(mk_hook.called)
        self.assertTrue(mk_get.called)
        self.assertIn('var1', mock_tool.call_args[1])
        self.assertEqual('test_var_val', mock_tool.call_args[1]['var1'])
        self.assertIn('var2', mock_tool.call_args[1])
        self.assertEqual('test_var_val', mock_tool.call_args[1]['var2'])
        self.assertNotIn('task_instance', mock_tool.call_args[1])
