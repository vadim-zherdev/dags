import logging
import pandas as pd
import numpy as np
import sqlalchemy as sa
from sqlalchemy.types import UserDefinedType, String, ARRAY

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
from pandas.io.json import json_normalize
from extensions.slack_alert import task_fail_slack_alert
from extensions.utils import get_genedata_helper, camel_to_snake


DB_CONNECTION_ID = 'informatics_db'
QCS_CHUNK = 10

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='load_genedata',
    default_args=args,
    schedule_interval='@hourly',
    catchup=False
)


class WellTagsType(UserDefinedType):
    def get_col_spec(self):
        return "VARCHAR[][]"

    def bind_expression(self, bindvalue):
        return sa.func.cast(bindvalue, ARRAY(String, dimensions=2))


def to_datetime(col):
    """
    This function converts timestamp into datatime

    Parameters
    ----------
    col: np.Series

    Returns
    -------
        np.Series
    """
    return pd.to_datetime(col, errors='ignore', unit='ms')


def col_normalize(name: str):
    """
    Doing camel to snake operations with applying
        some additional transformation
    Parameters
    ----------
    name: str

    Returns
    -------
        str
    """
    return camel_to_snake(name.replace('.', '_')).replace('__', '_')


def col_dive_level(name: str):
    """
    Take all parts placed after '.' of column name (if it exists)
    Parameters
    ----------
    name: full column name (looks like part1.part2)

    Returns
    -------
        str
    """
    return name[name.find('.') + 1:]


def unstack_sub_df(df, sub_index_prefix='compound'):
    """
    Transform input dataFrame with many columns to form of "name: value" table

    Parameters
    ----------
    df: pandas.DataFrame -- input data
    sub_index_prefix: prefix of index
                (also indicates the part of task: compounds or plates)

    Returns
    -------
            pandas.DataFrame
    """

    df.dropna(how='all', inplace=True)

    old_index = sub_index_prefix + 'Index'
    new_index = col_normalize(old_index)

    if df.shape[1] > 0:
        df = df.reset_index().melt(
            id_vars=['qcsRefId', 'layerIndex', old_index])
        df.rename(columns={'variable': 'name'}, inplace=True)
        df.dropna(subset=['value'], inplace=True)
        df = df.rename(columns={'qcsRefId': 'qcs_ref_id',
                                'layerIndex': 'layer_index',
                                old_index: new_index})
    else:
        logging.warning(f'This subDataframe of {sub_index_prefix} is empty')
        df = pd.DataFrame()
    return df


def extract_sub_df(input_df, prefix, add_fk=None):
    """
    Create two dataframes from the one, extracting second
        one by column name's prefix

    Parameters
    ----------
    input_df: pandas.DataFrame - root df
    prefix: str - prefix for names of columns
    add_fk: tuple - adding new column in the input dataFrame for
     foreign key connection. Consist of pair of a new FK for first level
     dataFrame and PK column name for sub dataFrame

    Returns
    -------
        input_df, output_df
    """
    cols = [col for col in input_df.columns if col.startswith(prefix)]
    if len(cols) > 0 and add_fk:
        input_df[add_fk[0]] = input_df[prefix + add_fk[1]]

    output_df = input_df[cols]
    input_df.drop(columns=cols, inplace=True)
    output_df = output_df.rename(columns=col_dive_level)
    return input_df, output_df


def adapt_df(input_df, new_index=None, date_cols=None):
    """
    Prepare dataframe to insert into a database

    Parameters
    ----------
    input_df: pandas.DataFrame
    new_index: str - new index to set it up
    date_cols: str or list - names of columns with dates to transform

    Returns
    -------
        pandas.DataFrame
    """
    input_df.rename(columns=col_normalize, inplace=True)
    if new_index:
        input_df.index.rename(new_index, inplace=True)

    if isinstance(date_cols, str):
        input_df[date_cols] = to_datetime(input_df[date_cols])
    elif isinstance(date_cols, list):
        for date_col in date_cols:
            input_df[date_col] = to_datetime(input_df[date_col])
    return input_df


def load_qcsessions(gd_api_helper):
    """
    Load and pars json QCSessions data

    Parameters
    ----------
    gd_api_helper: GDAPIHelper object

    Returns
    -------
        QCS list
    """

    qc_sessions = gd_api_helper.get_qc_sessions()
    root_df = json_normalize(qc_sessions).set_index('qcsRefId')

    # Prepare QCSessions and Experiments dataset
    qc_sessions_df = root_df.drop(columns='layers')

    qc_sessions_df, exps_df = extract_sub_df(
        qc_sessions_df, 'experiment.', add_fk=('expRefId', 'expRefId'))
    qc_sessions_df = adapt_df(
        qc_sessions_df, 'qcs_ref_id',
        date_cols=['creation_date', 'last_update']).reset_index()

    exps_df, annotations_df = extract_sub_df(exps_df, 'annotations.')
    exps_df = exps_df.drop_duplicates(subset='expRefId')
    exps_df = adapt_df(exps_df, date_cols='last_update').reset_index(drop=True)

    annotations_df = annotations_df.unstack().reset_index(name='value')\
        .rename(columns={'level_0': 'name', 'qcsRefId': 'qcs_ref_id'}).dropna()

    # Prepare Layers dataset
    layers_df = json_normalize(
        data=qc_sessions, record_path='layers', meta='qcsRefId'
    ).set_index(['qcsRefId', 'layerIndex'])

    layers_df, layer_annotations_df = extract_sub_df(layers_df, 'annotations.')
    layers_df, layer_normalization_df = extract_sub_df(
        layers_df, 'normalization.', add_fk=('normalizationMethodId', 'id'))
    layers_df, layer_condensing_df = extract_sub_df(
        layers_df, 'condensing.', add_fk=('condensingMethodId', 'id'))
    layers_df, layer_combination_df = extract_sub_df(
        layers_df, 'combination.', add_fk=('combinationMethodId', 'id'))
    layers_df, layer_kinetic_agg_df = extract_sub_df(
        layers_df, 'kineticAggregation.',
        add_fk=('kineticAggregationMethodId', 'id'))
    layers_df, layer_cell_agg_df = extract_sub_df(
        layers_df, 'cellAggregation.',
        add_fk=('cellAggregationMethodId', 'id'))

    layers_df.drop(columns=['subExperimentStats', 'calculatedFitColumns'],
                   inplace=True)
    layers_df.rename(columns=col_normalize, inplace=True)
    layers_df.index.rename(['qcs_ref_id', 'layer_index'], inplace=True)

    layers_with_calc_fit = [{**layer, 'qcsRefId': qcs['qcsRefId']}
                            for qcs in qc_sessions
                            for layer in qcs['layers']
                            if 'calculatedFitColumns' in layer]
    layer_calc_fit_df = json_normalize(
        data=layers_with_calc_fit,
        record_path='calculatedFitColumns',
        meta=['qcsRefId', 'layerIndex']).set_index(
        ['qcsRefId', 'layerIndex'])

    # subExperimentStats
    layers_with_sub_exp_stats = [{**layer, 'qcsRefId': qcs['qcsRefId']}
                                 for qcs in qc_sessions
                                 for layer in qcs['layers']
                                 if 'subExperimentStats' in layer]
    layers_sub_exp_stats_df = json_normalize(
        data=layers_with_sub_exp_stats,
        record_path='subExperimentStats',
        meta=['qcsRefId', 'layerIndex'])

    # Prepare Methods dataset
    methods_df = pd.concat([layer_normalization_df, layer_condensing_df,
                            layer_combination_df, layer_kinetic_agg_df,
                            layer_cell_agg_df, layer_calc_fit_df],
                           sort=False)
    methods_df.set_index('id', drop=False, inplace=True)
    methods_df, method_params_df = extract_sub_df(methods_df, 'parameters.')
    methods_df.dropna(inplace=True)
    methods_df.drop_duplicates(subset='id', inplace=True)

    # Prepare Method Parameters dataset
    method_params_df.rename(columns=col_normalize, inplace=True)
    method_params_df = method_params_df.unstack().reset_index(
        name='value')
    method_params_df.rename(
        columns={'level_0': 'name', 'id': 'method_id'},
        inplace=True)
    method_params_df.dropna(inplace=True)
    method_params_df.drop_duplicates(
        subset=['method_id', 'name'], inplace=True)

    # Prepare Layer Annotations dataset
    layer_annotations_df.rename(
        columns=col_normalize, inplace=True)
    if layer_annotations_df.empty:
        layer_annotations_df = pd.DataFrame(
            [], columns=['qcs_ref_id', 'layer_index', 'name', 'value'])
    else:
        layer_annotations_df = layer_annotations_df.unstack().reset_index(
            name='value')
        layer_annotations_df.rename(
            columns={'level_0': 'name',
                     'qcsRefId': 'qcs_ref_id',
                     'layerIndex': 'layer_index'},
            inplace=True)
        layer_annotations_df.dropna(inplace=True)

    # Prepare Layer Calculated Fit Columns dataset
    layer_calc_fit_df.drop(columns=[
        cl for cl in layer_calc_fit_df.columns if cl.startswith('parameters.')
    ], inplace=True)
    layer_calc_fit_df.rename(columns={'id': 'method_id'}, inplace=True)
    layer_calc_fit_df.index.rename(['qcs_ref_id', 'layer_index'], inplace=True)
    layer_calc_fit_df.reset_index(inplace=True)

    # Prepare Layer Sub Experiment Stats and Sub Experiment datasets
    layers_sub_exp_stats_df.reset_index(inplace=True)
    layers_sub_exp_stats_df.rename(
        columns={'index': 'subExperimentStatIndex'}, inplace=True)
    layers_sub_exp_stats_df.set_index(
        ['qcsRefId', 'layerIndex', 'subExperimentStatIndex'], inplace=True)
    layers_sub_exp_stats_df, layers_sub_exp_df = extract_sub_df(
        layers_sub_exp_stats_df, 'subExperiment.')

    layers_sub_exp_df.reset_index(inplace=True)
    layers_sub_exp_df = layers_sub_exp_df.melt(
        id_vars=['qcsRefId', 'layerIndex', 'subExperimentStatIndex'])
    layers_sub_exp_df.rename(columns={'variable': 'name'}, inplace=True)
    layers_sub_exp_df.dropna(subset=['value'], inplace=True)

    layers_sub_exp_stats_df.reset_index(inplace=True)
    layers_sub_exp_stats_df.rename(columns=col_normalize, inplace=True)
    layers_sub_exp_df.rename(columns=col_normalize, inplace=True)

    # table_name, table_data, custom_type
    qcs_tbls = [
        ('gd_experiments', exps_df, None),
        ('gd_qcsessions', qc_sessions_df, None),
        ('gd_qcsession_annotations', annotations_df, None),
        ('gd_methods', methods_df, None),
        ('gd_method_parameters', method_params_df, None),
        ('gd_layers', layers_df.reset_index(), None),
        ('gd_layer_annotations', layer_annotations_df, None),
        ('gd_layer_calculated_fit_columns', layer_calc_fit_df, None),
        ('gd_layer_sub_experiment_stats', layers_sub_exp_stats_df, None),
        ('gd_layer_sub_experiment', layers_sub_exp_df, None)]

    logging.info('All QCSessions data are ready to inserting')
    return list(qc_sessions_df.qcs_ref_id), qcs_tbls


def load_compounds(helper, qcs):
    """
    Loading and parsing compounds data by QCSessions indices.

    Parameters
    ----------
    helper: GDAPIHelper object
    qcs: list of QCS indices

    Returns
    -------

    """
    logging.debug('Request compounds for QCSessions {}'.format(qcs))
    compounds = helper.get_compounds(qcsRefIds=qcs)
    compounds_df = json_normalize(compounds).set_index(
        ['qcsRefId', 'layerIndex', 'compoundIndex'])

    compounds_str = ', '.join(compounds_df.compoundId.values)
    logging.debug(f'Loaded compounds: {compounds_str}')

    compounds_df, fitting_df = extract_sub_df(compounds_df, 'fitting.')
    fitting_df.rename(
        columns={'SES0': 'SeS0', 'SESInf': 'SeSInf'}, inplace=True)
    fitting_df = fitting_df.dropna(how='all')

    fitting_df, fit_calc_columns_df = extract_sub_df(
        fitting_df, 'calculatedColumns.')
    fitting_df, fit_fit_columns_df = extract_sub_df(fitting_df, 'fitColumns.')
    fit_calc_columns_df = unstack_sub_df(fit_calc_columns_df)

    fitting_df = adapt_df(
        fitting_df, ['qcs_ref_id', 'layer_index', 'compound_index'])\
        .reset_index()

    # convert ['NaN'] to [nan]
    fitting_df['activities'] = fitting_df['activities'].apply(
        lambda x: list(map(float, x)))

    compounds_df, calc_columns_df = extract_sub_df(
        compounds_df, 'calculatedColumns.')
    calc_columns_df = unstack_sub_df(calc_columns_df)

    # optional dfs
    compounds_df, sub_experiment_df = extract_sub_df(
        compounds_df, 'subExperiment.')
    compounds_df, annotations_df = extract_sub_df(
        compounds_df, 'annotations.')
    compounds_df, condensing_scope_df = extract_sub_df(
        compounds_df, 'condensingScope.')
    compounds_df, condens_columns_df = extract_sub_df(
        compounds_df, 'condensing.columns')

    compounds_df = adapt_df(
        compounds_df, ['qcs_ref_id', 'layer_index', 'compound_index']
    ).reset_index()

    # optional fields
    optional_dfs = [sub_experiment_df, annotations_df, condensing_scope_df,
                    fit_fit_columns_df, condens_columns_df]
    optional_dfs = [unstack_sub_df(df) for df in optional_dfs]

    tables = [
        'gd_compounds',
        'gd_compound_fitting',
        'gd_compound_fitting_calculated_columns',
        'gd_compound_calculated_columns',
        'gd_compound_sub_experiment',
        'gd_compound_annotations',
        'gd_compound_condensing_scope',
        'gd_compound_fitting_fit_columns',
        'gd_compound_condensing_columns']

    compound_tables = list(zip(
        tables,
        [compounds_df, fitting_df, fit_calc_columns_df,
         calc_columns_df] + optional_dfs,
        [None for x in range(len(tables))]))

    logging.debug('Compounds data are ready to insert into database')
    return compound_tables


def add_chunk(initial_tbl, additional_tbl):
    """
    Add new chunk of data to initial
    Parameters
    ----------
    initial_tbl

    Returns
    -------

    """
    new_tbl = []
    if len(initial_tbl) == 0:
        new_tbl = additional_tbl
    else:
        for i, _ in enumerate(initial_tbl):
            data = pd.concat(
                [initial_tbl[i][1], additional_tbl[i][1]], sort=False)
            new_tbl.append((initial_tbl[i][0], data, initial_tbl[i][2]))
    return new_tbl


def to_structure(name: str):
    """Transform column names for tables like name1.param1"""
    return name[:name.find('.')], name[name.find('.') + 1:]


def load_plates(helper, qcs):
    """
    Loading and parsing plates data by QCSessions indices.

    Parameters
    ----------
    helper: GDAPIHelper object
    qcs: list of QCS indices

    Returns
    -------

    """
    logging.debug('Request plates for QCSessions {}'.format(qcs))
    plates = helper.get_plates(qcsRefIds=qcs)
    plates_df = json_normalize(plates).set_index(
        ['qcsRefId', 'layerIndex', 'plateIndex'])

    plates_str = ', '.join(plates_df.barcode.values)
    logging.debug(f'Next plates (barcode) were downloaded: {plates_str}')

    plates_df, well_types_df = extract_sub_df(plates_df, 'wellTypes.')

    index = pd.MultiIndex.from_tuples([to_structure(col)
                                       for col in well_types_df.columns])
    well_types_df = well_types_df.rename(columns=to_structure) \
        .reindex(index, axis=1) \
        .stack(level=0) \
        .reset_index() \
        .rename(columns={'level_3': 'name'})

    plates_df, properties_df = extract_sub_df(plates_df, 'properties.')
    plates_df, annotations_df = extract_sub_df(plates_df, 'annotations.')
    plates_df, wells_df = extract_sub_df(plates_df, 'wells.')
    plates_df, columns_df = extract_sub_df(plates_df, 'columns.')

    plates_df.drop(
        ['subExperimentStats'], axis=1, inplace=True, errors='ignore')

    plates_df['date'] = to_datetime(plates_df['date'])
    plates_df.reset_index(inplace=True)

    wells_df, well_con_scope_df = extract_sub_df(wells_df, 'condensingScopes.')
    wells_df, well_sub_exps = extract_sub_df(wells_df, 'subExperiments.')
    wells_df, well_properties_df = extract_sub_df(wells_df, 'properties.')

    # to change ['Nan'] to [nan]
    wells_df['concentrations'] = wells_df['concentrations'].apply(
        lambda x: list(map(float, x)))
    wells_df.reset_index(inplace=True)

    # subExperimentStats
    sub_exp_stats = [{**qcs, 'qcsRefId': qcs['qcsRefId']}
                     for qcs in plates
                     if 'subExperimentStats' in qcs]
    if len(sub_exp_stats) > 0:
        sub_exp_stats_df = json_normalize(
            data=sub_exp_stats,
            record_path='subExperimentStats',
            meta=['qcsRefId', 'layerIndex', 'plateIndex'])\
            .set_index(['qcsRefId', 'layerIndex', 'plateIndex'])
    else:
        sub_exp_stats_df = pd.DataFrame(
            index=['qcsRefId', 'layerIndex', 'plateIndex'])

    sub_exp_stats_df, sub_exp_df = extract_sub_df(
        sub_exp_stats_df, 'subExperiment.')
    sub_exp_stats_df, sub_exp_cols_df = extract_sub_df(
        sub_exp_stats_df, 'columns.')
    ses_ind = 'sub_experiment_stat_index'
    sub_exp_df = unstack_sub_df(sub_exp_df, sub_index_prefix='plate')\
        .reset_index().rename(columns={'index': ses_ind})
    sub_exp_cols_df = unstack_sub_df(
        sub_exp_cols_df, sub_index_prefix='plate').reset_index()\
        .rename(columns={'index': ses_ind})

    if sub_exp_stats_df.shape[1] > 0:
        sub_exp_stats_df.reset_index(inplace=True)

        if sub_exp_df.shape[0] > 0:
            sub_exp_stats_df[ses_ind] = sub_exp_df[ses_ind]
        elif sub_exp_cols_df.shape[0] > 0:
            sub_exp_stats_df[ses_ind] = sub_exp_cols_df[ses_ind]
        else:
            sub_exp_stats_df[ses_ind] = np.nan
    else:
        sub_exp_stats_df = pd.DataFrame()

    stacked_dfs = [properties_df, annotations_df, columns_df,
                   well_properties_df, well_sub_exps, well_con_scope_df]
    stacked_dfs = [unstack_sub_df(df, sub_index_prefix='plate'
                                  ) for df in stacked_dfs]
    stacked_dfs[3:] = [df.rename(
        columns={'value': 'values'}) for df in stacked_dfs[3:]]

    plates_dfs = [plates_df, well_types_df, wells_df, sub_exp_stats_df,
                  sub_exp_df, sub_exp_cols_df] + stacked_dfs
    plates_dfs = list(map(adapt_df, plates_dfs))

    tables = [
        'gd_plates',
        'gd_plate_well_types',
        'gd_plate_wells',
        'gd_plate_sub_experiment_stats',
        'gd_plate_sub_experiment',
        'gd_plate_sub_experiment_columns',
        'gd_plate_properties',
        'gd_plate_annotations',
        'gd_plate_columns',
        'gd_plate_well_properties',
        'gd_plate_well_sub_experiments',
        'gd_plate_well_condensing_scopes']
    plate_tables = list(zip(tables,
                            plates_dfs,
                            [None for x in range(len(tables))]))
    plate_tables[2] = (plate_tables[2][0], plate_tables[2][1],
                       {'tags': WellTagsType})

    logging.debug('Plates data are ready to insert into database')
    return plate_tables


def load_genedata():
    """
    Load qcsessions and compounds data from genedata server by http request
    Returns
    -------

    """
    gd_api_helper = get_genedata_helper()
    hook = PostgresHook(postgres_conn_id=DB_CONNECTION_ID)

    # load QCSessions data
    qc_sessions, all_tables = load_qcsessions(
        gd_api_helper=gd_api_helper)

    # load compounds data by chunks
    start = 0
    compound_tables = list()
    plates_tables = list()
    while start < len(qc_sessions):
        comp_tables_chunk = load_compounds(
            helper=gd_api_helper,
            qcs=','.join(qc_sessions[start: start + QCS_CHUNK])
        )
        compound_tables = add_chunk(compound_tables, comp_tables_chunk)
        pl_tables_chunk = load_plates(
            helper=gd_api_helper,
            qcs=','.join(qc_sessions[start: start + QCS_CHUNK])
        )
        plates_tables = add_chunk(plates_tables, pl_tables_chunk)
        start += QCS_CHUNK

    all_tables += compound_tables + plates_tables

    sqlalchemy_engine = hook.get_sqlalchemy_engine()
    if sqlalchemy_engine:
        with sqlalchemy_engine.begin() as conn:
            logging.info('Clearing all previous data..')
            conn.execute('SELECT stg_infodw.gd_cleanup()')
            logging.info('Starting to import into database...')

            for table_name, table_data, dtype in all_tables:
                logging.debug('Import {} rows into {} table'.format(
                    table_data.shape[0], table_name
                ))
                table_data.to_sql(
                    table_name, conn,
                    schema='stg_infodw', index=False, dtype=dtype,
                    if_exists='append', method='multi')
                logging.debug(
                    'Import into {} table was successfully done'.format(
                        table_name))
            logging.info('Import was successfully done')
    else:
        logging.error('Some problems with sqlalchemy_engine.\
         Data was not imported')

    logging.info('Task successfully done')
    return 'finish'


if __name__.startswith('unusual_prefix'):
    load_data_task = PythonOperator(
        task_id='load_genedata',
        python_callable=load_genedata,
        dag=dag,
    )
    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )

    load_data_task >> finish_task

if __name__ == "__main__":
    load_genedata()
