CREATE SEQUENCE IF NOT EXISTS stg_infodw.guava_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.guava_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.guava_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_guava_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_guava_files_src_filename
    ON stg_infodw.guava_files (src_filename);
CREATE UNIQUE INDEX IF NOT EXISTS idx_guava_files_plate_id_result_stage
    ON stg_infodw.guava_files (plate_id, result_stage);

CREATE TABLE IF NOT EXISTS stg_infodw.guava_file_contents
(
    guava_file_id INT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_guava_file_contents PRIMARY KEY (guava_file_id, row_letter, column_number),
    CONSTRAINT fk_guava_file_contents_guava_files FOREIGN KEY (guava_file_id)
        REFERENCES stg_infodw.guava_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.guava_file_audit
(
    guava_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_guava_file_audit_guava_files FOREIGN KEY (guava_file_id)
        REFERENCES stg_infodw.guava_files (id)
);

CREATE INDEX IF NOT EXISTS idx_guava_file_audit_guava_file_id
    ON stg_infodw.guava_file_audit (guava_file_id);


CREATE OR REPLACE VIEW stg_infodw.guava_missing_files_v
AS SELECT
    exp.recordid AS experiment_id,
    exp.datarecordname AS experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    sq.plate_id::VARCHAR(32) AS plate_id,
    res.dir || '/' || res.files AS filepath,
    sq.result_stage,
    TO_TIMESTAMP(res.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at
FROM
(
    SELECT
        prf.plateid AS plate_id,
        CASE
            WHEN UPPER(prf.dir) LIKE '%RAW_DATA%' THEN 'RAW_DATA'
            WHEN UPPER(prf.dir) LIKE '%ANALYZED_DATA%' THEN 'ANALYZED_DATA'
            ELSE NULL
        END::stg_infodw.result_stage_enum AS result_stage,
        MAX(prf.recordid) AS prf_id
    FROM stg_infodw.sapio_plateresultfile prf
    WHERE
        prf.instrumentname LIKE 'GUAVA%'
        AND prf.dir LIKE '%Lab Data%'
        AND UPPER(prf.files) LIKE '%.CSV'
    GROUP BY
        plate_id,
        result_stage
) sq
INNER JOIN stg_infodw.sapio_plateresultfile res
    ON res.recordid = sq.prf_id
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = res.experimentrecordid
WHERE
    NULLIF(LTRIM(res.files), '') IS NOT NULL
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.guava_files gf
        WHERE
            gf.plate_id = sq.plate_id
            AND gf.result_stage = sq.result_stage
    )
ORDER BY
    exp_created_at ASC;


CREATE OR REPLACE FUNCTION stg_infodw.guava_create_files
(
    src stg_infodw.data_source_enum
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    CREATE TEMP TABLE tmp_guava_files ON COMMIT DROP AS
    SELECT
        plate_id,
        result_stage
    FROM stg_infodw.guava_missing_files_v;

    INSERT INTO stg_infodw.guava_files
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        plate_id,
        src_filename,
        result_stage,
        plate_created_at,
        load_status,
        data_source
    )
    SELECT
        msf.experiment_id,
        msf.experiment_name,
        msf.exp_created_at,
        msf.plate_id,
        msf.filepath,
        msf.result_stage,
        msf.plate_created_at,
        'created' AS load_status,
        src AS data_source
    FROM stg_infodw.guava_missing_files_v msf
    INNER JOIN tmp_guava_files tmp
        ON tmp.plate_id = msf.plate_id
        AND tmp.result_stage = msf.result_stage;

    INSERT INTO stg_infodw.guava_file_audit
    (
        guava_file_id,
        action_name,
        action_result
    )
    SELECT
        gf.id,
        'Insert entity record',
        'Success'
    FROM stg_infodw.guava_files gf
    INNER JOIN tmp_guava_files tmp
        ON tmp.plate_id = gf.plate_id
        AND tmp.result_stage = gf.result_stage;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.guava_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_contents AS ioc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.guava_files gf
            ON gf.plate_id = io.plate_id
            AND gf.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioc.instr_output_id = io.id
            AND io.instrument = 'GUAVA'
            AND gf.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.guava_files gf
        WHERE
            gf.plate_id = io.plate_id
            AND gf.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'GUAVA'
            AND gf.load_status = 'invalidated';

        DELETE FROM stg_infodw.guava_file_contents AS gfc
        USING stg_infodw.guava_files gf
        WHERE
            gfc.guava_file_id = gf.id
            AND gf.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.guava_file_audit
        (
            guava_file_id,
            action_name,
            action_result
        )
        SELECT
            gf.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.guava_files gf
        WHERE
            gf.load_status = 'invalidated';

        -- Write audit records for files
        INSERT INTO stg_infodw.guava_file_audit
        (
            guava_file_id,
            action_name,
            action_result
        )
        SELECT
            gf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.guava_files gf
        WHERE
            gf.load_status IN ('created', 'invalidated');

        RETURN QUERY
        UPDATE stg_infodw.guava_files ugf
        SET
            load_status = 'started'
        WHERE
            ugf.load_status IN ('created', 'invalidated')
        RETURNING
            ugf.id,
            ugf.experiment_id,
            ugf.experiment_name,
            ugf.plate_id,
            ugf.src_filename,
            ugf.result_stage,
            ugf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.guava_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        gf.experiment_id,
        gf.experiment_name,
        gf.exp_created_at,
        'GUAVA' AS instrument,
        gf.plate_id,
        nbi.egnyte_folder_path AS output_path,
        gf.result_stage::TEXT::prc_infodw.result_stage_enum,
        gf.plate_created_at
    FROM stg_infodw.guava_files gf
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = gf.experiment_id
        AND nbi.instrument_used LIKE 'GUAVA%'
    WHERE
        gf.load_status = 'loaded';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        result_type,
        result_value
    )
    SELECT
        io.id,
        gfc.row_letter,
        gfc.column_number,
        'ORIGINAL' AS result_type,
        gfc.result_value
    FROM stg_infodw.guava_files gf
    INNER JOIN stg_infodw.guava_file_contents gfc
        ON gfc.guava_file_id = gf.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = gf.plate_id
        AND io.result_stage::TEXT = gf.result_stage::TEXT
        AND io.instrument = 'GUAVA'
    WHERE
        gf.load_status = 'loaded'
    ORDER BY
        gf.id,
        gfc.row_letter,
        gfc.column_number;

    -- Insert audit records
    INSERT INTO stg_infodw.guava_file_audit
    (
        guava_file_id,
        action_name,
        action_result
    )
    SELECT
        gf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.guava_files gf
    WHERE
        gf.load_status = 'loaded';

    -- Update statuses
    UPDATE stg_infodw.guava_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.util_reload_plate
(
    plate_id VARCHAR(32),
    pipeline stg_infodw.pipeline_enum DEFAULT NULL
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _plate_id ALIAS FOR plate_id;
    _row_count INT;
BEGIN

    IF COALESCE(pipeline, 'MSD') = 'MSD' THEN

        UPDATE stg_infodw.msd_files AS mf
        SET
            load_status = 'invalidated'
        WHERE
            mf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% MSD files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'GUAVA') = 'GUAVA' THEN

        UPDATE stg_infodw.guava_files AS gf
        SET
            load_status = 'invalidated'
        WHERE
            gf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% GUAVA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FORTESSA') = 'FORTESSA' THEN

        UPDATE stg_infodw.fortessa_files AS ff
        SET
            load_status = 'invalidated'
        WHERE
            ff.plate_id = _plate_id
            AND ff.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FORTESSA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'MZXML') = 'MZXML' THEN

        UPDATE stg_infodw.qe_experiments AS qe
        SET
            status = 'invalidated'
        WHERE
            qe.experiment_id IN
            (
                SELECT
                    qf.experiment_id
                FROM stg_infodw.qe_files qf
                WHERE
                    qf.result_stage = 'RAW_DATA'
                    AND qf.plate_id = _plate_id
            );

        UPDATE stg_infodw.qe_worklists AS qw
        SET
            status = 'invalidated'
        WHERE
            EXISTS
            (
                SELECT
                    qf.experiment_id
                FROM stg_infodw.qe_files qf
                WHERE
                    qf.result_stage = 'RAW_DATA'
                    AND qf.plate_id = _plate_id
                    AND qf.experiment_id = qw.experiment_id
                    AND qf.worklist_filename = qw.src_filename
            );

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.result_stage = 'RAW_DATA'
            AND qf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% mzXML files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QE') = 'QE' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.result_stage = 'ANALYZED_DATA'
            AND EXISTS
            (
                SELECT 1
                FROM stg_infodw.qe_file_data qfd
                WHERE
                    qfd.qe_file_id = qf.id
                    AND SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = _plate_id
            );

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QE files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QC') = 'QC' THEN

        UPDATE stg_infodw.qc_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QC files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FIATOF') = 'FIATOF' THEN

        UPDATE fia.processingout AS po
        SET
            load_status = 'invalidated'
        WHERE
            po.processingout_id IN
            (
                SELECT
                    j.processingout_id
                FROM fia.jsonsample js
                INNER JOIN fia.samplefield sf
                    ON sf.samplefield_id = js.samplefield_id
                INNER JOIN fia."json" j
                    ON j.json_id = js.json_id
                WHERE
                    sf.samplefield_name = 'dsSampleCode'
                    AND SUBSTRING(js.value, '(P\-\d+)_[A-Z]\d+.*') = _plate_id
            );

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FIATOF files were marked for reload.', _row_count;

    END IF;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.util_reload_experiment
(
    exp_name TEXT,
    pipeline stg_infodw.pipeline_enum DEFAULT NULL
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _exp_id INT;
    _row_count INT;
BEGIN

    SELECT
        exp.recordid
    INTO _exp_id
    FROM stg_infodw.sapio_elnexperiment exp
    WHERE
        SUBSTRING(exp.datarecordname, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

    IF COALESCE(pipeline, 'MSD') = 'MSD' THEN

        UPDATE stg_infodw.msd_files AS mf
        SET
            load_status = 'invalidated'
        WHERE
            mf.experiment_id = _exp_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% MSD files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'GUAVA') = 'GUAVA' THEN

        UPDATE stg_infodw.guava_files AS gf
        SET
            load_status = 'invalidated'
        WHERE
            gf.experiment_id = _exp_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% GUAVA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FORTESSA') = 'FORTESSA' THEN

        UPDATE stg_infodw.fortessa_files AS ff
        SET
            load_status = 'invalidated'
        WHERE
            ff.experiment_id = _exp_id
            AND ff.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FORTESSA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'MZXML') = 'MZXML' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.experiment_id = _exp_id
            AND qf.result_stage = 'RAW_DATA';

        UPDATE stg_infodw.qe_worklists AS qw
        SET
            status = 'invalidated'
        WHERE
            qw.experiment_id = _exp_id;

        UPDATE stg_infodw.qe_experiments AS qe
        SET
            status = 'invalidated'
        WHERE
            qe.experiment_id = _exp_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% mzXML files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QE') = 'QE' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.experiment_id = _exp_id
            AND qf.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QE files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QC') = 'QC' THEN

        UPDATE stg_infodw.qc_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            SUBSTRING(qf.experiment_name, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QC files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FIATOF') = 'FIATOF' THEN

        UPDATE fia.processingout AS po
        SET
            load_status = 'invalidated'
        WHERE
            SUBSTRING(po.exp_id, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FIATOF files were marked for reload.', _row_count;

    END IF;

END;
$BODY$;
