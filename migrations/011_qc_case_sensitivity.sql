CREATE OR REPLACE FUNCTION stg_infodw.qc_cleanup_file
(
    src_filename VARCHAR(1024)
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _src_filename ALIAS FOR src_filename;
BEGIN

    -- Clean processing tables
    DELETE FROM prc_infodw.qc_output_arrays AS qoa
    USING prc_infodw.qc_output qo
    WHERE
        qo.id = qoa.qc_output_id
        AND qo.file_path || '/' || qo.file_name = _src_filename;

    DELETE FROM prc_infodw.qc_output AS qo
    WHERE
        qo.file_path || '/' || qo.file_name = _src_filename;

    -- Clean staging contents table
    DELETE FROM stg_infodw.qc_file_contents AS qfc
    USING stg_infodw.qc_files qf
    WHERE
        qf.id = qfc.qc_file_id
        AND qf.src_filename = _src_filename;

    -- Insert audit record
    INSERT INTO stg_infodw.qc_file_audit
    (
        qc_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Cleanup for new file version' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qc_files qf
    WHERE
        qf.src_filename = _src_filename;

    -- Update statuse
    UPDATE stg_infodw.qc_files AS qf
    SET
        load_status = 'started'
    WHERE
        qf.src_filename = _src_filename;

END;
$BODY$;
