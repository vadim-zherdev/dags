-- Drop all legacy gd_ tables
DO
$DO$
DECLARE
    _tbl TEXT;
BEGIN

    FOR _tbl  IN
        SELECT
            quote_ident(table_schema) || '.' || quote_ident(table_name)
        FROM information_schema.tables
        WHERE
            table_name LIKE 'gd_%'
            AND table_schema = 'stg_infodw'
    LOOP
        EXECUTE 'DROP TABLE ' || _tbl || ' CASCADE';
    END LOOP;

END
$DO$;


CREATE TABLE IF NOT EXISTS stg_infodw.gd_experiments
(
    exp_ref_id VARCHAR(16) NOT NULL,
    folder_name VARCHAR(1024) NOT NULL,
    name VARCHAR(256) NOT NULL,
    description TEXT NULL,
    creator_login_name VARCHAR(128) NOT NULL,
    last_update TIMESTAMP NOT NULL,
    corporate_id VARCHAR(128) NULL,
    CONSTRAINT pk_gd_experiments PRIMARY KEY (exp_ref_id)
);


CREATE TABLE IF NOT EXISTS stg_infodw.gd_qcsessions
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    name VARCHAR(256) NOT NULL,
    description TEXT NULL,
    reported BOOLEAN NOT NULL,
    valid BOOLEAN NULL,
    valid_description TEXT NULL,
    creation_date TIMESTAMP NOT NULL,
    last_update TIMESTAMP NOT NULL,
    creator_login_name VARCHAR(128) NOT NULL,
    public BOOLEAN NOT NULL,
    unique_compound_ids INT NOT NULL,
    unique_combination_ids INT NOT NULL,
    exp_ref_id VARCHAR(16) NOT NULL,
    CONSTRAINT pk_gd_qcsessions PRIMARY KEY (qcs_ref_id),
    CONSTRAINT fk_gd_qcsessions_gd_experiments FOREIGN KEY (exp_ref_id)
        REFERENCES stg_infodw.gd_experiments (exp_ref_id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_qcsession_annotations
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_qcsession_annotations PRIMARY KEY (qcs_ref_id, name),
    CONSTRAINT fk_gd_qcsession_annotations_gd_qcsessions FOREIGN KEY (qcs_ref_id)
        REFERENCES stg_infodw.gd_qcsessions (qcs_ref_id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_methods
(
    id VARCHAR(256) NOT NULL,
    trace_channel VARCHAR NULL,
    cell_population VARCHAR NULL,
    CONSTRAINT pk_gd_methods PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_method_parameters
(
    method_id VARCHAR(256) NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_method_parameters PRIMARY KEY (method_id, name),
    CONSTRAINT fk_gd_method_parameters_gd_methods FOREIGN KEY (method_id)
        REFERENCES stg_infodw.gd_methods (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_layers
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    type VARCHAR(256) NOT NULL,
    activity_label VARCHAR(256) NOT NULL,
    has_fitted_compounds BOOLEAN NOT NULL,
    has_condensed_compounds BOOLEAN NOT NULL,
    has_combinations BOOLEAN NOT NULL,
    robust_z_prime DOUBLE PRECISION NULL,
    global_standard_deviation DOUBLE PRECISION NULL,
    signal_to_background DOUBLE PRECISION NULL,
    normalization_method_id VARCHAR(256) NOT NULL,
    condensing_method_id VARCHAR(256) NULL,
    combination_method_id VARCHAR(256) NULL,
    kinetic_aggregation_method_id VARCHAR(256) NULL,
    cell_aggregation_method_id VARCHAR(256) NULL,
    layer_calculation_id VARCHAR(32) NULL,
    layer_calculation_formula VARCHAR(128) NULL,
    correction VARCHAR NULL,
    CONSTRAINT pk_gd_layers PRIMARY KEY (qcs_ref_id, layer_index),
    CONSTRAINT fk_gd_layers_gd_qcsessions FOREIGN KEY (qcs_ref_id)
        REFERENCES stg_infodw.gd_qcsessions (qcs_ref_id),
    CONSTRAINT fk_gd_layers_gd_methods_normalization FOREIGN KEY (normalization_method_id)
        REFERENCES stg_infodw.gd_methods (id),
    CONSTRAINT fk_gd_layers_gd_methods_condensing FOREIGN KEY (condensing_method_id)
        REFERENCES stg_infodw.gd_methods (id),
    CONSTRAINT fk_gd_layers_gd_methods_combination FOREIGN KEY (combination_method_id)
        REFERENCES stg_infodw.gd_methods (id),
    CONSTRAINT fk_gd_layers_gd_methods_kinetic_aggregation FOREIGN KEY (kinetic_aggregation_method_id)
        REFERENCES stg_infodw.gd_methods (id),
    CONSTRAINT fk_gd_layers_gd_methods_cell_aggregation FOREIGN KEY (cell_aggregation_method_id)
        REFERENCES stg_infodw.gd_methods (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_layer_annotations
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_layer_annotations PRIMARY KEY (qcs_ref_id, layer_index, name),
    CONSTRAINT fk_gd_layer_annotations_gd_layers FOREIGN KEY (qcs_ref_id, layer_index)
        REFERENCES stg_infodw.gd_layers (qcs_ref_id, layer_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_layer_calculated_fit_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    method_id VARCHAR(256) NOT NULL,
    CONSTRAINT pk_gd_layer_calculated_fit_columns PRIMARY KEY (qcs_ref_id, layer_index),
    CONSTRAINT fk_gd_layer_calculated_fit_columns_gd_layers FOREIGN KEY (qcs_ref_id, layer_index)
        REFERENCES stg_infodw.gd_layers (qcs_ref_id, layer_index),
    CONSTRAINT fk_gd_layer_calculated_fit_columns_gd_methods FOREIGN KEY (method_id)
        REFERENCES stg_infodw.gd_methods (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_layer_sub_experiment_stats
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    sub_experiment_stat_index INT NOT NULL,
    robust_z_prime DOUBLE PRECISION NULL,
    global_standard_deviation DOUBLE PRECISION NULL,
    CONSTRAINT pk_gd_layer_sub_experiment_stats PRIMARY KEY (qcs_ref_id, layer_index, sub_experiment_stat_index),
    CONSTRAINT fk_gd_layer_sub_experiment_stats_gd_layers FOREIGN KEY (qcs_ref_id, layer_index)
        REFERENCES stg_infodw.gd_layers (qcs_ref_id, layer_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_layer_sub_experiment
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    sub_experiment_stat_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_layer_sub_experiment PRIMARY KEY (qcs_ref_id, layer_index, sub_experiment_stat_index, name),
    CONSTRAINT fk_gd_layer_sub_experiment_gd_layer_sub_experiment_stats FOREIGN KEY (qcs_ref_id, layer_index, sub_experiment_stat_index)
        REFERENCES stg_infodw.gd_layer_sub_experiment_stats (qcs_ref_id, layer_index, sub_experiment_stat_index)
);


CREATE TABLE IF NOT EXISTS stg_infodw.gd_compounds
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_id VARCHAR(128) NOT NULL,
    compound_index INT NOT NULL,
    condensing_activity DOUBLE PRECISION NULL,
    condensing_valid_data_point_count INT NULL,
    conc_unit_base_unit VARCHAR(10) NOT NULL,
    conc_unit_log10mult INT NOT NULL,
    tags VARCHAR(256)[] NULL,
    CONSTRAINT pk_gd_compounds PRIMARY KEY (qcs_ref_id, layer_index, compound_index),
    CONSTRAINT fk_gd_compounds_gd_layers FOREIGN KEY (qcs_ref_id, layer_index)
        REFERENCES stg_infodw.gd_layers (qcs_ref_id, layer_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_sub_experiment
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_sub_experiment PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_sub_experiment_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_condensing_scope
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_condensing_scope PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_condensing_scope_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_annotations
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_annotations PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_annotations_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_condensing_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_condensing_columns PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_condensing_columns_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_calculated_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_calculated_columns PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_calculated_columns_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_fitting
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    is_dummy_fit_result BOOLEAN NOT NULL,
    is_constant_fit_result BOOLEAN NOT NULL,
    is_fixed_difference BOOLEAN NOT NULL,
    linear_ac50 DOUBLE PRECISION NULL,
    log10_ac50 DOUBLE PRECISION NULL,
    linear_qac50 DOUBLE PRECISION NULL,
    log10_qac50 DOUBLE PRECISION NULL,
    q_ac50_mode VARCHAR(16) NULL,
    fit_model TEXT NULL,
    fit_model_name VARCHAR(256) NULL,
    s0 DOUBLE PRECISION NULL,
    s_inf DOUBLE PRECISION NULL,
    hill_coefficient DOUBLE PRECISION NULL,
    se_hill_coefficient DOUBLE PRECISION NULL,
    se_log10_ac50 DOUBLE PRECISION NULL,
    se_s0 DOUBLE PRECISION NULL,
    se_s_inf DOUBLE PRECISION NULL,
    sconst DOUBLE PRECISION NULL,
    activities DOUBLE PRECISION[] NULL,
    linear_concentrations DOUBLE PRECISION[] NULL,
    log10_concentrations DOUBLE PRECISION[] NULL,
    masked BOOLEAN[] NULL,
    is_valid BOOLEAN NOT NULL,
    is_done BOOLEAN NOT NULL,
    data_mode VARCHAR(256) NULL,
    se_sconst DOUBLE PRECISION NULL,
    lower_confidence_limit DOUBLE PRECISION NULL,
    upper_confidence_limit DOUBLE PRECISION NULL,
    log10_lower_confidence_limit DOUBLE PRECISION NULL,
    log10_upper_confidence_limit DOUBLE PRECISION NULL,
    r_square DOUBLE PRECISION NULL,
    t_factor DOUBLE PRECISION NULL,
    chi_square_over_f DOUBLE PRECISION NULL,
    fit_strategy VARCHAR(256) NULL,
    fit_strategy_name VARCHAR(256) NULL,
    algorithm_version VARCHAR(256) NULL,
    description TEXT NULL,
    drc_plot VARCHAR(1024) NULL,
    CONSTRAINT pk_gd_compound_fitting PRIMARY KEY (qcs_ref_id, layer_index, compound_index),
    CONSTRAINT fk_gd_compound_fitting_columns_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_fitting_fit_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_fitting_fit_columns PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_fitting_fit_columns_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_compound_fitting_calculated_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    compound_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_compound_fitting_calculated_columns PRIMARY KEY (qcs_ref_id, layer_index, compound_index, name),
    CONSTRAINT fk_gd_compound_fitting_calculated_columns_gd_compounds FOREIGN KEY (qcs_ref_id, layer_index, compound_index)
        REFERENCES stg_infodw.gd_compounds (qcs_ref_id, layer_index, compound_index)
);


CREATE TABLE IF NOT EXISTS stg_infodw.gd_plates
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    barcode VARCHAR(128) NOT NULL,
    date TIMESTAMP NOT NULL,
    source VARCHAR(1024) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    run_index INT NOT NULL,
    stack_id VARCHAR(128) NULL,
    row_count INT NOT NULL,
    column_count INT NOT NULL,
    well_count INT NOT NULL,
    control_plate BOOLEAN NOT NULL,
    masked BOOLEAN NOT NULL,
    groups VARCHAR[] NULL,
    signal_to_background DOUBLE PRECISION NULL,
    z_prime DOUBLE PRECISION NULL,
    z_prime_robust DOUBLE PRECISION NULL,
    z DOUBLE PRECISION NULL,
    z_robust DOUBLE PRECISION NULL,
    CONSTRAINT pk_gd_plates PRIMARY KEY (qcs_ref_id, layer_index, plate_index),
    CONSTRAINT fk_gd_plates_gd_layers FOREIGN KEY (qcs_ref_id, layer_index)
        REFERENCES stg_infodw.gd_layers (qcs_ref_id, layer_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_properties
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_plate_properties PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_properties_gd_plates FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plates (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_annotations
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_plate_annotations PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_annotations_gd_plates FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plates (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_plate_columns PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_columns_gd_plates FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plates (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_well_types
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    mean DOUBLE PRECISION NOT NULL,
    median DOUBLE PRECISION NOT NULL,
    sd DOUBLE PRECISION NOT NULL,
    rsd DOUBLE PRECISION NOT NULL,
    min DOUBLE PRECISION NOT NULL,
    max DOUBLE PRECISION NOT NULL,
    CONSTRAINT pk_gd_plate_well_types PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_well_types_gd_plates FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plates (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_wells
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    types VARCHAR[] NOT NULL,
    raw_values DOUBLE PRECISION[] NOT NULL,
    corrected_values DOUBLE PRECISION[] NOT NULL,
    masked BOOLEAN[] NOT NULL,
    tags VARCHAR[][] NULL,
    masking_reasons VARCHAR[] NULL,
    compound_ids VARCHAR[] NULL,
    compound_ids_b VARCHAR[] NULL,
    concentrations DOUBLE PRECISION[] NULL,
    concentrations_b DOUBLE PRECISION[] NULL,
    CONSTRAINT pk_gd_plate_wells PRIMARY KEY (qcs_ref_id, layer_index, plate_index),
    CONSTRAINT fk_gd_plate_wells_gd_plates FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plates (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_well_properties
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    values VARCHAR[] NOT NULL,
    CONSTRAINT pk_gd_plate_well_properties PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_well_properties_gd_plate_wells FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plate_wells (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_well_sub_experiments
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    values VARCHAR[] NOT NULL,
    CONSTRAINT pk_gd_plate_well_sub_experiments PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_well_sub_experiments_gd_plate_wells FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plate_wells (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_well_condensing_scopes
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    values VARCHAR[] NOT NULL,
    CONSTRAINT pk_gd_plate_well_condensing_scopes PRIMARY KEY (qcs_ref_id, layer_index, plate_index, name),
    CONSTRAINT fk_gd_plate_well_condensing_scopes_gd_plate_wells FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plate_wells (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_sub_experiment_stats
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    sub_experiment_stat_index INT NOT NULL,
    robust_z_prime DOUBLE PRECISION NULL,
    z_prime DOUBLE PRECISION NULL,
    robust_z DOUBLE PRECISION NULL,
    z DOUBLE PRECISION NULL,
    sb DOUBLE PRECISION NULL,
    CONSTRAINT pk_gd_plate_sub_experiment_stats PRIMARY KEY (qcs_ref_id, layer_index, plate_index, sub_experiment_stat_index),
    CONSTRAINT fk_gd_plate_sub_experiment_stats_gd_plates FOREIGN KEY (qcs_ref_id, layer_index, plate_index)
        REFERENCES stg_infodw.gd_plates (qcs_ref_id, layer_index, plate_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_sub_experiment
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    sub_experiment_stat_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_plate_sub_experiment PRIMARY KEY (qcs_ref_id, layer_index, sub_experiment_stat_index, name),
    CONSTRAINT fk_gd_plate_sub_experiment_gd_plate_sub_experiment_stats FOREIGN KEY (qcs_ref_id, layer_index, plate_index, sub_experiment_stat_index)
        REFERENCES stg_infodw.gd_plate_sub_experiment_stats (qcs_ref_id, layer_index, plate_index, sub_experiment_stat_index)
);

CREATE TABLE IF NOT EXISTS stg_infodw.gd_plate_sub_experiment_columns
(
    qcs_ref_id VARCHAR(16) NOT NULL,
    layer_index INT NOT NULL,
    plate_index INT NOT NULL,
    sub_experiment_stat_index INT NOT NULL,
    name VARCHAR(256) NOT NULL,
    value VARCHAR NOT NULL,
    CONSTRAINT pk_gd_plate_sub_experiment_columns PRIMARY KEY (qcs_ref_id, layer_index, sub_experiment_stat_index, name),
    CONSTRAINT fk_gd_plate_sub_experiment_columns_gd_plate_sub_experiment_stats FOREIGN KEY (qcs_ref_id, layer_index, plate_index, sub_experiment_stat_index)
        REFERENCES stg_infodw.gd_plate_sub_experiment_stats (qcs_ref_id, layer_index, plate_index, sub_experiment_stat_index)
);


CREATE OR REPLACE FUNCTION stg_infodw.gd_cleanup()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    DELETE FROM stg_infodw.gd_plate_sub_experiment_columns;
    DELETE FROM stg_infodw.gd_plate_sub_experiment;
    DELETE FROM stg_infodw.gd_plate_sub_experiment_stats;
    DELETE FROM stg_infodw.gd_plate_well_condensing_scopes;
    DELETE FROM stg_infodw.gd_plate_well_sub_experiments;
    DELETE FROM stg_infodw.gd_plate_well_properties;
    DELETE FROM stg_infodw.gd_plate_wells;
    DELETE FROM stg_infodw.gd_plate_well_types;
    DELETE FROM stg_infodw.gd_plate_columns;
    DELETE FROM stg_infodw.gd_plate_annotations;
    DELETE FROM stg_infodw.gd_plate_properties;
    DELETE FROM stg_infodw.gd_plates;

    DELETE FROM stg_infodw.gd_compound_fitting_calculated_columns;
    DELETE FROM stg_infodw.gd_compound_fitting_fit_columns;
    DELETE FROM stg_infodw.gd_compound_fitting;
    DELETE FROM stg_infodw.gd_compound_calculated_columns;
    DELETE FROM stg_infodw.gd_compound_condensing_columns;
    DELETE FROM stg_infodw.gd_compound_annotations;
    DELETE FROM stg_infodw.gd_compound_condensing_scope;
    DELETE FROM stg_infodw.gd_compound_sub_experiment;
    DELETE FROM stg_infodw.gd_compounds;

    DELETE FROM stg_infodw.gd_layer_sub_experiment;
    DELETE FROM stg_infodw.gd_layer_sub_experiment_stats;
    DELETE FROM stg_infodw.gd_layer_calculated_fit_columns;
    DELETE FROM stg_infodw.gd_layer_annotations;
    DELETE FROM stg_infodw.gd_layers;
    DELETE FROM stg_infodw.gd_method_parameters;
    DELETE FROM stg_infodw.gd_methods;
    DELETE FROM stg_infodw.gd_qcsession_annotations;
    DELETE FROM stg_infodw.gd_qcsessions;

    DELETE FROM stg_infodw.gd_experiments;

END;
$BODY$;
