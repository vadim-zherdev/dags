CREATE OR REPLACE VIEW stg_infodw.fortessa_missing_files_v
AS SELECT
    exp.recordid AS experiment_id,
    exp.datarecordname AS experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    sq.plate_id::VARCHAR(32) AS plate_id,
    res.dir || '/' || res.files AS filepath,
    sq.result_stage,
    TO_TIMESTAMP(res.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at
FROM
(
    SELECT
        prf.plateid AS plate_id,
        CASE
            WHEN UPPER(prf.dir) LIKE '%RAW_DATA%' THEN 'RAW_DATA'
            WHEN UPPER(prf.dir) LIKE '%ANALYZED_DATA%' THEN 'ANALYZED_DATA'
            WHEN UPPER(prf.files) LIKE '%.CSV' THEN 'ANALYZED_DATA'
            ELSE NULL
        END::stg_infodw.result_stage_enum AS result_stage,
        MAX(prf.recordid) AS prf_id
    FROM stg_infodw.sapio_plateresultfile prf
    WHERE
        prf.instrumentname LIKE 'FORTESSA%'
        AND prf.dir LIKE '%Lab Data%'
    GROUP BY
        plate_id,
        result_stage
) sq
INNER JOIN stg_infodw.sapio_plateresultfile res
    ON res.recordid = sq.prf_id
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = res.experimentrecordid
WHERE
    NULLIF(LTRIM(res.files), '') IS NOT NULL
    AND sq.result_stage = 'ANALYZED_DATA'
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = sq.plate_id
            AND ff.result_stage = sq.result_stage
    )
ORDER BY
    exp_created_at ASC;


CREATE OR REPLACE FUNCTION stg_infodw.fortessa_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Collect metadata for each well in temporary table
    CREATE TEMP TABLE tmp_fortessa_intraplate_calc ON COMMIT DROP AS
    SELECT
        pmd.plate_id,
        pmd.plate_row,
        pmd.plate_column,
        pmd.sample,
        pmd.sample_type,
        pmd.stimulation
    FROM prc_infodw.instr_plate_metadata_mv pmd
    WHERE
        EXISTS
        (
            SELECT 1
            FROM stg_infodw.fortessa_files ff
            WHERE
                ff.plate_id = pmd.plate_id
                AND ff.load_status = 'loaded'
                AND ff.result_stage = 'ANALYZED_DATA'
        );

    CREATE UNIQUE INDEX idx_tmp_fortessa_intraplate_calc_plate_id_plate_row_plate_column
        ON tmp_fortessa_intraplate_calc (plate_id ASC, plate_row ASC, plate_column ASC);

    -- Warn about plates without controls
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status = 'loaded'
            AND ff.result_stage = 'ANALYZED_DATA'
            AND NOT EXISTS
            (
                SELECT 1
                FROM tmp_fortessa_intraplate_calc tmp
                WHERE
                    tmp.plate_id = ff.plate_id
                    AND tmp.sample_type = 'control'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                ff.id,
                ff.plate_id,
                ff.src_filename
            FROM stg_infodw.fortessa_files ff
            WHERE
                ff.load_status = 'loaded'
                AND ff.result_stage = 'ANALYZED_DATA'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM tmp_fortessa_intraplate_calc tmp
                    WHERE
                        tmp.plate_id = ff.plate_id
                        AND tmp.sample_type = 'control'
                )
        LOOP

            INSERT INTO stg_infodw.fortessa_file_audit
            (
                fortessa_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: no controls found for this plate'
            );

            RAISE WARNING 'No controls found for plate %.', _rec.plate_id;

            filename := _rec.src_filename;
            output := 'No controls found for this plate';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Fill new field names
    INSERT INTO stg_infodw.fortessa_fieldname_lookup
    (
        fieldname,
        normalized_fieldname
    )
    SELECT
        ffc.result_field,
        CASE
            WHEN ffc.result_field ILIKE '%Ki67%' THEN 'Ki67'
            WHEN ffc.result_field ILIKE '%Zap70%' THEN 'Zap70'
            WHEN ffc.result_field ILIKE '%CD4%' THEN 'CD4'
            ELSE 'Viable'
        END
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.fortessa_file_contents ffc
        ON ffc.fortessa_file_id = ff.id
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
        AND ffc.result_field <> 'Count'
        AND ffc.result_field NOT IN
        (
            SELECT
                fieldname
            FROM stg_infodw.fortessa_fieldname_lookup
        )
    GROUP BY
        ffc.result_field;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        ff.experiment_id,
        ff.experiment_name,
        ff.exp_created_at,
        'FORTESSA' AS instrument,
        ff.plate_id,
        nbi.egnyte_folder_path AS output_path,
        ff.result_stage::TEXT::prc_infodw.result_stage_enum,
        ff.plate_created_at
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = ff.experiment_id
        AND nbi.instrument_used LIKE 'FORTESSA%'
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        sample,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        SUBSTRING(REPLACE(ffc.sample, ' ', '_'), '_([A-Z])\d{2}[_\.]') AS row_letter,
        SUBSTRING(REPLACE(ffc.sample, ' ', '_'), '_[A-Z](\d{2})[_\.]')::SMALLINT AS column_number,
        ffc.sample,
        ffc.result_field,
        ffl.normalized_fieldname,
        'ORIGINAL' AS result_type,
        ffc.result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.fortessa_file_contents ffc
        ON ffc.fortessa_file_id = ff.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    LEFT JOIN stg_infodw.fortessa_fieldname_lookup ffl
        ON ffl.fieldname = ffc.result_field
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
    ORDER BY
        ff.id,
        ffc.sample,
        ffc.result_field;

    -- Insert NTC averages
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        ioc.result_field,
        ioc.result_normalized_field,
        'NTC_AVERAGE' AS result_type,
        AVG(ioc.result_value) AS result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
        AND ioc.result_type = 'ORIGINAL'
    INNER JOIN tmp_fortessa_intraplate_calc tmp
        ON tmp.plate_id = io.plate_id
        AND tmp.plate_row = ioc.row_letter
        AND tmp.plate_column = ioc.column_number
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
        AND tmp.sample_type = 'control'
        AND tmp.stimulation <> 'Rested'
        AND ioc.sample NOT LIKE 'Compensation%'
        AND ioc.sample NOT IN ('Mean', 'SD')
    GROUP BY
        io.id,
        ioc.result_field,
        ioc.result_normalized_field
    ORDER BY
        io.id,
        ioc.result_field;

    -- Insert intraplate calculations
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        sample,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        ioco.row_letter,
        ioco.column_number,
        ioco.sample,
        ioco.result_field,
        ioco.result_normalized_field,
        'INTRAPLATE_NORMALIZED' AS result_type,
        100::DOUBLE PRECISION * ioco.result_value / ioca.result_value AS result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    INNER JOIN prc_infodw.instr_output_contents ioco
        ON ioco.instr_output_id = io.id
        AND ioco.result_type = 'ORIGINAL'
    INNER JOIN prc_infodw.instr_output_contents ioca
        ON ioca.instr_output_id = io.id
        AND ioca.result_type = 'NTC_AVERAGE'
        AND ioca.result_field = ioco.result_field
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
        AND ioco.sample NOT LIKE 'Compensation%'
        AND ioco.sample NOT IN ('Mean', 'SD')
    ORDER BY
        io.id,
        ioco.sample,
        ioco.result_field;

    -- Insert audit records
    INSERT INTO stg_infodw.fortessa_file_audit
    (
        fortessa_file_id,
        action_name,
        action_result
    )
    SELECT
        ff.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.fortessa_files ff
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA';

    -- Update statuses
    UPDATE stg_infodw.fortessa_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded'
        AND result_stage = 'ANALYZED_DATA';

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.msd_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        INSERT INTO stg_infodw.msd_file_audit
        (
            msd_file_id,
            action_name,
            action_result
        )
        SELECT
            mf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status = 'created';

        RETURN QUERY
        UPDATE stg_infodw.msd_files umf
        SET
            load_status = 'started'
        WHERE
            umf.load_status = 'created'
        RETURNING
            umf.id,
            umf.experiment_id,
            umf.experiment_name,
            umf.plate_id,
            umf.src_filename,
            umf.result_stage,
            umf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.fortessa_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        INSERT INTO stg_infodw.fortessa_file_audit
        (
            fortessa_file_id,
            action_name,
            action_result
        )
        SELECT
            ff.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status = 'created'
            AND ff.result_stage = 'ANALYZED_DATA';

        RETURN QUERY
        UPDATE stg_infodw.fortessa_files uff
        SET
            load_status = 'started'
        WHERE
            uff.load_status = 'created'
            AND uff.result_stage = 'ANALYZED_DATA'
        RETURNING
            uff.id,
            uff.experiment_id,
            uff.experiment_name,
            uff.plate_id,
            uff.src_filename,
            uff.result_stage,
            uff.load_status;

END;
$BODY$;


UPDATE stg_infodw.fortessa_files
SET
    result_stage = 'ANALYZED_DATA'
WHERE
    load_status = 'processed'
    AND result_stage = 'RAW_DATA';

UPDATE prc_infodw.instr_output
SET
    result_stage = 'ANALYZED_DATA'
WHERE
    instrument = 'FORTESSA'
    AND result_stage = 'RAW_DATA';


ALTER TABLE prc_infodw.instr_output_contents DROP CONSTRAINT fk_instr_output_contents_msd_files;
ALTER TABLE prc_infodw.instr_output_contents
    ADD CONSTRAINT fk_instr_output_contents_instr_output FOREIGN KEY (instr_output_id)
        REFERENCES prc_infodw.instr_output (id);
ALTER TABLE prc_infodw.instr_output ALTER COLUMN plate_created_at DROP NOT NULL;

CREATE INDEX IF NOT EXISTS idx_instr_output_instrument_result_stage
    ON prc_infodw.instr_output (instrument, result_stage);

CREATE TABLE IF NOT EXISTS prc_infodw.instr_output_qe_contents
(
    instr_output_id INT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    sample VARCHAR(256) NOT NULL,
    compound VARCHAR(256) NULL,
    is_std BOOLEAN NOT NULL,
    result_type prc_infodw.result_type_enum NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_instr_output_qe_contents PRIMARY KEY (instr_output_id, metagroup_id, group_id, sample, result_type),
    CONSTRAINT fk_instr_output_qe_contents_instr_output FOREIGN KEY (instr_output_id)
        REFERENCES prc_infodw.instr_output (id)
);

CREATE INDEX IF NOT EXISTS idx_instr_output_qe_contents_instr_output_id_row_letter_column_number
    ON prc_infodw.instr_output_qe_contents (instr_output_id, row_letter, column_number);

CREATE INDEX IF NOT EXISTS idx_instr_output_qe_contents_sample
    ON prc_infodw.instr_output_qe_contents (sample);


CREATE TABLE IF NOT EXISTS stg_infodw.qe_experiments
(
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    src_path VARCHAR(1024) NOT NULL,
    status stg_infodw.entry_status_enum NOT NULL,
    CONSTRAINT pk_qe_experiments PRIMARY KEY (experiment_id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_worklists
(
    experiment_id INT NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    plate_id VARCHAR(32) NULL,
    status stg_infodw.entry_status_enum NOT NULL,
    CONSTRAINT pk_qe_worklists PRIMARY KEY (experiment_id, src_filename)
);

CREATE SEQUENCE IF NOT EXISTS stg_infodw.qe_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.qe_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.qe_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NULL,
    row_letter CHAR(1) NULL,
    column_number SMALLINT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_qe_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_qe_files_src_filename
    ON stg_infodw.qe_files (src_filename);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_file_peaks
(
    qe_file_id INT NOT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    peak_label VARCHAR(1024) NULL,
    good_peak_count INT NULL,
    med_mz DOUBLE PRECISION NULL,
    med_rt DOUBLE PRECISION NULL,
    max_quality DOUBLE PRECISION NULL,
    isotope_label VARCHAR(1024) NULL,
    compound VARCHAR(256) NULL,
    compound_id VARCHAR(256) NULL,
    formula VARCHAR(256) NULL,
    expected_rt_diff DOUBLE PRECISION NULL,
    ppm_diff DOUBLE PRECISION NULL,
    parent DOUBLE PRECISION NULL,
    is_std BOOLEAN NOT NULL,
    CONSTRAINT pk_qe_file_peaks PRIMARY KEY (qe_file_id, metagroup_id, group_id),
    CONSTRAINT fk_qe_file_peaks_qe_files FOREIGN KEY (qe_file_id) REFERENCES stg_infodw.qe_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_file_data
(
    qe_file_id INT NOT NULL,
    row_letter CHAR(1) NULL,
    column_number SMALLINT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    sample_id VARCHAR(256) NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_qe_file_data PRIMARY KEY (qe_file_id, metagroup_id, group_id, sample_id),
    CONSTRAINT fk_qe_file_data_qe_files FOREIGN KEY (qe_file_id) REFERENCES stg_infodw.qe_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_file_audit
(
    qe_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_qe_file_audit_qe_files FOREIGN KEY (qe_file_id) REFERENCES stg_infodw.qe_files (id)
);

CREATE INDEX IF NOT EXISTS idx_qe_file_audit_qe_file_id
    ON stg_infodw.qe_file_audit (qe_file_id);


CREATE OR REPLACE VIEW stg_infodw.qe_unprocessed_experiments_v
AS SELECT
    nbi.experiment_id,
    nbi.experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    nbi.egnyte_folder_path,
    qee.status
FROM stg_infodw.sapio_rhe_nb_instru nbi
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = nbi.experiment_id
LEFT JOIN stg_infodw.qe_experiments qee
    ON qee.experiment_id = nbi.experiment_id
WHERE
    nbi.instrument_used LIKE 'QE%'
    AND COALESCE(qee.status::VARCHAR, 'none') IN ('none', 'created', 'failed')
ORDER BY
    exp.datecreated ASC;


CREATE OR REPLACE VIEW infodw.qe_curated_data_v
AS SELECT
    ions.id AS qe_csv_id,
    ios.id AS std_qe_csv_id,
    ions.experiment_name::VARCHAR(1000) AS exp_name,
    ioqcns.metagroup_id::INT AS metagroupid,
    ioqcns.compound::VARCHAR(1000) AS metabolite,
    ioqcs.metagroup_id::INT AS std_metagroupid,
    ioqcns.sample::VARCHAR(1000) AS sample_id,
    ions.plate_id || '_' || ioqcns.row_letter || ioqcns.column_number AS on_plate_id,
    ioqcns.result_value AS intensity,
    ioqcs.sample::VARCHAR(1000) AS std_sample_id,
    ios.plate_id || '_' || ioqcs.row_letter || ioqcs.column_number AS std_on_plate_id,
    ioqcs.result_value AS std_intensity
FROM prc_infodw.instr_output ios
INNER JOIN prc_infodw.instr_output_qe_contents ioqcs
    ON ioqcs.instr_output_id = ios.id
INNER JOIN prc_infodw.instr_output_qe_contents ioqcns
    ON ioqcns.sample = ioqcs.sample
INNER JOIN prc_infodw.instr_output ions
    ON ions.id = ioqcns.instr_output_id
WHERE
    ios.instrument = 'QE'
    AND ios.result_stage = 'ANALYZED_DATA'
    AND ioqcs.is_std
    AND NOT ioqcns.is_std
    AND ios.experiment_id = ions.experiment_id
ORDER BY
    ions.id,
    ioqcns.metagroup_id,
    ioqcns.compound,
    ioqcns.sample;


CREATE OR REPLACE FUNCTION stg_infodw.qe_recalc_statuses()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    UPDATE stg_infodw.qe_worklists wl
    SET
        status = 'processed'
    WHERE
        wl.status IN ('created', 'started', 'failed')
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_files qf
            WHERE
                qf.experiment_id = wl.experiment_id
                AND qf.result_stage = 'RAW_DATA'
                AND qf.load_status <> 'processed'
        );

    UPDATE stg_infodw.qe_experiments exp
    SET
        status = 'processed'
    WHERE
        exp.status IN ('created', 'started', 'failed')
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_worklists wl
            WHERE
                wl.experiment_id = exp.experiment_id
                AND wl.status <> 'processed'
        );

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_insert_missing_experiments
(
    all_exps BOOLEAN,
    exp_name TEXT DEFAULT NULL
)
RETURNS TABLE
(
    experiment_id INT,
    experiment_name TEXT,
    exp_created_at TIMESTAMP,
    src_path VARCHAR(1024),
    status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    RETURN QUERY
    INSERT INTO stg_infodw.qe_experiments AS iqe
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        src_path,
        status
    )
    SELECT
        nbi.experiment_id AS experiment_id,
        nbi.experiment_name AS experiment_name,
        TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
        nbi.egnyte_folder_path AS src_path,
        'created' AS status
    FROM stg_infodw.sapio_rhe_nb_instru nbi
    INNER JOIN stg_infodw.sapio_elnexperiment exp on
        exp.recordid = nbi.experiment_id
    WHERE
        nbi.instrument_used LIKE 'QE%'
        AND (all_exps OR nbi.experiment_name = exp_name)
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_experiments qe
            WHERE
                qe.experiment_id = nbi.experiment_id
        )
    RETURNING
        iqe.experiment_id,
        iqe.experiment_name,
        iqe.exp_created_at,
        iqe.src_path,
        iqe.status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_create_analyzed_file
(
    filename VARCHAR(1024),
    exp_name TEXT,
    src stg_infodw.data_source_enum
)
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    qe_file_id INT;
BEGIN

    IF NOT EXISTS
    (
        SELECT
            1
        FROM stg_infodw.qe_files qf
        WHERE
            qf.src_filename = filename
    )
    THEN

        INSERT INTO stg_infodw.qe_files AS iqf
        (
            experiment_id,
            experiment_name,
            exp_created_at,
            src_filename,
            result_stage,
            load_status,
            data_source
        )
        SELECT
            exp.recordid AS experiment_id,
            exp.datarecordname AS experiment_name,
            TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
            filename AS src_filename,
            'ANALYZED_DATA' AS result_stage,
            'created' AS load_status,
            src AS data_source
        FROM stg_infodw.sapio_elnexperiment exp
        WHERE
            REPLACE(REPLACE(exp.datarecordname, ' ', '_'), '-', '_') = REPLACE(REPLACE(exp_name, ' ', '_'), '-', '_')
        LIMIT 1
        RETURNING iqf.id INTO qe_file_id;

        IF qe_file_id IS NOT NULL THEN

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                qe_file_id,
                'Insert entity record',
                'Success'
            );

       END IF;

    END IF;

    RETURN QUERY
    SELECT
        qf.id,
        qf.experiment_id,
        qf.src_filename,
        qf.result_stage,
        qf.load_status
    FROM stg_infodw.qe_files qf
    WHERE
        qf.id = qe_file_id;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_start_analyzed_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        INSERT INTO stg_infodw.qe_file_audit
        (
            qe_file_id,
            action_name,
            action_result
        )
        SELECT
            qf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status = 'created'
            AND qf.result_stage = 'ANALYZED_DATA';

        RETURN QUERY
        UPDATE stg_infodw.qe_files uqf
        SET
            load_status = 'started'
        WHERE
            uqf.load_status = 'created'
            AND uqf.result_stage = 'ANALYZED_DATA'
        RETURNING
            uqf.id,
            uqf.experiment_id,
            uqf.experiment_name,
            uqf.src_filename,
            uqf.result_stage,
            uqf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_process_analyzed_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Detect files with no plate/well data
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status = 'loaded'
            AND qf.result_stage = 'ANALYZED_DATA'
            AND NOT EXISTS
            (
                SELECT 1
                FROM stg_infodw.qe_file_data qfd
                WHERE
                    qfd.qe_file_id = qf.id
                    AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                qf.id,
                qf.src_filename
            FROM stg_infodw.qe_files qf
            WHERE
                qf.load_status = 'loaded'
                AND qf.result_stage = 'ANALYZED_DATA'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM stg_infodw.qe_file_data qfd
                    WHERE
                        qfd.qe_file_id = qf.id
                        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
                )
        LOOP

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: file does not contain plate/well data'
            );

            UPDATE stg_infodw.qe_files
            SET
                load_status = 'failed'
            WHERE
                id = _rec.id;

            RAISE WARNING 'File % does not contain plate/well data.', _rec.src_filename;

            filename := _rec.src_filename;
            output := 'File does not contain plate/well data';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Detect data overlapping problems and mark failed
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.qe_files qf
        INNER JOIN stg_infodw.qe_file_data qfd
            ON qfd.qe_file_id = qf.id
        WHERE
            qf.load_status IN ('loaded', 'processed')
            AND qf.result_stage = 'ANALYZED_DATA'
        GROUP BY
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*'),
            qfd.metagroup_id,
            qfd.group_id,
            qfd.sample_id
        HAVING
            COUNT(*) > 1
    )
    THEN

        FOR _rec IN
            SELECT DISTINCT
                qf.id,
                qf.src_filename
            FROM stg_infodw.qe_files qf
            INNER JOIN stg_infodw.qe_file_data qfd
                ON qfd.qe_file_id = qf.id
            INNER JOIN
            (
                SELECT
                    SUBSTRING(qfd1.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id,
                    qfd1.metagroup_id,
                    qfd1.group_id,
                    qfd1.sample_id
                FROM stg_infodw.qe_files qf1
                INNER JOIN stg_infodw.qe_file_data qfd1
                    ON qfd1.qe_file_id = qf1.id
                WHERE
                    qf1.load_status IN ('loaded', 'processed')
                    AND qf1.result_stage = 'ANALYZED_DATA'
                GROUP BY
                    SUBSTRING(qfd1.sample_id, '(P\-\d+)_[A-Z]\d+.*'),
                    qfd1.metagroup_id,
                    qfd1.group_id,
                    qfd1.sample_id
                HAVING
                    COUNT(*) > 1
            ) sq1
                ON sq1.plate_id = SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*')
                AND sq1.metagroup_id = qfd.metagroup_id
                AND sq1.group_id = qfd.group_id
                AND sq1.sample_id = qfd.sample_id
            WHERE
                qf.load_status = 'loaded'
                AND qf.result_stage = 'ANALYZED_DATA'
        LOOP

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: file contains data overlapping other files data by key'
            );

            UPDATE stg_infodw.qe_files
            SET
                load_status = 'failed'
            WHERE
                id = _rec.id;

            RAISE WARNING 'Data overlapping for file %.', _rec.src_filename;

            filename := _rec.src_filename;
            output := 'Data overlapping with other files by key';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage
    )
    SELECT
        sq.experiment_id,
        sq.experiment_name,
        sq.exp_created_at,
        'QE' AS instrument,
        sq.plate_id,
        nbi.egnyte_folder_path AS output_path,
        'ANALYZED_DATA' AS result_stage
    FROM
    (
        SELECT
            qf.experiment_id,
            qf.experiment_name,
            qf.exp_created_at,
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id
        FROM stg_infodw.qe_files qf
        INNER JOIN stg_infodw.qe_file_data qfd
            ON qfd.qe_file_id = qf.id
        WHERE
            qf.load_status = 'loaded'
            AND qf.result_stage = 'ANALYZED_DATA'
    ) sq
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = sq.experiment_id
        AND nbi.instrument_used LIKE 'QE%'
    WHERE
        sq.plate_id IS NOT NULL
    GROUP BY
        sq.experiment_id,
        sq.experiment_name,
        sq.exp_created_at,
        sq.plate_id,
        nbi.egnyte_folder_path;

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_qe_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        metagroup_id,
        group_id,
        sample,
        compound,
        is_std,
        result_type,
        result_value
    )
    SELECT
        io.id,
        qfd.row_letter,
        qfd.column_number,
        qfp.metagroup_id,
        qfp.group_id,
        qfd.sample_id,
        qfp.compound,
        qfp.is_std,
        'ORIGINAL' AS result_type,
        qfd.result_value AS result_value
    FROM stg_infodw.qe_files qf
    INNER JOIN stg_infodw.qe_file_peaks qfp
        ON qfp.qe_file_id = qf.id
    INNER JOIN stg_infodw.qe_file_data qfd
        ON qfd.qe_file_id = qf.id
        AND qfd.metagroup_id = qfp.metagroup_id
        AND qfd.group_id = qfp.group_id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*')
        AND io.result_stage::TEXT = qf.result_stage::TEXT
        AND io.instrument = 'QE'
    WHERE
        qf.load_status = 'loaded'
        AND qf.result_stage = 'ANALYZED_DATA'
        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
    ORDER BY
        qf.id,
        qfd.sample_id,
        qfp.metagroup_id,
        qfp.group_id;

    -- Insert audit records
    INSERT INTO stg_infodw.qe_file_audit
    (
        qe_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qe_files qf
    WHERE
        qf.load_status = 'loaded'
        AND qf.result_stage = 'ANALYZED_DATA';

    -- Update statuses
    UPDATE stg_infodw.qe_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded'
        AND result_stage = 'ANALYZED_DATA';

END;
$BODY$;


DROP VIEW IF EXISTS infodw.msd_analyzed_data_v;
CREATE OR REPLACE VIEW infodw.msd_analyzed_data_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.plate_id,
    ioc.row_letter,
    ioc.column_number,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';

DROP MATERIALIZED VIEW IF EXISTS infodw.msd_normalized_mv;
CREATE MATERIALIZED VIEW infodw.msd_normalized_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
WITH DATA;

DROP VIEW IF EXISTS infodw.msd_normalized_rollup_v;
CREATE OR REPLACE VIEW infodw.msd_normalized_rollup_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    AVG(ioc.result_value) AS results_avg,
    STDDEV(ioc.result_value) AS results_sttdev,
    pmd.sample,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
    AND pmd.sample_type = 'crispr'
GROUP BY
    io.experiment_name,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine;

DROP MATERIALIZED VIEW IF EXISTS infodw.msd_raw_mv;
CREATE MATERIALIZED VIEW infodw.msd_raw_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND
    (
        ioc.result_type = 'ORIGINAL'
        OR ioc.result_type = 'MSD/GUAVA' AND io.result_stage = 'ANALYZED_DATA'
    )
WITH DATA;

DROP MATERIALIZED VIEW IF EXISTS infodw.msd_plate_results_mv;
CREATE MATERIALIZED VIEW infodw.msd_plate_results_mv
AS SELECT
    io.experiment_name,
    io.plate_id,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.spot_number,
    pmd.sample AS genename,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine,
    COUNT(pmd.plate_row) AS plate_well_cnt,
    COUNT(ioc.row_letter) AS result_well_cnt,
    AVG(ioc.result_value) AS avg_result,
    MAX(ioc.result_value) AS mx_result,
    MIN(ioc.result_value) AS mn_result
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
GROUP BY
    io.experiment_name,
    io.plate_id,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine
ORDER BY
    io.experiment_name,
    io.plate_id
WITH DATA;

DROP MATERIALIZED VIEW IF EXISTS infodw.screening_msd_results_mv;
CREATE MATERIALIZED VIEW infodw.screening_msd_results_mv
AS SELECT DISTINCT
    pmd.sample,
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_value AS result,
    ioc.spot_number,
    kit.cytokine,
    pmd.stimulation
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    pmd.stimulation = 'IC+ Re-Activated'
    AND pmd.experiment_name NOT LIKE '%Valid%'
    AND pmd.experiment_name NOT LIKE '%SOC%'
ORDER BY
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    well_position,
    pmd.sample,
    result_stage,
    kit.cytokine
WITH DATA;

DROP VIEW IF EXISTS infodw.msd_control_calc_v;
CREATE OR REPLACE VIEW infodw.msd_control_calc_v
AS WITH control_calc1 AS
(
    SELECT
        AVG(ioc.result_value) AS control_ra_avg,
        STDDEV(ioc.result_value) AS control_ra_stddv,
        pmd.sample AS control_name,
        pmd.sample_type AS control_purpose,
        ioc.spot_number,
        io.result_stage,
        io.result_stage || ':' || pmd.stimulation || ':the AVG of NTC1 + NTC2' AS description,
        pmd.stimulation,
        pmd.cell_type,
        io.plate_id
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    WHERE
        io.instrument = 'MSD'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.sample_type = 'control'
    GROUP BY
        pmd.sample,
        pmd.sample_type,
        ioc.spot_number,
        io.plate_id,
        io.result_stage,
        pmd.stimulation,
        pmd.cell_type
),
control_calc2 AS
(
    SELECT
        AVG(cc.control_ra_avg) AS control_ra_avg,
        STDDEV(cc.control_ra_avg) AS control_ra_stddv,
        'CONTROLS AVG' AS control_name,
        cc.control_purpose,
        cc.spot_number,
        cc.result_stage,
        cc.result_stage || ':IC+ Re-Activated:AVG of the AVG of NTC1 + NTC2' AS description,
        cc.stimulation,
        cc.cell_type,
        cc.plate_id
    FROM control_calc1 cc
    GROUP BY
        cc.spot_number,
        cc.plate_id,
        cc.result_stage,
        cc.control_purpose,
        cc.stimulation,
        cc.cell_type
)
SELECT
    cc.plate_id,
    cc.spot_number,
    kit.cytokine,
    cc.description,
    cc.stimulation,
    cc.cell_type,
    cc.control_name,
    cc.control_purpose,
    cc.result_stage::VARCHAR AS result_stage,
    cc.control_avg,
    cc.control_stddev
FROM
(
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc1 cc1
    UNION ALL
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc2 cc2
) cc
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = cc.plate_id
    AND kit.spot_number = cc.spot_number
ORDER BY
    cc.plate_id,
    cc.spot_number;

DROP VIEW IF EXISTS infodw.msd_zprime_v;
CREATE OR REPLACE VIEW infodw.msd_zprime_v
AS WITH sq1 AS
(
    SELECT
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type,
        STDDEV(CASE WHEN pmd.sample = 'ZAP70' THEN ioc.result_value END) AS stddev_zap70,
        STDDEV(CASE WHEN pmd.sample = 'NTC crRNA_1' THEN ioc.result_value END) AS stddev_ntc1,
        STDDEV(CASE WHEN pmd.sample = 'NTC crRNA_2' THEN ioc.result_value END) AS stddev_ntc2,
        AVG(CASE WHEN pmd.sample = 'ZAP70' THEN ioc.result_value END) AS avg_zap70,
        AVG(CASE WHEN pmd.sample = 'NTC crRNA_1' THEN ioc.result_value END) AS avg_ntc1,
        AVG(CASE WHEN pmd.sample = 'NTC crRNA_2' THEN ioc.result_value END) AS avg_ntc2
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    INNER JOIN stg_infodw.msd_kit kit
        ON kit.plate_id = io.plate_id
        AND kit.spot_number = ioc.spot_number
    WHERE
        io.instrument = 'MSD'
        AND io.result_stage = 'RAW_DATA'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.sample IN ('ZAP70', 'NTC crRNA_1', 'NTC crRNA_2', 'NTC1', 'NTC2', 'NTC')
        AND pmd.stimulation = 'IC+ Re-Activated'
    GROUP BY
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type
),
sq2 AS
(
    SELECT
        sq1.experiment_name,
        sq1.plate_id,
        sq1.spot_number,
        sq1.cytokine,
        sq1.result_stage,
        sq1.result_type,
        sq1.stddev_zap70,
        sq1.avg_zap70,
        sq1.stddev_ntc1,
        sq1.stddev_ntc2,
        (sq1.stddev_ntc1 + sq1.stddev_ntc2) / 2::DOUBLE PRECISION AS stddev_ntc,
        (sq1.avg_ntc1 + sq1.avg_ntc2) / 2::DOUBLE PRECISION AS avg_ntc
    FROM sq1
)
SELECT
    sq2.experiment_name,
    sq2.plate_id,
    sq2.spot_number,
    sq2.cytokine,
    sq2.result_stage::VARCHAR AS result_stage,
    sq2.result_type::VARCHAR AS result_type,
    sq2.stddev_zap70,
    sq2.avg_zap70,
    sq2.stddev_ntc,
    sq2.avg_ntc,
    1::DOUBLE PRECISION - 3::DOUBLE PRECISION * ((sq2.stddev_zap70 + sq2.stddev_ntc) / ABS(sq2.avg_ntc - sq2.avg_zap70)) AS z_prime
FROM sq2
ORDER BY
    sq2.plate_id,
    sq2.spot_number;


CREATE OR REPLACE VIEW infodw.fortessa_csvres_v
AS SELECT
    ioc.id AS fortessa_res_id,
    ioc.sample,
    io.id AS fortessa_exp_id,
    ioc.result_field AS fieldname,
    ioc.result_value AS value,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


CREATE OR REPLACE VIEW infodw.fortessa_well_metadata_v
AS SELECT
    pmd.donor,
    pmd.plate_id,
    pmd.plate_id || '_' || pmd.plate_row || pmd.plate_column AS on_plate_id,
    pmd.stimulation,
    pmd.sample,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.id AS fortessa_exp_id
FROM prc_infodw.instr_output io
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA';


CREATE OR REPLACE VIEW infodw.fortessa_csvres_merged_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.sample AS gene,
    pmd.stimulation,
    pmd.donor
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND ioc.sample NOT LIKE 'Compensation%'
    AND ioc.sample NOT IN ('Mean', 'SD');


CREATE OR REPLACE VIEW infodw.fortessa_ntc_v
AS SELECT
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    ioc.result_value AS average_ntc
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'NTC_AVERAGE';


CREATE OR REPLACE VIEW infodw.fortessa_normalized_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.sample AS gene,
    pmd.stimulation,
    pmd.donor,
    ioca.result_value AS average_ntc,
    iocn.result_value AS normalized_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_output_contents ioca
    ON ioca.instr_output_id = io.id
    AND ioca.result_field = ioc.result_field
    AND ioca.result_type = 'NTC_AVERAGE'
INNER JOIN prc_infodw.instr_output_contents iocn
    ON iocn.instr_output_id = io.id
    AND iocn.result_field = ioc.result_field
    AND iocn.row_letter = ioc.row_letter
    AND iocn.column_number = ioc.column_number
    AND iocn.result_type = 'INTRAPLATE_NORMALIZED'
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';
