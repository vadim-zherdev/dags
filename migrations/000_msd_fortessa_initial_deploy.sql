CREATE INDEX IF NOT EXISTS idx_sapio_cell_relatedrecord143
    ON stg_infodw.sapio_cell (relatedrecord143);

CREATE INDEX IF NOT EXISTS idx_sapio_sample_plateid
    ON stg_infodw.sapio_sample (plateid);
 
CREATE SCHEMA IF NOT EXISTS prc_infodw;
 
CREATE OR REPLACE VIEW stg_infodw.instr_plate_metadata_v
AS SELECT
    w.plate_id,
    w.plate_row,
    w.plate_column,
    (
        SELECT
            MAX
            (
                CASE
                    WHEN wsmp.wellelement_subtype LIKE 'NTC%' THEN wsmp.wellelement_subtype
                    ELSE cg.genename
                END
            )
        FROM stg_infodw.sapio_rhe_plate_well wsmp
        LEFT JOIN stg_infodw.sapio_crisprguide cg
            ON cg.recordid = wsmp.data_type_subtype_record_id
        WHERE
            wsmp.plate_id = w.plate_id
            AND wsmp.plate_row = w.plate_row
            AND wsmp.plate_column = w.plate_column
            AND wsmp.wellelement_datatype = 'CRISPRGuide'
    ) AS sample,
    (
        SELECT
            MAX
            (
                CASE
                    WHEN wsmpt.wellelement_subtype LIKE 'NTC%' THEN 'control'
                    ELSE 'crispr'
                END
            )
        FROM stg_infodw.sapio_rhe_plate_well wsmpt
        WHERE
            wsmpt.plate_id = w.plate_id
            AND wsmpt.plate_row = w.plate_row
            AND wsmpt.plate_column = w.plate_column
            AND wsmpt.wellelement_datatype = 'CRISPRGuide'
    ) AS sample_type,
    (
        SELECT
            MAX
            (
                CASE
                    WHEN wst.wellelement_subtype IN ('IC+', 'Stimulated') THEN 'IC+ Re-Activated'
                    ELSE wst.wellelement_subtype
                END
            )
        FROM stg_infodw.sapio_rhe_plate_well wst
        WHERE
            wst.plate_id = w.plate_id
            AND wst.plate_row = w.plate_row
            AND wst.plate_column = w.plate_column
            AND wst.wellelement_datatype = 'Stimulation'
    ) AS stimulation,
    (
        SELECT
            COALESCE(MAX(c.ontologynickname), 'NONE')
        FROM stg_infodw.sapio_rhe_plate_well wcl
        LEFT JOIN stg_infodw.sapio_cell c
            ON c.relatedrecord143 = wcl.wellelement_subtype
        WHERE
            wcl.plate_id = w.plate_id
            AND wcl.plate_row = w.plate_row
            AND wcl.plate_column = w.plate_column
            AND wcl.wellelement_datatype = 'Sample'
    ) AS cell_type,
    (
        SELECT
            MAX(d.donorid)
        FROM stg_infodw.sapio_sample s
        INNER JOIN stg_infodw.sapio_donorid d
            ON s.relatedchild452 = d.recordid
        WHERE
            s.plateid = w.plate_id
    ) AS donor
FROM stg_infodw.sapio_rhe_plate_well w
GROUP BY
    w.plate_id,
    w.plate_row,
    w.plate_column;


DROP MATERIALIZED VIEW IF EXISTS prc_infodw.instr_plate_metadata_mv;
CREATE MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv
AS SELECT
    pmd.plate_id,
    pmd.plate_row,
    pmd.plate_column,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.cell_type,
    pmd.donor,
    (
        SELECT
            MAX(ep.experiment_name)
        FROM stg_infodw.sapio_rhe_experiments_plates ep
        WHERE
            ep.plate_id = pmd.plate_id
    ) AS experiment_name
FROM stg_infodw.instr_plate_metadata_v pmd
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_primary
    ON prc_infodw.instr_plate_metadata_mv (plate_id, plate_row, plate_column);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_sample
    ON prc_infodw.instr_plate_metadata_mv (sample);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_sample_type
    ON prc_infodw.instr_plate_metadata_mv (sample_type);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_stimulation
    ON prc_infodw.instr_plate_metadata_mv (stimulation);
 
CREATE OR REPLACE FUNCTION prc_infodw.sapio_refresh_mvs()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    REFRESH MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv;

END;
$BODY$;
 
CREATE TYPE stg_infodw.entry_status_enum AS ENUM
    ('created', 'started', 'loaded', 'processed', 'failed');

CREATE TYPE stg_infodw.result_stage_enum AS ENUM
    ('RAW_DATA', 'ANALYZED_DATA', 'METADATA', 'MAVEN_DATA', 'JSON');

CREATE TYPE stg_infodw.data_source_enum AS ENUM
    ('biobright', 'egnyte', 'aws-s3', 'migration', 'manual');

CREATE TYPE prc_infodw.result_stage_enum AS ENUM
    ('RAW_DATA', 'ANALYZED_DATA', 'METADATA', 'MAVEN_DATA', 'JSON');

CREATE TYPE prc_infodw.instrument_enum AS ENUM
    ('MSD', 'GUAVA', 'FORTESSA', 'AMAXA', 'BRAVO', 'MOXIGO', 'FIATOF', 'QE');

CREATE TYPE prc_infodw.result_type_enum AS ENUM
    ('ORIGINAL', 'NTC_AVERAGE', 'INTRAPLATE_NORMALIZED', 'MSD/GUAVA', 'GUAVA_NORMALIZED');
 
CREATE SEQUENCE IF NOT EXISTS stg_infodw.msd_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.msd_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.msd_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_msd_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_msd_files_src_filename
    ON stg_infodw.msd_files (src_filename);
CREATE UNIQUE INDEX IF NOT EXISTS idx_msd_files_plate_id_result_stage
    ON stg_infodw.msd_files (plate_id, result_stage);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_file_metadata
(
    msd_file_id INT NOT NULL,
    meta_name VARCHAR(256) NOT NULL,
    meta_value TEXT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    CONSTRAINT pk_msd_file_metadata PRIMARY KEY (msd_file_id, meta_name),
    CONSTRAINT fk_msd_file_metadata_msd_files FOREIGN KEY (msd_file_id) REFERENCES stg_infodw.msd_files (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_msd_file_metadata_msd_file_id_meta_name
    ON stg_infodw.msd_file_metadata (msd_file_id, meta_name);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_file_contents
(
    msd_file_id INT NOT NULL,
    spot_number SMALLINT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_msd_file_contents PRIMARY KEY (msd_file_id, spot_number, row_letter, column_number),
    CONSTRAINT fk_msd_file_contents_msd_files FOREIGN KEY (msd_file_id) REFERENCES stg_infodw.msd_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_kit
(
    plate_id VARCHAR(32) NOT NULL,
    spot_number SMALLINT NOT NULL,
    cytokine VARCHAR(256) NOT NULL,
    CONSTRAINT pk_msd_kit PRIMARY KEY (plate_id, spot_number)
);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_file_audit
(
    msd_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_msd_file_audit_msd_files FOREIGN KEY (msd_file_id) REFERENCES stg_infodw.msd_files (id)
);

CREATE INDEX IF NOT EXISTS idx_msd_file_audit_msd_file_id
    ON stg_infodw.msd_file_audit (msd_file_id);
 
CREATE OR REPLACE VIEW stg_infodw.msd_missing_files_v
AS SELECT
    exp.recordid AS experiment_id,
    exp.datarecordname AS experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    sq.plate_id::VARCHAR(32) AS plate_id,
    res.barcode,
    res.dir || '/' || res.files AS filepath,
    sq.result_stage,
    TO_TIMESTAMP(res.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at
FROM
(
    SELECT
        prf.plateid AS plate_id,
        CASE
            WHEN UPPER(prf.dir) LIKE '%RAW_DATA%' THEN 'RAW_DATA'
            WHEN UPPER(prf.dir) LIKE '%ANALYZED_DATA%' THEN 'ANALYZED_DATA'
            ELSE NULL
        END::stg_infodw.result_stage_enum AS result_stage,
        MAX(prf.recordid) AS prf_id
    FROM stg_infodw.sapio_plateresultfile prf
    WHERE
        prf.instrumentname LIKE 'MSD%'
        AND prf.dir LIKE '%Lab Data%'
    GROUP BY
        plate_id,
        result_stage
) sq
INNER JOIN stg_infodw.sapio_plateresultfile res
    ON res.recordid = sq.prf_id
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = res.experimentrecordid
WHERE
    NULLIF(LTRIM(res.files), '') IS NOT NULL
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.msd_files mf
        WHERE
            mf.plate_id = sq.plate_id
            AND mf.result_stage = sq.result_stage
    )
ORDER BY
    exp_created_at ASC;


CREATE OR REPLACE VIEW stg_infodw.msd_cytokine_v
AS SELECT DISTINCT
    cytokine
FROM stg_infodw.msd_kit;
 
CREATE SEQUENCE IF NOT EXISTS stg_infodw.fortessa_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.fortessa_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.fortessa_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_fortessa_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_fortessa_files_plate_id_result_stage
    ON stg_infodw.fortessa_files (plate_id, result_stage);

CREATE TABLE IF NOT EXISTS stg_infodw.fortessa_file_contents
(
    fortessa_file_id INT NOT NULL,
    sample VARCHAR(256) NOT NULL,
    result_field VARCHAR(256) NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_fortessa_file_contents PRIMARY KEY (fortessa_file_id, sample, result_field),
    CONSTRAINT fk_fortessa_file_contents_fortessa_files FOREIGN KEY (fortessa_file_id)
        REFERENCES stg_infodw.fortessa_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.fortessa_file_audit
(
    fortessa_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_fortessa_file_audit_fortessa_files FOREIGN KEY (fortessa_file_id)
        REFERENCES stg_infodw.fortessa_files (id)
);

CREATE INDEX IF NOT EXISTS idx_fortessa_file_audit_fortessa_file_id
    ON stg_infodw.fortessa_file_audit (fortessa_file_id);

 
CREATE OR REPLACE VIEW stg_infodw.fortessa_missing_files_v
AS SELECT
    exp.recordid AS experiment_id,
    exp.datarecordname AS experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    sq.plate_id::VARCHAR(32) AS plate_id,
    res.dir || '/' || res.files AS filepath,
    sq.result_stage,
    TO_TIMESTAMP(res.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at
FROM
(
    SELECT
        prf.plateid AS plate_id,
        CASE
            WHEN UPPER(prf.dir) LIKE '%RAW_DATA%' THEN 'RAW_DATA'
            WHEN UPPER(prf.dir) LIKE '%ANALYZED_DATA%' THEN 'ANALYZED_DATA'
            ELSE NULL
        END::stg_infodw.result_stage_enum AS result_stage,
        MAX(prf.recordid) AS prf_id
    FROM stg_infodw.sapio_plateresultfile prf
    WHERE
        prf.instrumentname LIKE 'FORTESSA%'
        AND prf.dir LIKE '%Lab Data%'
    GROUP BY
        plate_id,
        result_stage
) sq
INNER JOIN stg_infodw.sapio_plateresultfile res
    ON res.recordid = sq.prf_id
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = res.experimentrecordid
WHERE
    NULLIF(LTRIM(res.files), '') IS NOT NULL
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = sq.plate_id
            AND ff.result_stage = sq.result_stage
    )
ORDER BY
    exp_created_at ASC;
 
CREATE SEQUENCE IF NOT EXISTS prc_infodw.instr_output_id_seq;

CREATE TABLE IF NOT EXISTS prc_infodw.instr_output
(
    id INT NOT NULL DEFAULT nextval('prc_infodw.instr_output_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    instrument prc_infodw.instrument_enum NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    output_path VARCHAR(1024) NOT NULL,
    result_stage prc_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    CONSTRAINT pk_instr_output PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_output_instrument_plate_id_result_stage
    ON prc_infodw.instr_output (instrument, plate_id, result_stage);

CREATE SEQUENCE IF NOT EXISTS prc_infodw.instr_output_contents_id_seq;

CREATE TABLE IF NOT EXISTS prc_infodw.instr_output_contents
(
    id INT NOT NULL DEFAULT nextval('prc_infodw.instr_output_contents_id_seq'),
    instr_output_id INT NOT NULL,
    row_letter CHAR(1) NULL,
    column_number SMALLINT NULL,
    spot_number SMALLINT NULL,
    sample VARCHAR(256) NULL,
    result_field VARCHAR(256) NULL,
    result_normalized_field VARCHAR(32) NULL,
    result_type prc_infodw.result_type_enum NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_instr_output_contents PRIMARY KEY (id),
    CONSTRAINT fk_instr_output_contents_msd_files FOREIGN KEY (instr_output_id)
        REFERENCES prc_infodw.instr_output (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_output_contents_msd_primary
    ON prc_infodw.instr_output_contents (instr_output_id, row_letter, column_number, spot_number, result_type);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_output_contents_fortessa_primary
    ON prc_infodw.instr_output_contents (instr_output_id, sample, result_field, result_type);

 
CREATE OR REPLACE VIEW infodw.fortessa_csvres_v
AS SELECT
    ioc.id AS fortessa_res_id,
    ioc.sample,
    io.id AS fortessa_exp_id,
    ioc.result_field AS fieldname,
    ioc.result_value AS value,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND ioc.result_type = 'ORIGINAL';


CREATE OR REPLACE VIEW infodw.fortessa_well_metadata_v
AS SELECT
    pmd.donor,
    pmd.plate_id,
    pmd.plate_id || '_' || pmd.plate_row || pmd.plate_column AS on_plate_id,
    pmd.stimulation,
    pmd.sample,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.id AS fortessa_exp_id
FROM prc_infodw.instr_output io
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
WHERE
    io.instrument = 'FORTESSA';


CREATE OR REPLACE VIEW infodw.fortessa_csvres_merged_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.sample AS gene,
    pmd.stimulation,
    pmd.donor
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND ioc.result_type = 'ORIGINAL'
    AND ioc.sample NOT LIKE 'Compensation%'
    AND ioc.sample NOT IN ('Mean', 'SD');


CREATE OR REPLACE VIEW infodw.fortessa_ntc_v
AS SELECT
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    ioc.result_value AS average_ntc
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND ioc.result_type = 'NTC_AVERAGE';


CREATE OR REPLACE VIEW infodw.fortessa_normalized_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.sample AS gene,
    pmd.stimulation,
    pmd.donor,
    ioca.result_value AS average_ntc,
    iocn.result_value AS normalized_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_output_contents ioca
    ON ioca.instr_output_id = io.id
    AND ioca.result_field = ioc.result_field
    AND ioca.result_type = 'NTC_AVERAGE'
INNER JOIN prc_infodw.instr_output_contents iocn
    ON iocn.instr_output_id = io.id
    AND iocn.result_field = ioc.result_field
    AND iocn.row_letter = ioc.row_letter
    AND iocn.column_number = ioc.column_number
    AND iocn.result_type = 'INTRAPLATE_NORMALIZED'
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND ioc.result_type = 'ORIGINAL';
 
CREATE OR REPLACE VIEW infodw.instr_plate_metadata_v
AS SELECT
    sq.experiment_name,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    pmd.plate_row,
    pmd.plate_column,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    sq.instrumentname AS instrumentname,
    pmd.plate_id || '_' || pmd.plate_row || pmd.plate_column AS on_plate_id,
    (
        SELECT
            COUNT(*)
        FROM stg_infodw.sapio_rhe_plate_well rpw
        WHERE
            rpw.plate_id = pmd.plate_id
            AND rpw.plate_row = pmd.plate_row
            AND rpw.plate_column = pmd.plate_column
    ) AS cnt_on_plate_id
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN
(
    SELECT
        prf.plateid AS plate_id,
        MAX(prf.instrumentname) AS instrumentname,
        MAX(exp.datarecordname) AS experiment_name
    FROM stg_infodw.sapio_plateresultfile prf
    INNER JOIN stg_infodw.sapio_elnexperiment exp
        ON exp.recordid = prf.experimentrecordid
    GROUP BY
        prf.plateid
) sq
    ON sq.plate_id = pmd.plate_id
ORDER BY
    experiment_name,
    plate_id,
    well_position;


CREATE OR REPLACE VIEW infodw.msd_analyzed_data_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.plate_id,
    ioc.row_letter,
    ioc.column_number,
    io.result_stage,
    ioc.result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_normalized_mv;
CREATE MATERIALIZED VIEW infodw.msd_normalized_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage,
    ioc.result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
WITH DATA;


CREATE OR REPLACE VIEW infodw.msd_normalized_rollup_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.result_stage,
    ioc.result_type,
    AVG(ioc.result_value) AS results_avg,
    STDDEV(ioc.result_value) AS results_sttdev,
    pmd.sample,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
    AND pmd.sample_type = 'crispr'
GROUP BY
    io.experiment_name,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_raw_mv;
CREATE MATERIALIZED VIEW infodw.msd_raw_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage,
    ioc.result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND
    (
        ioc.result_type = 'ORIGINAL'
        OR ioc.result_type = 'MSD/GUAVA' AND io.result_stage = 'ANALYZED_DATA'
    )
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_ntc_mv;
CREATE MATERIALIZED VIEW infodw.msd_ntc_mv
AS SELECT
    io.experiment_name,
    exp.createdby,
    io.exp_created_at AS datecreated,
    CASE
        WHEN ioc.row_letter = 'G' AND ioc.column_number IN (5, 6, 7) THEN 'NTC1'
        WHEN ioc.row_letter = 'G' AND ioc.column_number IN (10, 11, 12) THEN 'NTC2'
        ELSE NULL
    END AS ntc,
    pmd.sample,
    ioc.result_value AS msd_result,
    pmd.donor,
    io.plate_id,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    pmd.stimulation,
    ioc.spot_number
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = io.experiment_id
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND pmd.sample_type = 'control'
    AND pmd.stimulation = 'IC+ Re-Activated'
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.crispr_experiment_mv;
CREATE MATERIALIZED VIEW infodw.crispr_experiment_mv
AS SELECT DISTINCT
    g.genename AS crispr_gene_request,
    sq.experiment_name,
    sq.experiment_owner,
    sq.experiment_folder,
    sq.plate_id,
    sq.genename,
    sq.well_cnt,
    sq.cytokine_cnt
FROM stg_infodw.sapio_crisprguide g
LEFT JOIN
(
    SELECT
        ex.datarecordname AS experiment_name,
        ex.createdby AS experiment_owner,
        ex.relatedelnexperimentparent AS experiment_folder,
        ep.plate_id,
        cg.genename,
        COUNT(DISTINCT pw.on_plate_id) AS well_cnt,
        COUNT(DISTINCT kit.cytokine) AS cytokine_cnt
    FROM stg_infodw.sapio_elnexperiment ex
    LEFT JOIN stg_infodw.sapio_rhe_experiments_plates ep
        ON ex.recordid = ep.experiment_id
    LEFT JOIN stg_infodw.sapio_rhe_plate_well pw
        ON pw.plate_id = ep.plate_id
        AND pw.wellelement_datatype = 'CRISPRGuide'
    LEFT JOIN stg_infodw.sapio_crisprguide cg
        ON cg.crisprguide = pw.wellelement_subtype
    LEFT JOIN stg_infodw.msd_kit kit
        ON kit.plate_id = ep.plate_id
    WHERE
        ex.relatedelnexperimentparent IN ('Target ID', 'Screening')
    GROUP BY
        ex.datarecordname,
        ex.createdby,
        ex.relatedelnexperimentparent,
        ep.plate_id,
        cg.genename
) sq
    ON sq.genename = g.genename
ORDER BY
    sq.experiment_name,
    sq.plate_id
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_plate_results_mv;
CREATE MATERIALIZED VIEW infodw.msd_plate_results_mv
AS SELECT
    io.experiment_name,
    io.plate_id,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample AS genename,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine,
    COUNT(pmd.plate_row) AS plate_well_cnt,
    COUNT(ioc.row_letter) AS result_well_cnt,
    AVG(ioc.result_value) AS avg_result,
    MAX(ioc.result_value) AS mx_result,
    MIN(ioc.result_value) AS mn_result
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
GROUP BY
    io.experiment_name,
    io.plate_id,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine
ORDER BY
    io.experiment_name,
    io.plate_id
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.screening_msd_results_mv;
CREATE MATERIALIZED VIEW infodw.screening_msd_results_mv
AS SELECT DISTINCT
    pmd.sample,
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage,
    ioc.result_value AS result,
    ioc.spot_number,
    kit.cytokine,
    pmd.stimulation
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    pmd.stimulation = 'IC+ Re-Activated'
    AND pmd.experiment_name NOT LIKE '%Valid%'
    AND pmd.experiment_name NOT LIKE '%SOC%'
ORDER BY
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    well_position,
    pmd.sample,
    io.result_stage,
    kit.cytokine
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_pathway_mv;
CREATE MATERIALIZED VIEW infodw.msd_pathway_mv
AS SELECT DISTINCT
    pathway.pathway,
    pathway.symbol,
    m.experiment_name,
    CONCAT(m.plate_row, LPAD(m.plate_column::VARCHAR, 2, '0')) AS well_position,
    m.plate_id,
    kit.cytokine
FROM
(
    SELECT
        crn.rxn,
        g.entrez_id,
        g.symbol,
        COALESCE(crn.pathway, 'No metabolic pathway available') AS pathway
    FROM stg_infodw.mdb_gene g
    LEFT JOIN stg_infodw.mdb_immap_gpr gpr
        ON gpr.entrez_id = g.entrez_id
    LEFT JOIN stg_infodw.mdb_crn_reactions crn
        ON gpr.rxn_id = crn.rxn_id
) pathway
INNER JOIN prc_infodw.instr_plate_metadata_mv m
    ON m.sample = pathway.symbol
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = m.plate_id
WITH DATA;


DROP VIEW IF EXISTS infodw.msd_control_calc_v;
CREATE OR REPLACE VIEW infodw.msd_control_calc_v
AS WITH control_calc1 AS
(
    SELECT
        AVG(ioc.result_value) AS control_ra_avg,
        STDDEV(ioc.result_value) AS control_ra_stddv,
        pmd.sample AS control_name,
        pmd.sample_type AS control_purpose,
        ioc.spot_number,
        io.result_stage,
        io.result_stage || ':' || pmd.stimulation || ':the AVG of NTC1 + NTC2' AS description,
        pmd.stimulation,
        pmd.cell_type,
        io.plate_id
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    WHERE
        io.instrument = 'MSD'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.sample_type = 'control'
    GROUP BY
        pmd.sample,
        pmd.sample_type,
        ioc.spot_number,
        io.plate_id,
        io.result_stage,
        pmd.stimulation,
        pmd.cell_type
),
control_calc2 AS
(
    SELECT
        AVG(cc.control_ra_avg) AS control_ra_avg,
        STDDEV(cc.control_ra_avg) AS control_ra_stddv,
        'CONTROLS AVG' AS control_name,
        cc.control_purpose,
        cc.spot_number,
        cc.result_stage,
        cc.result_stage || ':IC+ Re-Activated:AVG of the AVG of NTC1 + NTC2' AS description,
        cc.stimulation,
        cc.cell_type,
        cc.plate_id
    FROM control_calc1 cc
    GROUP BY
        cc.spot_number,
        cc.plate_id,
        cc.result_stage,
        cc.control_purpose,
        cc.stimulation,
        cc.cell_type
)
SELECT
    cc.plate_id,
    cc.spot_number,
    kit.cytokine,
    cc.description,
    cc.stimulation,
    cc.cell_type,
    cc.control_name,
    cc.control_purpose,
    cc.result_stage,
    cc.control_avg,
    cc.control_stddev
FROM
(
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc1 cc1
    UNION ALL
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc2 cc2
) cc
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = cc.plate_id
    AND kit.spot_number = cc.spot_number
ORDER BY
    cc.plate_id,
    cc.spot_number;


DROP VIEW IF EXISTS infodw.msd_zprime_v;
CREATE OR REPLACE VIEW infodw.msd_zprime_v
AS WITH sq1 AS
(
    SELECT
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type,
        STDDEV(CASE WHEN pmd.sample = 'ZAP70' THEN ioc.result_value END) AS stddev_zap70,
        STDDEV(CASE WHEN pmd.sample = 'NTC crRNA_1' THEN ioc.result_value END) AS stddev_ntc1,
        STDDEV(CASE WHEN pmd.sample = 'NTC crRNA_2' THEN ioc.result_value END) AS stddev_ntc2,
        AVG(CASE WHEN pmd.sample = 'ZAP70' THEN ioc.result_value END) AS avg_zap70,
        AVG(CASE WHEN pmd.sample = 'NTC crRNA_1' THEN ioc.result_value END) AS avg_ntc1,
        AVG(CASE WHEN pmd.sample = 'NTC crRNA_2' THEN ioc.result_value END) AS avg_ntc2
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    INNER JOIN stg_infodw.msd_kit kit
        ON kit.plate_id = io.plate_id
        AND kit.spot_number = ioc.spot_number
    WHERE
        io.instrument = 'MSD'
        AND io.result_stage = 'RAW_DATA'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.sample IN ('ZAP70', 'NTC crRNA_1', 'NTC crRNA_2', 'NTC1', 'NTC2', 'NTC')
        AND pmd.stimulation = 'IC+ Re-Activated'
    GROUP BY
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type
),
sq2 AS
(
    SELECT
        sq1.experiment_name,
        sq1.plate_id,
        sq1.spot_number,
        sq1.cytokine,
        sq1.result_stage,
        sq1.result_type,
        sq1.stddev_zap70,
        sq1.avg_zap70,
        sq1.stddev_ntc1,
        sq1.stddev_ntc2,
        (sq1.stddev_ntc1 + sq1.stddev_ntc2) / 2::DOUBLE PRECISION AS stddev_ntc,
        (sq1.avg_ntc1 + sq1.avg_ntc2) / 2::DOUBLE PRECISION AS avg_ntc
    FROM sq1
)
SELECT
    sq2.experiment_name,
    sq2.plate_id,
    sq2.spot_number,
    sq2.cytokine,
    sq2.result_stage,
    sq2.result_type,
    sq2.stddev_zap70,
    sq2.avg_zap70,
    sq2.stddev_ntc,
    sq2.avg_ntc,
    1::DOUBLE PRECISION - 3::DOUBLE PRECISION * ((sq2.stddev_zap70 + sq2.stddev_ntc) / ABS(sq2.avg_ntc - sq2.avg_zap70)) AS z_prime
FROM sq2
ORDER BY
    sq2.plate_id,
    sq2.spot_number;


CREATE OR REPLACE VIEW prc_infodw.crispr_screen_helper_msd_v
AS SELECT
    io.plate_id,
    ioc.row_letter,
    ioc.column_number,
    (
        SELECT
            mf.src_filename
        FROM stg_infodw.msd_files mf
        WHERE
            mf.plate_id = io.plate_id
            AND mf.result_stage::TEXT = io.result_stage::TEXT
    ) AS src_file_name,
    ioc.spot_number,
    kit.cytokine,
    'M_' || LEFT(io.result_stage::TEXT, 1) || '_' || LEFT(ioc.result_type::TEXT, 1) AS result_category,
    ioc.result_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD';


CREATE OR REPLACE VIEW prc_infodw.crispr_screen_helper_fortessa_v
AS SELECT
    io.plate_id,
    ioc.row_letter,
    ioc.column_number,
    (
        SELECT
            ff.src_filename
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = io.plate_id
            AND ff.result_stage::TEXT = io.result_stage::TEXT
    ) AS src_file_name,
    ioc.result_type::VARCHAR AS result_type,
    'F_' || LEFT(ioc.result_type::TEXT, 1) || '_' || UPPER(ioc.result_normalized_field) AS result_category,
    AVG(ioc.result_value) AS result_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND COALESCE(ioc.sample, '') NOT LIKE 'Compensation%'
    AND COALESCE(ioc.sample, '') NOT IN ('Mean', 'SD')
    AND ioc.result_normalized_field IS NOT NULL
GROUP BY
    io.plate_id,
    io.result_stage,
    ioc.row_letter,
    ioc.column_number,
    ioc.result_type,
    ioc.result_normalized_field;


DROP VIEW infodw.crispr_screen_data_all_v;
CREATE OR REPLACE VIEW infodw.crispr_screen_data_all_v
AS SELECT
    rqg.genename AS request_gene,
    pmd.sample AS plate_gene,
    SUBSTRING(pmd.experiment_name, 'Bio-\d+') AS experiment_name,
    pmd.experiment_name AS full_experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    pmd.stimulation,
    pmd.plate_row AS row_letter,
    pmd.plate_column AS column_number,
    COALESCE(ct_msd.src_file_name, ct_fortessa.src_file_name) AS src_file_name,
    ct_msd.spot_number,
    ct_msd.cytokine,
    ct_msd.raw_result,
    ct_msd.analyzed_result,
    ct_msd.raw_normalized_result,
    ct_msd.analyzed_normalized_result,
    ct_msd.raw_msd_guava_result,
    ct_msd.analyzed_msd_guava_result,
    ct_msd.guava_normalized_result,
    ct_fortessa.viability_result,
    ct_fortessa.ki67_result,
    ct_fortessa.cd4_result,
    ct_fortessa.zap70_result,
    ct_fortessa.viability_normalized,
    ct_fortessa.ki67_normalized,
    ct_fortessa.cd4_normalized,
    ct_fortessa.zap70_normalized,
    ct_fortessa_ntc.viability_average_ntc,
    ct_fortessa_ntc.ki67_average_ntc,
    ct_fortessa_ntc.cd4_average_ntc,
    ct_fortessa_ntc.zap70_average_ntc
FROM
(
    SELECT DISTINCT
        genename
    FROM stg_infodw.sapio_crisprguide_v
) rqg
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.sample = rqg.genename
LEFT JOIN crosstab
(
    $$
    SELECT
        DENSE_RANK() OVER(ORDER BY plate_id, row_letter, column_number, spot_number) AS row_name,
        plate_id,
        row_letter,
        column_number,
        src_file_name,
        spot_number,
        cytokine,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_msd_v
    $$,
    $$
        VALUES ('M_R_O'), ('M_A_O'), ('M_R_I'), ('M_A_I'), ('M_R_M'), ('M_A_M'), ('M_A_G')
    $$
) AS ct_msd
(
    row_name BIGINT,
    plate_id VARCHAR(32),
    row_letter CHAR(1),
    column_number SMALLINT,
    src_file_name VARCHAR(1024),
    spot_number SMALLINT,
    cytokine VARCHAR(256),
    raw_result DOUBLE PRECISION,
    analyzed_result DOUBLE PRECISION,
    raw_normalized_result DOUBLE PRECISION,
    analyzed_normalized_result DOUBLE PRECISION,
    raw_msd_guava_result DOUBLE PRECISION,
    analyzed_msd_guava_result DOUBLE PRECISION,
    guava_normalized_result DOUBLE PRECISION
)
    ON ct_msd.plate_id = pmd.plate_id
    AND ct_msd.row_letter = pmd.plate_row
    AND ct_msd.column_number = pmd.plate_column
LEFT JOIN crosstab
(
    $$
    SELECT
        DENSE_RANK() OVER(ORDER BY plate_id, row_letter, column_number) AS row_name,
        plate_id,
        row_letter,
        column_number,
        src_file_name,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_fortessa_v
    WHERE
        result_type <> 'NTC_AVERAGE'
    $$,
    $$
        VALUES ('F_O_VIABLE'), ('F_O_KI67'), ('F_O_CD4'), ('F_O_ZAP70'),
            ('F_I_VIABLE'), ('F_I_KI67'), ('F_I_CD4'), ('F_I_ZAP70')
    $$
) AS ct_fortessa
(
    row_name BIGINT,
    plate_id VARCHAR(32),
    row_letter CHAR(1),
    column_number SMALLINT,
    src_file_name VARCHAR(1024),
    viability_result DOUBLE PRECISION,
    ki67_result DOUBLE PRECISION,
    cd4_result DOUBLE PRECISION,
    zap70_result DOUBLE PRECISION,
    viability_normalized DOUBLE PRECISION,
    ki67_normalized DOUBLE PRECISION,
    cd4_normalized DOUBLE PRECISION,
    zap70_normalized DOUBLE PRECISION
)
    ON ct_fortessa.plate_id = pmd.plate_id
    AND ct_fortessa.row_letter = pmd.plate_row
    AND ct_fortessa.column_number = pmd.plate_column
LEFT JOIN crosstab
(
    $$
    SELECT
        plate_id,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_fortessa_v
    WHERE
        result_type = 'NTC_AVERAGE'
    $$,
    $$
        VALUES ('F_N_VIABLE'), ('F_N_KI67'), ('F_N_CD4'), ('F_N_ZAP70')
    $$
) AS ct_fortessa_ntc
(
    plate_id VARCHAR(32),
    viability_average_ntc DOUBLE PRECISION,
    ki67_average_ntc DOUBLE PRECISION,
    cd4_average_ntc DOUBLE PRECISION,
    zap70_average_ntc DOUBLE PRECISION
)
    ON ct_fortessa_ntc.plate_id = pmd.plate_id;
 
CREATE OR REPLACE FUNCTION stg_infodw.msd_create_files
(
    src stg_infodw.data_source_enum
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    CREATE TEMP TABLE tmp_msd_files ON COMMIT DROP AS
    SELECT
        plate_id,
        result_stage
    FROM stg_infodw.msd_missing_files_v;

    INSERT INTO stg_infodw.msd_files
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        plate_id,
        src_filename,
        result_stage,
        plate_created_at,
        load_status,
        data_source
    )
    SELECT
        msf.experiment_id,
        msf.experiment_name,
        msf.exp_created_at,
        msf.plate_id,
        msf.filepath,
        msf.result_stage,
        msf.plate_created_at,
        'created' AS load_status,
        src AS data_source
    FROM stg_infodw.msd_missing_files_v msf
    INNER JOIN tmp_msd_files tmp
        ON tmp.plate_id = msf.plate_id
        AND tmp.result_stage = msf.result_stage;

    INSERT INTO stg_infodw.msd_file_audit
    (
        msd_file_id,
        action_name,
        action_result
    )
    SELECT
        mf.id,
        'Insert entity record',
        'Success'
    FROM stg_infodw.msd_files mf
    INNER JOIN tmp_msd_files tmp
        ON tmp.plate_id = mf.plate_id
        AND tmp.result_stage = mf.result_stage;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.msd_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Collect metadata for each well in temporary table
    CREATE TEMP TABLE tmp_msd_intraplate_calc ON COMMIT DROP AS
    SELECT
        pmd.plate_id,
        pmd.plate_row,
        pmd.plate_column,
        pmd.sample,
        pmd.sample_type,
        pmd.stimulation
    FROM prc_infodw.instr_plate_metadata_mv pmd
    WHERE
        EXISTS
        (
            SELECT 1
            FROM stg_infodw.msd_files mf
            WHERE
                mf.plate_id = pmd.plate_id
                AND mf.load_status = 'loaded'
        );

    CREATE UNIQUE INDEX idx_tmp_msd_intraplate_calc_plate_id_plate_row_plate_column
        ON tmp_msd_intraplate_calc (plate_id ASC, plate_row ASC, plate_column ASC);

    -- Warn about plates without controls
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status = 'loaded'
            AND NOT EXISTS
            (
                SELECT 1
                FROM tmp_msd_intraplate_calc tmp
                WHERE
                    tmp.plate_id = mf.plate_id
                    AND tmp.sample_type = 'control'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                mf.id,
                mf.plate_id,
                mf.src_filename
            FROM stg_infodw.msd_files mf
            WHERE
                mf.load_status = 'loaded'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM tmp_msd_intraplate_calc tmp
                    WHERE
                        tmp.plate_id = mf.plate_id
                        AND tmp.sample_type = 'control'
                )
        LOOP

            INSERT INTO stg_infodw.msd_file_audit
            (
                msd_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: no controls found for this plate'
            );

            RAISE WARNING 'No controls found for plate %.', _rec.plate_id;

            filename := _rec.src_filename;
            output := 'No controls found for this plate';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        mf.experiment_id,
        mf.experiment_name,
        mf.exp_created_at,
        'MSD' AS instrument,
        mf.plate_id,
        nbi.egnyte_folder_path AS output_path,
        mf.result_stage::TEXT::prc_infodw.result_stage_enum,
        mf.plate_created_at
    FROM stg_infodw.msd_files mf
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = mf.experiment_id
        AND nbi.instrument_used LIKE 'MSD%'
    WHERE
        mf.load_status = 'loaded';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        spot_number,
        result_type,
        result_value
    )
    SELECT
        io.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number,
        'ORIGINAL' AS result_type,
        mfc.result_value
    FROM stg_infodw.msd_files mf
    INNER JOIN stg_infodw.msd_file_contents mfc
        ON mfc.msd_file_id = mf.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = mf.plate_id
        AND io.result_stage::TEXT = mf.result_stage::TEXT
        AND io.instrument = 'MSD'
    WHERE
        mf.load_status = 'loaded'
    ORDER BY
        mf.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number;

    -- Insert intraplate calculations
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        spot_number,
        result_type,
        result_value
    )
    SELECT
        io.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number,
        'INTRAPLATE_NORMALIZED' AS result_type,
        CASE
            WHEN tmp.stimulation = 'IC+ Re-Activated' AND sq.avg_value > 0
                THEN 100::DOUBLE PRECISION * mfc.result_value / sq.avg_value
            ELSE 0
        END AS normalized_value
    FROM
    (
        SELECT
            agg.msd_file_id,
            agg.spot_number,
            agg.stimulation,
            AVG(agg.avg_value) AS avg_value
        FROM
        (
            SELECT
                mfs.id AS msd_file_id,
                mfcs.spot_number,
                tmps.sample,
                tmps.stimulation,
                AVG(mfcs.result_value) AS avg_value
            FROM tmp_msd_intraplate_calc tmps
            INNER JOIN stg_infodw.msd_files mfs
                ON mfs.plate_id = tmps.plate_id
                AND mfs.load_status = 'loaded'
            INNER JOIN stg_infodw.msd_file_contents mfcs
                ON mfcs.msd_file_id = mfs.id
                AND mfcs.row_letter = tmps.plate_row
                AND mfcs.column_number = tmps.plate_column
            WHERE
                tmps.sample_type = 'control'
            GROUP BY
                mfs.id,
                mfcs.spot_number,
                tmps.sample,
                tmps.stimulation
        ) agg
        GROUP BY
            agg.msd_file_id,
            agg.spot_number,
            agg.stimulation
    ) sq
    INNER JOIN stg_infodw.msd_files mf
        ON mf.id = sq.msd_file_id
    INNER JOIN stg_infodw.msd_file_contents mfc
        ON mfc.msd_file_id = mf.id
        AND mfc.spot_number = sq.spot_number
    INNER JOIN tmp_msd_intraplate_calc tmp
        ON tmp.plate_id = mf.plate_id
        AND tmp.plate_row = mfc.row_letter
        AND tmp.plate_column = mfc.column_number
        AND tmp.stimulation = sq.stimulation
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = mf.plate_id
        AND io.result_stage::TEXT = mf.result_stage::TEXT
        AND io.instrument = 'MSD'
    ORDER BY
        mf.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number;

    -- Insert audit records
    INSERT INTO stg_infodw.msd_file_audit
    (
        msd_file_id,
        action_name,
        action_result
    )
    SELECT
        mf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.msd_files mf
    WHERE
        mf.load_status = 'loaded';

    -- Update statuses
    UPDATE stg_infodw.msd_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;


CREATE OR REPLACE FUNCTION prc_infodw.msd_refresh_mvs()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    REFRESH MATERIALIZED VIEW infodw.msd_raw_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_normalized_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_ntc_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_pathway_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_plate_results_mv;

    REFRESH MATERIALIZED VIEW infodw.crispr_experiment_mv;
    REFRESH MATERIALIZED VIEW infodw.screening_msd_results_mv;

END;
$BODY$;
 
CREATE OR REPLACE FUNCTION stg_infodw.fortessa_create_files
(
    src stg_infodw.data_source_enum
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    CREATE TEMP TABLE tmp_fortessa_files ON COMMIT DROP AS
    SELECT
        plate_id,
        result_stage
    FROM stg_infodw.fortessa_missing_files_v;

    INSERT INTO stg_infodw.fortessa_files
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        plate_id,
        src_filename,
        result_stage,
        plate_created_at,
        load_status,
        data_source
    )
    SELECT
        fmf.experiment_id,
        fmf.experiment_name,
        fmf.exp_created_at,
        fmf.plate_id,
        fmf.filepath,
        fmf.result_stage,
        fmf.plate_created_at,
        'created' AS load_status,
        src AS data_source
    FROM stg_infodw.fortessa_missing_files_v fmf
    INNER JOIN tmp_fortessa_files tmp
        ON tmp.plate_id = fmf.plate_id
        AND tmp.result_stage = fmf.result_stage;

    INSERT INTO stg_infodw.fortessa_file_audit
    (
        fortessa_file_id,
        action_name,
        action_result
    )
    SELECT
        ff.id,
        'Insert entity record',
        'Success'
    FROM stg_infodw.fortessa_files ff
    INNER JOIN tmp_fortessa_files tmp
        ON tmp.plate_id = ff.plate_id
        AND tmp.result_stage = ff.result_stage;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.fortessa_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Collect metadata for each well in temporary table
    CREATE TEMP TABLE tmp_fortessa_intraplate_calc ON COMMIT DROP AS
    SELECT
        pmd.plate_id,
        pmd.plate_row,
        pmd.plate_column,
        pmd.sample,
        pmd.sample_type,
        pmd.stimulation
    FROM prc_infodw.instr_plate_metadata_mv pmd
    WHERE
        EXISTS
        (
            SELECT 1
            FROM stg_infodw.fortessa_files ff
            WHERE
                ff.plate_id = pmd.plate_id
                AND ff.load_status = 'loaded'
        );

    CREATE UNIQUE INDEX idx_tmp_fortessa_intraplate_calc_plate_id_plate_row_plate_column
        ON tmp_fortessa_intraplate_calc (plate_id ASC, plate_row ASC, plate_column ASC);

    -- Warn about plates without controls
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status = 'loaded'
            AND NOT EXISTS
            (
                SELECT 1
                FROM tmp_fortessa_intraplate_calc tmp
                WHERE
                    tmp.plate_id = ff.plate_id
                    AND tmp.sample_type = 'control'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                ff.id,
                ff.plate_id,
                ff.src_filename
            FROM stg_infodw.fortessa_files ff
            WHERE
                ff.load_status = 'loaded'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM tmp_fortessa_intraplate_calc tmp
                    WHERE
                        tmp.plate_id = ff.plate_id
                        AND tmp.sample_type = 'control'
                )
        LOOP

            INSERT INTO stg_infodw.fortessa_file_audit
            (
                fortessa_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: no controls found for this plate'
            );

            RAISE WARNING 'No controls found for plate %.', _rec.plate_id;

            filename := _rec.src_filename;
            output := 'No controls found for this plate';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Fill new field names
    INSERT INTO stg_infodw.fortessa_fieldname_lookup
    (
        fieldname,
        normalized_fieldname
    )
    SELECT
        ffc.result_field,
        CASE
            WHEN ffc.result_field ILIKE '%Ki67%' THEN 'Ki67'
            WHEN ffc.result_field ILIKE '%Zap70%' THEN 'Zap70'
            WHEN ffc.result_field ILIKE '%CD4%' THEN 'CD4'
            ELSE 'Viable'
        END
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.fortessa_file_contents ffc
        ON ffc.fortessa_file_id = ff.id
    WHERE
        ff.load_status = 'loaded'
        AND ffc.result_field <> 'Count'
        AND ffc.result_field NOT IN
        (
            SELECT
                fieldname
            FROM stg_infodw.fortessa_fieldname_lookup
        )
    GROUP BY
        ffc.result_field;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        ff.experiment_id,
        ff.experiment_name,
        ff.exp_created_at,
        'FORTESSA' AS instrument,
        ff.plate_id,
        nbi.egnyte_folder_path AS output_path,
        ff.result_stage::TEXT::prc_infodw.result_stage_enum,
        ff.plate_created_at
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = ff.experiment_id
        AND nbi.instrument_used LIKE 'FORTESSA%'
    WHERE
        ff.load_status = 'loaded';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        sample,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        SUBSTRING(REPLACE(ffc.sample, ' ', '_'), '_([A-Z])\d{2}[_\.]') AS row_letter,
        SUBSTRING(REPLACE(ffc.sample, ' ', '_'), '_[A-Z](\d{2})[_\.]')::SMALLINT AS column_number,
        ffc.sample,
        ffc.result_field,
        ffl.normalized_fieldname,
        'ORIGINAL' AS result_type,
        ffc.result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.fortessa_file_contents ffc
        ON ffc.fortessa_file_id = ff.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    LEFT JOIN stg_infodw.fortessa_fieldname_lookup ffl
        ON ffl.fieldname = ffc.result_field
    WHERE
        ff.load_status = 'loaded'
    ORDER BY
        ff.id,
        ffc.sample,
        ffc.result_field;

    -- Insert NTC averages
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        ioc.result_field,
        ioc.result_normalized_field,
        'NTC_AVERAGE' AS result_type,
        AVG(ioc.result_value) AS result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
        AND ioc.result_type = 'ORIGINAL'
    INNER JOIN tmp_fortessa_intraplate_calc tmp
        ON tmp.plate_id = io.plate_id
        AND tmp.plate_row = ioc.row_letter
        AND tmp.plate_column = ioc.column_number
    WHERE
        ff.load_status = 'loaded'
        AND tmp.sample_type = 'control'
        AND tmp.stimulation <> 'Rested'
        AND ioc.sample NOT LIKE 'Compensation%'
        AND ioc.sample NOT IN ('Mean', 'SD')
    GROUP BY
        io.id,
        ioc.result_field,
        ioc.result_normalized_field
    ORDER BY
        io.id,
        ioc.result_field;

    -- Insert intraplate calculations
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        sample,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        ioco.row_letter,
        ioco.column_number,
        ioco.sample,
        ioco.result_field,
        ioco.result_normalized_field,
        'INTRAPLATE_NORMALIZED' AS result_type,
        100::DOUBLE PRECISION * ioco.result_value / ioca.result_value AS result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    INNER JOIN prc_infodw.instr_output_contents ioco
        ON ioco.instr_output_id = io.id
        AND ioco.result_type = 'ORIGINAL'
    INNER JOIN prc_infodw.instr_output_contents ioca
        ON ioca.instr_output_id = io.id
        AND ioca.result_type = 'NTC_AVERAGE'
        AND ioca.result_field = ioco.result_field
    WHERE
        ff.load_status = 'loaded'
        AND ioco.sample NOT LIKE 'Compensation%'
        AND ioco.sample NOT IN ('Mean', 'SD')
    ORDER BY
        io.id,
        ioco.sample,
        ioco.result_field;

    -- Insert audit records
    INSERT INTO stg_infodw.fortessa_file_audit
    (
        fortessa_file_id,
        action_name,
        action_result
    )
    SELECT
        ff.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.fortessa_files ff
    WHERE
        ff.load_status = 'loaded';

    -- Update statuses
    UPDATE stg_infodw.fortessa_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;
 

BEGIN;

INSERT INTO prc_infodw.instr_output_contents
(
    instr_output_id,
    row_letter,
    column_number,
    spot_number,
    result_type,
    result_value
)
SELECT
    io.id,
    mr.row_letter,
    mr.column_number::SMALLINT,
    mr.spot_number,
    'MSD/GUAVA' AS result_type,
    mr.result AS result_value
FROM assay.msd_raw mr
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = mr.plate_id
    AND io.instrument = 'MSD'
    AND io.result_stage::VARCHAR = mr.result_stage
WHERE
    mr.result_stage IN ('RAW_DATA', 'ANALYZED_DATA')
    AND mr.result_type IN ('RAW_MSD/GUAVA', 'ANALYZED_MSD/GUAVA');

INSERT INTO prc_infodw.instr_output_contents
(
    instr_output_id,
    row_letter,
    column_number,
    spot_number,
    result_type,
    result_value
)
SELECT
    io.id,
    mr.row_letter,
    mr.column_number::SMALLINT,
    mr.spot_number,
    'GUAVA_NORMALIZED' AS result_type,
    mr.result AS result_value
FROM assay.msd_raw mr
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = mr.plate_id
    AND io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
WHERE
    mr.result_stage = 'ANALYZED_MSD/GUAVA'
    AND mr.result_type = 'GUAVA NORMALIZED_DATA';

COMMIT;
