ALTER TYPE stg_infodw.entry_status_enum ADD VALUE 'invalidated';


CREATE OR REPLACE FUNCTION stg_infodw.msd_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_contents AS ioc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.msd_files mf
            ON mf.plate_id = io.plate_id
            AND mf.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioc.instr_output_id = io.id
            AND io.instrument = 'MSD'
            AND mf.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.msd_files mf
        WHERE
            mf.plate_id = io.plate_id
            AND mf.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'MSD'
            AND mf.load_status = 'invalidated';

        DELETE FROM stg_infodw.msd_file_contents AS mfc
        USING stg_infodw.msd_files mf
        WHERE
            mfc.msd_file_id = mf.id
            AND mf.load_status = 'invalidated';

        DELETE FROM stg_infodw.msd_file_metadata mfm
        USING stg_infodw.msd_files mf
        WHERE
            mfm.msd_file_id = mf.id
            AND mf.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.msd_file_audit
        (
            msd_file_id,
            action_name,
            action_result
        )
        SELECT
            mf.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status = 'invalidated';

        -- Write audit records for files
        INSERT INTO stg_infodw.msd_file_audit
        (
            msd_file_id,
            action_name,
            action_result
        )
        SELECT
            mf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status IN ('created', 'invalidated');

        RETURN QUERY
        UPDATE stg_infodw.msd_files umf
        SET
            load_status = 'started'
        WHERE
            umf.load_status IN ('created', 'invalidated')
        RETURNING
            umf.id,
            umf.experiment_id,
            umf.experiment_name,
            umf.plate_id,
            umf.src_filename,
            umf.result_stage,
            umf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.fortessa_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_contents AS ioc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.fortessa_files ff
            ON ff.plate_id = io.plate_id
            AND ff.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioc.instr_output_id = io.id
            AND io.instrument = 'FORTESSA'
            AND ff.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = io.plate_id
            AND ff.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'FORTESSA'
            AND ff.load_status = 'invalidated';

        DELETE FROM stg_infodw.fortessa_file_contents AS ffc
        USING stg_infodw.fortessa_files ff
        WHERE
            ffc.fortessa_file_id = ff.id
            AND ff.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.fortessa_file_audit
        (
            fortessa_file_id,
            action_name,
            action_result
        )
        SELECT
            ff.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status = 'invalidated'
            AND ff.result_stage = 'ANALYZED_DATA';

        -- Write audit records for files
        INSERT INTO stg_infodw.fortessa_file_audit
        (
            fortessa_file_id,
            action_name,
            action_result
        )
        SELECT
            ff.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status IN ('created', 'invalidated')
            AND ff.result_stage = 'ANALYZED_DATA';

        RETURN QUERY
        UPDATE stg_infodw.fortessa_files uff
        SET
            load_status = 'started'
        WHERE
            uff.load_status IN ('created', 'invalidated')
            AND uff.result_stage = 'ANALYZED_DATA'
        RETURNING
            uff.id,
            uff.experiment_id,
            uff.experiment_name,
            uff.plate_id,
            uff.src_filename,
            uff.result_stage,
            uff.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_start_analyzed_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_qe_contents AS ioqc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.qe_file_data qfd
            ON SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = io.plate_id
        INNER JOIN stg_infodw.qe_files qf
            ON qf.id = qfd.qe_file_id
            AND qf.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioqc.instr_output_id = io.id
            AND io.instrument = 'QE'
            AND qf.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.qe_file_data qfd
        INNER JOIN stg_infodw.qe_files qf
            ON qf.id = qfd.qe_file_id
        WHERE
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = io.plate_id
            AND qf.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'QE'
            AND qf.load_status = 'invalidated';

        DELETE FROM stg_infodw.qe_file_peaks AS qfp
        USING stg_infodw.qe_files qf
        WHERE
            qfp.qe_file_id = qf.id
            AND qf.load_status = 'invalidated';

        DELETE FROM stg_infodw.qe_file_data AS qfd
        USING stg_infodw.qe_files qf
        WHERE
            qfd.qe_file_id = qf.id
            AND qf.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.qe_file_audit
        (
            qe_file_id,
            action_name,
            action_result
        )
        SELECT
            qf.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status = 'invalidated'
            AND qf.result_stage = 'ANALYZED_DATA';

        -- Write audit records for files
        INSERT INTO stg_infodw.qe_file_audit
        (
            qe_file_id,
            action_name,
            action_result
        )
        SELECT
            qf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status IN ('created', 'invalidated')
            AND qf.result_stage = 'ANALYZED_DATA';

        RETURN QUERY
        UPDATE stg_infodw.qe_files uqf
        SET
            load_status = 'started'
        WHERE
            uqf.load_status IN ('created', 'invalidated')
            AND uqf.result_stage = 'ANALYZED_DATA'
        RETURNING
            uqf.id,
            uqf.experiment_id,
            uqf.experiment_name,
            uqf.src_filename,
            uqf.result_stage,
            uqf.load_status;

END;
$BODY$;


CREATE TYPE stg_infodw.pipeline_enum AS ENUM
    ('MSD', 'GUAVA', 'FORTESSA', 'FIATOF', 'QE', 'QC');


CREATE OR REPLACE FUNCTION stg_infodw.util_reload_plate
(
    plate_id VARCHAR(32),
    pipeline stg_infodw.pipeline_enum DEFAULT NULL
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _plate_id ALIAS FOR plate_id;
    _row_count INT;
BEGIN

    IF COALESCE(pipeline, 'MSD') = 'MSD' THEN

        UPDATE stg_infodw.msd_files AS mf
        SET
            load_status = 'invalidated'
        WHERE
            mf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% MSD files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FORTESSA') = 'FORTESSA' THEN

        UPDATE stg_infodw.fortessa_files AS ff
        SET
            load_status = 'invalidated'
        WHERE
            ff.plate_id = _plate_id
            AND ff.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FORTESSA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QE') = 'QE' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.result_stage = 'ANALYZED_DATA'
            AND EXISTS
            (
                SELECT 1
                FROM stg_infodw.qe_file_data qfd
                WHERE
                    qfd.qe_file_id = qf.id
                    AND SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = _plate_id
            );

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QE files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QC') = 'QC' THEN

        UPDATE stg_infodw.qc_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QC files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FIATOF') = 'FIATOF' THEN

        UPDATE fia.processingout AS po
        SET
            load_status = 'invalidated'
        WHERE
            po.processingout_id IN
            (
                SELECT
                    j.processingout_id
                FROM fia.jsonsample js
                INNER JOIN fia.samplefield sf
                    ON sf.samplefield_id = js.samplefield_id
                INNER JOIN fia."json" j
                    ON j.json_id = js.json_id
                WHERE
                    sf.samplefield_name = 'dsSampleCode'
                    AND SUBSTRING(js.value, '(P\-\d+)_[A-Z]\d+.*') = _plate_id
            );

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FIATOF files were marked for reload.', _row_count;

    END IF;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.util_reload_experiment
(
    exp_name TEXT,
    pipeline stg_infodw.pipeline_enum DEFAULT NULL
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _exp_id INT;
    _row_count INT;
BEGIN

    SELECT
        exp.recordid
    INTO _exp_id
    FROM stg_infodw.sapio_elnexperiment exp
    WHERE
        SUBSTRING(exp.datarecordname, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

    IF COALESCE(pipeline, 'MSD') = 'MSD' THEN

        UPDATE stg_infodw.msd_files AS mf
        SET
            load_status = 'invalidated'
        WHERE
            mf.experiment_id = _exp_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% MSD files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FORTESSA') = 'FORTESSA' THEN

        UPDATE stg_infodw.fortessa_files AS ff
        SET
            load_status = 'invalidated'
        WHERE
            ff.experiment_id = _exp_id
            AND ff.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FORTESSA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QE') = 'QE' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.experiment_id = _exp_id
            AND qf.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QE files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QC') = 'QC' THEN

        UPDATE stg_infodw.qc_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            SUBSTRING(qf.experiment_name, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QC files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FIATOF') = 'FIATOF' THEN

        UPDATE fia.processingout AS po
        SET
            load_status = 'invalidated'
        WHERE
            SUBSTRING(po.exp_id, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FIATOF files were marked for reload.', _row_count;

    END IF;

END;
$BODY$;


UPDATE stg_infodw.qc_files
SET
    load_status = 'invalidated'
WHERE
    load_status IN ('started', 'failed');

