CREATE OR REPLACE FUNCTION fia.json_cleanup
(
    p_json_id int
)
RETURNS int4
LANGUAGE plpgsql
AS $BODY$
DECLARE
    jsondiff_tables varchar[] := array[
        'met_diff',
        'crn_met_diff'];
    json_tables varchar[] := array[
        'ion',
        'annotation',
        'intensities',
        'jsonsample',
        'raw_ion',
        'jsondiff',
        'raw_intensities'];
    total int;
    row_count int;
    table_name varchar;
BEGIN
    DELETE FROM fia.jsonsample_qc
    WHERE jsonsample_id in (
        SELECT jsonsample_id
        FROM fia.jsonsample
        WHERE json_id = p_json_id);
    GET DIAGNOSTICS total = ROW_COUNT;
    FOREACH table_name IN ARRAY jsondiff_tables LOOP
        EXECUTE 'DELETE FROM fia.' || table_name ||
            ' WHERE jsondiff_id IN (
                SELECT jsondiff_id
                FROM fia.jsondiff
                WHERE json_id = $1)'
        USING p_json_id;
        GET DIAGNOSTICS row_count = ROW_COUNT;
        total := total + row_count;
    END LOOP;
    FOREACH table_name IN ARRAY json_tables LOOP
        EXECUTE 'DELETE FROM fia.' || table_name ||
            ' WHERE json_id = $1'
        USING p_json_id;
        GET DIAGNOSTICS row_count = ROW_COUNT;
        total := total + row_count;
    END LOOP;
    RETURN total;
END;
$BODY$;
