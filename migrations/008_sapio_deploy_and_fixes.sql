CREATE OR REPLACE FUNCTION stg_infodw.fill_sample_lineage()
RETURNS INT
LANGUAGE plpgsql
AS $BODY$
DECLARE
    result INT;
BEGIN

    TRUNCATE TABLE stg_infodw.sapio_rhe_sample_lineage;

    INSERT INTO stg_infodw.sapio_rhe_sample_lineage
    (
        samplename,
        recordid,
        parentsamplename,
        parentrecordid
    )
    SELECT
        child.othersampleid,
        child.recordid,
        child.parentsample,
        parent.recordid
    FROM stg_infodw.sapio_sample child
    LEFT OUTER JOIN stg_infodw.sapio_sample parent ON
        child.parentsample = parent.othersampleid;

    GET DIAGNOSTICS result = ROW_COUNT;

    RETURN result;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qc_cleanup_file
(
    src_filename VARCHAR(1024)
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _src_filename VARCHAR(1024);
BEGIN

    _src_filename := LOWER(src_filename);

    -- Clean processing tables
    DELETE FROM prc_infodw.qc_output_arrays AS qoa
    USING prc_infodw.qc_output qo
    WHERE
        qo.id = qoa.qc_output_id
        AND LOWER(qo.file_path || '/' || qo.file_name) = _src_filename;

    DELETE FROM prc_infodw.qc_output AS qo
    WHERE
        LOWER(qo.file_path || '/' || qo.file_name) = _src_filename;

    -- Clean staging contents table
    DELETE FROM stg_infodw.qc_file_contents AS qfc
    USING stg_infodw.qc_files qf
    WHERE
        qf.id = qfc.qc_file_id
        AND LOWER(qf.src_filename) = _src_filename;

    -- Insert audit record
    INSERT INTO stg_infodw.qc_file_audit
    (
        qc_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Cleanup for new file version' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qc_files qf
    WHERE
        LOWER(qf.src_filename) = _src_filename;

    -- Update statuses
    UPDATE stg_infodw.qc_files AS qf
    SET
        load_status = 'started'
    WHERE
        LOWER(qf.src_filename) = _src_filename;

END;
$BODY$;


UPDATE stg_infodw.qc_files
SET
    load_status = 'invalidated'
WHERE
    load_status IN ('started', 'failed');
