ALTER TABLE stg_infodw.sapio_rhe_plate_well
    ADD CONSTRAINT sapio_rhe_plate_well_pk
    PRIMARY KEY
    (
    wellelement_record_id
    );

ALTER TABLE stg_infodw.sapio_resultinfomsdplate
    ALTER COLUMN datecreated TYPE numeric(13,0) USING datecreated::numeric;
ALTER TABLE stg_infodw.sapio_resultinfomsdplate
    ALTER COLUMN veloxlastmodifieddate TYPE numeric(13,0) USING veloxlastmodifieddate::numeric;
ALTER TABLE stg_infodw.sapio_resultinfomsdplate
    ALTER COLUMN sampledilutionfactor DROP NOT NULL;
ALTER TABLE stg_infodw.sapio_resultinfomsdplate
    ALTER COLUMN stdcurvedilutionfactor DROP NOT NULL;
