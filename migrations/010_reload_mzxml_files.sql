ALTER TYPE stg_infodw.pipeline_enum ADD VALUE 'MZXML';

DELETE FROM stg_infodw.qe_file_audit WHERE qe_file_id IN
    (SELECT id FROM stg_infodw.qe_files WHERE result_stage = 'RAW_DATA');
DELETE FROM stg_infodw.qe_files WHERE result_stage = 'RAW_DATA';
DELETE FROM stg_infodw.qe_worklists;
DELETE FROM stg_infodw.qe_experiments;

ALTER TABLE stg_infodw.qe_files ADD COLUMN worklist_filename VARCHAR(1024) NULL;
ALTER TABLE stg_infodw.qe_files ADD CONSTRAINT ck_qe_files_worklist CHECK
    (result_stage <> 'RAW_DATA' OR worklist_filename IS NOT NULL);
ALTER TABLE stg_infodw.qe_files ADD CONSTRAINT fk_qe_files_qe_worklists FOREIGN KEY (experiment_id, worklist_filename)
    REFERENCES stg_infodw.qe_worklists (experiment_id, src_filename);


CREATE OR REPLACE FUNCTION stg_infodw.util_reload_plate
(
    plate_id VARCHAR(32),
    pipeline stg_infodw.pipeline_enum DEFAULT NULL
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _plate_id ALIAS FOR plate_id;
    _row_count INT;
BEGIN

    IF COALESCE(pipeline, 'MSD') = 'MSD' THEN

        UPDATE stg_infodw.msd_files AS mf
        SET
            load_status = 'invalidated'
        WHERE
            mf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% MSD files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FORTESSA') = 'FORTESSA' THEN

        UPDATE stg_infodw.fortessa_files AS ff
        SET
            load_status = 'invalidated'
        WHERE
            ff.plate_id = _plate_id
            AND ff.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FORTESSA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'MZXML') = 'MZXML' THEN

        UPDATE stg_infodw.qe_experiments AS qe
        SET
            status = 'invalidated'
        WHERE
            qe.experiment_id IN
            (
                SELECT
                    qf.experiment_id
                FROM stg_infodw.qe_files qf
                WHERE
                    qf.result_stage = 'RAW_DATA'
                    AND qf.plate_id = _plate_id
            );

        UPDATE stg_infodw.qe_worklists AS qw
        SET
            status = 'invalidated'
        WHERE
            EXISTS
            (
                SELECT
                    qf.experiment_id
                FROM stg_infodw.qe_files qf
                WHERE
                    qf.result_stage = 'RAW_DATA'
                    AND qf.plate_id = _plate_id
                    AND qf.experiment_id = qw.experiment_id
                    AND qf.worklist_filename = qw.src_filename
            );

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.result_stage = 'RAW_DATA'
            AND qf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% mzXML files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QE') = 'QE' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.result_stage = 'ANALYZED_DATA'
            AND EXISTS
            (
                SELECT 1
                FROM stg_infodw.qe_file_data qfd
                WHERE
                    qfd.qe_file_id = qf.id
                    AND SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = _plate_id
            );

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QE files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QC') = 'QC' THEN

        UPDATE stg_infodw.qc_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.plate_id = _plate_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QC files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FIATOF') = 'FIATOF' THEN

        UPDATE fia.processingout AS po
        SET
            load_status = 'invalidated'
        WHERE
            po.processingout_id IN
            (
                SELECT
                    j.processingout_id
                FROM fia.jsonsample js
                INNER JOIN fia.samplefield sf
                    ON sf.samplefield_id = js.samplefield_id
                INNER JOIN fia."json" j
                    ON j.json_id = js.json_id
                WHERE
                    sf.samplefield_name = 'dsSampleCode'
                    AND SUBSTRING(js.value, '(P\-\d+)_[A-Z]\d+.*') = _plate_id
            );

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FIATOF files were marked for reload.', _row_count;

    END IF;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.util_reload_experiment
(
    exp_name TEXT,
    pipeline stg_infodw.pipeline_enum DEFAULT NULL
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _exp_id INT;
    _row_count INT;
BEGIN

    SELECT
        exp.recordid
    INTO _exp_id
    FROM stg_infodw.sapio_elnexperiment exp
    WHERE
        SUBSTRING(exp.datarecordname, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

    IF COALESCE(pipeline, 'MSD') = 'MSD' THEN

        UPDATE stg_infodw.msd_files AS mf
        SET
            load_status = 'invalidated'
        WHERE
            mf.experiment_id = _exp_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% MSD files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FORTESSA') = 'FORTESSA' THEN

        UPDATE stg_infodw.fortessa_files AS ff
        SET
            load_status = 'invalidated'
        WHERE
            ff.experiment_id = _exp_id
            AND ff.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FORTESSA files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'MZXML') = 'MZXML' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.experiment_id = _exp_id
            AND qf.result_stage = 'RAW_DATA';

        UPDATE stg_infodw.qe_worklists AS qw
        SET
            status = 'invalidated'
        WHERE
            qw.experiment_id = _exp_id;

        UPDATE stg_infodw.qe_experiments AS qe
        SET
            status = 'invalidated'
        WHERE
            qe.experiment_id = _exp_id;

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% mzXML files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QE') = 'QE' THEN

        UPDATE stg_infodw.qe_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            qf.experiment_id = _exp_id
            AND qf.result_stage = 'ANALYZED_DATA';

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QE files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'QC') = 'QC' THEN

        UPDATE stg_infodw.qc_files AS qf
        SET
            load_status = 'invalidated'
        WHERE
            SUBSTRING(qf.experiment_name, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% QC files were marked for reload.', _row_count;

    END IF;

    IF COALESCE(pipeline, 'FIATOF') = 'FIATOF' THEN

        UPDATE fia.processingout AS po
        SET
            load_status = 'invalidated'
        WHERE
            SUBSTRING(po.exp_id, 'Bio-\d+') = SUBSTRING(exp_name, 'Bio-\d+');

        GET DIAGNOSTICS _row_count = ROW_COUNT;

        RAISE NOTICE '% FIATOF files were marked for reload.', _row_count;

    END IF;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_insert_missing_experiments
(
    all_exps BOOLEAN,
    exp_name TEXT DEFAULT NULL
)
RETURNS TABLE
(
    experiment_id INT,
    experiment_name TEXT,
    exp_created_at TIMESTAMP,
    src_path VARCHAR(1024),
    status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    INSERT INTO stg_infodw.qe_experiments
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        src_path,
        status
    )
    SELECT
        nbi.experiment_id AS experiment_id,
        nbi.experiment_name AS experiment_name,
        TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
        nbi.egnyte_folder_path AS src_path,
        'created' AS status
    FROM stg_infodw.sapio_rhe_nb_instru nbi
    INNER JOIN stg_infodw.sapio_elnexperiment exp on
        exp.recordid = nbi.experiment_id
    WHERE
        nbi.instrument_used LIKE 'QE%'
        AND (all_exps OR nbi.experiment_name = exp_name)
        AND NULLIF(LTRIM(nbi.egnyte_folder_path), '') IS NOT NULL
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_experiments qe
            WHERE
                qe.experiment_id = nbi.experiment_id
        );

    RETURN QUERY
    SELECT
        qe.experiment_id,
        qe.experiment_name,
        qe.exp_created_at,
        qe.src_path,
        qe.status
    FROM stg_infodw.qe_experiments qe
    WHERE
        qe.status IN ('created', 'invalidated');

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_recalc_statuses()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    UPDATE stg_infodw.qe_worklists wl
    SET
        status = 'processed'
    WHERE
        wl.status IN ('created', 'started', 'failed')
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_files qf
            WHERE
                qf.experiment_id = wl.experiment_id
                AND qf.worklist_filename = wl.src_filename
                AND qf.result_stage = 'RAW_DATA'
                AND qf.load_status <> 'processed'
        );

    UPDATE stg_infodw.qe_experiments exp
    SET
        status = 'processed'
    WHERE
        exp.status IN ('created', 'started', 'failed')
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_worklists wl
            WHERE
                wl.experiment_id = exp.experiment_id
                AND wl.status <> 'processed'
        );

END;
$BODY$;

