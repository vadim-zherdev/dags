DROP VIEW IF EXISTS infodw.instr_plate_metadata_v;
DROP VIEW IF EXISTS infodw.msd_analyzed_data_v;
DROP MATERIALIZED VIEW IF EXISTS infodw.msd_normalized_mv;
DROP VIEW IF EXISTS infodw.msd_normalized_rollup_v;
DROP MATERIALIZED VIEW IF EXISTS infodw.msd_raw_mv;
DROP MATERIALIZED VIEW IF EXISTS infodw.msd_ntc_mv;
DROP MATERIALIZED VIEW IF EXISTS infodw.msd_plate_results_mv;
DROP MATERIALIZED VIEW IF EXISTS infodw.screening_msd_results_mv;
DROP MATERIALIZED VIEW IF EXISTS infodw.msd_pathway_mv;
DROP VIEW IF EXISTS infodw.msd_control_calc_v;
DROP VIEW IF EXISTS infodw.msd_zprime_v;
DROP VIEW IF EXISTS infodw.crispr_screen_data_all_v;
DROP MATERIALIZED VIEW IF EXISTS infodw.crispr_screen_data_all_mv;

DROP VIEW IF EXISTS infodw.fortessa_well_metadata_v;
DROP VIEW IF EXISTS infodw.fortessa_csvres_merged_v;
DROP VIEW IF EXISTS infodw.fortessa_normalized_v;

DROP VIEW IF EXISTS stg_infodw.qc_metadata_v;

DROP MATERIALIZED VIEW IF EXISTS prc_infodw.instr_plate_metadata_mv;
CREATE MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv
AS SELECT
    pwp.plate_id,
    pwp.plate_row,
    pwp.plate_column,
    pwp.on_plate_id,
    pwp.spw_sample AS orig_sample,
    pwp.spw_stimulation AS orig_stimulation,
    pwp.spw_timepoint AS orig_timepoint,
    pwp.spw_isotope_label AS orig_isotope_label,
    pwp.spw_compound AS orig_compound,
    pwp.spw_annotation AS orig_annotation,
    CASE
        WHEN COALESCE(pwp.spw_insert_guide, pwp.spw_crispr_guide) LIKE 'NTC%'
            THEN COALESCE(pwp.spw_insert_guide, pwp.spw_crispr_guide)
        ELSE COALESCE(itg.genename, crg.genename)
    END AS gene,
    CASE
        WHEN COALESCE(pwp.spw_insert_guide, pwp.spw_crispr_guide) LIKE 'NTC%' THEN 'control'
        ELSE 'crispr'
    END AS gene_type,
    CASE
        WHEN pwp.spw_stimulation IN ('IC+', 'Stimulated') THEN 'IC+ Re-Activated'
        ELSE pwp.spw_stimulation
    END AS stimulation,
    COALESCE(cell.ontologynickname, 'NONE') AS cell_type,
    ctrl.controlsubtype AS control_subtype,
    (
        SELECT
            MAX(d.donorid)
        FROM stg_infodw.sapio_sample s
        INNER JOIN stg_infodw.sapio_donorid d
            ON s.relatedchild452 = d.recordid
        WHERE
            s.datarecordname = pwp.spw_sample
    ) AS donor,
    (
        SELECT
            MAX(ep.experiment_name)
        FROM stg_infodw.sapio_rhe_experiments_plates ep
        WHERE
            ep.plate_id = pwp.plate_id
    ) AS experiment_name
FROM stg_infodw.sapio_plate_well_pivot_v pwp
LEFT JOIN stg_infodw.sapio_crisprguide crg
    ON crg.datarecordname = pwp.spw_crispr_guide
LEFT JOIN stg_infodw.sapio_insertguide itg
    ON itg.datarecordname = pwp.spw_insert_guide
LEFT JOIN stg_infodw.sapio_cell cell
    ON cell.relatedrecord143 = pwp.spw_sample
LEFT JOIN stg_infodw.sapio_control ctrl
    ON ctrl.relatedrecord143 = pwp.spw_sample
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_primary
    ON prc_infodw.instr_plate_metadata_mv (plate_id, plate_row, plate_column);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_on_plate_id
    ON prc_infodw.instr_plate_metadata_mv (on_plate_id);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_gene
    ON prc_infodw.instr_plate_metadata_mv (gene);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_gene_type
    ON prc_infodw.instr_plate_metadata_mv (gene_type);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_stimulation
    ON prc_infodw.instr_plate_metadata_mv (stimulation);


CREATE OR REPLACE VIEW infodw.instr_plate_metadata_v
AS SELECT
    sq.experiment_name,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    pmd.plate_row,
    pmd.plate_column,
    pmd.gene,
    pmd.gene_type,
    pmd.stimulation,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    sq.instrumentname AS instrumentname,
    pmd.on_plate_id,
    (
        SELECT
            COUNT(*)
        FROM stg_infodw.sapio_rhe_plate_well rpw
        WHERE
            rpw.on_plate_id = pmd.on_plate_id
    ) AS cnt_on_plate_id
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN
(
    SELECT
        prf.plateid AS plate_id,
        MAX(prf.instrumentname) AS instrumentname,
        MAX(exp.datarecordname) AS experiment_name
    FROM stg_infodw.sapio_plateresultfile prf
    INNER JOIN stg_infodw.sapio_elnexperiment exp
        ON exp.recordid = prf.experimentrecordid
    GROUP BY
        prf.plateid
) sq
    ON sq.plate_id = pmd.plate_id
ORDER BY
    experiment_name,
    plate_id,
    well_position;


CREATE OR REPLACE VIEW infodw.msd_analyzed_data_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.plate_id,
    ioc.row_letter,
    ioc.column_number,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.gene,
    pmd.gene_type,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


CREATE MATERIALIZED VIEW infodw.msd_normalized_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.gene,
    pmd.gene_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
WITH DATA;


CREATE OR REPLACE VIEW infodw.msd_normalized_rollup_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    AVG(ioc.result_value) AS results_avg,
    STDDEV(ioc.result_value) AS results_sttdev,
    pmd.gene,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
    AND pmd.gene_type = 'crispr'
GROUP BY
    io.experiment_name,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.gene,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine;


CREATE MATERIALIZED VIEW infodw.msd_raw_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.gene,
    pmd.gene_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND
    (
        ioc.result_type = 'ORIGINAL'
        OR ioc.result_type = 'MSD/GUAVA' AND io.result_stage = 'ANALYZED_DATA'
    )
WITH DATA;


CREATE MATERIALIZED VIEW infodw.msd_ntc_mv
AS SELECT
    io.experiment_name,
    exp.createdby,
    io.exp_created_at AS datecreated,
    CASE
        WHEN ioc.row_letter = 'G' AND ioc.column_number IN (5, 6, 7) THEN 'NTC1'
        WHEN ioc.row_letter = 'G' AND ioc.column_number IN (10, 11, 12) THEN 'NTC2'
        ELSE NULL
    END AS ntc,
    pmd.gene,
    ioc.result_value AS msd_result,
    pmd.donor,
    io.plate_id,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    pmd.stimulation,
    ioc.spot_number
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = io.experiment_id
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND pmd.gene_type = 'control'
    AND pmd.stimulation = 'IC+ Re-Activated'
WITH DATA;


CREATE MATERIALIZED VIEW infodw.msd_plate_results_mv
AS SELECT
    io.experiment_name,
    io.plate_id,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.spot_number,
    pmd.gene,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine,
    pmd.orig_sample AS sample,
    COALESCE(ioc.result_value, 0) AS result_value
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
ORDER BY
    io.experiment_name,
    io.plate_id
WITH DATA;


CREATE MATERIALIZED VIEW infodw.screening_msd_results_mv
AS SELECT DISTINCT
    pmd.gene,
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_value AS result,
    ioc.spot_number,
    kit.cytokine,
    pmd.stimulation
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    pmd.stimulation = 'IC+ Re-Activated'
    AND pmd.experiment_name NOT LIKE '%Valid%'
    AND pmd.experiment_name NOT LIKE '%SOC%'
ORDER BY
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    well_position,
    pmd.gene,
    result_stage,
    kit.cytokine
WITH DATA;


CREATE MATERIALIZED VIEW infodw.msd_pathway_mv
AS SELECT DISTINCT
    pathway.pathway,
    pathway.symbol,
    m.experiment_name,
    CONCAT(m.plate_row, LPAD(m.plate_column::VARCHAR, 2, '0')) AS well_position,
    m.plate_id,
    kit.cytokine
FROM
(
    SELECT
        crn.rxn,
        g.entrez_id,
        g.symbol,
        COALESCE(crn.pathway, 'No metabolic pathway available') AS pathway
    FROM stg_infodw.mdb_gene g
    LEFT JOIN stg_infodw.mdb_immap_gpr gpr
        ON gpr.entrez_id = g.entrez_id
    LEFT JOIN stg_infodw.mdb_crn_reactions crn
        ON gpr.rxn_id = crn.rxn_id
) pathway
INNER JOIN prc_infodw.instr_plate_metadata_mv m
    ON m.gene = pathway.symbol
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = m.plate_id
WITH DATA;


CREATE OR REPLACE VIEW infodw.msd_control_calc_v
AS WITH control_calc1 AS
(
    SELECT
        AVG(ioc.result_value) AS control_ra_avg,
        STDDEV(ioc.result_value) AS control_ra_stddv,
        pmd.gene AS control_name,
        pmd.gene_type AS control_purpose,
        ioc.spot_number,
        io.result_stage,
        io.result_stage || ':' || pmd.stimulation || ':the AVG of NTC1 + NTC2' AS description,
        pmd.stimulation,
        pmd.cell_type,
        io.plate_id
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    WHERE
        io.instrument = 'MSD'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.gene_type = 'control'
    GROUP BY
        pmd.gene,
        pmd.gene_type,
        ioc.spot_number,
        io.plate_id,
        io.result_stage,
        pmd.stimulation,
        pmd.cell_type
),
control_calc2 AS
(
    SELECT
        AVG(cc.control_ra_avg) AS control_ra_avg,
        STDDEV(cc.control_ra_avg) AS control_ra_stddv,
        'CONTROLS AVG' AS control_name,
        cc.control_purpose,
        cc.spot_number,
        cc.result_stage,
        cc.result_stage || ':IC+ Re-Activated:AVG of the AVG of NTC1 + NTC2' AS description,
        cc.stimulation,
        cc.cell_type,
        cc.plate_id
    FROM control_calc1 cc
    GROUP BY
        cc.spot_number,
        cc.plate_id,
        cc.result_stage,
        cc.control_purpose,
        cc.stimulation,
        cc.cell_type
)
SELECT
    cc.plate_id,
    cc.spot_number,
    kit.cytokine,
    cc.description,
    cc.stimulation,
    cc.cell_type,
    cc.control_name,
    cc.control_purpose,
    cc.result_stage::VARCHAR AS result_stage,
    cc.control_avg,
    cc.control_stddev
FROM
(
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc1 cc1
    UNION ALL
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc2 cc2
) cc
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = cc.plate_id
    AND kit.spot_number = cc.spot_number
ORDER BY
    cc.plate_id,
    cc.spot_number;


CREATE OR REPLACE VIEW infodw.msd_zprime_v
AS WITH sq1 AS
(
    SELECT
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type,
        STDDEV(CASE WHEN pmd.gene = 'ZAP70' THEN ioc.result_value END) AS stddev_zap70,
        STDDEV(CASE WHEN pmd.gene IN ('NTC crRNA_1', 'NTC_crRNAko_1') THEN ioc.result_value END) AS stddev_ntc1,
        STDDEV(CASE WHEN pmd.gene IN ('NTC crRNA_2', 'NTC_crRNAko_2') THEN ioc.result_value END) AS stddev_ntc2,
        AVG(CASE WHEN pmd.gene = 'ZAP70' THEN ioc.result_value END) AS avg_zap70,
        AVG(CASE WHEN pmd.gene IN ('NTC crRNA_1', 'NTC_crRNAko_1') THEN ioc.result_value END) AS avg_ntc1,
        AVG(CASE WHEN pmd.gene IN ('NTC crRNA_2', 'NTC_crRNAko_2') THEN ioc.result_value END) AS avg_ntc2
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    INNER JOIN stg_infodw.msd_kit kit
        ON kit.plate_id = io.plate_id
        AND kit.spot_number = ioc.spot_number
    WHERE
        io.instrument = 'MSD'
        AND io.result_stage = 'RAW_DATA'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.gene IN
        (
            'ZAP70', 'NTC crRNA_1', 'NTC crRNA_2',
            'NTC_crRNAko_1', 'NTC_crRNAko_2', 'NTC_crRNAko_3',
            'NTC1', 'NTC2', 'NTC'
        )
        AND pmd.stimulation = 'IC+ Re-Activated'
    GROUP BY
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type
),
sq2 AS
(
    SELECT
        sq1.experiment_name,
        sq1.plate_id,
        sq1.spot_number,
        sq1.cytokine,
        sq1.result_stage,
        sq1.result_type,
        sq1.stddev_zap70,
        sq1.avg_zap70,
        sq1.stddev_ntc1,
        sq1.stddev_ntc2,
        (sq1.stddev_ntc1 + sq1.stddev_ntc2) / 2::DOUBLE PRECISION AS stddev_ntc,
        (sq1.avg_ntc1 + sq1.avg_ntc2) / 2::DOUBLE PRECISION AS avg_ntc
    FROM sq1
)
SELECT
    sq2.experiment_name,
    sq2.plate_id,
    sq2.spot_number,
    sq2.cytokine,
    sq2.result_stage::VARCHAR AS result_stage,
    sq2.result_type::VARCHAR AS result_type,
    sq2.stddev_zap70,
    sq2.avg_zap70,
    sq2.stddev_ntc,
    sq2.avg_ntc,
    1::DOUBLE PRECISION - 3::DOUBLE PRECISION * ((sq2.stddev_zap70 + sq2.stddev_ntc) / ABS(sq2.avg_ntc - sq2.avg_zap70)) AS z_prime
FROM sq2
ORDER BY
    sq2.plate_id,
    sq2.spot_number;


CREATE MATERIALIZED VIEW infodw.crispr_screen_data_all_mv
AS SELECT
    rqg.genename AS request_gene,
    pmd.gene AS plate_gene,
    SUBSTRING(pmd.experiment_name, 'Bio-\d+') AS experiment_name,
    pmd.experiment_name AS full_experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    pmd.stimulation,
    pmd.plate_row AS row_letter,
    pmd.plate_column AS column_number,
    COALESCE(ct_msd.src_file_name, ct_fortessa.src_file_name) AS src_file_name,
    ct_msd.spot_number,
    ct_msd.cytokine,
    ct_msd.raw_result,
    ct_msd.analyzed_result,
    ct_msd.raw_normalized_result,
    ct_msd.analyzed_normalized_result,
    ct_msd.raw_msd_guava_result,
    ct_msd.analyzed_msd_guava_result,
    ct_msd.guava_normalized_result,
    ct_fortessa.viability_result,
    ct_fortessa.ki67_result,
    ct_fortessa.cd4_result,
    ct_fortessa.zap70_result,
    ct_fortessa.viability_normalized,
    ct_fortessa.ki67_normalized,
    ct_fortessa.cd4_normalized,
    ct_fortessa.zap70_normalized,
    ct_fortessa_ntc.viability_average_ntc,
    ct_fortessa_ntc.ki67_average_ntc,
    ct_fortessa_ntc.cd4_average_ntc,
    ct_fortessa_ntc.zap70_average_ntc
FROM
(
    SELECT
        genename
    FROM stg_infodw.sapio_crisprguide
    UNION
    SELECT
        genename
    FROM stg_infodw.sapio_insertguide
) rqg
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.gene = rqg.genename
LEFT JOIN crosstab
(
    $$
    SELECT
        DENSE_RANK() OVER(ORDER BY plate_id, row_letter, column_number, spot_number) AS row_name,
        plate_id,
        row_letter,
        column_number,
        src_file_name,
        spot_number,
        cytokine,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_msd_v
    $$,
    $$
        VALUES ('M_R_O'), ('M_A_O'), ('M_R_I'), ('M_A_I'), ('M_R_M'), ('M_A_M'), ('M_A_G')
    $$
) AS ct_msd
(
    row_name BIGINT,
    plate_id VARCHAR(32),
    row_letter CHAR(1),
    column_number SMALLINT,
    src_file_name VARCHAR(1024),
    spot_number SMALLINT,
    cytokine VARCHAR(256),
    raw_result DOUBLE PRECISION,
    analyzed_result DOUBLE PRECISION,
    raw_normalized_result DOUBLE PRECISION,
    analyzed_normalized_result DOUBLE PRECISION,
    raw_msd_guava_result DOUBLE PRECISION,
    analyzed_msd_guava_result DOUBLE PRECISION,
    guava_normalized_result DOUBLE PRECISION
)
    ON ct_msd.plate_id = pmd.plate_id
    AND ct_msd.row_letter = pmd.plate_row
    AND ct_msd.column_number = pmd.plate_column
LEFT JOIN crosstab
(
    $$
    SELECT
        DENSE_RANK() OVER(ORDER BY plate_id, row_letter, column_number) AS row_name,
        plate_id,
        row_letter,
        column_number,
        src_file_name,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_fortessa_v
    WHERE
        result_type <> 'NTC_AVERAGE'
    $$,
    $$
        VALUES ('F_O_VIABLE'), ('F_O_KI67'), ('F_O_CD4'), ('F_O_ZAP70'),
            ('F_I_VIABLE'), ('F_I_KI67'), ('F_I_CD4'), ('F_I_ZAP70')
    $$
) AS ct_fortessa
(
    row_name BIGINT,
    plate_id VARCHAR(32),
    row_letter CHAR(1),
    column_number SMALLINT,
    src_file_name VARCHAR(1024),
    viability_result DOUBLE PRECISION,
    ki67_result DOUBLE PRECISION,
    cd4_result DOUBLE PRECISION,
    zap70_result DOUBLE PRECISION,
    viability_normalized DOUBLE PRECISION,
    ki67_normalized DOUBLE PRECISION,
    cd4_normalized DOUBLE PRECISION,
    zap70_normalized DOUBLE PRECISION
)
    ON ct_fortessa.plate_id = pmd.plate_id
    AND ct_fortessa.row_letter = pmd.plate_row
    AND ct_fortessa.column_number = pmd.plate_column
LEFT JOIN crosstab
(
    $$
    SELECT
        plate_id,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_fortessa_v
    WHERE
        result_type = 'NTC_AVERAGE'
    $$,
    $$
        VALUES ('F_N_VIABLE'), ('F_N_KI67'), ('F_N_CD4'), ('F_N_ZAP70')
    $$
) AS ct_fortessa_ntc
(
    plate_id VARCHAR(32),
    viability_average_ntc DOUBLE PRECISION,
    ki67_average_ntc DOUBLE PRECISION,
    cd4_average_ntc DOUBLE PRECISION,
    zap70_average_ntc DOUBLE PRECISION
)
    ON ct_fortessa_ntc.plate_id = pmd.plate_id
WITH DATA;


CREATE OR REPLACE VIEW infodw.crispr_screen_data_all_v
AS SELECT
    request_gene,
    plate_gene,
    experiment_name,
    full_experiment_name,
    donor,
    plate_id,
    cell_type,
    well_position,
    stimulation,
    row_letter,
    column_number,
    src_file_name,
    spot_number,
    cytokine,
    raw_result,
    analyzed_result,
    raw_normalized_result,
    analyzed_normalized_result,
    raw_msd_guava_result,
    analyzed_msd_guava_result,
    guava_normalized_result,
    viability_result,
    ki67_result,
    cd4_result,
    zap70_result,
    viability_normalized,
    ki67_normalized,
    cd4_normalized,
    zap70_normalized,
    viability_average_ntc,
    ki67_average_ntc,
    cd4_average_ntc,
    zap70_average_ntc
FROM infodw.crispr_screen_data_all_mv;


CREATE OR REPLACE VIEW infodw.fortessa_well_metadata_v
AS SELECT
    pmd.donor,
    pmd.plate_id,
    pmd.on_plate_id,
    pmd.stimulation,
    pmd.gene,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.id AS fortessa_exp_id
FROM prc_infodw.instr_output io
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA';


CREATE OR REPLACE VIEW infodw.fortessa_csvres_merged_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.gene,
    pmd.stimulation,
    pmd.donor
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND ioc.sample NOT LIKE 'Compensation%'
    AND ioc.sample NOT IN ('Mean', 'SD');


CREATE OR REPLACE VIEW infodw.fortessa_normalized_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.gene,
    pmd.stimulation,
    pmd.donor,
    ioca.result_value AS average_ntc,
    iocn.result_value AS normalized_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_output_contents ioca
    ON ioca.instr_output_id = io.id
    AND ioca.result_field = ioc.result_field
    AND ioca.result_type = 'NTC_AVERAGE'
INNER JOIN prc_infodw.instr_output_contents iocn
    ON iocn.instr_output_id = io.id
    AND iocn.result_field = ioc.result_field
    AND iocn.row_letter = ioc.row_letter
    AND iocn.column_number = ioc.column_number
    AND iocn.result_type = 'INTRAPLATE_NORMALIZED'
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


CREATE OR REPLACE VIEW stg_infodw.qc_metadata_v
AS SELECT
    pmd.experiment_name,
    SUBSTRING(pmd.experiment_name, 'Bio-\d+') AS experiment_shortname,
    pmd.on_plate_id,
    pmd.orig_sample AS sample,
    COALESCE(pmd.control_subtype, cell_type) AS cell_type
FROM prc_infodw.instr_plate_metadata_mv pmd;


CREATE OR REPLACE FUNCTION prc_infodw.msd_refresh_mvs()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    REFRESH MATERIALIZED VIEW infodw.msd_raw_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_normalized_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_ntc_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_pathway_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_plate_results_mv;

    REFRESH MATERIALIZED VIEW infodw.crispr_experiment_mv;
    REFRESH MATERIALIZED VIEW infodw.screening_msd_results_mv;

    REFRESH MATERIALIZED VIEW infodw.crispr_screen_data_all_mv;

END;
$BODY$;
