CREATE OR REPLACE VIEW stg_infodw.sapio_plate_well_pivot_v
AS SELECT
    ct_meta.on_plate_id,
    ct_meta.plate_id,
    ct_meta.plate_row,
    ct_meta.plate_column,
    ct_meta.spw_crispr_guide,
    ct_meta.spw_insert_guide,
    ct_meta.spw_stimulation,
    ct_meta.spw_sample,
    ct_meta.spw_timepoint,
    ct_meta.spw_isotope_label,
    ct_meta.spw_compound,
    ct_meta.spw_annotation
FROM crosstab
(
    $$
    SELECT
        pw.on_plate_id,
        pw.plate_id,
        pw.plate_row,
        pw.plate_column,
        pw.wellelement_datatype,
        pw.wellelement_subtype
    FROM stg_infodw.sapio_rhe_plate_well pw
    ORDER BY
        pw.on_plate_id,
        pw.wellelement_datatype,
        pw.wellelement_subtype
    $$,
    $$
    VALUES ('CRISPRGuide'), ('InsertGuide'), ('Stimulation'), ('Sample'),
        ('Timepoint'), ('IsotopeLabel'), ('Compound'), ('Annotation')
    $$
) AS ct_meta
(
    on_plate_id VARCHAR(12),
    plate_id VARCHAR(8),
    plate_row CHAR(1),
    plate_column SMALLINT,
    spw_crispr_guide VARCHAR(256),
    spw_insert_guide VARCHAR(256),
    spw_stimulation VARCHAR(256),
    spw_sample VARCHAR(256),
    spw_timepoint VARCHAR(256),
    spw_isotope_label VARCHAR(256),
    spw_compound VARCHAR(256),
    spw_annotation VARCHAR(256)
);


ALTER MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv RENAME TO instr_plate_metadata_mv_old;
DROP INDEX prc_infodw.idx_instr_plate_metadata_mv_primary;
DROP INDEX prc_infodw.idx_instr_plate_metadata_mv_sample;
DROP INDEX prc_infodw.idx_instr_plate_metadata_mv_sample_type;
DROP INDEX prc_infodw.idx_instr_plate_metadata_mv_stimulation;


CREATE MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv
AS SELECT
    pwp.plate_id,
    pwp.plate_row,
    pwp.plate_column,
    pwp.on_plate_id,
    pwp.spw_sample AS orig_sample,
    pwp.spw_stimulation AS orig_stimulation,
    pwp.spw_timepoint AS orig_timepoint,
    pwp.spw_isotope_label AS orig_isotope_label,
    pwp.spw_compound AS orig_compound,
    pwp.spw_annotation AS orig_annotation,
    CASE
        WHEN pwp.spw_crispr_guide LIKE 'NTC%' THEN pwp.spw_crispr_guide
        ELSE crg.genename
    END AS sample,
    CASE
        WHEN pwp.spw_crispr_guide LIKE 'NTC%' THEN 'control'
        ELSE 'crispr'
    END AS sample_type,
    CASE
        WHEN pwp.spw_stimulation IN ('IC+', 'Stimulated') THEN 'IC+ Re-Activated'
        ELSE pwp.spw_stimulation
    END AS stimulation,
    COALESCE(cell.ontologynickname, 'NONE') AS cell_type,
    ctrl.controlsubtype AS control_subtype,
    (
        SELECT
            MAX(d.donorid)
        FROM stg_infodw.sapio_sample s
        INNER JOIN stg_infodw.sapio_donorid d
            ON s.relatedchild452 = d.recordid
        WHERE
            s.plateid = pwp.plate_id
    ) AS donor,
    (
        SELECT
            MAX(ep.experiment_name)
        FROM stg_infodw.sapio_rhe_experiments_plates ep
        WHERE
            ep.plate_id = pwp.plate_id
    ) AS experiment_name
FROM stg_infodw.sapio_plate_well_pivot_v pwp
LEFT JOIN stg_infodw.sapio_crisprguide crg
    ON crg.datarecordname = pwp.spw_crispr_guide
LEFT JOIN stg_infodw.sapio_cell cell
    ON cell.relatedrecord143 = pwp.spw_sample
LEFT JOIN stg_infodw.sapio_control ctrl
    ON ctrl.relatedrecord143 = pwp.spw_sample
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_primary
    ON prc_infodw.instr_plate_metadata_mv (plate_id, plate_row, plate_column);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_on_plate_id
    ON prc_infodw.instr_plate_metadata_mv (on_plate_id);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_sample
    ON prc_infodw.instr_plate_metadata_mv (sample);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_sample_type
    ON prc_infodw.instr_plate_metadata_mv (sample_type);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_stimulation
    ON prc_infodw.instr_plate_metadata_mv (stimulation);


DROP VIEW IF EXISTS infodw.instr_plate_metadata_v;
CREATE OR REPLACE VIEW infodw.instr_plate_metadata_v
AS SELECT
    sq.experiment_name,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    pmd.plate_row,
    pmd.plate_column,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    sq.instrumentname AS instrumentname,
    pmd.on_plate_id,
    (
        SELECT
            COUNT(*)
        FROM stg_infodw.sapio_rhe_plate_well rpw
        WHERE
            rpw.on_plate_id = pmd.on_plate_id
    ) AS cnt_on_plate_id
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN
(
    SELECT
        prf.plateid AS plate_id,
        MAX(prf.instrumentname) AS instrumentname,
        MAX(exp.datarecordname) AS experiment_name
    FROM stg_infodw.sapio_plateresultfile prf
    INNER JOIN stg_infodw.sapio_elnexperiment exp
        ON exp.recordid = prf.experimentrecordid
    GROUP BY
        prf.plateid
) sq
    ON sq.plate_id = pmd.plate_id
ORDER BY
    experiment_name,
    plate_id,
    well_position;


DROP VIEW IF EXISTS infodw.msd_analyzed_data_v;
CREATE OR REPLACE VIEW infodw.msd_analyzed_data_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.plate_id,
    ioc.row_letter,
    ioc.column_number,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_normalized_mv;
CREATE MATERIALIZED VIEW infodw.msd_normalized_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
WITH DATA;


DROP VIEW IF EXISTS infodw.msd_normalized_rollup_v;
CREATE OR REPLACE VIEW infodw.msd_normalized_rollup_v
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    AVG(ioc.result_value) AS results_avg,
    STDDEV(ioc.result_value) AS results_sttdev,
    pmd.sample,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
    AND pmd.sample_type = 'crispr'
GROUP BY
    io.experiment_name,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample,
    pmd.stimulation,
    pmd.donor,
    pmd.cell_type,
    kit.cytokine;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_raw_mv;
CREATE MATERIALIZED VIEW infodw.msd_raw_mv
AS SELECT
    io.experiment_name,
    ioc.spot_number,
    ioc.row_letter,
    ioc.column_number,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.result_value AS result,
    io.output_path AS src_file_name,
    pmd.sample,
    pmd.sample_type,
    pmd.stimulation,
    pmd.donor,
    kit.cytokine,
    pmd.cell_type,
    io.plate_id
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    io.instrument = 'MSD'
    AND
    (
        ioc.result_type = 'ORIGINAL'
        OR ioc.result_type = 'MSD/GUAVA' AND io.result_stage = 'ANALYZED_DATA'
    )
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_ntc_mv;
CREATE MATERIALIZED VIEW infodw.msd_ntc_mv
AS SELECT
    io.experiment_name,
    exp.createdby,
    io.exp_created_at AS datecreated,
    CASE
        WHEN ioc.row_letter = 'G' AND ioc.column_number IN (5, 6, 7) THEN 'NTC1'
        WHEN ioc.row_letter = 'G' AND ioc.column_number IN (10, 11, 12) THEN 'NTC2'
        ELSE NULL
    END AS ntc,
    pmd.sample,
    ioc.result_value AS msd_result,
    pmd.donor,
    io.plate_id,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    pmd.stimulation,
    ioc.spot_number
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = io.experiment_id
WHERE
    io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND pmd.sample_type = 'control'
    AND pmd.stimulation = 'IC+ Re-Activated'
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_plate_results_mv;
CREATE MATERIALIZED VIEW infodw.msd_plate_results_mv
AS SELECT
    io.experiment_name,
    io.plate_id,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_type::VARCHAR AS result_type,
    ioc.spot_number,
    pmd.sample AS genename,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine,
    COUNT(pmd.plate_row) AS plate_well_cnt,
    COUNT(ioc.row_letter) AS result_well_cnt,
    AVG(ioc.result_value) AS avg_result,
    MAX(ioc.result_value) AS mx_result,
    MIN(ioc.result_value) AS mn_result
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
GROUP BY
    io.experiment_name,
    io.plate_id,
    io.result_stage,
    ioc.result_type,
    ioc.spot_number,
    pmd.sample,
    pmd.donor,
    pmd.stimulation,
    pmd.cell_type,
    kit.cytokine
ORDER BY
    io.experiment_name,
    io.plate_id
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.screening_msd_results_mv;
CREATE MATERIALIZED VIEW infodw.screening_msd_results_mv
AS SELECT DISTINCT
    pmd.sample,
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.result_stage::VARCHAR AS result_stage,
    ioc.result_value AS result,
    ioc.spot_number,
    kit.cytokine,
    pmd.stimulation
FROM prc_infodw.instr_plate_metadata_mv pmd
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = pmd.plate_id
    AND io.instrument = 'MSD'
LEFT JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
    AND ioc.row_letter = pmd.plate_row
    AND ioc.column_number = pmd.plate_column
    AND ioc.result_type IN ('INTRAPLATE_NORMALIZED', 'GUAVA_NORMALIZED')
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = io.plate_id
    AND kit.spot_number = ioc.spot_number
WHERE
    pmd.stimulation = 'IC+ Re-Activated'
    AND pmd.experiment_name NOT LIKE '%Valid%'
    AND pmd.experiment_name NOT LIKE '%SOC%'
ORDER BY
    pmd.experiment_name,
    pmd.donor,
    pmd.plate_id,
    well_position,
    pmd.sample,
    result_stage,
    kit.cytokine
WITH DATA;


DROP MATERIALIZED VIEW IF EXISTS infodw.msd_pathway_mv;
CREATE MATERIALIZED VIEW infodw.msd_pathway_mv
AS SELECT DISTINCT
    pathway.pathway,
    pathway.symbol,
    m.experiment_name,
    CONCAT(m.plate_row, LPAD(m.plate_column::VARCHAR, 2, '0')) AS well_position,
    m.plate_id,
    kit.cytokine
FROM
(
    SELECT
        crn.rxn,
        g.entrez_id,
        g.symbol,
        COALESCE(crn.pathway, 'No metabolic pathway available') AS pathway
    FROM stg_infodw.mdb_gene g
    LEFT JOIN stg_infodw.mdb_immap_gpr gpr
        ON gpr.entrez_id = g.entrez_id
    LEFT JOIN stg_infodw.mdb_crn_reactions crn
        ON gpr.rxn_id = crn.rxn_id
) pathway
INNER JOIN prc_infodw.instr_plate_metadata_mv m
    ON m.sample = pathway.symbol
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = m.plate_id
WITH DATA;


DROP VIEW IF EXISTS infodw.msd_control_calc_v;
CREATE OR REPLACE VIEW infodw.msd_control_calc_v
AS WITH control_calc1 AS
(
    SELECT
        AVG(ioc.result_value) AS control_ra_avg,
        STDDEV(ioc.result_value) AS control_ra_stddv,
        pmd.sample AS control_name,
        pmd.sample_type AS control_purpose,
        ioc.spot_number,
        io.result_stage,
        io.result_stage || ':' || pmd.stimulation || ':the AVG of NTC1 + NTC2' AS description,
        pmd.stimulation,
        pmd.cell_type,
        io.plate_id
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    WHERE
        io.instrument = 'MSD'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.sample_type = 'control'
    GROUP BY
        pmd.sample,
        pmd.sample_type,
        ioc.spot_number,
        io.plate_id,
        io.result_stage,
        pmd.stimulation,
        pmd.cell_type
),
control_calc2 AS
(
    SELECT
        AVG(cc.control_ra_avg) AS control_ra_avg,
        STDDEV(cc.control_ra_avg) AS control_ra_stddv,
        'CONTROLS AVG' AS control_name,
        cc.control_purpose,
        cc.spot_number,
        cc.result_stage,
        cc.result_stage || ':IC+ Re-Activated:AVG of the AVG of NTC1 + NTC2' AS description,
        cc.stimulation,
        cc.cell_type,
        cc.plate_id
    FROM control_calc1 cc
    GROUP BY
        cc.spot_number,
        cc.plate_id,
        cc.result_stage,
        cc.control_purpose,
        cc.stimulation,
        cc.cell_type
)
SELECT
    cc.plate_id,
    cc.spot_number,
    kit.cytokine,
    cc.description,
    cc.stimulation,
    cc.cell_type,
    cc.control_name,
    cc.control_purpose,
    cc.result_stage::VARCHAR AS result_stage,
    cc.control_avg,
    cc.control_stddev
FROM
(
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc1 cc1
    UNION ALL
    SELECT
        control_ra_avg AS control_avg,
        control_ra_stddv AS control_stddev,
        control_name,
        control_purpose,
        spot_number,
        result_stage,
        description,
        stimulation,
        cell_type,
        plate_id
    FROM control_calc2 cc2
) cc
LEFT JOIN stg_infodw.msd_kit kit
    ON kit.plate_id = cc.plate_id
    AND kit.spot_number = cc.spot_number
ORDER BY
    cc.plate_id,
    cc.spot_number;


CREATE OR REPLACE VIEW infodw.msd_zprime_v
AS WITH sq1 AS
(
    SELECT
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type,
        STDDEV(CASE WHEN pmd.sample = 'ZAP70' THEN ioc.result_value END) AS stddev_zap70,
        STDDEV(CASE WHEN pmd.sample = 'NTC crRNA_1' THEN ioc.result_value END) AS stddev_ntc1,
        STDDEV(CASE WHEN pmd.sample = 'NTC crRNA_2' THEN ioc.result_value END) AS stddev_ntc2,
        AVG(CASE WHEN pmd.sample = 'ZAP70' THEN ioc.result_value END) AS avg_zap70,
        AVG(CASE WHEN pmd.sample = 'NTC crRNA_1' THEN ioc.result_value END) AS avg_ntc1,
        AVG(CASE WHEN pmd.sample = 'NTC crRNA_2' THEN ioc.result_value END) AS avg_ntc2
    FROM prc_infodw.instr_output io
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
    INNER JOIN prc_infodw.instr_plate_metadata_mv pmd
        ON pmd.plate_id = io.plate_id
        AND pmd.plate_row = ioc.row_letter
        AND pmd.plate_column = ioc.column_number
    INNER JOIN stg_infodw.msd_kit kit
        ON kit.plate_id = io.plate_id
        AND kit.spot_number = ioc.spot_number
    WHERE
        io.instrument = 'MSD'
        AND io.result_stage = 'RAW_DATA'
        AND ioc.result_type = 'ORIGINAL'
        AND pmd.sample IN ('ZAP70', 'NTC crRNA_1', 'NTC crRNA_2', 'NTC1', 'NTC2', 'NTC')
        AND pmd.stimulation = 'IC+ Re-Activated'
    GROUP BY
        io.experiment_name,
        ioc.spot_number,
        io.plate_id,
        kit.cytokine,
        io.result_stage,
        ioc.result_type
),
sq2 AS
(
    SELECT
        sq1.experiment_name,
        sq1.plate_id,
        sq1.spot_number,
        sq1.cytokine,
        sq1.result_stage,
        sq1.result_type,
        sq1.stddev_zap70,
        sq1.avg_zap70,
        sq1.stddev_ntc1,
        sq1.stddev_ntc2,
        (sq1.stddev_ntc1 + sq1.stddev_ntc2) / 2::DOUBLE PRECISION AS stddev_ntc,
        (sq1.avg_ntc1 + sq1.avg_ntc2) / 2::DOUBLE PRECISION AS avg_ntc
    FROM sq1
)
SELECT
    sq2.experiment_name,
    sq2.plate_id,
    sq2.spot_number,
    sq2.cytokine,
    sq2.result_stage::VARCHAR AS result_stage,
    sq2.result_type::VARCHAR AS result_type,
    sq2.stddev_zap70,
    sq2.avg_zap70,
    sq2.stddev_ntc,
    sq2.avg_ntc,
    1::DOUBLE PRECISION - 3::DOUBLE PRECISION * ((sq2.stddev_zap70 + sq2.stddev_ntc) / ABS(sq2.avg_ntc - sq2.avg_zap70)) AS z_prime
FROM sq2
ORDER BY
    sq2.plate_id,
    sq2.spot_number;


DROP VIEW IF EXISTS infodw.crispr_screen_data_all_v;
CREATE OR REPLACE VIEW infodw.crispr_screen_data_all_v
AS SELECT
    rqg.genename AS request_gene,
    pmd.sample AS plate_gene,
    SUBSTRING(pmd.experiment_name, 'Bio-\d+') AS experiment_name,
    pmd.experiment_name AS full_experiment_name,
    pmd.donor,
    pmd.plate_id,
    pmd.cell_type,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    pmd.stimulation,
    pmd.plate_row AS row_letter,
    pmd.plate_column AS column_number,
    COALESCE(ct_msd.src_file_name, ct_fortessa.src_file_name) AS src_file_name,
    ct_msd.spot_number,
    ct_msd.cytokine,
    ct_msd.raw_result,
    ct_msd.analyzed_result,
    ct_msd.raw_normalized_result,
    ct_msd.analyzed_normalized_result,
    ct_msd.raw_msd_guava_result,
    ct_msd.analyzed_msd_guava_result,
    ct_msd.guava_normalized_result,
    ct_fortessa.viability_result,
    ct_fortessa.ki67_result,
    ct_fortessa.cd4_result,
    ct_fortessa.zap70_result,
    ct_fortessa.viability_normalized,
    ct_fortessa.ki67_normalized,
    ct_fortessa.cd4_normalized,
    ct_fortessa.zap70_normalized,
    ct_fortessa_ntc.viability_average_ntc,
    ct_fortessa_ntc.ki67_average_ntc,
    ct_fortessa_ntc.cd4_average_ntc,
    ct_fortessa_ntc.zap70_average_ntc
FROM
(
    SELECT DISTINCT
        genename
    FROM stg_infodw.sapio_crisprguide_v
) rqg
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.sample = rqg.genename
LEFT JOIN crosstab
(
    $$
    SELECT
        DENSE_RANK() OVER(ORDER BY plate_id, row_letter, column_number, spot_number) AS row_name,
        plate_id,
        row_letter,
        column_number,
        src_file_name,
        spot_number,
        cytokine,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_msd_v
    $$,
    $$
        VALUES ('M_R_O'), ('M_A_O'), ('M_R_I'), ('M_A_I'), ('M_R_M'), ('M_A_M'), ('M_A_G')
    $$
) AS ct_msd
(
    row_name BIGINT,
    plate_id VARCHAR(32),
    row_letter CHAR(1),
    column_number SMALLINT,
    src_file_name VARCHAR(1024),
    spot_number SMALLINT,
    cytokine VARCHAR(256),
    raw_result DOUBLE PRECISION,
    analyzed_result DOUBLE PRECISION,
    raw_normalized_result DOUBLE PRECISION,
    analyzed_normalized_result DOUBLE PRECISION,
    raw_msd_guava_result DOUBLE PRECISION,
    analyzed_msd_guava_result DOUBLE PRECISION,
    guava_normalized_result DOUBLE PRECISION
)
    ON ct_msd.plate_id = pmd.plate_id
    AND ct_msd.row_letter = pmd.plate_row
    AND ct_msd.column_number = pmd.plate_column
LEFT JOIN crosstab
(
    $$
    SELECT
        DENSE_RANK() OVER(ORDER BY plate_id, row_letter, column_number) AS row_name,
        plate_id,
        row_letter,
        column_number,
        src_file_name,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_fortessa_v
    WHERE
        result_type <> 'NTC_AVERAGE'
    $$,
    $$
        VALUES ('F_O_VIABLE'), ('F_O_KI67'), ('F_O_CD4'), ('F_O_ZAP70'),
            ('F_I_VIABLE'), ('F_I_KI67'), ('F_I_CD4'), ('F_I_ZAP70')
    $$
) AS ct_fortessa
(
    row_name BIGINT,
    plate_id VARCHAR(32),
    row_letter CHAR(1),
    column_number SMALLINT,
    src_file_name VARCHAR(1024),
    viability_result DOUBLE PRECISION,
    ki67_result DOUBLE PRECISION,
    cd4_result DOUBLE PRECISION,
    zap70_result DOUBLE PRECISION,
    viability_normalized DOUBLE PRECISION,
    ki67_normalized DOUBLE PRECISION,
    cd4_normalized DOUBLE PRECISION,
    zap70_normalized DOUBLE PRECISION
)
    ON ct_fortessa.plate_id = pmd.plate_id
    AND ct_fortessa.row_letter = pmd.plate_row
    AND ct_fortessa.column_number = pmd.plate_column
LEFT JOIN crosstab
(
    $$
    SELECT
        plate_id,
        result_category,
        result_value
    FROM prc_infodw.crispr_screen_helper_fortessa_v
    WHERE
        result_type = 'NTC_AVERAGE'
    $$,
    $$
        VALUES ('F_N_VIABLE'), ('F_N_KI67'), ('F_N_CD4'), ('F_N_ZAP70')
    $$
) AS ct_fortessa_ntc
(
    plate_id VARCHAR(32),
    viability_average_ntc DOUBLE PRECISION,
    ki67_average_ntc DOUBLE PRECISION,
    cd4_average_ntc DOUBLE PRECISION,
    zap70_average_ntc DOUBLE PRECISION
)
    ON ct_fortessa_ntc.plate_id = pmd.plate_id;


DROP VIEW IF EXISTS infodw.fortessa_well_metadata_v;
CREATE OR REPLACE VIEW infodw.fortessa_well_metadata_v
AS SELECT
    pmd.donor,
    pmd.plate_id,
    pmd.on_plate_id,
    pmd.stimulation,
    pmd.sample,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.id AS fortessa_exp_id
FROM prc_infodw.instr_output io
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA';


DROP VIEW IF EXISTS infodw.fortessa_csvres_merged_v;
CREATE OR REPLACE VIEW infodw.fortessa_csvres_merged_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.sample AS gene,
    pmd.stimulation,
    pmd.donor
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND ioc.sample NOT LIKE 'Compensation%'
    AND ioc.sample NOT IN ('Mean', 'SD');


DROP VIEW IF EXISTS infodw.fortessa_normalized_v;
CREATE OR REPLACE VIEW infodw.fortessa_normalized_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.sample AS gene,
    pmd.stimulation,
    pmd.donor,
    ioca.result_value AS average_ntc,
    iocn.result_value AS normalized_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_output_contents ioca
    ON ioca.instr_output_id = io.id
    AND ioca.result_field = ioc.result_field
    AND ioca.result_type = 'NTC_AVERAGE'
INNER JOIN prc_infodw.instr_output_contents iocn
    ON iocn.instr_output_id = io.id
    AND iocn.result_field = ioc.result_field
    AND iocn.row_letter = ioc.row_letter
    AND iocn.column_number = ioc.column_number
    AND iocn.result_type = 'INTRAPLATE_NORMALIZED'
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


DROP MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv_old;
DROP VIEW stg_infodw.instr_plate_metadata_v;


CREATE TYPE stg_infodw.rep_enum AS ENUM
    ('a', 'b', 'unknown');

CREATE SEQUENCE IF NOT EXISTS stg_infodw.qc_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.qc_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.qc_files_id_seq'),
    file_created_at TIMESTAMP NOT NULL,
    file_updated_at TIMESTAMP NOT NULL,
    dotd_acq_date TIMESTAMP NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    plate_id VARCHAR(6) NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    replicate stg_infodw.rep_enum  NOT NULL, -- Injection "replicate" as in a or b or unknown
    experiment_name VARCHAR(1024) NOT NULL,
    sample VARCHAR(256) NOT NULL,
    cell_type VARCHAR(256) NOT NULL,
    CONSTRAINT pk_qc_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_qc_files_src_filename
    ON stg_infodw.qc_files (src_filename);

CREATE TABLE IF NOT EXISTS stg_infodw.qc_file_contents
(
    qc_file_id INT NOT NULL,
    normalized_scaled_vector NUMERIC[] NOT NULL,  -- Spectrum as a normalized, scaled vector
    nz_array INT[] NOT NULL,                      -- A list m/z indexes for the normalized, scaled, vector
    num_centroids INT NOT NULL,                   -- Number of centroids (processed)
    summed_tic NUMERIC NOT NULL,                  -- Summed total ion current (TIC) - processed
    signal_to_noise NUMERIC NOT NULL,             -- Signal-to-noise (S/N)
    acq_date TIMESTAMP NOT NULL,                  -- Date of acquisition
    spectrum_ab_array NUMERIC[] NOT NULL,          -- Spectrum as ab pairs
    spectrum_mz_array NUMERIC[] NOT NULL,          -- Spectrum as m/z pairs
    mz_min INT NOT NULL,                           -- Min. m/z value in spectrum
    mz_max DOUBLE PRECISION NOT NULL,              -- Max. m/z value in spectrum
    mode VARCHAR(4) NOT NULL,                      -- Polarity
    fiamat_tic BIGINT NOT NULL,                    -- TIC (.fiamat file)
    CONSTRAINT pk_qc_file_contents PRIMARY KEY (qc_file_id),
    CONSTRAINT fk_qc_file_contents_qc_files FOREIGN KEY (qc_file_id)
        REFERENCES stg_infodw.qc_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qc_file_audit
(
    qc_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_qc_file_audit_qc_files FOREIGN KEY (qc_file_id)
        REFERENCES stg_infodw.qc_files (id)
);

CREATE INDEX IF NOT EXISTS idx_qc_file_audit_qc_file_id
    ON stg_infodw.qc_file_audit (qc_file_id);


CREATE SEQUENCE IF NOT EXISTS prc_infodw.qc_output_id_seq;

CREATE TABLE IF NOT EXISTS prc_infodw.qc_output
(
    id INT NOT NULL DEFAULT nextval('prc_infodw.qc_output_id_seq'),
    file_path VARCHAR(1024) NOT NULL,
    file_name VARCHAR(256) NOT NULL,
    on_plate_id VARCHAR(16) NOT NULL,
    experiment_name VARCHAR(1024) NOT NULL,
    short_experiment_name VARCHAR(16) NOT NULL,
    sample VARCHAR(256) NOT NULL,
    cell_type VARCHAR(256) NOT NULL,
    num_centroids INT NOT NULL,
    summed_tic NUMERIC NOT NULL,
    signal_to_noise NUMERIC NOT NULL,
    acq_date TIMESTAMP NOT NULL,
    mz_min INT NOT NULL,
    mz_max DOUBLE PRECISION NOT NULL,
    mode VARCHAR(4) NOT NULL,
    fiamat_tic BIGINT NOT NULL,
    replicate CHAR(1) NOT NULL,
    dotd_acq_date TIMESTAMP NOT NULL,
    CONSTRAINT pk_qc_output PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_qc_output_file_path_file_name
    ON prc_infodw.qc_output ((file_path || '/' || file_name));

CREATE TABLE IF NOT EXISTS prc_infodw.qc_output_arrays
(
    qc_output_id INT NOT NULL,
    normalized_scaled_vector NUMERIC[] NOT NULL,
    nz_array INT[] NOT NULL,
    spectrum_ab_array NUMERIC[] NOT NULL,
    spectrum_mz_array NUMERIC[] NOT NULL,
    CONSTRAINT pk_qc_output_arrays PRIMARY KEY (qc_output_id),
    CONSTRAINT fk_qc_output_arrays_qc_output FOREIGN KEY (qc_output_id)
        REFERENCES prc_infodw.qc_output (id)
);


CREATE OR REPLACE VIEW stg_infodw.qc_metadata_v
AS SELECT
    pmd.experiment_name,
    SUBSTRING(pmd.experiment_name, 'Bio-\d+') AS experiment_shortname,
    pmd.on_plate_id,
    pmd.orig_sample AS sample,
    COALESCE(pmd.control_subtype, cell_type) AS cell_type
FROM prc_infodw.instr_plate_metadata_mv pmd;


CREATE OR REPLACE FUNCTION stg_infodw.qc_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    -- Insert into output table
    INSERT INTO prc_infodw.qc_output
    (
        file_path,
        file_name,
        on_plate_id,
        experiment_name,
        short_experiment_name,
        sample,
        cell_type,
        num_centroids,
        summed_tic,
        signal_to_noise,
        acq_date,
        mz_min,
        mz_max,
        mode,
        fiamat_tic,
        replicate,
        dotd_acq_date
    )
    SELECT
        SUBSTRING(qf.src_filename, '(.+)/[\w\d_\-\.]+$') AS file_path,
        SUBSTRING(qf.src_filename, '.+/([\w\d_\-\.]+)$') AS file_name,
        qf.plate_id || '_' || qf.row_letter || qf.column_number AS on_plate_id,
        qf.experiment_name,
        SUBSTRING(qf.experiment_name, 'Bio-\d+') AS short_experiment_name,
        qf.sample,
        qf.cell_type,
        qfc.num_centroids,
        qfc.summed_tic,
        qfc.signal_to_noise,
        qfc.acq_date,
        qfc.mz_min,
        qfc.mz_max,
        qfc.mode,
        qfc.fiamat_tic,
        LEFT(qf.replicate::VARCHAR, 1) AS replicate,
        qf.dotd_acq_date
    FROM stg_infodw.qc_files qf
    INNER JOIN stg_infodw.qc_file_contents qfc
        ON qfc.qc_file_id = qf.id
    WHERE
        qf.load_status = 'loaded';

    -- Insert into arrays table
    INSERT INTO prc_infodw.qc_output_arrays
    (
        qc_output_id,
        normalized_scaled_vector,
        nz_array,
        spectrum_ab_array,
        spectrum_mz_array
    )
    SELECT
        qo.id,
        qfc.normalized_scaled_vector,
        qfc.nz_array,
        qfc.spectrum_ab_array,
        qfc.spectrum_mz_array
    FROM stg_infodw.qc_files qf
    INNER JOIN stg_infodw.qc_file_contents qfc
        ON qfc.qc_file_id = qf.id
    INNER JOIN prc_infodw.qc_output qo
        ON qo.file_path || '/' || qo.file_name = qf.src_filename
    WHERE
        qf.load_status = 'loaded'
    ORDER BY
        qo.id;

    -- Update statuses
    UPDATE stg_infodw.qc_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;


DROP VIEW IF EXISTS infodw.fia_qc_data_v2;
DROP VIEW IF EXISTS infodw.fia_qc_data_v;
DROP VIEW IF EXISTS infodw.fia_qc_data_unnest_v2;
DROP VIEW IF EXISTS infodw.fia_qc_data_unnest_v;


CREATE OR REPLACE VIEW infodw.fia_qc_data_unnest_v
AS SELECT
    qo.id AS spectrum_id,
    UNNEST(qoa.normalized_scaled_vector) AS normalized_scaled_vector,
    UNNEST(qoa.nz_array) AS nz_array,
    qo.num_centroids,
    qo.summed_tic,
    qo.signal_to_noise,
    qo.acq_date,
    UNNEST(qoa.spectrum_ab_array) AS spectrum_ab_array,
    UNNEST(qoa.spectrum_mz_array) AS spectrum_mz_array,
    qo.experiment_name,
    qo.short_experiment_name AS experiment_shortname,
    qo.file_path,
    qo.file_name,
    qo.on_plate_id,
    qo.sample,
    qo.cell_type AS celltype,
    qo.mz_min,
    qo.mz_max,
    qo.mode,
    qo.fiamat_tic,
    qo.replicate AS rep,
    qo.dotd_acq_date,
    NULL::VARCHAR AS metabolite
FROM prc_infodw.qc_output qo
INNER JOIN prc_infodw.qc_output_arrays qoa
    ON qoa.qc_output_id = qo.id;


CREATE OR REPLACE VIEW infodw.fia_qc_data_unnest_v2
AS SELECT
    qo.id AS spectrum_id,
    qo.num_centroids,
    qo.summed_tic,
    qo.signal_to_noise,
    qo.acq_date,
    UNNEST(qoa.spectrum_ab_array) AS spectrum_ab_array,
    UNNEST(qoa.spectrum_mz_array) AS spectrum_mz_array,
    qo.experiment_name,
    qo.short_experiment_name AS experiment_shortname,
    qo.file_path,
    qo.file_name,
    qo.on_plate_id,
    qo.sample,
    qo.cell_type AS celltype,
    qo.mz_min,
    qo.mz_max,
    qo.mode,
    qo.fiamat_tic,
    qo.replicate AS rep,
    qo.dotd_acq_date,
    NULL::VARCHAR AS metabolite
FROM prc_infodw.qc_output qo
INNER JOIN prc_infodw.qc_output_arrays qoa
    ON qoa.qc_output_id = qo.id;


CREATE OR REPLACE VIEW infodw.fia_qc_data_v
AS SELECT
    qdu.spectrum_id,
    qdu.normalized_scaled_vector,
    qdu.nz_array,
    qdu.num_centroids,
    qdu.summed_tic,
    qdu.signal_to_noise,
    qdu.acq_date,
    qdu.spectrum_ab_array,
    qdu.spectrum_mz_array,
    qdu.experiment_name,
    qdu.experiment_shortname,
    qdu.file_path,
    qdu.file_name,
    qdu.on_plate_id,
    qdu.sample,
    qdu.celltype,
    qdu.mz_min,
    qdu.mz_max,
    qdu.mode,
    qdu.fiamat_tic,
    qdu.rep,
    qdu.dotd_acq_date,
    (
        SELECT
            qdm.metabolite
        FROM stg_infodw.fia_qc_data_metabolites qdm
        WHERE
            qdu.spectrum_mz_array BETWEEN qdm.low_mass AND qdm.high_mass
    ) AS metabolite
FROM infodw.fia_qc_data_unnest_v qdu;


CREATE OR REPLACE VIEW infodw.fia_qc_data_v2
AS SELECT
    qdu.spectrum_id,
    qdu.num_centroids,
    qdu.summed_tic,
    qdu.signal_to_noise,
    qdu.acq_date,
    qdu.spectrum_ab_array,
    qdu.spectrum_mz_array,
    qdu.experiment_name,
    qdu.experiment_shortname,
    qdu.file_path,
    qdu.file_name,
    qdu.on_plate_id,
    qdu.sample,
    qdu.celltype,
    qdu.mz_min,
    qdu.mz_max,
    qdu.mode,
    qdu.fiamat_tic,
    qdu.rep,
    qdu.dotd_acq_date,
    (
        SELECT
            qdm.metabolite
        FROM stg_infodw.fia_qc_data_metabolites qdm
        WHERE
            qdu.spectrum_mz_array BETWEEN qdm.low_mass AND qdm.high_mass
    ) AS metabolite
FROM infodw.fia_qc_data_unnest_v qdu;
