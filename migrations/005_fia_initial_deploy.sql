CREATE TABLE IF NOT EXISTS stg_infodw.fia_processingout_audit
(
    processingout_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_fia_processingout_audit_processingout FOREIGN KEY (processingout_id)
        REFERENCES fia.processingout (processingout_id)
);

CREATE INDEX IF NOT EXISTS idx_fia_processingout_audit_processingout_id
    ON stg_infodw.fia_processingout_audit (processingout_id);


CREATE OR REPLACE FUNCTION stg_infodw.fia_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _input_status CONSTANT VARCHAR := 'Diff data loaded';
    _output_status CONSTANT VARCHAR := 'Processed';
BEGIN

    REFRESH MATERIALIZED VIEW infodw.fia_jsonsample_on_plate_mv;

    -- Delete previous extended curated data
    RAISE NOTICE 'Deleting from extended curated data table..';

    DELETE FROM infodw.fia_extended_curated_data_consumables
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about starting inserting extended curated data
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'Begin inserting into extended curated data consumables table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Perform actual record insertion
    RAISE NOTICE 'Inserting into extended curated data table..';

	INSERT INTO infodw.fia_extended_curated_data_consumables
	SELECT
	    * -- TODO: try to get rid of asterisk
	FROM infodw.fia_extended_curated_data_consumables_v
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about finishing inserting extended curated data
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'End inserting into extended curated data consumables table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Delete previous crn
    RAISE NOTICE 'Deleting from CRN difference table..';

    DELETE FROM infodw.fia_crn_met_diff_split
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about starting inserting crn
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'Begin inserting into CRN difference table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Perform actual record insertion
    RAISE NOTICE 'Inserting into CRN difference table..';

	INSERT INTO infodw.fia_crn_met_diff_split
	SELECT
	    * -- TODO: try to get rid of asterisk
	FROM infodw.fia_crn_met_diff_split_v
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about finishing inserting crn
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'End inserting into CRN difference table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Update statuses
    RAISE NOTICE 'Updating statuses..';

    UPDATE fia.processingout
    SET
        load_status = _output_status
    WHERE
        load_status = _input_status;

END;
$BODY$;
