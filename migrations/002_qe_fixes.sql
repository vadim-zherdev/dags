CREATE OR REPLACE FUNCTION prc_infodw.sapio_refresh_mvs()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    REFRESH MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv;

    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_annotation_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_compound_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_reagent_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_media_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_stimulation_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_crisprguide_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_insertguide_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_sample_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_stimulation_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_timepoint_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_rhe_sample_donor_cell_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_isotopetracer_mv;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_process_analyzed_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Detect files with no plate/well data
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status = 'loaded'
            AND qf.result_stage = 'ANALYZED_DATA'
            AND NOT EXISTS
            (
                SELECT 1
                FROM stg_infodw.qe_file_data qfd
                WHERE
                    qfd.qe_file_id = qf.id
                    AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                qf.id,
                qf.src_filename
            FROM stg_infodw.qe_files qf
            WHERE
                qf.load_status = 'loaded'
                AND qf.result_stage = 'ANALYZED_DATA'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM stg_infodw.qe_file_data qfd
                    WHERE
                        qfd.qe_file_id = qf.id
                        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
                )
        LOOP

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: file does not contain plate/well data'
            );

            UPDATE stg_infodw.qe_files
            SET
                load_status = 'failed'
            WHERE
                id = _rec.id;

            RAISE WARNING 'File % does not contain plate/well data.', _rec.src_filename;

            filename := _rec.src_filename;
            output := 'File does not contain plate/well data';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Detect data overlapping problems and mark failed
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.qe_files qf
        INNER JOIN stg_infodw.qe_file_data qfd
            ON qfd.qe_file_id = qf.id
        WHERE
            qf.load_status IN ('loaded', 'processed')
            AND qf.result_stage = 'ANALYZED_DATA'
        GROUP BY
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*'),
            qfd.metagroup_id,
            qfd.group_id,
            qfd.sample_id
        HAVING
            COUNT(*) > 1
    )
    THEN

        FOR _rec IN
            SELECT DISTINCT
                qf.id,
                qf.src_filename
            FROM stg_infodw.qe_files qf
            INNER JOIN stg_infodw.qe_file_data qfd
                ON qfd.qe_file_id = qf.id
            INNER JOIN
            (
                SELECT
                    SUBSTRING(qfd1.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id,
                    qfd1.metagroup_id,
                    qfd1.group_id,
                    qfd1.sample_id
                FROM stg_infodw.qe_files qf1
                INNER JOIN stg_infodw.qe_file_data qfd1
                    ON qfd1.qe_file_id = qf1.id
                WHERE
                    qf1.load_status IN ('loaded', 'processed')
                    AND qf1.result_stage = 'ANALYZED_DATA'
                GROUP BY
                    SUBSTRING(qfd1.sample_id, '(P\-\d+)_[A-Z]\d+.*'),
                    qfd1.metagroup_id,
                    qfd1.group_id,
                    qfd1.sample_id
                HAVING
                    COUNT(*) > 1
            ) sq1
                ON sq1.plate_id = SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*')
                AND sq1.metagroup_id = qfd.metagroup_id
                AND sq1.group_id = qfd.group_id
                AND sq1.sample_id = qfd.sample_id
            WHERE
                qf.load_status = 'loaded'
                AND qf.result_stage = 'ANALYZED_DATA'
        LOOP

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: file contains data overlapping other files data by key'
            );

            UPDATE stg_infodw.qe_files
            SET
                load_status = 'failed'
            WHERE
                id = _rec.id;

            RAISE WARNING 'Data overlapping for file %.', _rec.src_filename;

            filename := _rec.src_filename;
            output := 'Data overlapping with other files by key';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage
    )
    SELECT
        sq.experiment_id,
        sq.experiment_name,
        sq.exp_created_at,
        'QE' AS instrument,
        sq.plate_id,
        nbi.egnyte_folder_path AS output_path,
        'ANALYZED_DATA' AS result_stage
    FROM
    (
        SELECT
            qf.experiment_id,
            qf.experiment_name,
            qf.exp_created_at,
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id
        FROM stg_infodw.qe_files qf
        INNER JOIN stg_infodw.qe_file_data qfd
            ON qfd.qe_file_id = qf.id
        WHERE
            qf.load_status = 'loaded'
            AND qf.result_stage = 'ANALYZED_DATA'
    ) sq
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = sq.experiment_id
        AND nbi.instrument_used LIKE 'QE%'
    WHERE
        sq.plate_id IS NOT NULL
    GROUP BY
        sq.experiment_id,
        sq.experiment_name,
        sq.exp_created_at,
        sq.plate_id,
        nbi.egnyte_folder_path;

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_qe_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        metagroup_id,
        group_id,
        sample,
        isotope_label,
        compound,
        is_std,
        result_type,
        result_value
    )
    SELECT
        io.id,
        qfd.row_letter,
        qfd.column_number,
        qfp.metagroup_id,
        qfp.group_id,
        qfd.sample_id,
        qfp.isotope_label,
        qfp.compound,
        qfp.is_std,
        'ORIGINAL' AS result_type,
        qfd.result_value AS result_value
    FROM stg_infodw.qe_files qf
    INNER JOIN stg_infodw.qe_file_peaks qfp
        ON qfp.qe_file_id = qf.id
    INNER JOIN stg_infodw.qe_file_data qfd
        ON qfd.qe_file_id = qf.id
        AND qfd.metagroup_id = qfp.metagroup_id
        AND qfd.group_id = qfp.group_id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*')
        AND io.result_stage::TEXT = qf.result_stage::TEXT
        AND io.instrument = 'QE'
    WHERE
        qf.load_status = 'loaded'
        AND qf.result_stage = 'ANALYZED_DATA'
        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
    ORDER BY
        qf.id,
        qfd.sample_id,
        qfp.metagroup_id,
        qfp.group_id;

    -- Insert audit records
    INSERT INTO stg_infodw.qe_file_audit
    (
        qe_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qe_files qf
    WHERE
        qf.load_status = 'loaded'
        AND qf.result_stage = 'ANALYZED_DATA';

    -- Update statuses
    UPDATE stg_infodw.qe_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded'
        AND result_stage = 'ANALYZED_DATA';

END;
$BODY$;


ALTER TABLE prc_infodw.instr_output_qe_contents RENAME TO instr_output_qe_contents_old;
ALTER TABLE prc_infodw.instr_output_qe_contents_old RENAME CONSTRAINT pk_instr_output_qe_contents TO pk_instr_output_qe_contents_old;
ALTER TABLE prc_infodw.instr_output_qe_contents_old RENAME CONSTRAINT fk_instr_output_qe_contents_instr_output TO fk_instr_output_qe_contents_instr_output_old;

DROP INDEX prc_infodw.idx_instr_output_qe_contents_instr_output_id_row_letter_column_number;
DROP INDEX prc_infodw.idx_instr_output_qe_contents_sample;

CREATE TABLE prc_infodw.instr_output_qe_contents
(
    instr_output_id INT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    sample VARCHAR(256) NOT NULL,
    isotope_label VARCHAR(1024) NULL,
    compound VARCHAR(256) NULL,
    is_std BOOLEAN NOT NULL,
    result_type prc_infodw.result_type_enum NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_instr_output_qe_contents PRIMARY KEY (instr_output_id, metagroup_id, group_id, sample, result_type),
    CONSTRAINT fk_instr_output_qe_contents_instr_output FOREIGN KEY (instr_output_id)
        REFERENCES prc_infodw.instr_output (id)
);

CREATE INDEX IF NOT EXISTS idx_instr_output_qe_contents_instr_output_id_row_letter_column_number
    ON prc_infodw.instr_output_qe_contents (instr_output_id, row_letter, column_number);

CREATE INDEX IF NOT EXISTS idx_instr_output_qe_contents_sample
    ON prc_infodw.instr_output_qe_contents (sample);

INSERT INTO prc_infodw.instr_output_qe_contents
SELECT
    qe_old.instr_output_id,
    qe_old.row_letter,
    qe_old.column_number,
    qe_old.metagroup_id,
    qe_old.group_id,
    qe_old.sample,
    sq.isotope_label,
    qe_old.compound,
    qe_old.is_std,
    qe_old.result_type,
    qe_old.result_value
FROM prc_infodw.instr_output_qe_contents_old qe_old
INNER JOIN prc_infodw.instr_output io
    ON io.id = qe_old.instr_output_id
INNER JOIN
(
    SELECT
        qf.result_stage::TEXT AS result_stage,
        SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id,
        qfd.metagroup_id,
        qfd.group_id,
        qfd.sample_id,
        qfp.isotope_label
    FROM stg_infodw.qe_files qf
    INNER JOIN stg_infodw.qe_file_data qfd
        ON qfd.qe_file_id = qf.id
    INNER JOIN stg_infodw.qe_file_peaks qfp
        ON qfp.qe_file_id = qf.id
        AND qfp.metagroup_id = qfd.metagroup_id
        AND qfp.group_id = qfd.group_id
    WHERE
        qf.load_status = 'processed'
        AND qf.result_stage = 'ANALYZED_DATA'
        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
) sq
    ON sq.result_stage = io.result_stage::TEXT
    AND sq.plate_id = io.plate_id
    AND sq.metagroup_id = qe_old.metagroup_id
    AND sq.group_id = qe_old.group_id
    AND sq.sample_id = qe_old.sample;


CREATE OR REPLACE VIEW infodw.qe_curated_data_v
AS SELECT
    ions.id AS qe_csv_id,
    ios.id AS std_qe_csv_id,
    ions.experiment_name::VARCHAR(1000) AS exp_name,
    ioqcns.metagroup_id::INT AS metagroupid,
    ioqcns.compound::VARCHAR(1000) AS metabolite,
    ioqcs.metagroup_id::INT AS std_metagroupid,
    ioqcns.sample::VARCHAR(1000) AS sample_id,
    ions.plate_id || '_' || ioqcns.row_letter || ioqcns.column_number AS on_plate_id,
    ioqcns.result_value AS intensity,
    ioqcs.sample::VARCHAR(1000) AS std_sample_id,
    ios.plate_id || '_' || ioqcs.row_letter || ioqcs.column_number AS std_on_plate_id,
    ioqcs.result_value AS std_intensity
FROM prc_infodw.instr_output ios
INNER JOIN prc_infodw.instr_output_qe_contents ioqcs
    ON ioqcs.instr_output_id = ios.id
INNER JOIN prc_infodw.instr_output_qe_contents ioqcns
    ON ioqcns.sample = ioqcs.sample
INNER JOIN prc_infodw.instr_output ions
    ON ions.id = ioqcns.instr_output_id
WHERE
    ios.instrument = 'QE'
    AND ios.result_stage = 'ANALYZED_DATA'
    AND ioqcs.is_std
    AND NOT ioqcns.is_std
    AND ios.experiment_id = ions.experiment_id
ORDER BY
    ions.id,
    ioqcns.metagroup_id,
    ioqcns.compound,
    ioqcns.sample;


CREATE OR REPLACE VIEW infodw.qe_non_standard_data_v
AS SELECT
    io.id AS qe_csv_id,
    io.experiment_name::VARCHAR(1000) AS exp_name,
    ioqc.metagroup_id::INT AS metagroupid,
    ioqc.group_id::INT AS groupid,
    ioqc.isotope_label::VARCHAR(1000) AS isotopelabel,
    ioqc.compound::VARCHAR(1000) AS compound,
    0::INT AS is_std,
    ioqc.sample::VARCHAR(1000) AS sample_id,
    io.plate_id || '_' || ioqc.row_letter || ioqc.column_number AS on_plate_id,
    ioqc.result_value AS value,
    SUM(ioqc.result_value) OVER (PARTITION BY ioqc.compound, ioqc.sample) AS pool_total
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_qe_contents ioqc
    ON ioqc.instr_output_id = io.id
WHERE
    io.instrument = 'QE'
    AND io.result_stage = 'ANALYZED_DATA'
    AND NOT ioqc.is_std;


DROP TABLE prc_infodw.instr_output_qe_contents_old;
