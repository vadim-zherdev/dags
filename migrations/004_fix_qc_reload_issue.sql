CREATE OR REPLACE FUNCTION stg_infodw.qc_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    -- Insert into output table
    INSERT INTO prc_infodw.qc_output
    (
        file_path,
        file_name,
        on_plate_id,
        experiment_name,
        short_experiment_name,
        sample,
        cell_type,
        num_centroids,
        summed_tic,
        signal_to_noise,
        acq_date,
        mz_min,
        mz_max,
        mode,
        fiamat_tic,
        replicate,
        dotd_acq_date
    )
    SELECT
        SUBSTRING(qf.src_filename, '(.+)/[\w\d_\-\.]+$') AS file_path,
        SUBSTRING(qf.src_filename, '.+/([\w\d_\-\.]+)$') AS file_name,
        qf.plate_id || '_' || qf.row_letter || qf.column_number AS on_plate_id,
        qf.experiment_name,
        SUBSTRING(qf.experiment_name, 'Bio-\d+') AS short_experiment_name,
        qf.sample,
        qf.cell_type,
        qfc.num_centroids,
        qfc.summed_tic,
        qfc.signal_to_noise,
        qfc.acq_date,
        qfc.mz_min,
        qfc.mz_max,
        qfc.mode,
        qfc.fiamat_tic,
        LEFT(qf.replicate::VARCHAR, 1) AS replicate,
        qf.dotd_acq_date
    FROM stg_infodw.qc_files qf
    INNER JOIN stg_infodw.qc_file_contents qfc
        ON qfc.qc_file_id = qf.id
    WHERE
        qf.load_status = 'loaded';

    -- Insert into arrays table
    INSERT INTO prc_infodw.qc_output_arrays
    (
        qc_output_id,
        normalized_scaled_vector,
        nz_array,
        spectrum_ab_array,
        spectrum_mz_array
    )
    SELECT
        qo.id,
        qfc.normalized_scaled_vector,
        qfc.nz_array,
        qfc.spectrum_ab_array,
        qfc.spectrum_mz_array
    FROM stg_infodw.qc_files qf
    INNER JOIN stg_infodw.qc_file_contents qfc
        ON qfc.qc_file_id = qf.id
    INNER JOIN prc_infodw.qc_output qo
        ON qo.file_path || '/' || qo.file_name = qf.src_filename
    WHERE
        qf.load_status = 'loaded'
    ORDER BY
        qo.id;

    -- Insert audit records
    INSERT INTO stg_infodw.qc_file_audit
    (
        qc_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qc_files qf
    WHERE
        qf.load_status = 'loaded';

    -- Update statuses
    UPDATE stg_infodw.qc_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qc_cleanup_file
(
    src_filename VARCHAR(1024)
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _src_filename ALIAS FOR src_filename;
BEGIN

    -- Clean processing tables
    DELETE FROM prc_infodw.qc_output_arrays AS qoa
    USING prc_infodw.qc_output qo
    WHERE
        qo.id = qoa.qc_output_id
        AND qo.file_path || '/' || qo.file_name = _src_filename;

    DELETE FROM prc_infodw.qc_output AS qo
    WHERE
        qo.file_path || '/' || qo.file_name = _src_filename;

    -- Clean staging contents table
    DELETE FROM stg_infodw.qc_file_contents AS qfc
    USING stg_infodw.qc_files qf
    WHERE
        qf.id = qfc.qc_file_id
        AND qf.src_filename = _src_filename;

    -- Insert audit record
    INSERT INTO stg_infodw.qc_file_audit
    (
        qc_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Cleanup for new file version' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qc_files qf
    WHERE
        qf.src_filename = _src_filename;

    -- Update statuse
    UPDATE stg_infodw.qc_files AS qf
    SET
        load_status = 'started'
    WHERE
        qf.src_filename = _src_filename;

END;
$BODY$;


ALTER TABLE stg_infodw.qc_files ADD COLUMN data_source stg_infodw.data_source_enum NULL;
UPDATE stg_infodw.qc_files SET data_source = 'biobright';
ALTER TABLE stg_infodw.qc_files ALTER COLUMN data_source SET NOT NULL;


DELETE FROM prc_infodw.qc_output_arrays;
DELETE FROM prc_infodw.qc_output;
DELETE FROM stg_infodw.qc_file_audit;
DELETE FROM stg_infodw.qc_file_contents;
DELETE FROM stg_infodw.qc_files;
