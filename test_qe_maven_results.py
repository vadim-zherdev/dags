from pandas import DataFrame
from unittest import TestCase
from unittest.mock import patch

from qe_maven_results_dag import (
    INSTRU_NAME, parse_analyzed_files, merge_output, SUCCESS_STATUS, nan)
from extensions.mocks import MockDSAPIHelper

MOCK_EXP_LIST = [(100500, 'testExp', '/something/Bio-666_Test/INSTR01',)]


class MockTask:
    def __init__(self, content=None):
        self.content = content or {}

    def xcom_pull(self, task_ids=None, key=None):
        return self.content[task_ids][key or 'None']

    def xcom_push(self, key=None, value=None):
        if key and value:
            self.content[key] = value


class MockPostgresHook:
    def __init__(self, **kwargs):
        pass

    def get_first(self, *args, **kwargs):
        return 'record from db'

    def get_records(self, *args, **kwargs):
        return MOCK_EXP_LIST

    def insert_rows(self, *args, **kwargs):
        pass

    def run(self, *args, **kwargs):
        pass


class MockEmptyPostgresHook(MockPostgresHook):
    def get_first(self, *args, **kwargs):
        pass

    def get_records(self, *args, **kwargs):
        return []


@patch('qe_maven_results_dag.get_ds_helper')
class ParseResultsTestCase(TestCase):
    def setUp(self):
        self.good_ds_response = [{
            'id': 13,
            'attributes': {
                'client_path': (
                    f't:\\\\something\\\\Bio-666\\\\{INSTRU_NAME}\\\\else'),
            },
        }], [{
            'id': 13,
            'attributes': {
                's3_link': 'somefakelink',
            },
        }]
        self.bad_ds_response = [{
            'id': 33,
            'attributes': {
                'client_path': 'blah\\\\blah',
            },
        }], None
        results_file_index_labels = [
            'label', 'metaGroupId', 'groupId', 'goodPeakCount', 'medMz',
            'medRt', 'maxQuality', 'isotopeLabel', 'compound', 'compoundId',
            'formula', 'expectedRtDiff', 'ppmDiff', 'parent']
        results_file_good_data_labels = ['P-123_A1_tst', 'P-456_C3']
        results_file_bad_data_labels = ['Pozor', 'P-ABC_wrong']
        results_file_data = [
            ('tstlbl', 12, 13, 14, 15, 16, 17, 'istplbl', 'cmpnd', 18, 'frml',
                'rtdff', 'pmdff', 'prnt', 18, 19),
            ('tstlbl2', 19, 20, 21, 22, 23, 24, 'istplbl2', 'cmpnd2', 18, '',
                'rtdff2', 'pmdff2', 'prnt2', 25, 26),
            ('tstlbl3', 27, 28, 29, 30, 31, 32, 'istplbl3', 'cmpnd3', 33, None,
                'rtdff3', 'pmdff3', 'prnt3', 34, 35),
            ('tstlbl4', 36, 37, 38, 39, 40, 41, 'istplbl4', 'cmpnd4', 42, nan,
                'rtdff4', 'pmdff4', 'prnt4', 43, 44),
        ]
        self.good_df = DataFrame.from_records(
            results_file_data,
            columns=results_file_index_labels + results_file_good_data_labels)
        self.bad_df = DataFrame.from_records(
            results_file_data,
            columns=results_file_index_labels + results_file_bad_data_labels)

    @patch('qe_maven_results_dag.TransactPostgresHook', new=MockPostgresHook)
    @patch('qe_maven_results_dag.load_dataframe')
    @patch('qe_maven_results_dag.read_csv')
    def test_file_parsed(self, mk_csv, mk_load_df, mk_ds_conn):
        mk_ds_conn.return_value = MockDSAPIHelper(
            mock_data=self.good_ds_response, mock_file_content=b'file')
        mk_csv.return_value = self.good_df
        with self.assertLogs(level='INFO') as logs:
            result = parse_analyzed_files(task_instance=MockTask())
        self.assertIn('Success for', logs.records[1].getMessage())
        self.assertTrue(mk_csv.called)
        self.assertEqual(mk_load_df.call_count, 2)
        df = mk_load_df.call_args_list[0][0][0]
        self.assertIn('peak_label', df.columns)
        self.assertIn('good_peak_count', df.columns)
        self.assertIn('is_std', df.columns)
        self.assertNotIn('P-123_A1_tst', df.columns)
        self.assertEqual(df['peak_label'][0], 'tstlbl')
        self.assertEqual(df['is_std'][0], False)
        self.assertEqual(df['is_std'][1], True)
        self.assertEqual(df['is_std'][2], True)
        df = mk_load_df.call_args_list[1][0][0]
        self.assertNotIn('peak_label', df.columns)
        self.assertIn('row_letter', df.columns)
        self.assertIn('column_number', df.columns)
        self.assertIn('result_value', df.columns)
        self.assertEqual(df['result_value'].count(), 8)
        self.assertEqual(df['result_value'][0], 18)
        self.assertEqual(df['result_value'][1], 25)
        self.assertEqual(df['result_value'][2], 34)
        self.assertEqual(df['result_value'][3], 43)
        self.assertEqual(df['result_value'][4], 19)
        self.assertEqual(df['result_value'][5], 26)
        self.assertEqual(df['result_value'][6], 35)
        self.assertEqual(df['result_value'][7], 44)
        self.assertEqual(df['column_number'][0], '1')
        self.assertEqual(df['column_number'][1], '1')
        self.assertEqual(df['column_number'][2], '1')
        self.assertEqual(df['column_number'][3], '1')
        self.assertEqual(df['column_number'][4], '3')
        self.assertEqual(df['column_number'][5], '3')
        self.assertEqual(df['column_number'][6], '3')
        self.assertEqual(df['column_number'][7], '3')
        self.assertEqual(df['row_letter'][0], 'A')
        self.assertEqual(df['row_letter'][1], 'A')
        self.assertEqual(df['row_letter'][2], 'A')
        self.assertEqual(df['row_letter'][3], 'A')
        self.assertEqual(df['row_letter'][4], 'C')
        self.assertEqual(df['row_letter'][5], 'C')
        self.assertEqual(df['row_letter'][6], 'C')
        self.assertEqual(df['row_letter'][7], 'C')
        self.assertTrue(mk_load_df.call_args_list[0][1]['reorder_columns'])
        self.assertTrue(mk_load_df.call_args_list[1][1]['reorder_columns'])
        self.assertEqual('process_qe_analyzed_data', result)

        mk_csv.return_value = self.bad_df
        with self.assertLogs(level='INFO') as logs:
            result = parse_analyzed_files(task_instance=MockTask())
        self.assertIn('Success for', logs.records[1].getMessage())
        self.assertEqual(mk_csv.call_count, 2)
        self.assertEqual(mk_load_df.call_count, 4)
        self.assertEqual('process_qe_analyzed_data', result)

    @patch('qe_maven_results_dag.TransactPostgresHook',
           new=MockEmptyPostgresHook)
    def test_no_files_to_analyze(self, mk_postgres):
        with self.assertLogs(level='DEBUG') as logs:
            result = parse_analyzed_files(task_instance=MockTask())
        self.assertIn('0 analyzed files', logs.records[0].getMessage())
        self.assertEqual('finish', result)
        self.assertTrue(mk_postgres.called)


class MergeOutputTestCase(TestCase):
    def test_results_merged(self):
        mock_task = MockTask(content={
            'parse_qe_analyzed_data': {
                'output': {
                    'good_file': SUCCESS_STATUS,
                    'bad_file': 'Error1',
                    'other_file': SUCCESS_STATUS,
                }
            },
            'process_qe_analyzed_data': {
                'None': [
                    ('good_file', SUCCESS_STATUS),
                    ('bad_file', 'Error2'),
                    ('unexpected_file', SUCCESS_STATUS),
                    ('unexpected_bad_file', 'Error3'),
                ]
            }
        })
        result = merge_output(task_instance=mock_task)
        self.assertIn('*:white_check_mark: Success*: `good_file`', result)
        self.assertIn('*:x: Error*: `bad_file` (Error2)', result)
        self.assertIn('*:white_check_mark: Success*: `other_file`', result)
        self.assertIn(
            '*:white_check_mark: Success*: `unexpected_file`', result)
        self.assertIn('*:x: Error*: `unexpected_bad_file` (Error3)', result)
