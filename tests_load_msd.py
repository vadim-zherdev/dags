from io import StringIO
import logging
from unittest import TestCase
from unittest.mock import patch

from load_msd_dag import (
    extract_files,
    new_files_check,
    fill_kits,
    merge_output)


class MockPostgresHook:
    def __init__(self, postgres_conn_id=None, first=None, autocommit=False):
        pass

    def get_first(self, query, *args, **kwargs):
        if query.find('COUNT(*)') > -1:
            return (13,)

    def get_records(self, query, *args, **kwargs):
        if query.find('WHERE plate_id IN %s') > -1:
            return [('P-3',)]
        else:
            return [(1, 100500, 'Super experiment', 'P-123',
                     'somefakefile', 'RAW_DATA')]

    def insert_rows(self, *args, **kwargs):
        pass

    def run(self, *args, **kwargs):
        pass

    def get_sqlalchemy_engine(self):
        pass


class MockPostgresHookEmpty(MockPostgresHook):
    def get_first(self, *args, **kwargs):
        return (0,)


class MockDSAPIHelper:
    def __init__(self, **kwargs):
        pass

    def get_file(self, **kwargs):
        return StringIO(
            'Spots Per Well:\t1\n'
            'Wells Per Col:\t1\n'
            'Wells Per Row:\t1\n'
            '==========Data==========\n'
            '\t1\n'
            'A\t25\n'
        )


class MockTaskInstance:
    def xcom_pull(self, task_ids=None, key=None):
        if task_ids == 'extract_files':
            if key == 'output':
                return {
                    'file1': 'OK',
                    'file2': 'OK',
                    'file3': 'Parse error'
                }
            elif key == 'plate_spot_number':
                return {
                    'P-1': 10,
                    'P-2': 1,
                    'P-3': 10
                }
        elif task_ids == 'process_files':
            return [('file2', 'Calculation error',)]
        else:
            return None

    def xcom_push(self, key, value):
        pass


@patch('load_msd_dag.get_ds_helper', new=MockDSAPIHelper)
class DAGTestCase(TestCase):
    def setUp(self):
        logging.basicConfig(level='DEBUG')

    @patch('load_msd_dag.TransactPostgresHook', new=MockPostgresHook)
    def test_extract_files(self):
        with self.assertLogs(level='DEBUG') as logs:
            extract_files(ti=MockTaskInstance())
        self.assertEqual(len(logs.records), 6)
        self.assertIn(
            '1 files found for processing',
            logs.records[0].getMessage())
        self.assertIn(
            'Started loading file with id=1',
            logs.records[1].getMessage())
        self.assertEqual(
            'Successfully parsed file somefakefile',
            logs.records[2].getMessage())
        self.assertEqual(
            'Successfully filled metadata for file id=1',
            logs.records[3].getMessage())
        self.assertEqual(
            'Saving to database skipped',
            logs.records[4].getMessage())
        self.assertEqual(
            'Finished loading file with id=1',
            logs.records[5].getMessage())

    @patch('load_msd_dag.PostgresHook', new=MockPostgresHook)
    def test_new_files_check_positive(self):
        with self.assertLogs(level='INFO') as logs:
            self.assertEqual(new_files_check(), 'create_files')
        self.assertEqual(
            logs.records[0].getMessage(),
            'Found 13 new files. Moving to load step.')

    @patch('load_msd_dag.PostgresHook', new=MockPostgresHookEmpty)
    def test_new_files_check_negative(self):
        with self.assertLogs(level='INFO') as logs:
            self.assertEqual(new_files_check(), 'finish')
        self.assertEqual(
            logs.records[0].getMessage(),
            'No new files found. Moving to finish.')

    @patch('load_msd_dag.PostgresHook', new=MockPostgresHook)
    def test_fill_kits(self):
        with self.assertLogs(level='INFO') as logs:
            fill_kits(ti=MockTaskInstance())
        self.assertEqual(
            logs.records[0].getMessage(),
            'Writing MSD kits to database. Count to write: 2.')

    def test_merge_output(self):
        self.assertEqual(merge_output(ti=MockTaskInstance()),
                         "*OK*: `file1`\n"
                         "*Error*: `file2` (Calculation error)\n"
                         "*Error*: `file3` (Parse error)")
