# DAG to load and process MSD files
Described in [Wiki](http://gitlab00.rheosrx.lan/rheos-informatics/airflow-dags/wikis/MSD-DAG)

# QE pipeline
## DAG to convert QE raw files into mzXML format
mzxml_convert_dag.py

Described in [WIKI](http://gitlab00.rheosrx.lan/rheos-informatics/airflow-dags/wikis/QE-DAG).
## DAG to parse Maven results
qe_maven_results_dag.py

# FIA
Described in [WIKI](http://gitlab00.rheosrx.lan/rheos-informatics/airflow-dags/wikis/qe-maven-results-parsing-dag)
## DAG to parse FIATOF GMET xlsx files
fiatof_xlsx_dag.py

Described in [WIKI](http://gitlab00.rheosrx.lan/rheos-informatics/airflow-dags/wikis/fiatof-xlsx-parsing-dag)
#Sapio
## DAG to import Sapio data
load_sapio_dag.py

Described in [WIKI](http://gitlab00.rheosrx.lan/rheos-informatics/airflow-dags/wikis/sapio-import-dag)
