CREATE TYPE stg_infodw.entry_status_enum AS ENUM
    ('created', 'started', 'loaded', 'processed', 'failed', 'invalidated');

CREATE TYPE stg_infodw.result_stage_enum AS ENUM
    ('RAW_DATA', 'ANALYZED_DATA', 'METADATA', 'MAVEN_DATA', 'JSON');

CREATE TYPE stg_infodw.data_source_enum AS ENUM
    ('biobright', 'egnyte', 'aws-s3', 'migration', 'manual');

CREATE TYPE stg_infodw.rep_enum AS ENUM
    ('a', 'b', 'unknown');

CREATE TYPE stg_infodw.pipeline_enum AS ENUM
    ('MSD', 'GUAVA', 'FORTESSA', 'FIATOF', 'MZXML', 'QE', 'QC');

CREATE TYPE prc_infodw.result_stage_enum AS ENUM
    ('RAW_DATA', 'ANALYZED_DATA', 'METADATA', 'MAVEN_DATA', 'JSON');

CREATE TYPE prc_infodw.instrument_enum AS ENUM
    ('MSD', 'GUAVA', 'FORTESSA', 'AMAXA', 'BRAVO', 'MOXIGO', 'FIATOF', 'QE');

CREATE TYPE prc_infodw.result_type_enum AS ENUM
    ('ORIGINAL', 'NTC_AVERAGE', 'INTRAPLATE_NORMALIZED', 'MSD/GUAVA', 'GUAVA_NORMALIZED');
