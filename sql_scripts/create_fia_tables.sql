CREATE TABLE IF NOT EXISTS stg_infodw.fia_processingout_audit
(
    processingout_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_fia_processingout_audit_processingout FOREIGN KEY (processingout_id)
        REFERENCES fia.processingout (processingout_id)
);

CREATE INDEX IF NOT EXISTS idx_fia_processingout_audit_processingout_id
    ON stg_infodw.fia_processingout_audit (processingout_id);
