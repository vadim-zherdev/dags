CREATE OR REPLACE VIEW stg_infodw.guava_missing_files_v
AS SELECT
    exp.recordid AS experiment_id,
    exp.datarecordname AS experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    sq.plate_id::VARCHAR(32) AS plate_id,
    res.dir || '/' || res.files AS filepath,
    sq.result_stage,
    TO_TIMESTAMP(res.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at
FROM
(
    SELECT
        prf.plateid AS plate_id,
        CASE
            WHEN UPPER(prf.dir) LIKE '%RAW_DATA%' THEN 'RAW_DATA'
            WHEN UPPER(prf.dir) LIKE '%ANALYZED_DATA%' THEN 'ANALYZED_DATA'
            ELSE NULL
        END::stg_infodw.result_stage_enum AS result_stage,
        MAX(prf.recordid) AS prf_id
    FROM stg_infodw.sapio_plateresultfile prf
    WHERE
        prf.instrumentname LIKE 'GUAVA%'
        AND prf.dir LIKE '%Lab Data%'
        AND UPPER(prf.files) LIKE '%.CSV'
    GROUP BY
        plate_id,
        result_stage
) sq
INNER JOIN stg_infodw.sapio_plateresultfile res
    ON res.recordid = sq.prf_id
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = res.experimentrecordid
WHERE
    NULLIF(LTRIM(res.files), '') IS NOT NULL
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.sapio_plateresultfile prf
        WHERE
            prf.instrumentname LIKE 'GUAVA%'
            AND prf.dir || '/' || prf.files = res.dir || '/' || res.files
            AND prf.recordid > res.recordid
    )
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.guava_files gf
        WHERE
            gf.plate_id = sq.plate_id
            AND gf.result_stage = sq.result_stage
    )
ORDER BY
    exp_created_at ASC;
