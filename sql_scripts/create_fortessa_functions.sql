CREATE OR REPLACE FUNCTION stg_infodw.fortessa_create_files
(
    src stg_infodw.data_source_enum
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    CREATE TEMP TABLE tmp_fortessa_files ON COMMIT DROP AS
    SELECT
        plate_id,
        result_stage
    FROM stg_infodw.fortessa_missing_files_v;

    INSERT INTO stg_infodw.fortessa_files
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        plate_id,
        src_filename,
        result_stage,
        plate_created_at,
        load_status,
        data_source
    )
    SELECT
        fmf.experiment_id,
        fmf.experiment_name,
        fmf.exp_created_at,
        fmf.plate_id,
        fmf.filepath,
        fmf.result_stage,
        fmf.plate_created_at,
        'created' AS load_status,
        src AS data_source
    FROM stg_infodw.fortessa_missing_files_v fmf
    INNER JOIN tmp_fortessa_files tmp
        ON tmp.plate_id = fmf.plate_id
        AND tmp.result_stage = fmf.result_stage;

    INSERT INTO stg_infodw.fortessa_file_audit
    (
        fortessa_file_id,
        action_name,
        action_result
    )
    SELECT
        ff.id,
        'Insert entity record',
        'Success'
    FROM stg_infodw.fortessa_files ff
    INNER JOIN tmp_fortessa_files tmp
        ON tmp.plate_id = ff.plate_id
        AND tmp.result_stage = ff.result_stage;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.fortessa_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_contents AS ioc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.fortessa_files ff
            ON ff.plate_id = io.plate_id
            AND ff.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioc.instr_output_id = io.id
            AND io.instrument = 'FORTESSA'
            AND ff.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = io.plate_id
            AND ff.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'FORTESSA'
            AND ff.load_status = 'invalidated';

        DELETE FROM stg_infodw.fortessa_file_contents AS ffc
        USING stg_infodw.fortessa_files ff
        WHERE
            ffc.fortessa_file_id = ff.id
            AND ff.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.fortessa_file_audit
        (
            fortessa_file_id,
            action_name,
            action_result
        )
        SELECT
            ff.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status = 'invalidated'
            AND ff.result_stage = 'ANALYZED_DATA';

        -- Write audit records for files
        INSERT INTO stg_infodw.fortessa_file_audit
        (
            fortessa_file_id,
            action_name,
            action_result
        )
        SELECT
            ff.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status IN ('created', 'invalidated')
            AND ff.result_stage = 'ANALYZED_DATA';

        RETURN QUERY
        UPDATE stg_infodw.fortessa_files uff
        SET
            load_status = 'started'
        WHERE
            uff.load_status IN ('created', 'invalidated')
            AND uff.result_stage = 'ANALYZED_DATA'
        RETURNING
            uff.id,
            uff.experiment_id,
            uff.experiment_name,
            uff.plate_id,
            uff.src_filename,
            uff.result_stage,
            uff.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.fortessa_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Collect metadata for each well in temporary table
    CREATE TEMP TABLE tmp_fortessa_intraplate_calc ON COMMIT DROP AS
    SELECT
        pmd.plate_id,
        pmd.plate_row,
        pmd.plate_column,
        pmd.sample,
        pmd.sample_type,
        pmd.stimulation
    FROM prc_infodw.instr_plate_metadata_mv pmd
    WHERE
        EXISTS
        (
            SELECT 1
            FROM stg_infodw.fortessa_files ff
            WHERE
                ff.plate_id = pmd.plate_id
                AND ff.load_status = 'loaded'
                AND ff.result_stage = 'ANALYZED_DATA'
        );

    CREATE UNIQUE INDEX idx_tmp_fortessa_intraplate_calc_plate_id_plate_row_plate_column
        ON tmp_fortessa_intraplate_calc (plate_id ASC, plate_row ASC, plate_column ASC);

    -- Warn about plates without controls
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.load_status = 'loaded'
            AND ff.result_stage = 'ANALYZED_DATA'
            AND NOT EXISTS
            (
                SELECT 1
                FROM tmp_fortessa_intraplate_calc tmp
                WHERE
                    tmp.plate_id = ff.plate_id
                    AND tmp.sample_type = 'control'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                ff.id,
                ff.plate_id,
                ff.src_filename
            FROM stg_infodw.fortessa_files ff
            WHERE
                ff.load_status = 'loaded'
                AND ff.result_stage = 'ANALYZED_DATA'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM tmp_fortessa_intraplate_calc tmp
                    WHERE
                        tmp.plate_id = ff.plate_id
                        AND tmp.sample_type = 'control'
                )
        LOOP

            INSERT INTO stg_infodw.fortessa_file_audit
            (
                fortessa_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: no controls found for this plate'
            );

            RAISE WARNING 'No controls found for plate %.', _rec.plate_id;

            filename := _rec.src_filename;
            output := 'No controls found for this plate';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Fill new field names
    INSERT INTO stg_infodw.fortessa_fieldname_lookup
    (
        fieldname,
        normalized_fieldname
    )
    SELECT
        ffc.result_field,
        CASE
            WHEN ffc.result_field ILIKE '%Ki67%' THEN 'Ki67'
            WHEN ffc.result_field ILIKE '%Zap70%' THEN 'Zap70'
            WHEN ffc.result_field ILIKE '%CD4%' THEN 'CD4'
            ELSE 'Viable'
        END
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.fortessa_file_contents ffc
        ON ffc.fortessa_file_id = ff.id
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
        AND ffc.result_field <> 'Count'
        AND ffc.result_field NOT IN
        (
            SELECT
                fieldname
            FROM stg_infodw.fortessa_fieldname_lookup
        )
    GROUP BY
        ffc.result_field;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        ff.experiment_id,
        ff.experiment_name,
        ff.exp_created_at,
        'FORTESSA' AS instrument,
        ff.plate_id,
        nbi.egnyte_folder_path AS output_path,
        ff.result_stage::TEXT::prc_infodw.result_stage_enum,
        ff.plate_created_at
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = ff.experiment_id
        AND nbi.instrument_used LIKE 'FORTESSA%'
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        sample,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        SUBSTRING(REPLACE(ffc.sample, ' ', '_'), '_([A-Z])\d{2}[_\.]') AS row_letter,
        SUBSTRING(REPLACE(ffc.sample, ' ', '_'), '_[A-Z](\d{2})[_\.]')::SMALLINT AS column_number,
        ffc.sample,
        ffc.result_field,
        ffl.normalized_fieldname,
        'ORIGINAL' AS result_type,
        ffc.result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN stg_infodw.fortessa_file_contents ffc
        ON ffc.fortessa_file_id = ff.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    LEFT JOIN stg_infodw.fortessa_fieldname_lookup ffl
        ON ffl.fieldname = ffc.result_field
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
    ORDER BY
        ff.id,
        ffc.sample,
        ffc.result_field;

    -- Insert NTC averages
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        ioc.result_field,
        ioc.result_normalized_field,
        'NTC_AVERAGE' AS result_type,
        AVG(ioc.result_value) AS result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    INNER JOIN prc_infodw.instr_output_contents ioc
        ON ioc.instr_output_id = io.id
        AND ioc.result_type = 'ORIGINAL'
    INNER JOIN tmp_fortessa_intraplate_calc tmp
        ON tmp.plate_id = io.plate_id
        AND tmp.plate_row = ioc.row_letter
        AND tmp.plate_column = ioc.column_number
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
        AND tmp.sample_type = 'control'
        AND tmp.stimulation <> 'Rested'
        AND ioc.sample NOT LIKE 'Compensation%'
        AND ioc.sample NOT IN ('Mean', 'SD')
    GROUP BY
        io.id,
        ioc.result_field,
        ioc.result_normalized_field
    ORDER BY
        io.id,
        ioc.result_field;

    -- Insert intraplate calculations
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        sample,
        result_field,
        result_normalized_field,
        result_type,
        result_value
    )
    SELECT
        io.id,
        ioco.row_letter,
        ioco.column_number,
        ioco.sample,
        ioco.result_field,
        ioco.result_normalized_field,
        'INTRAPLATE_NORMALIZED' AS result_type,
        100::DOUBLE PRECISION * ioco.result_value / ioca.result_value AS result_value
    FROM stg_infodw.fortessa_files ff
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = ff.plate_id
        AND io.result_stage::TEXT = ff.result_stage::TEXT
        AND io.instrument = 'FORTESSA'
    INNER JOIN prc_infodw.instr_output_contents ioco
        ON ioco.instr_output_id = io.id
        AND ioco.result_type = 'ORIGINAL'
    INNER JOIN prc_infodw.instr_output_contents ioca
        ON ioca.instr_output_id = io.id
        AND ioca.result_type = 'NTC_AVERAGE'
        AND ioca.result_field = ioco.result_field
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA'
        AND ioco.sample NOT LIKE 'Compensation%'
        AND ioco.sample NOT IN ('Mean', 'SD')
    ORDER BY
        io.id,
        ioco.sample,
        ioco.result_field;

    -- Insert audit records
    INSERT INTO stg_infodw.fortessa_file_audit
    (
        fortessa_file_id,
        action_name,
        action_result
    )
    SELECT
        ff.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.fortessa_files ff
    WHERE
        ff.load_status = 'loaded'
        AND ff.result_stage = 'ANALYZED_DATA';

    -- Update statuses
    UPDATE stg_infodw.fortessa_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded'
        AND result_stage = 'ANALYZED_DATA';

END;
$BODY$;
