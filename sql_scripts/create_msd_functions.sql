CREATE OR REPLACE FUNCTION stg_infodw.msd_create_files
(
    src stg_infodw.data_source_enum
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    CREATE TEMP TABLE tmp_msd_files ON COMMIT DROP AS
    SELECT
        plate_id,
        result_stage
    FROM stg_infodw.msd_missing_files_v;

    INSERT INTO stg_infodw.msd_files
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        plate_id,
        src_filename,
        result_stage,
        plate_created_at,
        load_status,
        data_source
    )
    SELECT
        msf.experiment_id,
        msf.experiment_name,
        msf.exp_created_at,
        msf.plate_id,
        msf.filepath,
        msf.result_stage,
        msf.plate_created_at,
        'created' AS load_status,
        src AS data_source
    FROM stg_infodw.msd_missing_files_v msf
    INNER JOIN tmp_msd_files tmp
        ON tmp.plate_id = msf.plate_id
        AND tmp.result_stage = msf.result_stage;

    INSERT INTO stg_infodw.msd_file_audit
    (
        msd_file_id,
        action_name,
        action_result
    )
    SELECT
        mf.id,
        'Insert entity record',
        'Success'
    FROM stg_infodw.msd_files mf
    INNER JOIN tmp_msd_files tmp
        ON tmp.plate_id = mf.plate_id
        AND tmp.result_stage = mf.result_stage;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.msd_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_contents AS ioc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.msd_files mf
            ON mf.plate_id = io.plate_id
            AND mf.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioc.instr_output_id = io.id
            AND io.instrument = 'MSD'
            AND mf.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.msd_files mf
        WHERE
            mf.plate_id = io.plate_id
            AND mf.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'MSD'
            AND mf.load_status = 'invalidated';

        DELETE FROM stg_infodw.msd_file_contents AS mfc
        USING stg_infodw.msd_files mf
        WHERE
            mfc.msd_file_id = mf.id
            AND mf.load_status = 'invalidated';

        DELETE FROM stg_infodw.msd_file_metadata mfm
        USING stg_infodw.msd_files mf
        WHERE
            mfm.msd_file_id = mf.id
            AND mf.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.msd_file_audit
        (
            msd_file_id,
            action_name,
            action_result
        )
        SELECT
            mf.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status = 'invalidated';

        -- Write audit records for files
        INSERT INTO stg_infodw.msd_file_audit
        (
            msd_file_id,
            action_name,
            action_result
        )
        SELECT
            mf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status IN ('created', 'invalidated');

        RETURN QUERY
        UPDATE stg_infodw.msd_files umf
        SET
            load_status = 'started'
        WHERE
            umf.load_status IN ('created', 'invalidated')
        RETURNING
            umf.id,
            umf.experiment_id,
            umf.experiment_name,
            umf.plate_id,
            umf.src_filename,
            umf.result_stage,
            umf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.msd_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Collect metadata for each well in temporary table
    CREATE TEMP TABLE tmp_msd_intraplate_calc ON COMMIT DROP AS
    SELECT
        pmd.plate_id,
        pmd.plate_row,
        pmd.plate_column,
        pmd.sample,
        pmd.sample_type,
        pmd.stimulation
    FROM prc_infodw.instr_plate_metadata_mv pmd
    WHERE
        EXISTS
        (
            SELECT 1
            FROM stg_infodw.msd_files mf
            WHERE
                mf.plate_id = pmd.plate_id
                AND mf.load_status = 'loaded'
        );

    CREATE UNIQUE INDEX idx_tmp_msd_intraplate_calc_plate_id_plate_row_plate_column
        ON tmp_msd_intraplate_calc (plate_id ASC, plate_row ASC, plate_column ASC);

    -- Warn about plates without controls
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.msd_files mf
        WHERE
            mf.load_status = 'loaded'
            AND NOT EXISTS
            (
                SELECT 1
                FROM tmp_msd_intraplate_calc tmp
                WHERE
                    tmp.plate_id = mf.plate_id
                    AND tmp.sample_type = 'control'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                mf.id,
                mf.plate_id,
                mf.src_filename
            FROM stg_infodw.msd_files mf
            WHERE
                mf.load_status = 'loaded'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM tmp_msd_intraplate_calc tmp
                    WHERE
                        tmp.plate_id = mf.plate_id
                        AND tmp.sample_type = 'control'
                )
        LOOP

            INSERT INTO stg_infodw.msd_file_audit
            (
                msd_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: no controls found for this plate'
            );

            RAISE WARNING 'No controls found for plate %.', _rec.plate_id;

            filename := _rec.src_filename;
            output := 'No controls found for this plate';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        mf.experiment_id,
        mf.experiment_name,
        mf.exp_created_at,
        'MSD' AS instrument,
        mf.plate_id,
        nbi.egnyte_folder_path AS output_path,
        mf.result_stage::TEXT::prc_infodw.result_stage_enum,
        mf.plate_created_at
    FROM stg_infodw.msd_files mf
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = mf.experiment_id
        AND nbi.instrument_used LIKE 'MSD%'
    WHERE
        mf.load_status = 'loaded';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        spot_number,
        result_type,
        result_value
    )
    SELECT
        io.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number,
        'ORIGINAL' AS result_type,
        mfc.result_value
    FROM stg_infodw.msd_files mf
    INNER JOIN stg_infodw.msd_file_contents mfc
        ON mfc.msd_file_id = mf.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = mf.plate_id
        AND io.result_stage::TEXT = mf.result_stage::TEXT
        AND io.instrument = 'MSD'
    WHERE
        mf.load_status = 'loaded'
    ORDER BY
        mf.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number;

    -- Insert intraplate calculations
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        spot_number,
        result_type,
        result_value
    )
    SELECT
        io.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number,
        'INTRAPLATE_NORMALIZED' AS result_type,
        CASE
            WHEN tmp.stimulation = 'IC+ Re-Activated' AND sq.avg_value > 0
                THEN 100::DOUBLE PRECISION * mfc.result_value / sq.avg_value
            ELSE 0
        END AS normalized_value
    FROM
    (
        SELECT
            agg.msd_file_id,
            agg.spot_number,
            agg.stimulation,
            AVG(agg.avg_value) AS avg_value
        FROM
        (
            SELECT
                mfs.id AS msd_file_id,
                mfcs.spot_number,
                tmps.sample,
                tmps.stimulation,
                AVG(mfcs.result_value) AS avg_value
            FROM tmp_msd_intraplate_calc tmps
            INNER JOIN stg_infodw.msd_files mfs
                ON mfs.plate_id = tmps.plate_id
                AND mfs.load_status = 'loaded'
            INNER JOIN stg_infodw.msd_file_contents mfcs
                ON mfcs.msd_file_id = mfs.id
                AND mfcs.row_letter = tmps.plate_row
                AND mfcs.column_number = tmps.plate_column
            WHERE
                tmps.sample_type = 'control'
            GROUP BY
                mfs.id,
                mfcs.spot_number,
                tmps.sample,
                tmps.stimulation
        ) agg
        GROUP BY
            agg.msd_file_id,
            agg.spot_number,
            agg.stimulation
    ) sq
    INNER JOIN stg_infodw.msd_files mf
        ON mf.id = sq.msd_file_id
    INNER JOIN stg_infodw.msd_file_contents mfc
        ON mfc.msd_file_id = mf.id
        AND mfc.spot_number = sq.spot_number
    INNER JOIN tmp_msd_intraplate_calc tmp
        ON tmp.plate_id = mf.plate_id
        AND tmp.plate_row = mfc.row_letter
        AND tmp.plate_column = mfc.column_number
        AND tmp.stimulation = sq.stimulation
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = mf.plate_id
        AND io.result_stage::TEXT = mf.result_stage::TEXT
        AND io.instrument = 'MSD'
    ORDER BY
        mf.id,
        mfc.row_letter,
        mfc.column_number,
        mfc.spot_number;

    -- Insert audit records
    INSERT INTO stg_infodw.msd_file_audit
    (
        msd_file_id,
        action_name,
        action_result
    )
    SELECT
        mf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.msd_files mf
    WHERE
        mf.load_status = 'loaded';

    -- Update statuses
    UPDATE stg_infodw.msd_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;


CREATE OR REPLACE FUNCTION prc_infodw.msd_refresh_mvs()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    REFRESH MATERIALIZED VIEW infodw.msd_raw_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_normalized_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_ntc_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_pathway_mv;
    REFRESH MATERIALIZED VIEW infodw.msd_plate_results_mv;

    REFRESH MATERIALIZED VIEW infodw.crispr_experiment_mv;
    REFRESH MATERIALIZED VIEW infodw.screening_msd_results_mv;

    REFRESH MATERIALIZED VIEW infodw.crispr_screen_data_all_mv;

END;
$BODY$;
