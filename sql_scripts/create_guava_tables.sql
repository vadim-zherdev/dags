CREATE SEQUENCE IF NOT EXISTS stg_infodw.guava_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.guava_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.guava_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_guava_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_guava_files_src_filename
    ON stg_infodw.guava_files (src_filename);
CREATE UNIQUE INDEX IF NOT EXISTS idx_guava_files_plate_id_result_stage
    ON stg_infodw.guava_files (plate_id, result_stage);

CREATE TABLE IF NOT EXISTS stg_infodw.guava_file_contents
(
    guava_file_id INT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    result_value DOUBLE PRECISION NOT NULL,
    CONSTRAINT pk_guava_file_contents PRIMARY KEY (guava_file_id, row_letter, column_number),
    CONSTRAINT fk_guava_file_contents_guava_files FOREIGN KEY (guava_file_id)
        REFERENCES stg_infodw.guava_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.guava_file_audit
(
    guava_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_guava_file_audit_guava_files FOREIGN KEY (guava_file_id)
        REFERENCES stg_infodw.guava_files (id)
);

CREATE INDEX IF NOT EXISTS idx_guava_file_audit_guava_file_id
    ON stg_infodw.guava_file_audit (guava_file_id);

