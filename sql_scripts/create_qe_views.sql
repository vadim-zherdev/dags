CREATE OR REPLACE VIEW stg_infodw.qe_unprocessed_experiments_v
AS SELECT
    nbi.experiment_id,
    nbi.experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    nbi.egnyte_folder_path,
    qee.status
FROM stg_infodw.sapio_rhe_nb_instru nbi
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = nbi.experiment_id
LEFT JOIN stg_infodw.qe_experiments qee
    ON qee.experiment_id = nbi.experiment_id
WHERE
    nbi.instrument_used LIKE 'QE%'
    AND COALESCE(qee.status::VARCHAR, 'none') IN ('none', 'created', 'failed')
ORDER BY
    exp.datecreated ASC;
