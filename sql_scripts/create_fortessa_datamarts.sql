CREATE OR REPLACE VIEW infodw.fortessa_csvres_v
AS SELECT
    ioc.id AS fortessa_res_id,
    ioc.sample,
    io.id AS fortessa_exp_id,
    ioc.result_field AS fieldname,
    ioc.result_value AS value,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';


CREATE OR REPLACE VIEW infodw.fortessa_well_metadata_v
AS SELECT
    pmd.donor,
    pmd.plate_id,
    pmd.on_plate_id,
    pmd.stimulation,
    pmd.gene,
    CONCAT(pmd.plate_row, LPAD(pmd.plate_column::VARCHAR, 2, '0')) AS well_position,
    io.id AS fortessa_exp_id
FROM prc_infodw.instr_output io
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA';


CREATE OR REPLACE VIEW infodw.fortessa_csvres_merged_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.gene,
    pmd.stimulation,
    pmd.donor
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL'
    AND ioc.sample NOT LIKE 'Compensation%'
    AND ioc.sample NOT IN ('Mean', 'SD');


CREATE OR REPLACE VIEW infodw.fortessa_ntc_v
AS SELECT
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    ioc.result_value AS average_ntc
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'NTC_AVERAGE';


CREATE OR REPLACE VIEW infodw.fortessa_normalized_v
AS SELECT
    ioc.sample AS sample_fcs,
    ioc.result_field AS fieldname,
    io.id AS fortessa_exp_id,
    ioc.result_value AS value,
    ioc.sample,
    CONCAT(ioc.row_letter, LPAD(ioc.column_number::VARCHAR, 2, '0')) AS well_position,
    SUBSTRING(io.experiment_name, 'Bio-\d+') AS exp_id,
    io.plate_id,
    pmd.gene,
    pmd.stimulation,
    pmd.donor,
    ioca.result_value AS average_ntc,
    iocn.result_value AS normalized_value
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_contents ioc
    ON ioc.instr_output_id = io.id
INNER JOIN prc_infodw.instr_output_contents ioca
    ON ioca.instr_output_id = io.id
    AND ioca.result_field = ioc.result_field
    AND ioca.result_type = 'NTC_AVERAGE'
INNER JOIN prc_infodw.instr_output_contents iocn
    ON iocn.instr_output_id = io.id
    AND iocn.result_field = ioc.result_field
    AND iocn.row_letter = ioc.row_letter
    AND iocn.column_number = ioc.column_number
    AND iocn.result_type = 'INTRAPLATE_NORMALIZED'
LEFT JOIN prc_infodw.instr_plate_metadata_mv pmd
    ON pmd.plate_id = io.plate_id
    AND pmd.plate_row = ioc.row_letter
    AND pmd.plate_column = ioc.column_number
WHERE
    io.instrument = 'FORTESSA'
    AND io.result_stage = 'ANALYZED_DATA'
    AND ioc.result_type = 'ORIGINAL';
