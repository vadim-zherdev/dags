CREATE TABLE IF NOT EXISTS stg_infodw.qe_experiments
(
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    src_path VARCHAR(1024) NOT NULL,
    status stg_infodw.entry_status_enum NOT NULL,
    CONSTRAINT pk_qe_experiments PRIMARY KEY (experiment_id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_worklists
(
    experiment_id INT NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    plate_id VARCHAR(32) NULL,
    status stg_infodw.entry_status_enum NOT NULL,
    CONSTRAINT pk_qe_worklists PRIMARY KEY (experiment_id, src_filename)
);

CREATE SEQUENCE IF NOT EXISTS stg_infodw.qe_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.qe_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.qe_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    worklist_filename VARCHAR(1024) NULL,
    plate_id VARCHAR(32) NULL,
    row_letter CHAR(1) NULL,
    column_number SMALLINT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_qe_files PRIMARY KEY (id),
    CONSTRAINT ck_qe_files_worklist CHECK (result_stage <> 'RAW_DATA' OR worklist_filename IS NOT NULL),
    CONSTRAINT fk_qe_files_qe_worklists FOREIGN KEY (experiment_id, worklist_filename)
        REFERENCES stg_infodw.qe_worklists (experiment_id, src_filename)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_qe_files_src_filename
    ON stg_infodw.qe_files (src_filename);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_file_peaks
(
    qe_file_id INT NOT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    peak_label VARCHAR(1024) NULL,
    good_peak_count INT NULL,
    med_mz DOUBLE PRECISION NULL,
    med_rt DOUBLE PRECISION NULL,
    max_quality DOUBLE PRECISION NULL,
    isotope_label VARCHAR(1024) NULL,
    compound VARCHAR(256) NULL,
    compound_id VARCHAR(256) NULL,
    formula VARCHAR(256) NULL,
    expected_rt_diff DOUBLE PRECISION NULL,
    ppm_diff DOUBLE PRECISION NULL,
    parent DOUBLE PRECISION NULL,
    is_std BOOLEAN NOT NULL,
    CONSTRAINT pk_qe_file_peaks PRIMARY KEY (qe_file_id, metagroup_id, group_id),
    CONSTRAINT fk_qe_file_peaks_qe_files FOREIGN KEY (qe_file_id) REFERENCES stg_infodw.qe_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_file_data
(
    qe_file_id INT NOT NULL,
    row_letter CHAR(1) NULL,
    column_number SMALLINT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    sample_id VARCHAR(256) NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_qe_file_data PRIMARY KEY (qe_file_id, metagroup_id, group_id, sample_id),
    CONSTRAINT fk_qe_file_data_qe_files FOREIGN KEY (qe_file_id) REFERENCES stg_infodw.qe_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qe_file_audit
(
    qe_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_qe_file_audit_qe_files FOREIGN KEY (qe_file_id) REFERENCES stg_infodw.qe_files (id)
);

CREATE INDEX IF NOT EXISTS idx_qe_file_audit_qe_file_id
    ON stg_infodw.qe_file_audit (qe_file_id);
