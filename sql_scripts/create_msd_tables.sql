CREATE SEQUENCE IF NOT EXISTS stg_infodw.msd_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.msd_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.msd_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_msd_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_msd_files_src_filename
    ON stg_infodw.msd_files (src_filename);
CREATE UNIQUE INDEX IF NOT EXISTS idx_msd_files_plate_id_result_stage
    ON stg_infodw.msd_files (plate_id, result_stage);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_file_metadata
(
    msd_file_id INT NOT NULL,
    meta_name VARCHAR(256) NOT NULL,
    meta_value TEXT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    CONSTRAINT pk_msd_file_metadata PRIMARY KEY (msd_file_id, meta_name),
    CONSTRAINT fk_msd_file_metadata_msd_files FOREIGN KEY (msd_file_id) REFERENCES stg_infodw.msd_files (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_msd_file_metadata_msd_file_id_meta_name
    ON stg_infodw.msd_file_metadata (msd_file_id, meta_name);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_file_contents
(
    msd_file_id INT NOT NULL,
    spot_number SMALLINT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_msd_file_contents PRIMARY KEY (msd_file_id, spot_number, row_letter, column_number),
    CONSTRAINT fk_msd_file_contents_msd_files FOREIGN KEY (msd_file_id) REFERENCES stg_infodw.msd_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_kit
(
    plate_id VARCHAR(32) NOT NULL,
    spot_number SMALLINT NOT NULL,
    cytokine VARCHAR(256) NOT NULL,
    CONSTRAINT pk_msd_kit PRIMARY KEY (plate_id, spot_number)
);

CREATE TABLE IF NOT EXISTS stg_infodw.msd_file_audit
(
    msd_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_msd_file_audit_msd_files FOREIGN KEY (msd_file_id) REFERENCES stg_infodw.msd_files (id)
);

CREATE INDEX IF NOT EXISTS idx_msd_file_audit_msd_file_id
    ON stg_infodw.msd_file_audit (msd_file_id);
