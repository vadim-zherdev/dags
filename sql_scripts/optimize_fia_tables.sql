/*
 * Improve FIA tables structure
 */

CREATE OR REPLACE FUNCTION create_constraint_if_not_exists (
    c_name text, constraint_sql text
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN
    IF NOT EXISTS (SELECT 1
                   FROM pg_catalog.pg_constraint
                   WHERE conname = c_name) THEN
        EXECUTE constraint_sql;
    END IF;
END;
$BODY$;

BEGIN;

DELETE FROM fia.processingout WHERE processingout_foldername IS NULL;
ALTER TABLE fia.processingout ALTER COLUMN processingout_foldername SET NOT NULL;
DELETE FROM fia.processingout WHERE json_filename IS NULL;
ALTER TABLE fia.processingout ALTER COLUMN json_filename SET NOT NULL;

DELETE FROM fia."json" WHERE "label" IS NULL;
ALTER TABLE fia."json" ALTER COLUMN "label" SET NOT NULL;
DELETE FROM fia."json" j WHERE worklist_id IS NULL;
ALTER TABLE fia."json" ALTER COLUMN worklist_id SET NOT NULL;
DELETE FROM fia."json" WHERE filename IS NULL;
ALTER TABLE fia."json" ALTER COLUMN filename SET NOT NULL;
DELETE FROM fia."json" WHERE fiaversion IS NULL;
ALTER TABLE fia."json" ALTER COLUMN fiaversion SET NOT NULL;
DELETE FROM fia."json" WHERE fiaworkflow IS NULL;
ALTER TABLE fia."json" ALTER COLUMN fiaworkflow SET NOT NULL;
DELETE FROM	fia."json"
WHERE processingout_id NOT IN (
	SELECT processingout_id	FROM fia.processingout p);
SELECT create_constraint_if_not_exists(
    'json_processingout_fk',
    'ALTER TABLE fia."json" ADD CONSTRAINT json_processingout_fk FOREIGN KEY (processingout_id)
        REFERENCES fia.processingout(processingout_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia."json"  a
        USING fia."json" b
WHERE
    a.json_id < b.json_id
    AND upper(a.filename) = upper(b.filename);
ALTER TABLE fia."json" DROP CONSTRAINT IF EXISTS json_un;
CREATE UNIQUE INDEX IF NOT EXISTS json_name_upper_idx on fia."json" (UPPER(filename));

DELETE FROM fia.jsonsample WHERE json_id IS NULL;
ALTER TABLE fia.jsonsample ALTER COLUMN json_id SET NOT NULL;
DELETE FROM fia.jsonsample WHERE samplefield_id IS NULL;
ALTER TABLE fia.jsonsample ALTER COLUMN samplefield_id SET NOT NULL;
DELETE FROM fia.jsonsample WHERE value IS NULL;
ALTER TABLE fia.jsonsample ALTER COLUMN value SET NOT NULL;
DELETE FROM fia.jsonsample WHERE row_id IS NULL;
ALTER TABLE fia.jsonsample ALTER COLUMN row_id SET NOT NULL;
DELETE FROM fia.jsonsample  a
        USING fia.jsonsample b
WHERE
    a.jsonsample_id < b.jsonsample_id
    AND	a.json_id = b.json_id
   	AND a.samplefield_id = b.samplefield_id
	AND a.row_id = b.row_id;
SELECT create_constraint_if_not_exists(
    'jsonsample_json_isamplefield_row_un',
    'ALTER TABLE fia.jsonsample ADD CONSTRAINT jsonsample_json_isamplefield_row_un '
        'UNIQUE (json_id,samplefield_id,row_id);');

DELETE FROM fia.jsondiff WHERE json_id IS NULL;
ALTER TABLE fia.jsondiff ALTER COLUMN json_id SET NOT NULL;
DELETE FROM fia.jsondiff WHERE samplefield_id IS NULL;
ALTER TABLE fia.jsondiff ALTER COLUMN samplefield_id SET NOT NULL;
DELETE FROM fia.jsondiff WHERE numerator IS NULL;
ALTER TABLE fia.jsondiff ALTER COLUMN numerator SET NOT NULL;
DELETE FROM fia.jsondiff WHERE denominator IS NULL;
ALTER TABLE fia.jsondiff ALTER COLUMN denominator SET NOT NULL;
UPDATE fia.jsondiff
SET    "label" = NULL
WHERE  "label" = '';
DELETE FROM fia.jsondiff  a
        USING fia.jsondiff b
WHERE
    a.jsondiff_id < b.jsondiff_id
    AND	a.json_id = b.json_id
   	AND a."label" = b."label"
	AND a.samplefield_id = b.samplefield_id;
SELECT create_constraint_if_not_exists(
    'jsondiff_json_label_samplefield_un',
    'ALTER TABLE fia.jsondiff ADD CONSTRAINT jsondiff_json_label_samplefield_un '
        'UNIQUE (json_id,"label",samplefield_id);');

DELETE FROM fia.raw_ion
WHERE json_id NOT IN (
	SELECT json_id FROM fia."json");
SELECT create_constraint_if_not_exists(
    'ion_json_fk',
    'ALTER TABLE fia.ion ADD CONSTRAINT ion_json_fk FOREIGN KEY (json_id)'
        'REFERENCES fia."json"(json_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia.raw_ion  a
        USING fia.raw_ion b
WHERE
    a.ion_id < b.ion_id
    AND	a.json_id = b.json_id
   	AND a.ionidx = b.ionidx;
SELECT create_constraint_if_not_exists(
    'raw_ion_json_ionidx_un',
    'ALTER TABLE fia.raw_ion ADD CONSTRAINT raw_ion_json_ionidx_un '
        'UNIQUE (json_id,ionidx);');
DELETE FROM fia.raw_ion WHERE ionidx IS NULL;
ALTER TABLE fia.raw_ion ALTER COLUMN ionidx SET NOT NULL;
DELETE FROM fia.raw_ion WHERE ionmz IS NULL;
ALTER TABLE fia.raw_ion ALTER COLUMN ionmz SET NOT NULL;
DELETE FROM fia.raw_ion WHERE ionactive IS NULL;
ALTER TABLE fia.raw_ion ALTER COLUMN ionactive SET NOT NULL;
DELETE FROM fia.raw_ion WHERE ionaverageint IS NULL;
ALTER TABLE fia.raw_ion ALTER COLUMN ionaverageint SET NOT NULL;
DELETE FROM fia.raw_ion WHERE ionqualtic IS NULL;
ALTER TABLE fia.raw_ion ALTER COLUMN ionqualtic SET NOT NULL;
DELETE FROM fia.raw_ion WHERE json_id IS NULL;
ALTER TABLE fia.raw_ion ALTER COLUMN json_id SET NOT NULL;

DELETE FROM fia.raw_intensities ri
WHERE json_id NOT IN (
	SELECT json_id FROM fia."json");
SELECT create_constraint_if_not_exists(
    'raw_intensities_json_fk',
    'ALTER TABLE fia.raw_intensities ADD CONSTRAINT raw_intensities_json_fk FOREIGN KEY (json_id) '
        'REFERENCES fia."json"(json_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia.raw_intensities WHERE ionidx IS NULL;
ALTER TABLE fia.raw_intensities ALTER COLUMN ionidx SET NOT NULL;
DELETE FROM fia.raw_intensities WHERE intensity IS NULL;
ALTER TABLE fia.raw_intensities ALTER COLUMN intensity SET NOT NULL;
DELETE FROM fia.raw_intensities WHERE dsidx IS NULL;
ALTER TABLE fia.raw_intensities ALTER COLUMN dsidx SET NOT NULL;
DELETE FROM fia.raw_intensities WHERE json_id IS NULL;
ALTER TABLE fia.raw_intensities ALTER COLUMN json_id SET NOT NULL;
DELETE FROM fia.raw_intensities a
        USING fia.raw_intensities b
WHERE
    a.intensities_id < b.intensities_id
    AND	a.json_id = b.json_id
   	AND a.ionidx = b.ionidx
    AND a.dsidx = b.dsidx;
SELECT create_constraint_if_not_exists(
    'raw_intensities_json_ionidx_dsidx',
    'ALTER TABLE fia.raw_intensities ADD CONSTRAINT raw_intensities_json_ionidx_dsidx '
    'UNIQUE (json_id,ionidx,dsidx);');

DELETE FROM fia.ion
WHERE json_id NOT IN (
	SELECT json_id FROM fia."json");
SELECT create_constraint_if_not_exists(
    'raw_ion_json_fk',
    'ALTER TABLE fia.ion ADD CONSTRAINT raw_ion_json_fk FOREIGN KEY (json_id) '
        'REFERENCES fia."json"(json_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia.ion a
        USING fia.ion b
WHERE
    a.ion_id < b.ion_id
    AND	a.json_id = b.json_id
   	AND a.ionidx = b.ionidx;
SELECT create_constraint_if_not_exists(
    'ion_un_json_id_ionidx',
    'ALTER TABLE fia.ion ADD CONSTRAINT ion_un_json_id_ionidx '
        'UNIQUE (ionidx,json_id);');
DELETE FROM fia.ion WHERE ionidx IS NULL;
ALTER TABLE fia.ion ALTER COLUMN ionidx SET NOT NULL;
DELETE FROM fia.ion WHERE ionmz IS NULL;
ALTER TABLE fia.ion ALTER COLUMN ionmz SET NOT NULL;
DELETE FROM fia.ion WHERE ionactive IS NULL;
ALTER TABLE fia.ion ALTER COLUMN ionactive SET NOT NULL;
DELETE FROM fia.ion WHERE ionaverageint IS NULL;
ALTER TABLE fia.ion ALTER COLUMN ionaverageint SET NOT NULL;
DELETE FROM fia.ion WHERE ionqualtic IS NULL;
ALTER TABLE fia.ion ALTER COLUMN ionqualtic SET NOT NULL;
DELETE FROM fia.ion WHERE topannotationscore IS NULL;
ALTER TABLE fia.ion ALTER COLUMN topannotationscore SET NOT NULL;
DELETE FROM fia.ion WHERE json_id IS NULL;
ALTER TABLE fia.ion ALTER COLUMN json_id SET NOT NULL;

DELETE FROM fia.annotation WHERE ionidx IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN ionidx SET NOT NULL;
DELETE FROM fia.annotation WHERE mzdelta IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN mzdelta SET NOT NULL;
DELETE FROM fia.annotation WHERE formula IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN formula SET NOT NULL;
DELETE FROM fia.annotation WHERE ion IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN ion SET NOT NULL;
DELETE FROM fia.annotation WHERE score IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN score SET NOT NULL;
DELETE FROM fia.annotation WHERE "label" IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN "label" SET NOT NULL;
DELETE FROM fia.annotation WHERE hmdb_ids IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN hmdb_ids SET NOT NULL;
DELETE FROM fia.annotation WHERE kegg_ids IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN kegg_ids SET NOT NULL;
DELETE FROM fia.annotation WHERE other_ids IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN other_ids SET NOT NULL;
DELETE FROM fia.annotation WHERE json_id IS NULL;
ALTER TABLE fia.annotation ALTER COLUMN json_id SET NOT NULL;
DELETE FROM fia.annotation
WHERE json_id NOT IN (
	SELECT json_id FROM fia."json");
SELECT create_constraint_if_not_exists(
    'annotation_json_fk',
    'ALTER TABLE fia.annotation ADD CONSTRAINT annotation_json_fk FOREIGN KEY (json_id) '
        'REFERENCES fia."json"(json_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia.annotation a
        USING fia.annotation b
WHERE
    a.annotation_id < b.annotation_id
    AND	a.json_id = b.json_id
   	AND a.ionidx = b.ionidx
    AND a."label" = b."label";
SELECT create_constraint_if_not_exists(
    'annotation_json_ionidx_formula_ion_label',
    'ALTER TABLE fia.annotation ADD CONSTRAINT annotation_json_ionidx_formula_ion_label '
        'UNIQUE (json_id,ionidx,ion,"label");');

DELETE FROM fia.intensities
WHERE json_id NOT IN (
	SELECT json_id FROM fia."json");
SELECT create_constraint_if_not_exists(
    'intensities_json_fk',
    'ALTER TABLE fia.intensities ADD CONSTRAINT intensities_json_fk FOREIGN KEY (json_id) '
        'REFERENCES fia."json"(json_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia.intensities a
        USING fia.intensities b
WHERE
    a.intensities_id < b.intensities_id
    AND	a.json_id = b.json_id
   	AND a.ionidx = b.ionidx
    AND a.dsidx = b.dsidx;
SELECT create_constraint_if_not_exists(
    'intensities_json_ionidx_dsidx_un',
    'ALTER TABLE fia.intensities ADD CONSTRAINT intensities_json_ionidx_dsidx_un '
        'UNIQUE (json_id,ionidx,dsidx);');
DELETE FROM fia.intensities WHERE ionidx IS NULL;
ALTER TABLE fia.intensities ALTER COLUMN ionidx SET NOT NULL;
DELETE FROM fia.intensities WHERE dsidx IS NULL;
ALTER TABLE fia.intensities ALTER COLUMN dsidx SET NOT NULL;
DELETE FROM fia.intensities WHERE intensity IS NULL;
ALTER TABLE fia.intensities ALTER COLUMN intensity SET NOT NULL;
DELETE FROM fia.intensities WHERE json_id IS NULL;
ALTER TABLE fia.intensities ALTER COLUMN json_id SET NOT NULL;

DELETE FROM fia.met_diff
WHERE jsondiff_id IS NOT NULL
AND   jsondiff_id NOT IN (
	SELECT jsondiff_id FROM fia.jsondiff);
SELECT create_constraint_if_not_exists(
    'met_diff_json_diff_fk',
    'ALTER TABLE fia.met_diff ADD CONSTRAINT met_diff_json_diff_fk FOREIGN KEY (jsondiff_id) '
        'REFERENCES fia.jsondiff(jsondiff_id) ON DELETE RESTRICT ON UPDATE RESTRICT;');
DELETE FROM fia.met_diff WHERE ionidx IS NULL;
ALTER TABLE fia.met_diff ALTER COLUMN ionidx SET NOT NULL;
DELETE FROM fia.met_diff WHERE logfc IS NULL;
ALTER TABLE fia.met_diff ALTER COLUMN logfc SET NOT NULL;
DELETE FROM fia.met_diff WHERE pval IS NULL;
ALTER TABLE fia.met_diff ALTER COLUMN pval SET NOT NULL;
DELETE FROM fia.met_diff WHERE adjpval IS NULL;
ALTER TABLE fia.met_diff ALTER COLUMN adjpval SET NOT NULL;
ALTER TABLE fia.met_diff DROP CONSTRAINT IF EXISTS met_diff_study_contrast_id_key;

ALTER TABLE fia.samplefield ADD IF NOT EXISTS required boolean NOT NULL DEFAULT false;
UPDATE fia.samplefield SET required = TRUE WHERE samplefield_name <> 'dsAmount';
COMMIT;
