/*
 * 1.1. Fill MSD files which were failed to download or parse
 */

BEGIN;

CREATE TEMP TABLE tmp_msd_migration_context ON COMMIT DROP AS
SELECT
    mf.id
FROM stg_infodw.msd_files mf
WHERE
    mf.load_status = 'failed'
    AND EXISTS
    (
        SELECT 1
        FROM assay.msd_raw mr
        WHERE
            mr.plate_id = mf.plate_id
            AND mr.result_type = mf.result_stage::VARCHAR
    );

CREATE UNIQUE INDEX idx_tmp_msd_migration_context_id
    ON tmp_msd_migration_context (id ASC);

INSERT INTO stg_infodw.msd_file_contents
(
    msd_file_id,
    spot_number,
    row_letter,
    column_number,
    result_value
)
SELECT
    mf.id,
    mr.spot_number,
    mr.row_letter,
    mr.column_number::SMALLINT,
    mr.result
FROM tmp_msd_migration_context tmp
INNER JOIN stg_infodw.msd_files mf
    ON mf.id = tmp.id
INNER JOIN assay.msd_raw mr
    ON mr.plate_id = mf.plate_id
    AND mr.result_type = mf.result_stage::VARCHAR;

UPDATE stg_infodw.msd_files AS umf
SET
    src_filename = COALESCE(sq.src_filename, umf.src_filename),
    load_status = 'loaded',
    data_source = 'migration'
FROM
(
    SELECT
        mf.id,
        MAX(SUBSTRING(mr.src_file_name, '(/Shared/Lab Data/.+)$')) AS src_filename
    FROM tmp_msd_migration_context tmp
    INNER JOIN stg_infodw.msd_files mf
        ON mf.id = tmp.id
    INNER JOIN assay.msd_raw mr
        ON mr.plate_id = mf.plate_id
        AND mr.result_type = mf.result_stage::VARCHAR
    GROUP BY
        mf.id
) sq
WHERE
    sq.id = umf.id;

INSERT INTO stg_infodw.msd_file_audit
(
    msd_file_id,
    action_name,
    action_result
)
SELECT
    tmp.id,
    'Migrate data from old tables' AS action_name,
    'Success' AS action_result
FROM tmp_msd_migration_context tmp;

SELECT
    mpf.filename,
    mpf.output
FROM stg_infodw.msd_process_files() mpf;

COMMIT;

/*
 * 1.2. Fill MSD files which are absent in sapio_plateresultfile table but present in msd_raw table
 */

BEGIN;

CREATE TEMP TABLE tmp_msd_migration_context ON COMMIT DROP AS
SELECT
    mr.plate_id,
    mr.result_type
FROM assay.msd_raw mr
WHERE
    mr.result_type IN ('RAW_DATA', 'ANALYZED_DATA')
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.msd_files mf
        WHERE
            mf.plate_id = mr.plate_id
            AND mf.result_stage::VARCHAR = mr.result_type
    )
GROUP BY
    mr.plate_id,
    mr.result_type;

CREATE UNIQUE INDEX idx_tmp_msd_migration_context_plate_id_result_type
    ON tmp_msd_migration_context (plate_id ASC, result_type ASC);

INSERT INTO stg_infodw.msd_files
(
    experiment_id,
    experiment_name,
    exp_created_at,
    plate_id,
    src_filename,
    result_stage,
    plate_created_at,
    load_status,
    data_source
)
SELECT
    MAX(xp.recordid) AS experiment_id,
    MAX(xp.datarecordname) AS experiment_name,
    TO_TIMESTAMP(MAX(xp.datecreated) / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    mr.plate_id,
    MAX(COALESCE(SUBSTRING(mr.src_file_name, '(/Shared/Lab Data/.+)$'),
        mr.result_type || '/' ||mr.src_file_name)) AS src_filename,
    mr.result_type::stg_infodw.result_stage_enum AS result_stage,
    TO_TIMESTAMP(MAX(xp.datecreated) / 1000) AT TIME ZONE 'UTC' AS plate_created_at,
    'loaded' AS load_status,
    'migration' AS data_source
FROM tmp_msd_migration_context tmp
INNER JOIN assay.msd_raw mr
    ON mr.plate_id = tmp.plate_id
    AND mr.result_type = tmp.result_type
INNER JOIN stg_infodw.sapio_rhe_experiments_plates ep
    ON ep.plate_id = mr.plate_id
INNER JOIN stg_infodw.sapio_elnexperiment xp
    ON xp.recordid = ep.experiment_id
GROUP BY
    mr.plate_id,
    mr.result_type;

INSERT INTO stg_infodw.msd_file_contents
(
    msd_file_id,
    spot_number,
    row_letter,
    column_number,
    result_value
)
SELECT
    mf.id,
    mr.spot_number,
    mr.row_letter,
    mr.column_number::SMALLINT,
    mr.result
FROM tmp_msd_migration_context tmp
INNER JOIN assay.msd_raw mr
    ON mr.plate_id = tmp.plate_id
    AND mr.result_type = tmp.result_type
INNER JOIN stg_infodw.msd_files mf
    ON mf.plate_id = mr.plate_id
    AND mf.result_stage::VARCHAR = mr.result_type;

INSERT INTO stg_infodw.msd_file_audit
(
    msd_file_id,
    action_name,
    action_result
)
SELECT
    mf.id,
    'Migrate data from old tables' AS action_name,
    'Success' AS action_result
FROM tmp_msd_migration_context tmp
INNER JOIN stg_infodw.msd_files mf
    ON mf.plate_id = tmp.plate_id
    AND mf.result_stage::VARCHAR = tmp.result_type;

COMMIT;

/*
 * 1.3. Fill MSD GUAVA normalized data
 */

BEGIN;

INSERT INTO prc_infodw.instr_output_contents
(
    instr_output_id,
    row_letter,
    column_number,
    spot_number,
    result_type,
    result_value
)
SELECT
    io.id,
    mr.row_letter,
    mr.column_number::SMALLINT,
    mr.spot_number,
    'MSD/GUAVA' AS result_type,
    mr.result AS result_value
FROM assay.msd_raw mr
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = mr.plate_id
    AND io.instrument = 'MSD'
    AND io.result_stage::VARCHAR = mr.result_stage
WHERE
    mr.result_stage IN ('RAW_DATA', 'ANALYZED_DATA')
    AND mr.result_type IN ('RAW_MSD/GUAVA', 'ANALYZED_MSD/GUAVA');

INSERT INTO prc_infodw.instr_output_contents
(
    instr_output_id,
    row_letter,
    column_number,
    spot_number,
    result_type,
    result_value
)
SELECT
    io.id,
    mr.row_letter,
    mr.column_number::SMALLINT,
    mr.spot_number,
    'GUAVA_NORMALIZED' AS result_type,
    mr.result AS result_value
FROM assay.msd_raw mr
INNER JOIN prc_infodw.instr_output io
    ON io.plate_id = mr.plate_id
    AND io.instrument = 'MSD'
    AND io.result_stage = 'ANALYZED_DATA'
WHERE
    mr.result_stage = 'ANALYZED_MSD/GUAVA'
    AND mr.result_type = 'GUAVA NORMALIZED_DATA';

COMMIT;

/*
 * 2.1. Fill FORTESSA files which were failed to download or parse
 */

BEGIN;

CREATE TEMP TABLE tmp_fortessa_migration_context ON COMMIT DROP AS
SELECT
    ff.id
FROM stg_infodw.fortessa_files ff
WHERE
    ff.load_status = 'failed'
    AND ff.result_stage = 'ANALYZED_DATA'
    AND EXISTS
    (
        SELECT 1
        FROM assay.fortessa_exp fe
        WHERE
            fe.plate_id = ff.plate_id
            AND NOT EXISTS
            (
                SELECT 1
                FROM assay.fortessa_csvres fcr
                WHERE
                    fcr.fortessa_exp_id = fe.fortessa_exp_id
                    AND NULLIF(fcr.sample, '') IS NULL
            )
    );

CREATE UNIQUE INDEX idx_tmp_fortessa_migration_context_id
    ON tmp_fortessa_migration_context (id ASC);

INSERT INTO stg_infodw.fortessa_file_contents
(
    fortessa_file_id,
    sample,
    result_field,
    result_value
)
SELECT
    ff.id,
    fcr.sample,
    fcr.fieldname,
    fcr.value
FROM tmp_fortessa_migration_context tmp
INNER JOIN stg_infodw.fortessa_files ff
    ON ff.id = tmp.id
INNER JOIN assay.fortessa_exp fe
    ON fe.plate_id = ff.plate_id
INNER JOIN assay.fortessa_csvres fcr
    ON fcr.fortessa_exp_id = fe.fortessa_exp_id;

UPDATE stg_infodw.fortessa_files AS uff
SET
    src_filename = fe.csv_file,
    load_status = 'loaded',
    data_source = 'migration'
FROM tmp_fortessa_migration_context tmp
INNER JOIN stg_infodw.fortessa_files ff
    ON ff.id = tmp.id
INNER JOIN assay.fortessa_exp fe
    ON fe.plate_id = ff.plate_id
WHERE
    tmp.id = uff.id;

INSERT INTO stg_infodw.fortessa_file_audit
(
    fortessa_file_id,
    action_name,
    action_result
)
SELECT
    tmp.id,
    'Migrate data from old tables' AS action_name,
    'Success' AS action_result
FROM tmp_fortessa_migration_context tmp;

SELECT
    fpf.filename,
    fpf.output
FROM stg_infodw.fortessa_process_files() fpf;

COMMIT;

/*
 * 2.2. Fill FORTESSA files which are absent in sapio_plateresultfile table but present in fortessa_exp table
 */

BEGIN;

CREATE TEMP TABLE tmp_fortessa_migration_context ON COMMIT DROP AS
SELECT
    MAX(fe.fortessa_exp_id) AS fortessa_exp_id
FROM assay.fortessa_exp fe
WHERE
    NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = fe.plate_id
            AND ff.result_stage = 'ANALYZED_DATA'
    )
    AND NOT EXISTS
    (
        SELECT 1
        FROM assay.fortessa_csvres fcr
        WHERE
            fcr.fortessa_exp_id = fe.fortessa_exp_id
            AND NULLIF(fcr.sample, '') IS NULL
    )
GROUP BY
    fe.plate_id;

CREATE UNIQUE INDEX idx_tmp_fortessa_migration_context_fortessa_exp_id
    ON tmp_fortessa_migration_context (fortessa_exp_id ASC);

INSERT INTO stg_infodw.fortessa_files
(
    experiment_id,
    experiment_name,
    exp_created_at,
    plate_id,
    src_filename,
    result_stage,
    plate_created_at,
    load_status,
    data_source
)
SELECT
    xp.recordid AS experiment_id,
    xp.datarecordname AS experiment_name,
    TO_TIMESTAMP(xp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    fe.plate_id,
    fe.csv_file AS src_filename,
    'ANALYZED_DATA' AS result_stage,
    TO_TIMESTAMP(xp.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at,
    'loaded' AS load_status,
    'migration' AS data_source
FROM tmp_fortessa_migration_context tmp
INNER JOIN assay.fortessa_exp fe
    ON fe.fortessa_exp_id = tmp.fortessa_exp_id
INNER JOIN stg_infodw.sapio_rhe_experiments_plates ep
    ON ep.plate_id = fe.plate_id
INNER JOIN stg_infodw.sapio_elnexperiment xp
    ON xp.recordid = ep.experiment_id;

INSERT INTO stg_infodw.fortessa_file_contents
(
    fortessa_file_id,
    sample,
    result_field,
    result_value
)
SELECT
    ff.id,
    fcr.sample,
    fcr.fieldname,
    MAX(fcr.value) AS result_value
FROM tmp_fortessa_migration_context tmp
INNER JOIN assay.fortessa_exp fe
    ON fe.fortessa_exp_id = tmp.fortessa_exp_id
INNER JOIN assay.fortessa_csvres fcr
    ON fcr.fortessa_exp_id = fe.fortessa_exp_id
INNER JOIN stg_infodw.fortessa_files ff
    ON ff.plate_id = fe.plate_id
    AND ff.result_stage = 'ANALYZED_DATA'
GROUP BY
    ff.id,
    fcr.sample,
    fcr.fieldname;

INSERT INTO stg_infodw.fortessa_file_audit
(
    fortessa_file_id,
    action_name,
    action_result
)
SELECT
    ff.id,
    'Migrate data from old tables' AS action_name,
    'Success' AS action_result
FROM tmp_fortessa_migration_context tmp
INNER JOIN assay.fortessa_exp fe
    ON fe.fortessa_exp_id = tmp.fortessa_exp_id
INNER JOIN stg_infodw.fortessa_files ff
    ON ff.plate_id = fe.plate_id
    AND ff.result_stage = 'ANALYZED_DATA';

SELECT
    fpf.filename,
    fpf.output
FROM stg_infodw.fortessa_process_files() fpf;

COMMIT;
