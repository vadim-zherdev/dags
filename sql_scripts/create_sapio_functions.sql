CREATE OR REPLACE FUNCTION prc_infodw.sapio_refresh_mvs()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    REFRESH MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv;

    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_annotation_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_compound_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_reagent_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_media_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_stimulation_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_consumableitem_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_crisprguide_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_insertguide_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_sample_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_stimulation_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_timepoint_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_rhe_sample_donor_cell_mv;
    REFRESH MATERIALIZED VIEW infodw.stg_sapio_plate_well_isotopetracer_mv;

END;
$BODY$;

CREATE OR REPLACE FUNCTION stg_infodw.fill_sample_lineage()
RETURNS INT
LANGUAGE plpgsql
AS $BODY$
DECLARE
    result INT;
BEGIN

    TRUNCATE TABLE stg_infodw.sapio_rhe_sample_lineage;

    INSERT INTO stg_infodw.sapio_rhe_sample_lineage
    (
        samplename,
        recordid,
        parentsamplename,
        parentrecordid
    )
    SELECT
        child.othersampleid,
        child.recordid,
        child.parentsample,
        parent.recordid
    FROM stg_infodw.sapio_sample child
    LEFT OUTER JOIN stg_infodw.sapio_sample parent ON
        child.parentsample = parent.othersampleid;

    GET DIAGNOSTICS result = ROW_COUNT;

    RETURN result;

END;
$BODY$;

CREATE OR REPLACE FUNCTION stg_infodw.sapio_load_rhe_plate_well()
 RETURNS TEXT
 LANGUAGE plpgsql
AS $function$
DECLARE
    _row_count INT;
    _return_value TEXT;
BEGIN
	INSERT
		INTO
		stg_infodw.sapio_rhe_plate_well
	SELECT
		*
	FROM
		sapio_rhe_plate_well_tmp tmp
	WHERE
		NOT EXISTS (
		SELECT
			1
		FROM
			stg_infodw.sapio_rhe_plate_well srpw
		WHERE tmp.wellelement_record_id = srpw.wellelement_record_id);
		-- TODO: or create unique index and use "ON CONFLICT UPDATE'

    GET DIAGNOSTICS _row_count = ROW_COUNT;
    _return_value = _row_count || ' rows inserted, ';

    UPDATE
        stg_infodw.sapio_rhe_plate_well srpw
    SET
        plate_id = tmp.plate_id,
        plate_row = tmp.plate_row,
        plate_column = tmp.plate_column,
        sample_name = tmp.sample_name,
        sample_record_id = tmp.sample_record_id,
        on_plate_id = tmp.on_plate_id,
        wellelement_datatype = tmp.wellelement_datatype,
        wellelement_subtype = tmp.wellelement_subtype,
        wellelement_layer = tmp.wellelement_layer,
        data_type_subtype_record_id = tmp.data_type_subtype_record_id,
        wellelement_amount = tmp.wellelement_amount,
        wellelement_uom = tmp.wellelement_uom,
        wellelement_notes = tmp.wellelement_notes,
        wellelement_timepoint = tmp.wellelement_timepoint
    FROM
        sapio_rhe_plate_well_tmp tmp
    WHERE
        srpw.wellelement_record_id = tmp.wellelement_record_id;

    GET DIAGNOSTICS _row_count = ROW_COUNT;
    _return_value = _return_value || _row_count || ' rows updated, ';

	DELETE
	FROM
		stg_infodw.sapio_rhe_plate_well srpw
	WHERE
		NOT EXISTS (
		SELECT
			1
		FROM
			stg_infodw.sapio_wellelement sw
		WHERE
			sw.recordid = srpw.wellelement_record_id ) ;

    GET DIAGNOSTICS _row_count = ROW_COUNT;
    _return_value = _return_value || _row_count || ' rows deleted.';
    RETURN _return_value;
END;
$function$;
