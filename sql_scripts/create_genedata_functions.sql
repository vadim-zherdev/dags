CREATE OR REPLACE FUNCTION stg_infodw.gd_cleanup()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    DELETE FROM stg_infodw.gd_plate_sub_experiment_columns;
    DELETE FROM stg_infodw.gd_plate_sub_experiment;
    DELETE FROM stg_infodw.gd_plate_sub_experiment_stats;
    DELETE FROM stg_infodw.gd_plate_well_condensing_scopes;
    DELETE FROM stg_infodw.gd_plate_well_sub_experiments;
    DELETE FROM stg_infodw.gd_plate_well_properties;
    DELETE FROM stg_infodw.gd_plate_wells;
    DELETE FROM stg_infodw.gd_plate_well_types;
    DELETE FROM stg_infodw.gd_plate_columns;
    DELETE FROM stg_infodw.gd_plate_annotations;
    DELETE FROM stg_infodw.gd_plate_properties;
    DELETE FROM stg_infodw.gd_plates;

    DELETE FROM stg_infodw.gd_compound_fitting_calculated_columns;
    DELETE FROM stg_infodw.gd_compound_fitting_fit_columns;
    DELETE FROM stg_infodw.gd_compound_fitting;
    DELETE FROM stg_infodw.gd_compound_calculated_columns;
    DELETE FROM stg_infodw.gd_compound_condensing_columns;
    DELETE FROM stg_infodw.gd_compound_annotations;
    DELETE FROM stg_infodw.gd_compound_condensing_scope;
    DELETE FROM stg_infodw.gd_compound_sub_experiment;
    DELETE FROM stg_infodw.gd_compounds;

    DELETE FROM stg_infodw.gd_layer_sub_experiment;
    DELETE FROM stg_infodw.gd_layer_sub_experiment_stats;
    DELETE FROM stg_infodw.gd_layer_calculated_fit_columns;
    DELETE FROM stg_infodw.gd_layer_annotations;
    DELETE FROM stg_infodw.gd_layers;
    DELETE FROM stg_infodw.gd_method_parameters;
    DELETE FROM stg_infodw.gd_methods;
    DELETE FROM stg_infodw.gd_qcsession_annotations;
    DELETE FROM stg_infodw.gd_qcsessions;

    DELETE FROM stg_infodw.gd_experiments;

END;
$BODY$;
