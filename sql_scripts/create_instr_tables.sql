CREATE SEQUENCE IF NOT EXISTS prc_infodw.instr_output_id_seq;

CREATE TABLE IF NOT EXISTS prc_infodw.instr_output
(
    id INT NOT NULL DEFAULT nextval('prc_infodw.instr_output_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    instrument prc_infodw.instrument_enum NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    output_path VARCHAR(1024) NOT NULL,
    result_stage prc_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NULL,
    CONSTRAINT pk_instr_output PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_output_instrument_plate_id_result_stage
    ON prc_infodw.instr_output (instrument, plate_id, result_stage);

CREATE INDEX IF NOT EXISTS idx_instr_output_instrument_result_stage
    ON prc_infodw.instr_output (instrument, result_stage);

CREATE SEQUENCE IF NOT EXISTS prc_infodw.instr_output_contents_id_seq;

CREATE TABLE IF NOT EXISTS prc_infodw.instr_output_contents
(
    id INT NOT NULL DEFAULT nextval('prc_infodw.instr_output_contents_id_seq'),
    instr_output_id INT NOT NULL,
    row_letter CHAR(1) NULL,
    column_number SMALLINT NULL,
    spot_number SMALLINT NULL,
    sample VARCHAR(256) NULL,
    result_field VARCHAR(256) NULL,
    result_normalized_field VARCHAR(32) NULL,
    result_type prc_infodw.result_type_enum NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_instr_output_contents PRIMARY KEY (id),
    CONSTRAINT fk_instr_output_contents_instr_output FOREIGN KEY (instr_output_id)
        REFERENCES prc_infodw.instr_output (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_output_contents_msd_primary
    ON prc_infodw.instr_output_contents (instr_output_id, row_letter, column_number, spot_number, result_type);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_output_contents_fortessa_primary
    ON prc_infodw.instr_output_contents (instr_output_id, sample, result_field, result_type);

CREATE TABLE IF NOT EXISTS prc_infodw.instr_output_qe_contents
(
    instr_output_id INT NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    metagroup_id SMALLINT NOT NULL,
    group_id SMALLINT NOT NULL,
    sample VARCHAR(256) NOT NULL,
    isotope_label VARCHAR(1024) NULL,
    compound VARCHAR(256) NULL,
    is_std BOOLEAN NOT NULL,
    result_type prc_infodw.result_type_enum NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_instr_output_qe_contents PRIMARY KEY (instr_output_id, metagroup_id, group_id, sample, result_type),
    CONSTRAINT fk_instr_output_qe_contents_instr_output FOREIGN KEY (instr_output_id)
        REFERENCES prc_infodw.instr_output (id)
);

CREATE INDEX IF NOT EXISTS idx_instr_output_qe_contents_instr_output_id_row_letter_column_number
    ON prc_infodw.instr_output_qe_contents (instr_output_id, row_letter, column_number);

CREATE INDEX IF NOT EXISTS idx_instr_output_qe_contents_sample
    ON prc_infodw.instr_output_qe_contents (sample);
