CREATE OR REPLACE FUNCTION stg_infodw.fia_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _input_status CONSTANT VARCHAR := 'Diff data loaded';
    _output_status CONSTANT VARCHAR := 'Processed';
BEGIN

    REFRESH MATERIALIZED VIEW infodw.fia_jsonsample_on_plate_mv;

    -- Delete previous extended curated data
    RAISE NOTICE 'Deleting from extended curated data table..';

    DELETE FROM infodw.fia_extended_curated_data_consumables
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about starting inserting extended curated data
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'Begin inserting into extended curated data consumables table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Perform actual record insertion
    RAISE NOTICE 'Inserting into extended curated data table..';

	INSERT INTO infodw.fia_extended_curated_data_consumables
	SELECT
	    * -- TODO: try to get rid of asterisk
	FROM infodw.fia_extended_curated_data_consumables_v
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about finishing inserting extended curated data
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'End inserting into extended curated data consumables table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Delete previous crn
    RAISE NOTICE 'Deleting from CRN difference table..';

    DELETE FROM infodw.fia_crn_met_diff_split
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about starting inserting crn
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'Begin inserting into CRN difference table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Perform actual record insertion
    RAISE NOTICE 'Inserting into CRN difference table..';

	INSERT INTO infodw.fia_crn_met_diff_split
	SELECT
	    * -- TODO: try to get rid of asterisk
	FROM infodw.fia_crn_met_diff_split_v
    WHERE
        processingout_id IN
        (
            SELECT
                po.processingout_id
            FROM fia.processingout po
            WHERE
                po.load_status = _input_status
        );

    -- Write audit log about finishing inserting crn
    INSERT INTO stg_infodw.fia_processingout_audit
    (
        processingout_id,
        action_name,
        action_result
    )
    SELECT
        po.processingout_id,
        'End inserting into CRN difference table',
        'Success'
    FROM fia.processingout po
    WHERE
        po.load_status = _input_status;

    -- Update statuses
    RAISE NOTICE 'Updating statuses..';

    UPDATE fia.processingout
    SET
        load_status = _output_status
    WHERE
        load_status = _input_status;

END;
$BODY$;

CREATE OR REPLACE FUNCTION fia.json_cleanup
(
    p_json_id int
)
RETURNS int4
LANGUAGE plpgsql
AS $BODY$
DECLARE
    jsondiff_tables varchar[] := array[
        'met_diff',
        'crn_met_diff'];
    json_tables varchar[] := array[
        'ion',
        'annotation',
        'intensities',
        'jsonsample',
        'raw_ion',
        'jsondiff',
        'raw_intensities'];
    total int;
    row_count int;
    table_name varchar;
BEGIN
    DELETE FROM fia.jsonsample_qc
    WHERE jsonsample_id in (
        SELECT jsonsample_id
        FROM fia.jsonsample
        WHERE json_id = p_json_id);
    GET DIAGNOSTICS total = ROW_COUNT;
    FOREACH table_name IN ARRAY jsondiff_tables LOOP
        EXECUTE 'DELETE FROM fia.' || table_name ||
            ' WHERE jsondiff_id IN (
                SELECT jsondiff_id
                FROM fia.jsondiff
                WHERE json_id = $1)'
        USING p_json_id;
        GET DIAGNOSTICS row_count = ROW_COUNT;
        total := total + row_count;
    END LOOP;
    FOREACH table_name IN ARRAY json_tables LOOP
        EXECUTE 'DELETE FROM fia.' || table_name ||
            ' WHERE json_id = $1'
        USING p_json_id;
        GET DIAGNOSTICS row_count = ROW_COUNT;
        total := total + row_count;
    END LOOP;
    RETURN total;
END;
$BODY$;
