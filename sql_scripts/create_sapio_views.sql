CREATE OR REPLACE VIEW stg_infodw.sapio_plate_well_pivot_v
AS SELECT
    ct_meta.on_plate_id,
    ct_meta.plate_id,
    ct_meta.plate_row,
    ct_meta.plate_column,
    ct_meta.spw_crispr_guide,
    ct_meta.spw_insert_guide,
    ct_meta.spw_stimulation,
    ct_meta.spw_sample,
    ct_meta.spw_timepoint,
    ct_meta.spw_isotope_label,
    ct_meta.spw_compound,
    ct_meta.spw_annotation
FROM crosstab
(
    $$
    SELECT
        pw.on_plate_id,
        pw.plate_id,
        pw.plate_row,
        pw.plate_column,
        pw.wellelement_datatype,
        pw.wellelement_subtype
    FROM stg_infodw.sapio_rhe_plate_well pw
    ORDER BY
        pw.on_plate_id,
        pw.wellelement_datatype,
        pw.wellelement_subtype
    $$,
    $$
    VALUES ('CRISPRGuide'), ('InsertGuide'), ('Stimulation'), ('Sample'),
        ('Timepoint'), ('IsotopeLabel'), ('Compound'), ('Annotation')
    $$
) AS ct_meta
(
    on_plate_id VARCHAR(12),
    plate_id VARCHAR(8),
    plate_row CHAR(1),
    plate_column SMALLINT,
    spw_crispr_guide VARCHAR(256),
    spw_insert_guide VARCHAR(256),
    spw_stimulation VARCHAR(256),
    spw_sample VARCHAR(256),
    spw_timepoint VARCHAR(256),
    spw_isotope_label VARCHAR(256),
    spw_compound VARCHAR(256),
    spw_annotation VARCHAR(256)
);


DROP MATERIALIZED VIEW IF EXISTS prc_infodw.instr_plate_metadata_mv;
CREATE MATERIALIZED VIEW prc_infodw.instr_plate_metadata_mv
AS SELECT
    pwp.plate_id,
    pwp.plate_row,
    pwp.plate_column,
    pwp.on_plate_id,
    pwp.spw_sample AS orig_sample,
    pwp.spw_stimulation AS orig_stimulation,
    pwp.spw_timepoint AS orig_timepoint,
    pwp.spw_isotope_label AS orig_isotope_label,
    pwp.spw_compound AS orig_compound,
    pwp.spw_annotation AS orig_annotation,
    CASE
        WHEN COALESCE(pwp.spw_insert_guide, pwp.spw_crispr_guide) LIKE 'NTC%'
            THEN COALESCE(pwp.spw_insert_guide, pwp.spw_crispr_guide)
        ELSE COALESCE(itg.genename, crg.genename)
    END AS gene,
    CASE
        WHEN COALESCE(pwp.spw_insert_guide, pwp.spw_crispr_guide) LIKE 'NTC%' THEN 'control'
        ELSE 'crispr'
    END AS gene_type,
    CASE
        WHEN pwp.spw_stimulation IN ('IC+', 'Stimulated') THEN 'IC+ Re-Activated'
        ELSE pwp.spw_stimulation
    END AS stimulation,
    COALESCE(cell.ontologynickname, 'NONE') AS cell_type,
    ctrl.controlsubtype AS control_subtype,
    (
        SELECT
            MAX(d.donorid)
        FROM stg_infodw.sapio_sample s
        INNER JOIN stg_infodw.sapio_donorid d
            ON s.relatedchild452 = d.recordid
        WHERE
            s.datarecordname = pwp.spw_sample
    ) AS donor,
    (
        SELECT
            MAX(ep.experiment_name)
        FROM stg_infodw.sapio_rhe_experiments_plates ep
        WHERE
            ep.plate_id = pwp.plate_id
    ) AS experiment_name
FROM stg_infodw.sapio_plate_well_pivot_v pwp
LEFT JOIN stg_infodw.sapio_crisprguide crg
    ON crg.datarecordname = pwp.spw_crispr_guide
LEFT JOIN stg_infodw.sapio_insertguide itg
    ON itg.datarecordname = pwp.spw_insert_guide
LEFT JOIN stg_infodw.sapio_cell cell
    ON cell.relatedrecord143 = pwp.spw_sample
LEFT JOIN stg_infodw.sapio_control ctrl
    ON ctrl.relatedrecord143 = pwp.spw_sample
WITH DATA;

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_primary
    ON prc_infodw.instr_plate_metadata_mv (plate_id, plate_row, plate_column);

CREATE UNIQUE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_on_plate_id
    ON prc_infodw.instr_plate_metadata_mv (on_plate_id);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_gene
    ON prc_infodw.instr_plate_metadata_mv (gene);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_gene_type
    ON prc_infodw.instr_plate_metadata_mv (gene_type);

CREATE INDEX IF NOT EXISTS idx_instr_plate_metadata_mv_stimulation
    ON prc_infodw.instr_plate_metadata_mv (stimulation);
