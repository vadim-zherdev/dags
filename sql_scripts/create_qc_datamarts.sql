CREATE OR REPLACE VIEW infodw.fia_qc_data_unnest_v
AS SELECT
    qo.id AS spectrum_id,
    UNNEST(qoa.normalized_scaled_vector) AS normalized_scaled_vector,
    UNNEST(qoa.nz_array) AS nz_array,
    qo.num_centroids,
    qo.summed_tic,
    qo.signal_to_noise,
    qo.acq_date,
    UNNEST(qoa.spectrum_ab_array) AS spectrum_ab_array,
    UNNEST(qoa.spectrum_mz_array) AS spectrum_mz_array,
    qo.experiment_name,
    qo.short_experiment_name AS experiment_shortname,
    qo.file_path,
    qo.file_name,
    qo.on_plate_id,
    qo.sample,
    qo.cell_type AS celltype,
    qo.mz_min,
    qo.mz_max,
    qo.mode,
    qo.fiamat_tic,
    qo.replicate AS rep,
    qo.dotd_acq_date,
    NULL::VARCHAR AS metabolite
FROM prc_infodw.qc_output qo
INNER JOIN prc_infodw.qc_output_arrays qoa
    ON qoa.qc_output_id = qo.id;


CREATE OR REPLACE VIEW infodw.fia_qc_data_unnest_v2
AS SELECT
    qo.id AS spectrum_id,
    qo.num_centroids,
    qo.summed_tic,
    qo.signal_to_noise,
    qo.acq_date,
    UNNEST(qoa.spectrum_ab_array) AS spectrum_ab_array,
    UNNEST(qoa.spectrum_mz_array) AS spectrum_mz_array,
    qo.experiment_name,
    qo.short_experiment_name AS experiment_shortname,
    qo.file_path,
    qo.file_name,
    qo.on_plate_id,
    qo.sample,
    qo.cell_type AS celltype,
    qo.mz_min,
    qo.mz_max,
    qo.mode,
    qo.fiamat_tic,
    qo.replicate AS rep,
    qo.dotd_acq_date,
    NULL::VARCHAR AS metabolite
FROM prc_infodw.qc_output qo
INNER JOIN prc_infodw.qc_output_arrays qoa
    ON qoa.qc_output_id = qo.id;


CREATE OR REPLACE VIEW infodw.fia_qc_data_v
AS SELECT
    qdu.spectrum_id,
    qdu.normalized_scaled_vector,
    qdu.nz_array,
    qdu.num_centroids,
    qdu.summed_tic,
    qdu.signal_to_noise,
    qdu.acq_date,
    qdu.spectrum_ab_array,
    qdu.spectrum_mz_array,
    qdu.experiment_name,
    qdu.experiment_shortname,
    qdu.file_path,
    qdu.file_name,
    qdu.on_plate_id,
    qdu.sample,
    qdu.celltype,
    qdu.mz_min,
    qdu.mz_max,
    qdu.mode,
    qdu.fiamat_tic,
    qdu.rep,
    qdu.dotd_acq_date,
    (
        SELECT
            qdm.metabolite
        FROM stg_infodw.fia_qc_data_metabolites qdm
        WHERE
            qdu.spectrum_mz_array BETWEEN qdm.low_mass AND qdm.high_mass
    ) AS metabolite
FROM infodw.fia_qc_data_unnest_v qdu;


CREATE OR REPLACE VIEW infodw.fia_qc_data_v2
AS SELECT
    qdu.spectrum_id,
    qdu.num_centroids,
    qdu.summed_tic,
    qdu.signal_to_noise,
    qdu.acq_date,
    qdu.spectrum_ab_array,
    qdu.spectrum_mz_array,
    qdu.experiment_name,
    qdu.experiment_shortname,
    qdu.file_path,
    qdu.file_name,
    qdu.on_plate_id,
    qdu.sample,
    qdu.celltype,
    qdu.mz_min,
    qdu.mz_max,
    qdu.mode,
    qdu.fiamat_tic,
    qdu.rep,
    qdu.dotd_acq_date,
    (
        SELECT
            qdm.metabolite
        FROM stg_infodw.fia_qc_data_metabolites qdm
        WHERE
            qdu.spectrum_mz_array BETWEEN qdm.low_mass AND qdm.high_mass
    ) AS metabolite
FROM infodw.fia_qc_data_unnest_v qdu;
