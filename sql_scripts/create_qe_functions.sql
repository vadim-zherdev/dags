CREATE OR REPLACE FUNCTION stg_infodw.qe_recalc_statuses()
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    UPDATE stg_infodw.qe_worklists wl
    SET
        status = 'processed'
    WHERE
        wl.status IN ('created', 'started', 'failed')
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_files qf
            WHERE
                qf.experiment_id = wl.experiment_id
                AND qf.worklist_filename = wl.src_filename
                AND qf.result_stage = 'RAW_DATA'
                AND qf.load_status <> 'processed'
        );

    UPDATE stg_infodw.qe_experiments exp
    SET
        status = 'processed'
    WHERE
        exp.status IN ('created', 'started', 'failed')
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_worklists wl
            WHERE
                wl.experiment_id = exp.experiment_id
                AND wl.status <> 'processed'
        );

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_insert_missing_experiments
(
    all_exps BOOLEAN,
    exp_name TEXT DEFAULT NULL
)
RETURNS TABLE
(
    experiment_id INT,
    experiment_name TEXT,
    exp_created_at TIMESTAMP,
    src_path VARCHAR(1024),
    status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    INSERT INTO stg_infodw.qe_experiments
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        src_path,
        status
    )
    SELECT
        nbi.experiment_id AS experiment_id,
        nbi.experiment_name AS experiment_name,
        TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
        nbi.egnyte_folder_path AS src_path,
        'created' AS status
    FROM stg_infodw.sapio_rhe_nb_instru nbi
    INNER JOIN stg_infodw.sapio_elnexperiment exp on
        exp.recordid = nbi.experiment_id
    WHERE
        nbi.instrument_used LIKE 'QE%'
        AND (all_exps OR nbi.experiment_name = exp_name)
        AND NULLIF(LTRIM(nbi.egnyte_folder_path), '') IS NOT NULL
        AND NOT EXISTS
        (
            SELECT 1
            FROM stg_infodw.qe_experiments qe
            WHERE
                qe.experiment_id = nbi.experiment_id
        );

    RETURN QUERY
    SELECT
        qe.experiment_id,
        qe.experiment_name,
        qe.exp_created_at,
        qe.src_path,
        qe.status
    FROM stg_infodw.qe_experiments qe
    WHERE
        qe.status IN ('created', 'invalidated');

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_create_analyzed_file
(
    filename VARCHAR(1024),
    exp_name TEXT,
    src stg_infodw.data_source_enum
)
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    qe_file_id INT;
BEGIN

    IF NOT EXISTS
    (
        SELECT
            1
        FROM stg_infodw.qe_files qf
        WHERE
            qf.src_filename = filename
    )
    THEN

        INSERT INTO stg_infodw.qe_files AS iqf
        (
            experiment_id,
            experiment_name,
            exp_created_at,
            src_filename,
            result_stage,
            load_status,
            data_source
        )
        SELECT
            exp.recordid AS experiment_id,
            exp.datarecordname AS experiment_name,
            TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
            filename AS src_filename,
            'ANALYZED_DATA' AS result_stage,
            'created' AS load_status,
            src AS data_source
        FROM stg_infodw.sapio_elnexperiment exp
        WHERE
            REPLACE(REPLACE(exp.datarecordname, ' ', '_'), '-', '_') = REPLACE(REPLACE(exp_name, ' ', '_'), '-', '_')
        LIMIT 1
        RETURNING iqf.id INTO qe_file_id;

        IF qe_file_id IS NOT NULL THEN

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                qe_file_id,
                'Insert entity record',
                'Success'
            );

       END IF;

    END IF;

    RETURN QUERY
    SELECT
        qf.id,
        qf.experiment_id,
        qf.src_filename,
        qf.result_stage,
        qf.load_status
    FROM stg_infodw.qe_files qf
    WHERE
        qf.id = qe_file_id;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_start_analyzed_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_qe_contents AS ioqc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.qe_file_data qfd
            ON SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = io.plate_id
        INNER JOIN stg_infodw.qe_files qf
            ON qf.id = qfd.qe_file_id
            AND qf.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioqc.instr_output_id = io.id
            AND io.instrument = 'QE'
            AND qf.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.qe_file_data qfd
        INNER JOIN stg_infodw.qe_files qf
            ON qf.id = qfd.qe_file_id
        WHERE
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') = io.plate_id
            AND qf.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'QE'
            AND qf.load_status = 'invalidated';

        DELETE FROM stg_infodw.qe_file_peaks AS qfp
        USING stg_infodw.qe_files qf
        WHERE
            qfp.qe_file_id = qf.id
            AND qf.load_status = 'invalidated';

        DELETE FROM stg_infodw.qe_file_data AS qfd
        USING stg_infodw.qe_files qf
        WHERE
            qfd.qe_file_id = qf.id
            AND qf.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.qe_file_audit
        (
            qe_file_id,
            action_name,
            action_result
        )
        SELECT
            qf.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status = 'invalidated'
            AND qf.result_stage = 'ANALYZED_DATA';

        -- Write audit records for files
        INSERT INTO stg_infodw.qe_file_audit
        (
            qe_file_id,
            action_name,
            action_result
        )
        SELECT
            qf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status IN ('created', 'invalidated')
            AND qf.result_stage = 'ANALYZED_DATA';

        RETURN QUERY
        UPDATE stg_infodw.qe_files uqf
        SET
            load_status = 'started'
        WHERE
            uqf.load_status IN ('created', 'invalidated')
            AND uqf.result_stage = 'ANALYZED_DATA'
        RETURNING
            uqf.id,
            uqf.experiment_id,
            uqf.experiment_name,
            uqf.src_filename,
            uqf.result_stage,
            uqf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.qe_process_analyzed_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
DECLARE
    _rec RECORD;
BEGIN

    -- Detect files with no plate/well data
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.qe_files qf
        WHERE
            qf.load_status = 'loaded'
            AND qf.result_stage = 'ANALYZED_DATA'
            AND NOT EXISTS
            (
                SELECT 1
                FROM stg_infodw.qe_file_data qfd
                WHERE
                    qfd.qe_file_id = qf.id
                    AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
            )
    )
    THEN

        FOR _rec IN
            SELECT
                qf.id,
                qf.src_filename
            FROM stg_infodw.qe_files qf
            WHERE
                qf.load_status = 'loaded'
                AND qf.result_stage = 'ANALYZED_DATA'
                AND NOT EXISTS
                (
                    SELECT 1
                    FROM stg_infodw.qe_file_data qfd
                    WHERE
                        qfd.qe_file_id = qf.id
                        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
                )
        LOOP

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: file does not contain plate/well data'
            );

            UPDATE stg_infodw.qe_files
            SET
                load_status = 'failed'
            WHERE
                id = _rec.id;

            RAISE WARNING 'File % does not contain plate/well data.', _rec.src_filename;

            filename := _rec.src_filename;
            output := 'File does not contain plate/well data';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Detect data overlapping problems and mark failed
    IF EXISTS
    (
        SELECT 1
        FROM stg_infodw.qe_files qf
        INNER JOIN stg_infodw.qe_file_data qfd
            ON qfd.qe_file_id = qf.id
        WHERE
            qf.load_status IN ('loaded', 'processed')
            AND qf.result_stage = 'ANALYZED_DATA'
        GROUP BY
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*'),
            qfd.metagroup_id,
            qfd.group_id,
            qfd.sample_id
        HAVING
            COUNT(*) > 1
    )
    THEN

        FOR _rec IN
            SELECT DISTINCT
                qf.id,
                qf.src_filename
            FROM stg_infodw.qe_files qf
            INNER JOIN stg_infodw.qe_file_data qfd
                ON qfd.qe_file_id = qf.id
            INNER JOIN
            (
                SELECT
                    SUBSTRING(qfd1.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id,
                    qfd1.metagroup_id,
                    qfd1.group_id,
                    qfd1.sample_id
                FROM stg_infodw.qe_files qf1
                INNER JOIN stg_infodw.qe_file_data qfd1
                    ON qfd1.qe_file_id = qf1.id
                WHERE
                    qf1.load_status IN ('loaded', 'processed')
                    AND qf1.result_stage = 'ANALYZED_DATA'
                GROUP BY
                    SUBSTRING(qfd1.sample_id, '(P\-\d+)_[A-Z]\d+.*'),
                    qfd1.metagroup_id,
                    qfd1.group_id,
                    qfd1.sample_id
                HAVING
                    COUNT(*) > 1
            ) sq1
                ON sq1.plate_id = SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*')
                AND sq1.metagroup_id = qfd.metagroup_id
                AND sq1.group_id = qfd.group_id
                AND sq1.sample_id = qfd.sample_id
            WHERE
                qf.load_status = 'loaded'
                AND qf.result_stage = 'ANALYZED_DATA'
        LOOP

            INSERT INTO stg_infodw.qe_file_audit
            (
                qe_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                _rec.id,
                'Process data',
                'Error: file contains data overlapping other files data by key'
            );

            UPDATE stg_infodw.qe_files
            SET
                load_status = 'failed'
            WHERE
                id = _rec.id;

            RAISE WARNING 'Data overlapping for file %.', _rec.src_filename;

            filename := _rec.src_filename;
            output := 'Data overlapping with other files by key';
            RETURN NEXT;

        END LOOP;

    END IF;

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage
    )
    SELECT
        sq.experiment_id,
        sq.experiment_name,
        sq.exp_created_at,
        'QE' AS instrument,
        sq.plate_id,
        nbi.egnyte_folder_path AS output_path,
        'ANALYZED_DATA' AS result_stage
    FROM
    (
        SELECT
            qf.experiment_id,
            qf.experiment_name,
            qf.exp_created_at,
            SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*') AS plate_id
        FROM stg_infodw.qe_files qf
        INNER JOIN stg_infodw.qe_file_data qfd
            ON qfd.qe_file_id = qf.id
        WHERE
            qf.load_status = 'loaded'
            AND qf.result_stage = 'ANALYZED_DATA'
    ) sq
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = sq.experiment_id
        AND nbi.instrument_used LIKE 'QE%'
    WHERE
        sq.plate_id IS NOT NULL
    GROUP BY
        sq.experiment_id,
        sq.experiment_name,
        sq.exp_created_at,
        sq.plate_id,
        nbi.egnyte_folder_path;

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_qe_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        metagroup_id,
        group_id,
        sample,
        isotope_label,
        compound,
        is_std,
        result_type,
        result_value
    )
    SELECT
        io.id,
        qfd.row_letter,
        qfd.column_number,
        qfp.metagroup_id,
        qfp.group_id,
        qfd.sample_id,
        qfp.isotope_label,
        qfp.compound,
        qfp.is_std,
        'ORIGINAL' AS result_type,
        qfd.result_value AS result_value
    FROM stg_infodw.qe_files qf
    INNER JOIN stg_infodw.qe_file_peaks qfp
        ON qfp.qe_file_id = qf.id
    INNER JOIN stg_infodw.qe_file_data qfd
        ON qfd.qe_file_id = qf.id
        AND qfd.metagroup_id = qfp.metagroup_id
        AND qfd.group_id = qfp.group_id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = SUBSTRING(qfd.sample_id, '(P\-\d+)_[A-Z]\d+.*')
        AND io.result_stage::TEXT = qf.result_stage::TEXT
        AND io.instrument = 'QE'
    WHERE
        qf.load_status = 'loaded'
        AND qf.result_stage = 'ANALYZED_DATA'
        AND qfd.sample_id SIMILAR TO 'P-[0-9]+_[A-Z][0-9]+%'
    ORDER BY
        qf.id,
        qfd.sample_id,
        qfp.metagroup_id,
        qfp.group_id;

    -- Insert audit records
    INSERT INTO stg_infodw.qe_file_audit
    (
        qe_file_id,
        action_name,
        action_result
    )
    SELECT
        qf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.qe_files qf
    WHERE
        qf.load_status = 'loaded'
        AND qf.result_stage = 'ANALYZED_DATA';

    -- Update statuses
    UPDATE stg_infodw.qe_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded'
        AND result_stage = 'ANALYZED_DATA';

END;
$BODY$;
