CREATE INDEX IF NOT EXISTS idx_sapio_cell_relatedrecord143
    ON stg_infodw.sapio_cell (relatedrecord143);

CREATE INDEX IF NOT EXISTS idx_sapio_sample_plateid
    ON stg_infodw.sapio_sample (plateid);

CREATE INDEX IF NOT EXISTS idx_sapio_sample_datarecordname
    ON stg_infodw.sapio_sample (datarecordname);

CREATE INDEX IF NOT EXISTS idx_sapio_crisprguide_datarecordname
    ON stg_infodw.sapio_crisprguide (datarecordname);

CREATE INDEX IF NOT EXISTS idx_sapio_insertguide_datarecordname
    ON stg_infodw.sapio_insertguide (datarecordname);

CREATE INDEX IF NOT EXISTS idx_sapio_insertguide_genename
    ON stg_infodw.sapio_insertguide (genename);
