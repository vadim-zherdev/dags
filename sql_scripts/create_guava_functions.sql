CREATE OR REPLACE FUNCTION stg_infodw.guava_create_files
(
    src stg_infodw.data_source_enum
)
RETURNS void
LANGUAGE plpgsql
AS $BODY$
BEGIN

    CREATE TEMP TABLE tmp_guava_files ON COMMIT DROP AS
    SELECT
        plate_id,
        result_stage
    FROM stg_infodw.guava_missing_files_v;

    INSERT INTO stg_infodw.guava_files
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        plate_id,
        src_filename,
        result_stage,
        plate_created_at,
        load_status,
        data_source
    )
    SELECT
        msf.experiment_id,
        msf.experiment_name,
        msf.exp_created_at,
        msf.plate_id,
        msf.filepath,
        msf.result_stage,
        msf.plate_created_at,
        'created' AS load_status,
        src AS data_source
    FROM stg_infodw.guava_missing_files_v msf
    INNER JOIN tmp_guava_files tmp
        ON tmp.plate_id = msf.plate_id
        AND tmp.result_stage = msf.result_stage;

    INSERT INTO stg_infodw.guava_file_audit
    (
        guava_file_id,
        action_name,
        action_result
    )
    SELECT
        gf.id,
        'Insert entity record',
        'Success'
    FROM stg_infodw.guava_files gf
    INNER JOIN tmp_guava_files tmp
        ON tmp.plate_id = gf.plate_id
        AND tmp.result_stage = gf.result_stage;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.guava_start_files()
RETURNS TABLE
(
    id INT,
    experiment_id INT,
    experiment_name TEXT,
    plate_id VARCHAR(32),
    src_filename VARCHAR(1024),
    result_stage stg_infodw.result_stage_enum,
    load_status stg_infodw.entry_status_enum
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

        -- Clean up tables for invalidated files
        DELETE FROM prc_infodw.instr_output_contents AS ioc
        USING prc_infodw.instr_output io
        INNER JOIN stg_infodw.guava_files gf
            ON gf.plate_id = io.plate_id
            AND gf.result_stage::TEXT = io.result_stage::TEXT
        WHERE
            ioc.instr_output_id = io.id
            AND io.instrument = 'GUAVA'
            AND gf.load_status = 'invalidated';

        DELETE FROM prc_infodw.instr_output AS io
        USING stg_infodw.guava_files gf
        WHERE
            gf.plate_id = io.plate_id
            AND gf.result_stage::TEXT = io.result_stage::TEXT
            AND io.instrument = 'GUAVA'
            AND gf.load_status = 'invalidated';

        DELETE FROM stg_infodw.guava_file_contents AS gfc
        USING stg_infodw.guava_files gf
        WHERE
            gfc.guava_file_id = gf.id
            AND gf.load_status = 'invalidated';

        -- Write audit records for invalidated files
        INSERT INTO stg_infodw.guava_file_audit
        (
            guava_file_id,
            action_name,
            action_result
        )
        SELECT
            gf.id,
            'Cleanup for reloading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.guava_files gf
        WHERE
            gf.load_status = 'invalidated';

        -- Write audit records for files
        INSERT INTO stg_infodw.guava_file_audit
        (
            guava_file_id,
            action_name,
            action_result
        )
        SELECT
            gf.id,
            'Start loading file' AS action_name,
            'Success' AS action_result
        FROM stg_infodw.guava_files gf
        WHERE
            gf.load_status IN ('created', 'invalidated');

        RETURN QUERY
        UPDATE stg_infodw.guava_files ugf
        SET
            load_status = 'started'
        WHERE
            ugf.load_status IN ('created', 'invalidated')
        RETURNING
            ugf.id,
            ugf.experiment_id,
            ugf.experiment_name,
            ugf.plate_id,
            ugf.src_filename,
            ugf.result_stage,
            ugf.load_status;

END;
$BODY$;


CREATE OR REPLACE FUNCTION stg_infodw.guava_process_files()
RETURNS TABLE
(
    filename VARCHAR(1024),
    output TEXT
)
LANGUAGE plpgsql
AS $BODY$
BEGIN

    -- Insert entities
    INSERT INTO prc_infodw.instr_output
    (
        experiment_id,
        experiment_name,
        exp_created_at,
        instrument,
        plate_id,
        output_path,
        result_stage,
        plate_created_at
    )
    SELECT
        gf.experiment_id,
        gf.experiment_name,
        gf.exp_created_at,
        'GUAVA' AS instrument,
        gf.plate_id,
        nbi.egnyte_folder_path AS output_path,
        gf.result_stage::TEXT::prc_infodw.result_stage_enum,
        gf.plate_created_at
    FROM stg_infodw.guava_files gf
    INNER JOIN stg_infodw.sapio_rhe_nb_instru nbi
        ON nbi.experiment_id = gf.experiment_id
        AND nbi.instrument_used LIKE 'GUAVA%'
    WHERE
        gf.load_status = 'loaded';

    -- Insert original values
    INSERT INTO prc_infodw.instr_output_contents
    (
        instr_output_id,
        row_letter,
        column_number,
        result_type,
        result_value
    )
    SELECT
        io.id,
        gfc.row_letter,
        gfc.column_number,
        'ORIGINAL' AS result_type,
        gfc.result_value
    FROM stg_infodw.guava_files gf
    INNER JOIN stg_infodw.guava_file_contents gfc
        ON gfc.guava_file_id = gf.id
    INNER JOIN prc_infodw.instr_output io
        ON io.plate_id = gf.plate_id
        AND io.result_stage::TEXT = gf.result_stage::TEXT
        AND io.instrument = 'GUAVA'
    WHERE
        gf.load_status = 'loaded'
    ORDER BY
        gf.id,
        gfc.row_letter,
        gfc.column_number;

    -- Insert audit records
    INSERT INTO stg_infodw.guava_file_audit
    (
        guava_file_id,
        action_name,
        action_result
    )
    SELECT
        gf.id,
        'Process data' AS action_name,
        'Success' AS action_result
    FROM stg_infodw.guava_files gf
    WHERE
        gf.load_status = 'loaded';

    -- Update statuses
    UPDATE stg_infodw.guava_files
    SET
        load_status = 'processed'
    WHERE
        load_status = 'loaded';

END;
$BODY$;
