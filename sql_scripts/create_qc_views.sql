CREATE OR REPLACE VIEW stg_infodw.qc_metadata_v
AS SELECT
    pmd.experiment_name,
    SUBSTRING(pmd.experiment_name, 'Bio-\d+') AS experiment_shortname,
    pmd.on_plate_id,
    pmd.orig_sample AS sample,
    COALESCE(pmd.control_subtype, cell_type) AS cell_type
FROM prc_infodw.instr_plate_metadata_mv pmd;
