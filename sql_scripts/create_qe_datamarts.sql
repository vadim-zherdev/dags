CREATE OR REPLACE VIEW infodw.qe_curated_data_v
AS SELECT
    ions.id AS qe_csv_id,
    ios.id AS std_qe_csv_id,
    ions.experiment_name::VARCHAR(1000) AS exp_name,
    ioqcns.metagroup_id::INT AS metagroupid,
    ioqcns.compound::VARCHAR(1000) AS metabolite,
    ioqcs.metagroup_id::INT AS std_metagroupid,
    ioqcns.sample::VARCHAR(1000) AS sample_id,
    ions.plate_id || '_' || ioqcns.row_letter || ioqcns.column_number AS on_plate_id,
    ioqcns.result_value AS intensity,
    ioqcs.sample::VARCHAR(1000) AS std_sample_id,
    ios.plate_id || '_' || ioqcs.row_letter || ioqcs.column_number AS std_on_plate_id,
    ioqcs.result_value AS std_intensity
FROM prc_infodw.instr_output ios
INNER JOIN prc_infodw.instr_output_qe_contents ioqcs
    ON ioqcs.instr_output_id = ios.id
INNER JOIN prc_infodw.instr_output_qe_contents ioqcns
    ON ioqcns.sample = ioqcs.sample
INNER JOIN prc_infodw.instr_output ions
    ON ions.id = ioqcns.instr_output_id
WHERE
    ios.instrument = 'QE'
    AND ios.result_stage = 'ANALYZED_DATA'
    AND ioqcs.is_std
    AND NOT ioqcns.is_std
    AND ios.experiment_id = ions.experiment_id
ORDER BY
    ions.id,
    ioqcns.metagroup_id,
    ioqcns.compound,
    ioqcns.sample;


CREATE OR REPLACE VIEW infodw.qe_non_standard_data_v
AS SELECT
    io.id AS qe_csv_id,
    io.experiment_name::VARCHAR(1000) AS exp_name,
    ioqc.metagroup_id::INT AS metagroupid,
    ioqc.group_id::INT AS groupid,
    ioqc.isotope_label::VARCHAR(1000) AS isotopelabel,
    ioqc.compound::VARCHAR(1000) AS compound,
    0::INT AS is_std,
    ioqc.sample::VARCHAR(1000) AS sample_id,
    io.plate_id || '_' || ioqc.row_letter || ioqc.column_number AS on_plate_id,
    ioqc.result_value AS value,
    SUM(ioqc.result_value) OVER (PARTITION BY ioqc.compound, ioqc.sample) AS pool_total
FROM prc_infodw.instr_output io
INNER JOIN prc_infodw.instr_output_qe_contents ioqc
    ON ioqc.instr_output_id = io.id
WHERE
    io.instrument = 'QE'
    AND io.result_stage = 'ANALYZED_DATA'
    AND NOT ioqc.is_std;
