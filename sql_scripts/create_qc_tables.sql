CREATE SEQUENCE IF NOT EXISTS stg_infodw.qc_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.qc_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.qc_files_id_seq'),
    file_created_at TIMESTAMP NOT NULL,
    file_updated_at TIMESTAMP NOT NULL,
    dotd_acq_date TIMESTAMP NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    plate_id VARCHAR(6) NOT NULL,
    row_letter CHAR(1) NOT NULL,
    column_number SMALLINT NOT NULL,
    replicate stg_infodw.rep_enum  NOT NULL, -- Injection "replicate" as in a or b or unknown
    experiment_name VARCHAR(1024) NOT NULL,
    sample VARCHAR(256) NOT NULL,
    cell_type VARCHAR(256) NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_qc_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_qc_files_src_filename
    ON stg_infodw.qc_files (src_filename);

CREATE TABLE IF NOT EXISTS stg_infodw.qc_file_contents
(
    qc_file_id INT NOT NULL,
    normalized_scaled_vector NUMERIC[] NOT NULL,  -- Spectrum as a normalized, scaled vector
    nz_array INT[] NOT NULL,                      -- A list m/z indexes for the normalized, scaled, vector
    num_centroids INT NOT NULL,                   -- Number of centroids (processed)
    summed_tic NUMERIC NOT NULL,                  -- Summed total ion current (TIC) - processed
    signal_to_noise NUMERIC NOT NULL,             -- Signal-to-noise (S/N)
    acq_date TIMESTAMP NOT NULL,                  -- Date of acquisition
    spectrum_ab_array NUMERIC[] NOT NULL,          -- Spectrum as ab pairs
    spectrum_mz_array NUMERIC[] NOT NULL,          -- Spectrum as m/z pairs
    mz_min INT NOT NULL,                           -- Min. m/z value in spectrum
    mz_max DOUBLE PRECISION NOT NULL,              -- Max. m/z value in spectrum
    mode VARCHAR(4) NOT NULL,                      -- Polarity
    fiamat_tic BIGINT NOT NULL,                    -- TIC (.fiamat file)
    CONSTRAINT pk_qc_file_contents PRIMARY KEY (qc_file_id),
    CONSTRAINT fk_qc_file_contents_qc_files FOREIGN KEY (qc_file_id)
        REFERENCES stg_infodw.qc_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.qc_file_audit
(
    qc_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_qc_file_audit_qc_files FOREIGN KEY (qc_file_id)
        REFERENCES stg_infodw.qc_files (id)
);

CREATE INDEX IF NOT EXISTS idx_qc_file_audit_qc_file_id
    ON stg_infodw.qc_file_audit (qc_file_id);


CREATE SEQUENCE IF NOT EXISTS prc_infodw.qc_output_id_seq;

CREATE TABLE IF NOT EXISTS prc_infodw.qc_output
(
    id INT NOT NULL DEFAULT nextval('prc_infodw.qc_output_id_seq'),
    file_path VARCHAR(1024) NOT NULL,
    file_name VARCHAR(256) NOT NULL,
    on_plate_id VARCHAR(16) NOT NULL,
    experiment_name VARCHAR(1024) NOT NULL,
    short_experiment_name VARCHAR(16) NOT NULL,
    sample VARCHAR(256) NOT NULL,
    cell_type VARCHAR(256) NOT NULL,
    num_centroids INT NOT NULL,
    summed_tic NUMERIC NOT NULL,
    signal_to_noise NUMERIC NOT NULL,
    acq_date TIMESTAMP NOT NULL,
    mz_min INT NOT NULL,
    mz_max DOUBLE PRECISION NOT NULL,
    mode VARCHAR(4) NOT NULL,
    fiamat_tic BIGINT NOT NULL,
    replicate CHAR(1) NOT NULL,
    dotd_acq_date TIMESTAMP NOT NULL,
    CONSTRAINT pk_qc_output PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_qc_output_file_path_file_name
    ON prc_infodw.qc_output ((file_path || '/' || file_name));

CREATE TABLE IF NOT EXISTS prc_infodw.qc_output_arrays
(
    qc_output_id INT NOT NULL,
    normalized_scaled_vector NUMERIC[] NOT NULL,
    nz_array INT[] NOT NULL,
    spectrum_ab_array NUMERIC[] NOT NULL,
    spectrum_mz_array NUMERIC[] NOT NULL,
    CONSTRAINT pk_qc_output_arrays PRIMARY KEY (qc_output_id),
    CONSTRAINT fk_qc_output_arrays_qc_output FOREIGN KEY (qc_output_id)
        REFERENCES prc_infodw.qc_output (id)
);
