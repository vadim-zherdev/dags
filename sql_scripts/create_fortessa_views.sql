CREATE OR REPLACE VIEW stg_infodw.fortessa_missing_files_v
AS SELECT
    exp.recordid AS experiment_id,
    exp.datarecordname AS experiment_name,
    TO_TIMESTAMP(exp.datecreated / 1000) AT TIME ZONE 'UTC' AS exp_created_at,
    sq.plate_id::VARCHAR(32) AS plate_id,
    res.dir || '/' || res.files AS filepath,
    sq.result_stage,
    TO_TIMESTAMP(res.datecreated / 1000) AT TIME ZONE 'UTC' AS plate_created_at
FROM
(
    SELECT
        prf.plateid AS plate_id,
        CASE
            WHEN UPPER(prf.dir) LIKE '%RAW_DATA%' THEN 'RAW_DATA'
            WHEN UPPER(prf.dir) LIKE '%ANALYZED_DATA%' THEN 'ANALYZED_DATA'
            WHEN UPPER(prf.files) LIKE '%.CSV' THEN 'ANALYZED_DATA'
            ELSE NULL
        END::stg_infodw.result_stage_enum AS result_stage,
        MAX(prf.recordid) AS prf_id
    FROM stg_infodw.sapio_plateresultfile prf
    WHERE
        prf.instrumentname LIKE 'FORTESSA%'
        AND prf.dir LIKE '%Lab Data%'
    GROUP BY
        plate_id,
        result_stage
) sq
INNER JOIN stg_infodw.sapio_plateresultfile res
    ON res.recordid = sq.prf_id
INNER JOIN stg_infodw.sapio_elnexperiment exp
    ON exp.recordid = res.experimentrecordid
WHERE
    NULLIF(LTRIM(res.files), '') IS NOT NULL
    AND sq.result_stage = 'ANALYZED_DATA'
    AND NOT EXISTS
    (
        SELECT 1
        FROM stg_infodw.fortessa_files ff
        WHERE
            ff.plate_id = sq.plate_id
            AND ff.result_stage = sq.result_stage
    )
ORDER BY
    exp_created_at ASC;
