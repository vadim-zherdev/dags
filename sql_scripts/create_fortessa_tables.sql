CREATE SEQUENCE IF NOT EXISTS stg_infodw.fortessa_files_id_seq;

CREATE TABLE IF NOT EXISTS stg_infodw.fortessa_files
(
    id INT NOT NULL DEFAULT nextval('stg_infodw.fortessa_files_id_seq'),
    experiment_id INT NOT NULL,
    experiment_name TEXT NOT NULL,
    exp_created_at TIMESTAMP NOT NULL,
    plate_id VARCHAR(32) NOT NULL,
    src_filename VARCHAR(1024) NOT NULL,
    result_stage stg_infodw.result_stage_enum NOT NULL,
    plate_created_at TIMESTAMP NOT NULL,
    load_status stg_infodw.entry_status_enum NOT NULL,
    data_source stg_infodw.data_source_enum NOT NULL,
    CONSTRAINT pk_fortessa_files PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_fortessa_files_plate_id_result_stage
    ON stg_infodw.fortessa_files (plate_id, result_stage);

CREATE TABLE IF NOT EXISTS stg_infodw.fortessa_file_contents
(
    fortessa_file_id INT NOT NULL,
    sample VARCHAR(256) NOT NULL,
    result_field VARCHAR(256) NOT NULL,
    result_value DOUBLE PRECISION NULL,
    CONSTRAINT pk_fortessa_file_contents PRIMARY KEY (fortessa_file_id, sample, result_field),
    CONSTRAINT fk_fortessa_file_contents_fortessa_files FOREIGN KEY (fortessa_file_id)
        REFERENCES stg_infodw.fortessa_files (id)
);

CREATE TABLE IF NOT EXISTS stg_infodw.fortessa_file_audit
(
    fortessa_file_id INT NOT NULL,
    action_name VARCHAR(256) NOT NULL,
    username VARCHAR(256) NULL DEFAULT current_user,
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,
    action_result TEXT NULL,
    CONSTRAINT fk_fortessa_file_audit_fortessa_files FOREIGN KEY (fortessa_file_id)
        REFERENCES stg_infodw.fortessa_files (id)
);

CREATE INDEX IF NOT EXISTS idx_fortessa_file_audit_fortessa_file_id
    ON stg_infodw.fortessa_file_audit (fortessa_file_id);

