from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator,
)
from airflow.models import DAG
from airflow.operators.python_operator import (
    PythonOperator, BranchPythonOperator)
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
import logging
from numpy import nan
from pandas import read_csv
from sqlalchemy.exc import ProgrammingError, IntegrityError
from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.slack_alert import task_fail_slack_alert
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import (
    get_ds_helper, get_experiment_name, camel_to_snake, egnytize,
    load_dataframe, get_slack_conn)
from darwinsync_api_helper import FileNotFoundError

DB_CONNECTION_ID = 'informatics_db'
INSTRU_NAME = 'QE01'
EXP_NAME_SEARCH_PATTERN = (
    f'(^[a-zA-Z]:\\\\.*\\\\((Bio-[0-9]{{3}}).*)\\\\{INSTRU_NAME}\\\\).*'
)
SUCCESS_STATUS = 'Success'

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='qe_results_parse',
    default_args=args,
    schedule_interval='@hourly',
    catchup=False
)

SET_FILE_STATUS_QUERY = '''
    UPDATE
        stg_infodw.qe_files
    SET
        load_status = %(status)s
    WHERE
        src_filename = %(filepath)s
    AND
        load_status = 'started'
'''


def add_to_audit(file_id, action, result):
    """Insert a record to the audit table"""
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    hook.insert_rows(
        'stg_infodw.qe_file_audit',
        [(file_id, action, result)],
        target_fields=['qe_file_id', 'action_name', 'action_result'],
        commit_every=1)


def set_status(status, file_path=None):
    """Set status of a file

    If current status is not started, don't change it.
    """
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    hook.run(
        SET_FILE_STATUS_QUERY,
        parameters={'filepath': file_path, 'status': status},
        autocommit=True)


MAVEN_FILE_INDEX_COLUMNS = [
    'label', 'metaGroupId', 'groupId', 'goodPeakCount',
    'medMz', 'medRt', 'maxQuality', 'isotopeLabel', 'compound',
    'compoundId', 'formula', 'expectedRtDiff', 'ppmDiff',
    'parent',
]

WELL_COORDINATES_EXTRACT_PATTERN = (
    r'^P-[0-9]+_(?P<row_letter>[A-Z])(?P<column_number>[0-9]{1,2})')


GET_FILE_ID_QUERY = '''
    SELECT
        id
    FROM
        stg_infodw.qe_create_analyzed_file
        (
            exp_name := %(experiment_name)s,
            filename := %(src_file_path)s,
            src := %(src)s
        )
'''


def get_new_files():
    """Find on BioBright all csv files from ANALYZED_DATA subfolders
     and save them to the database

    If nothing found, return 'finish' task name,
    otherwize return 'parse_qe_analyzed_data' task name
    """
    ds_api_helper = get_ds_helper()
    files_data, files_included = ds_api_helper.get_files_index(
        client_path=f'{INSTRU_NAME}\\\\ANALYZED_DATA',
        client_file_ext='.csv', and_included=True, )
    files_paths = [
        egnytize(item['attributes']['client_path'])
        for item in files_data
    ]
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    for file_path in files_paths:
        experiment_name = get_experiment_name(file_path)
        file_data = hook.get_first(
            GET_FILE_ID_QUERY,
            parameters={
                'experiment_name': experiment_name,
                'src_file_path': file_path,
                'src': 'biobright',
            })
        logging.debug(
            f'File {file_path} ' + (
                'added' if file_data else 'already processed, skipped'))


def process_result(
        file_id, file_path, stage, message, result, status=None,
        exception=None):
    """Do everything needed when error occured

    Log the error
    Set file status to 'failed'
    Add row to audit table
    Add value to results dict
    """
    log_method = logging.error if exception else logging.info
    log_method(f'{message} for "{file_path}": {exception}')
    set_status(
        status if not exception and status else 'failed', file_path=file_path)
    add_to_audit(file_id, stage, message)
    result[file_path] = message


def parse_peaks(file_id, data_frame, file_path, hook, result):
    """Parse peaks from given DataFrame and save them in qe_files_peaks

    Return boolean success flag
    """
    data_frame.columns = camel_to_snake(MAVEN_FILE_INDEX_COLUMNS)
    data_frame = data_frame.assign(qe_file_id=file_id)
    data_frame = data_frame.assign(
        is_std=lambda x: x.formula.isin([None, '', nan]))
    data_frame.rename(
        columns={'label': 'peak_label', 'meta_group_id': 'metagroup_id'},
        inplace=True)
    try:
        load_dataframe(
            data_frame, hook, 'qe_file_peaks', 'stg_infodw',
            reorder_columns=True)
        return True
    except (ProgrammingError, IntegrityError) as e:
        process_result(
            file_id, file_path, 'Maven results', 'Peak parse error', result,
            exception=e)
        return False


def parse_data(file_id, data_frame, file_path, hook, values_columns, result):
    """Parse Maven results data from the given DataFrame and save it into
    qe_files_data table

    Return boolean success flag
    """
    df_data = data_frame.melt(
        id_vars=['metaGroupId', 'groupId'],
        value_vars=values_columns, var_name='sample_id',
        value_name='result_value')
    df_data.rename(
        columns={'metaGroupId': 'metagroup_id', 'groupId': 'group_id'},
        inplace=True)
    df_data = df_data.assign(qe_file_id=file_id)
    df_data = df_data.join(
        df_data['sample_id'].str.extract(
            WELL_COORDINATES_EXTRACT_PATTERN))
    try:
        load_dataframe(
            df_data, hook, 'qe_file_data', 'stg_infodw', reorder_columns=True)
        return True
    except (ProgrammingError, IntegrityError) as e:
        process_result(
            file_id, file_path, 'Load Maven results', 'Data parsing error',
            result, exception=e)
        return False


FILES_FIELDS = [
    'id',
    'src_filename',
]
GRAB_FILES_QUERY = f'''
    SELECT
        {','.join(FILES_FIELDS)}
    FROM stg_infodw.qe_start_analyzed_files()
'''
FILE_ID_POS = FILES_FIELDS.index('id')
FILE_NAME_POS = FILES_FIELDS.index('src_filename')


def parse_analyzed_files(**context):
    """Download, parse and load into DB .csv-files from qe_files table

    Get experiments list from the previous task through xcom
    Skip files that are already processed and have corresponding status.
    Extract plate ID and put in into qe_files table
    Extract peaks info and put it into qe_file_peaks table
    Extract data, unpivot it, extract well positions and put
    into qe_file_data table
    """
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    files_records = hook.get_records(GRAB_FILES_QUERY)
    ds_api_helper = get_ds_helper()
    logging.info(f'{len(files_records)} analyzed files found for processing')
    output = {}
    result = 'finish'
    for file_record in files_records:
        result = 'process_qe_analyzed_data'
        file_path = file_record[FILE_NAME_POS]
        file_id = file_record[FILE_ID_POS]
        bb_file_path = file_path.replace('/', r'\\')
        logging.debug(f"Processing file {bb_file_path}")
        try:
            file_content = ds_api_helper.get_file(client_path=bb_file_path)
        except FileNotFoundError as e:
            process_result(
                file_id, file_path, 'Load Maven results',
                'Not found on BioBright', output, exception=e)
            continue
        df_full = read_csv(file_content)
        if any([item not in df_full.columns
                for item in MAVEN_FILE_INDEX_COLUMNS]):
            process_result(
                file_id, file_path, 'Load Maven results', 'Unexpected format',
                output)
            continue
        if any(df_full.duplicated(['metaGroupId', 'groupId'])):
            process_result(
                file_id, file_path, 'Load Maven results',
                'Duplicate rows for metaGroupId/groupId', output)
            continue
        values_columns = [item for item in df_full.columns
                          if item not in MAVEN_FILE_INDEX_COLUMNS]
        df_peaks = df_full.drop(columns=values_columns)
        if not parse_peaks(file_id, df_peaks, file_path, hook, output):
            continue
        add_to_audit(
            file_id, 'Load Maven results', 'Peaks imported')
        df_data = df_full.drop(
            columns=[item for item in MAVEN_FILE_INDEX_COLUMNS
                     if item not in ['metaGroupId', 'groupId']])
        if not parse_data(file_id, df_data, file_path,
                          hook, values_columns, output):
            continue
        process_result(
            file_id, file_path, 'Load Maven results', SUCCESS_STATUS, output,
            status='loaded')
    context['task_instance'].xcom_push(key='output', value=output)
    return result


def merge_output(**context):
    """
    Merge output of parse and process stages
    and prepare it for showing in Slack
    """
    parse_files_output = context['task_instance'].xcom_pull(
        task_ids='parse_qe_analyzed_data', key='output')
    process_files_output = context['task_instance'].xcom_pull(
        task_ids='process_qe_analyzed_data')
    merged_output = parse_files_output
    merged_output.update({row[0]: row[1] for row in process_files_output})
    return '\n'.join(['*{result}*: `{filename}`{error}'.format(
        result=(f':white_check_mark: {SUCCESS_STATUS}'
                if result == SUCCESS_STATUS else ':x: Error'),
        filename=filename,
        error=f' ({result})' if result != SUCCESS_STATUS else '')
        for filename, result in merged_output.items()])


if __name__.startswith('unusual_prefix'):
    get_new_files_task = PythonOperator(
        task_id='get_qe_maven_results',
        python_callable=get_new_files,
        dag=dag
    )

    parse_analyzed_files_task = BranchPythonOperator(
        task_id='parse_qe_analyzed_data',
        python_callable=parse_analyzed_files,
        provide_context=True,
        dag=dag
    )

    process_analyzed_files_task = PostgresOutputOperator(
        task_id='process_qe_analyzed_data',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='''
            SELECT
                filename,
                output
            FROM stg_infodw.qe_process_analyzed_files()
            ''',
        dag=dag
    )

    merge_output_task = PythonOperator(
        task_id='merge_qe_output',
        python_callable=merge_output,
        provide_context=True,
        dag=dag
    )

    slack_conn = get_slack_conn(f'{dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: QE analyzed files were loaded and processed. "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull(task_ids='merge_qe_output') }}"),
        username='Airflow',
        dag=dag
    )

    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )

    get_new_files_task >> parse_analyzed_files_task >> \
        process_analyzed_files_task >> merge_output_task >> \
        slack_report_task >> finish_task
    parse_analyzed_files_task >> finish_task
