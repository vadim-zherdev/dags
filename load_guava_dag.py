"""DAG that loads GUAVA data from files into the database."""
import logging
from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator
)
from airflow.models import DAG, Variable
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import (
    BranchPythonOperator, PythonOperator
)
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago

from extensions.guava_utils import parse_guava_results, GUAVA_FILES_QUERY
from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.slack_alert import task_fail_slack_alert, prepare_report
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import get_slack_conn

DB_CONNECTION_ID = 'informatics_db'
SUCCESS_STATUS = 'OK'
FILES_SOURCE = 'egnyte'
COMPRESS_OUTPUT_FILES_COUNT = 30

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert,
}

guava_dag = DAG(
    dag_id='load_guava',
    default_args=args,
    schedule_interval='@hourly',
    catchup=False,
)


def parse_files(**context):
    """Get files to be processed, call the method to process them,
        push its results through xcom.
    """
    db_hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    files_data = db_hook.get_records(GUAVA_FILES_QUERY)
    if not files_data:
        logging.info('No files to process, skipping to finish')
        return 'finish'
    db_hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    check_bb = Variable.get('guava_biobright_check', default_var='false')
    output = parse_guava_results(
        files_data, db_hook, SUCCESS_STATUS, check_bb=check_bb == 'true')
    context['task_instance'].xcom_push(key='output', value=output)
    return 'process_guava_files'


def merge_output(**kwargs):
    """Get output of the previous stage and prepare it for showing in Slack."""
    task_instance = kwargs['task_instance']
    parse_files_output = task_instance.xcom_pull(
        task_ids='parse_guava_files', key='output')
    process_files_output = task_instance.xcom_pull(
        task_ids='process_guava_files')
    joint_output = parse_files_output
    joint_output.update({item[0]: item[1] for item in process_files_output})
    summary, full_report = prepare_report(
        joint_output, SUCCESS_STATUS, COMPRESS_OUTPUT_FILES_COUNT)
    task_instance.xcom_push(key='summary', value=summary)
    return full_report


if __name__.startswith('unusual_prefix'):
    create_files_task = PostgresOperator(
        task_id='create_guava_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT stg_infodw.guava_create_files(%(source)s)',
        parameters={'source': FILES_SOURCE},
        dag=guava_dag,
    )
    parse_files_task = BranchPythonOperator(
        task_id='parse_guava_files',
        python_callable=parse_files,
        provide_context=True,
        dag=guava_dag,
    )
    process_files_task = PostgresOutputOperator(
        task_id='process_guava_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT filename, output FROM stg_infodw.guava_process_files()',
        dag=guava_dag,
    )
    merge_output_task = PythonOperator(
        task_id='merge_output',
        python_callable=merge_output,
        provide_context=True,
        dag=guava_dag,
    )
    slack_conn = get_slack_conn(f'{guava_dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: GUAVA files were loaded and processed "
            "({{ task_instance.xcom_pull(task_ids='merge_output',"
            "key='summary') }}). "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull(task_ids='merge_output') }}"),
        username='Airflow',
        dag=guava_dag,
    )
    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=guava_dag,
    )
    create_files_task >> parse_files_task >> process_files_task >> \
        merge_output_task >> slack_report_task >> finish_task
    parse_files_task >> finish_task
