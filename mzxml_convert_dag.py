from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator,
)
from airflow.models import DAG, Variable
from airflow.operators.python_operator import (
    PythonOperator,
    BranchPythonOperator,
)
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from io import BytesIO
import logging
from os import path
from pandas import read_csv
from re import search
from tempfile import TemporaryDirectory
from collections import defaultdict
from extensions.slack_alert import task_fail_slack_alert, prepare_report
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.mzxml_utils import (
    get_files_from_bb_by_exp,
    get_files_from_egnyte_by_exp,
    call_msconvert,
    DownloadError,
    EgnyteError
)
from extensions.utils import (
    egnytize,
    get_ds_helper,
    get_egnyte_client,
    get_slack_conn,
)


INSTRU_NAME = 'QE01'
MZXML_SUBFOLDER = 'MAVEN_DATA'
DB_CONNECTION_ID = 'informatics_db'
BIOBRIGHT_CONNECTION_ID = 'biobright'
SUCCESS_STATUS = 'Success'
ON_PLATE_ID_PARSE_PATTERN = r'^(P-\d+)_([A-Z])(\d{1,2}).*'
COMPRESS_OUTPUT_FILES_COUNT = 30


args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='qe_mzxml_convert',
    default_args=args,
    schedule_interval='30 * * * *',
    catchup=False,
    max_active_runs=1,
)


def upload_to_egnyte(egnyte_client, egnyte_path, local_path):
    """Upload file to Egnyte at the given path"""
    if not local_path:
        return
    instru_folder = f'/{INSTRU_NAME}/'
    egnyte_path = egnyte_path[
            :egnyte_path.find(instru_folder) + len(instru_folder)
        ] + MZXML_SUBFOLDER
    local_folder, local_filename = path.split(local_path)
    new_file_full_path = f'{egnyte_path}/{local_filename}'
    logging.debug(f'Uploading file_to {new_file_full_path}...')
    file_obj = egnyte_client.file(new_file_full_path)
    with open(local_path, 'rb') as f:
        file_obj.upload(f)


SET_FILE_STATUS_QUERY = '''
    UPDATE stg_infodw.qe_files
    SET
        load_status = %(status)s
    WHERE
        src_filename = %(filepath)s
        AND result_stage = 'RAW_DATA'
    RETURNING
        experiment_id,
        worklist_filename
'''

SET_FILE_STATUS_BATCH_QUERY = '''
    UPDATE stg_infodw.qe_files
    SET
        load_status = %(status)s
    WHERE
        experiment_id =  %(exp_id)s
        AND result_stage = 'RAW_DATA'
'''

SET_WORKLIST_STATUS_QUERY = '''
    UPDATE stg_infodw.qe_worklists
    SET
        status = %(status)s
    WHERE
        experiment_id = %(exp_id)s
        AND src_filename = %(worklist_filename)s
    RETURNING
        experiment_id
'''

SET_WORKLIST_STATUS_BATCH_QUERY = '''
    UPDATE stg_infodw.qe_worklists
    SET
        status = %(status)s
    WHERE
        experiment_id = %(exp_id)s
'''

SET_EXP_STATUS_QUERY = '''
    UPDATE stg_infodw.qe_experiments
    SET
        status = %(status)s
    WHERE
        experiment_id = %(exp_id)s
'''


def set_failed_status(exp_id=None, worklist_filename=None, file_path=None):
    """Set "failed" status to a file, a worklist and/or an experiment

    Propagates "failed" status from the file to its worklist and the experiment
    """
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    if file_path:
        file_path_for_SQL = file_path.replace('\\\\', '\\')
        file_data = hook.get_first(
            SET_FILE_STATUS_QUERY,
            parameters={'filepath': file_path_for_SQL, 'status': 'failed'})
        logging.info(f'File "{file_path}" marked as failed')
        if not file_data:
            return
        worklist_filename = worklist_filename or file_data[1]
        exp_id = exp_id or file_data[0]
    if exp_id and worklist_filename:
        worklist_data = hook.get_first(
            SET_WORKLIST_STATUS_QUERY,
            parameters={
                'exp_id': exp_id,
                'worklist_filename': worklist_filename,
                'status': 'failed'})
        logging.info(
            f'Worklist for plate "{worklist_filename}" marked as failed')
        if not worklist_data:
            return
        exp_id = exp_id or worklist_data[0]
    if exp_id:
        hook.run(
            SET_EXP_STATUS_QUERY,
            parameters={'exp_id': exp_id, 'status': 'failed'},
            autocommit=True)
        logging.info(f'Experiment "{exp_id}" marked as failed')


def batch_set_failed_status(exp_id):
    """Set experiment and all it's descendants status to failed"""
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    hook.run(
        SET_FILE_STATUS_BATCH_QUERY,
        parameters={'exp_id': exp_id, 'status': 'failed'},
        autocommit=True)
    hook.run(
        SET_WORKLIST_STATUS_BATCH_QUERY,
        parameters={'exp_id': exp_id, 'status': 'failed'},
        autocommit=True)
    hook.run(
        SET_EXP_STATUS_QUERY,
        parameters={'exp_id': exp_id, 'status': 'failed'},
        autocommit=True)
    logging.info(f'Experiment "{exp_id}" marked as failed')


def convert(egnyte_client, egnyte_path, local_path):
    """Convert .raw file into two .mzXML files (neg & pos polarity)

    Return boolean success flag
    """
    # TODO: call it in parallel maybe
    for polarity in ['positive', 'negative']:
        converted_file_name = call_msconvert(local_path, polarity)
        if not converted_file_name:
            return False
        converted_file_path = path.join(
            path.split(local_path)[0], converted_file_name)
        upload_to_egnyte(egnyte_client, egnyte_path, converted_file_path)
    logging.info(f'File "{path.split(local_path)[1]}" processed')
    return True


EXP_TABLE_COLUMNS = [
    'experiment_id',
    'experiment_name',
    'exp_created_at',
    'src_path',
    'status',
]
EXP_TABLE_PATH_POS = EXP_TABLE_COLUMNS.index('src_path')
EXP_TABLE_ID_POS = EXP_TABLE_COLUMNS.index('experiment_id')
EXP_TABLE_NAME_POS = EXP_TABLE_COLUMNS.index('experiment_name')
EXP_TABLE_CREATED_POS = EXP_TABLE_COLUMNS.index('exp_created_at')
EXP_INSERT_QUERY = f'''
    SELECT
        {','.join(EXP_TABLE_COLUMNS)}
    FROM stg_infodw.qe_insert_missing_experiments
    (
        all_exps := %(all_exps)s,
        exp_name := %(exp_name)s
    )
'''


def get_new_experiments(**context):
    """Check the database for QE experiments absent in "qe_experiments" table
       and insert tnem in 'crested' status

    Push the experiments list through xcom
    Return the name of the next task: "get_qe_worklists"
    if some experiments found, "finish" otherwise.
    """
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    process_all_experiments = Variable.get(
        'qe_process_all_experiments', default_var='true')
    experiment_name = Variable.get('qe_experiment_name', default_var='%')
    exps_data = hook.get_records(
        EXP_INSERT_QUERY,
        parameters={
            'all_exps': process_all_experiments,
            'exp_name': experiment_name,
        }
    )
    if len(exps_data):
        logging.info(
            f'Found {len(exps_data)} new experiments. '
            'Moving to process step.')
        context['task_instance'].xcom_push('exps_data', exps_data)
        return 'get_new_qe_files'
    else:
        logging.info('No new experiments found. Moving to finish.')
        return 'finish'


WORKLISTS_TABLE_COLUMNS = [
    'experiment_id',
    'src_filename',
    'plate_id',
    'status',
]


RAW_FILES_COLUMNS_NAMES = [
    'experiment_id',
    'experiment_name',
    'exp_created_at',
    'worklist_filename',
    'plate_id',
    'row_letter',
    'column_number',
    'src_filename',
    'load_status',
    'result_stage',
    'data_source',
]
RAW_FILE_NAME_POS = RAW_FILES_COLUMNS_NAMES.index('src_filename')
RAW_FILE_EXP_ID_POS = RAW_FILES_COLUMNS_NAMES.index('experiment_id')
RAW_FILES_INSERT_QUERY = f"""
    INSERT INTO stg_infodw.qe_files
    (
        {','.join(RAW_FILES_COLUMNS_NAMES)}
    )
    VALUES
    (
        %(experiment_id)s,
        %(experiment_name)s,
        %(exp_created_at)s,
        %(worklist_filename)s,
        %(plate_id)s,
        %(row_letter)s,
        %(column_number)s,
        %(src_filename)s,
        %(load_status)s,
        'RAW_DATA',
        'biobright'
    )
    ON CONFLICT (src_filename) DO UPDATE SET load_status = %(load_status)s
    RETURNING
        id
"""

INSERT_AUDIT_BATCH_QUERY = '''
    INSERT INTO stg_infodw.qe_file_audit
    (
        qe_file_id,
        action_name,
        action_result
    )
    SELECT
        id,
        %(action)s,
        %(result)s
    FROM stg_infodw.qe_files
    WHERE
        experiment_id =  %(exp_id)s
        AND result_stage = 'RAW_DATA'
'''


def add_to_audit(file_id, action, result):
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    hook.insert_rows(
        'stg_infodw.qe_file_audit',
        [(file_id, action, result)],
        target_fields=['qe_file_id', 'action_name', 'action_result'],
        commit_every=1)


def batch_add_to_audit(exp_id, action, result):
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    hook.run(
        INSERT_AUDIT_BATCH_QUERY,
        parameters={'exp_id': exp_id, 'action': action, 'result': result},
        autocommit=True)


WORKLIST_INSERT_QUERY = '''
    INSERT INTO stg_infodw.qe_worklists
    (
        experiment_id,
        src_filename,
        plate_id,
        status
    )
    VALUES
    (
        %(experiment_id)s,
        %(src_filename)s,
        %(plate_id)s,
        'started'
    )
    ON CONFLICT (experiment_id, src_filename) DO UPDATE
        SET status = CASE
            WHEN qe_worklists.status = 'created' THEN 'started'
            WHEN qe_worklists.status = 'invalidated' THEN 'started'
            ELSE qe_worklists.status
        END
    RETURNING
        status,
        src_filename
'''


def get_new_files(**context):
    """Get worklists and raw files for new experiments from Egnyte and
       BioBright and insert into the tables
    """
    ds_api_helper = get_ds_helper(conn_id=BIOBRIGHT_CONNECTION_ID)
    egnyte_client = get_egnyte_client()
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    exps_data = context['task_instance'].xcom_pull(
        task_ids='get_new_qe_experiments', key='exps_data')
    downstream_task_id = 'recalc_qe_statuses'
    for exp_data in exps_data:
        exp_id = exp_data[EXP_TABLE_ID_POS]
        exp_path = exp_data[EXP_TABLE_PATH_POS]
        logging.debug(f'Processing experiment "{exp_path}"')
        set_status('started', exp_id=exp_id)
        worklists = (get_files_from_bb_by_exp(exp_path, ds_api_helper, '.csv',
                                              sub_path='WORKLIST\\\\')
                     or get_files_from_egnyte_by_exp(exp_path, egnyte_client,
                                                     '.csv', 'WORKLIST'))
        if not worklists:
            logging.warning(f'No worklists found for {exp_path}, skipped')
            set_failed_status(exp_id=exp_id)
            continue
        for file_obj in worklists:
            logging.debug(f'Processing worklist {file_obj.path}')
            _, file_ext = path.splitext(file_obj.path)
            if file_ext != '.csv':
                logging.debug(f'Skipping {file_obj.path}')
                continue
            try:
                with file_obj.download() as download:
                    worklist_content = download.read()
            except (DownloadError, EgnyteError):
                logging.warning(f'Error downloading {file_obj.path}, skipping')
                set_failed_status(exp_id=exp_id)
                continue
            df = read_csv(BytesIO(worklist_content), header=1)
            if not len(df.index):
                logging.warning(f'No data in {file_obj.path}, skipping')
                set_failed_status(exp_id=exp_id)
                continue
            if not all([column in list(df)
                        for column in ['File Name', 'Position']]):
                logging.warning(
                    '"File Name" and "Position" not found '
                    f'in {file_obj.path}')
                set_failed_status(exp_id=exp_id)
                continue
            match = search(ON_PLATE_ID_PARSE_PATTERN, df.at[0, 'File Name'])
            if not match:
                logging.warning(
                    f"Plate ID not recognized in {df.at[0, 'File Name']}")
                set_failed_status(exp_id=exp_id)
                continue
            plate_id = match.group(1)
            worklist_record = hook.get_first(
                WORKLIST_INSERT_QUERY,
                parameters={
                    'experiment_id': exp_id,
                    'src_filename': file_obj.path,
                    'plate_id': plate_id,
                })
            if worklist_record[0] == 'processed':
                logging.info(
                    f'Worklist {file_obj.path} already processed, '
                    f'skipped')
                continue
            for index, row in df.iterrows():
                match = search('.*:([A-Z])([0-9]{1,2})', row['Position'])
                if not match:
                    logging.warning(
                        f'Well not recognized in {row["Position"]}, skipped')
                    set_failed_status(
                        exp_id=exp_id, worklist_filename=worklist_record[1])
                    continue
                row_letter = match.group(1)
                column_number = match.group(2)
                src_filename = egnytize(
                    row['Path'].rstrip('\\/') + '/' +
                    row['File Name'] + '.raw')
                query_result = hook.get_first(
                    RAW_FILES_INSERT_QUERY,
                    parameters={
                        'experiment_id': exp_data[EXP_TABLE_ID_POS],
                        'experiment_name': exp_data[EXP_TABLE_NAME_POS],
                        'exp_created_at': exp_data[EXP_TABLE_CREATED_POS],
                        'worklist_filename': file_obj.path,
                        'plate_id': plate_id,
                        'row_letter': row_letter,
                        'column_number': column_number,
                        'src_filename': src_filename,
                        'load_status': 'created',
                    }
                )
                downstream_task_id = 'convert_qe_files'
                if query_result:
                    file_id = query_result[0]
                    add_to_audit(file_id, 'Insert/Update', 'Success')
                    logging.debug(
                        f'File {src_filename} inserted/updated '
                        'with status "created"')
                else:
                    logging.debug(f'File {src_filename} already present')
    return downstream_task_id


def set_status(status, exp_id=None, plate_id=None, file_path=None):
    """Set status of experiment, worklist, file from started to processed

    If current status is not started, don't change it.

    If file path is specified, set the file's status.
    If experiment ID and plate ID are both specified, set the worklist's
    status.
    If experiment ID is specified alone, set its status only.
    """
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    if file_path:
        hook.run(
            SET_FILE_STATUS_QUERY,
            parameters={'filepath': file_path, 'status': status},
            autocommit=True)
    if exp_id:
        if plate_id:
            hook.run(
                SET_WORKLIST_STATUS_QUERY,
                parameters={
                    'exp_id': exp_id,
                    'plate_id': plate_id,
                    'status': status,
                },
                autocommit=True
            )
        else:
            hook.run(
                SET_EXP_STATUS_QUERY,
                parameters={'exp_id': exp_id, 'status': status},
                autocommit=True)


EXP_PATH_PREFIX = '\\/Shared\\/Lab Data\\/Sapio\\/'
EXP_NAME_SEARCH_PATTERN = (
    f'({EXP_PATH_PREFIX}.*\\/Bio-[0-9]+.*\\/{INSTRU_NAME}\\/).*'
)
EXP_NAME_ALT_SEARCH_PATTERN = r'/(Bio-[0-9]+.*)/'


def get_exp_name(file_path):
    """Return experiment parth of the file name"""
    match = search(EXP_NAME_SEARCH_PATTERN, file_path)
    if match:
        return match.group(1)
    else:
        match = search(EXP_NAME_ALT_SEARCH_PATTERN, file_path)
        return f'/{match.group(1)}/{INSTRU_NAME}/' if match else None


UPDATE_FILES_STATUS_QUERY = f'''
    UPDATE stg_infodw.qe_files
    SET
        load_status = 'started'
    WHERE
        load_status IN ('created', 'invalidated')
        AND result_stage = 'RAW_DATA'
    RETURNING
        {','.join(RAW_FILES_COLUMNS_NAMES)}, id
'''
RAW_FILE_ID_POS = len(RAW_FILES_COLUMNS_NAMES)


def convert_files():
    """Get raw files list from get_worklists_task and convert them all

    Each file gets downloaded from BioBright, converted by msconvert
    and uploaded to Egnyte
    """
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    ds_api_helper = get_ds_helper(conn_id=BIOBRIGHT_CONNECTION_ID)
    egnyte_client = get_egnyte_client()
    files_data = hook.get_records(UPDATE_FILES_STATUS_QUERY)
    result = {}
    if not files_data:
        logging.info('No files found for conversion, exiting')
        return result

    exp_groups = defaultdict(list)
    for file_data in files_data:
        exp_groups[file_data[RAW_FILE_EXP_ID_POS]].append(file_data)

    for exp_id, exp_files_data in exp_groups.items():
        logging.debug(f'Processing experiment "{exp_id}"')
        exp_path = None
        for file_data in exp_files_data:
            exp_path = get_exp_name(file_data[RAW_FILE_NAME_POS])
            if exp_path:
                break
        if not exp_path:
            logging.warning(
                '''Experiment path could not be extracted '''
                f'''for experiment_id={exp_id}''')
            batch_add_to_audit(exp_id, 'Download',
                               'Experiment path not extracted')
            batch_set_failed_status(exp_id)
            result.update({
                file_data[RAW_FILE_NAME_POS]: 'Experiment path not extracted'
                for file_data in exp_files_data})
            continue
        raw_files = (get_files_from_bb_by_exp(exp_path, ds_api_helper, '.raw')
                     or get_files_from_egnyte_by_exp(exp_path,
                                                     egnyte_client, '.raw'))
        if len(raw_files) == 0:
            logging.warning(
                f'No files were found for experiment_id={exp_id}')
            batch_add_to_audit(exp_id, 'Download',
                               'Not found on BioBright/Egnyte')
            batch_set_failed_status(exp_id)
            result.update({
                file_data[RAW_FILE_NAME_POS]: 'Not found on BioBright/Egnyte'
                for file_data in exp_files_data})
            continue
        logging.info(
            f'{len(raw_files)} files found for experiment {exp_id}')

        for file_data in exp_files_data:
            file_name = file_data[RAW_FILE_NAME_POS]
            file_id = file_data[RAW_FILE_ID_POS]
            raw_file_name = file_name.split('/')[-1]
            raw_file_path = file_name[:-len(raw_file_name)]
            matched_files = [raw_file for raw_file in raw_files
                             if raw_file.name == raw_file_name]
            if len(matched_files) > 1:
                logging.error(f'Many files found for {file_name}')
                add_to_audit(
                    file_id, 'Download',
                    f'1 file expected, many files found')
                set_failed_status(file_path=file_name)
                result[file_name] = 'Ambiguous file'
                continue
            elif len(matched_files) == 0:
                logging.info(f'File {file_name} not found on BioBright/Egnyte')
                set_failed_status(file_path=file_name)
                add_to_audit(file_id, 'Download',
                             'Not found on BioBright/Egnyte')
                result[file_name] = 'Not found on BioBright/Egnyte'
                continue
            with TemporaryDirectory() as temp_dir:
                local_path = path.join(temp_dir, raw_file_name)
                try:
                    with matched_files[0].download() as buffer,\
                            open(local_path, 'wb+') as f:
                        for chunk in buffer.iter_content(chunk_size=8192):
                            if chunk:  # filter out keep-alive new chunks
                                f.write(chunk)
                except (DownloadError, EgnyteError):
                    logging.warning(f'Download failed for file "{file_name}"')
                    set_failed_status(file_path=file_name)
                    add_to_audit(file_id, 'Download', 'Failure')
                    result[file_name] = 'Download failed'
                    continue
                conversion_ok = convert(egnyte_client, raw_file_path,
                                        local_path)
            if conversion_ok:
                set_status('processed', file_path=file_name)
                add_to_audit(file_id, 'Download', 'Success')
            else:
                set_failed_status(file_path=file_name)
            status = (SUCCESS_STATUS if conversion_ok
                      else 'Conversion failure')
            add_to_audit(file_id, 'Conversion', status)
            result[file_name] = status

    return result


def prepare_slack_report(**context):
    """
    Prepare files conversion results for showing in Slack
    """
    task_instance = context['task_instance']
    convert_files_output = task_instance.xcom_pull(task_ids='convert_qe_files')
    summary, full_report = prepare_report(
        convert_files_output, SUCCESS_STATUS, COMPRESS_OUTPUT_FILES_COUNT)
    task_instance.xcom_push(key='summary', value=summary)
    return full_report


if __name__.startswith('unusual_prefix'):
    get_new_experiments_task = BranchPythonOperator(
        task_id='get_new_qe_experiments',
        python_callable=get_new_experiments,
        provide_context=True,
        dag=dag
    )
    get_new_files_task = BranchPythonOperator(
        task_id='get_new_qe_files',
        python_callable=get_new_files,
        provide_context=True,
        dag=dag
    )
    convert_files_task = PythonOperator(
        task_id='convert_qe_files',
        python_callable=convert_files,
        dag=dag
    )
    recalc_statuses_task = PostgresOperator(
        task_id="recalc_qe_statuses",
        postgres_conn_id=DB_CONNECTION_ID,
        sql="SELECT stg_infodw.qe_recalc_statuses()",
        autocommit=True,
        dag=dag
    )
    prepare_report_task = PythonOperator(
        task_id='prepare_qe_report',
        python_callable=prepare_slack_report,
        provide_context=True,
        dag=dag,
    )
    slack_conn = get_slack_conn(f'{dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: mzXML files were converted "
            "({{ task_instance.xcom_pull(task_ids='prepare_qe_report',"
                "key='summary') }}). "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull(task_ids='prepare_qe_report') }}"),
        username='Airflow',
        dag=dag
    )
    finish_task = DummyOperator(  # TODO: move to shared code?
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )

    get_new_experiments_task >> finish_task
    get_new_experiments_task >> get_new_files_task >> recalc_statuses_task
    get_new_files_task >> convert_files_task >> recalc_statuses_task >> \
        finish_task
    convert_files_task >> recalc_statuses_task
    convert_files_task >> prepare_report_task >> slack_report_task >> \
        finish_task
