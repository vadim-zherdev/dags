from json import dumps
from openpyxl import Workbook
from unittest import TestCase
from unittest.mock import patch

from pandas import DataFrame

from darwinsync_api_helper import FileNotFoundError

from fiatof_xlsx_dag import (
    parse_files,
    get_missed_keys,
    parse_json,
    parse_jsonsample,
    get_json_diff_id,
    QC_HEADER_COLUMNS_COUNT,
    process_raw_intensities,
)
from tests_mzxml_convert import MockDSAPIHelper
from extensions.mocks import (
    MockPostgresHook,
    MockPostgresHookWithResponse,
    MockPostgresHookWithSetResponse,
)


class MockTask:
    def __init__(self, task_id, value):
        self.task_id = task_id
        self.xcom_value = {task_id: value}

    def xcom_pull(self, task_ids=None, key='return_value'):
        return self.xcom_value[task_ids or self.task_id][key]

    def xcom_push(self, key, value):
        self.xcom_value[self.task_id][key] = value


def add_anntations_ws(wb):
    annotation_ws = wb.create_sheet('annotation')
    annotation_ws.append([
        'ionIdx', 'score', 'label (bona fide)', 'someids', 'otherids',
        'ionMz', 'ionActive', 'Unnamed: 10', 'ionQualTIC', 'ionAverageInt',
        'desc', 'with space', 'difference', 'log2(FC)', 'adj p-value (BH)',
        'p-value', 'kegg ids',
    ])
    annotation_ws.append([
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11' '12' '13',
        '14', '15', '16', '17',
    ])


def create_good_xlsx(filename):
    wb = Workbook()
    injection_ws = wb.active
    injection_ws.title = 'injections'
    injection_ws.append([
        1, 2, 3,
        'difference', 'ionIdx', 'log2(FC)', 'p-value', 'adj p-value (BH)'])
    injection_ws.append([4, 5, 6, 7, 8, 9, 10, 11])
    injection_ws.append(range(28, 36))
    injection_ws.append(range(36, 44))
    ions_ws = wb.create_sheet('ions')
    ions_ws.append([
        'ionIdx', 'ionActive', 'ionQualTIC', 'Top annotation ids',
        'Top annotation formula', 'Top annotation ion', 'Top annotation name',
        'Top annotation score',
        'difference', 'adj p-value (BH)', 'log2(FC)', 'p-value'
    ])
    ions_ws.append([
        '1', '2', '3', 'taids', 'taf', 'tai', 'tan', '6', '16', '28', '29',
        '30',
    ])
    ions_ws.append([
        '4', '5', '', 'taids2', 'taf2', 'tai2', 'tan2', '7', '17', '31', '32',
        '33',
    ])
    intensities_ws = wb.create_sheet('intensities1')
    intensities_ws.append([
        'ionIdx', 'ionMz', 'ionActive', '1/some', '2/data', 'difference',
        'p-value', 'log2(FC)', 'adj p-value (BH)',
    ])
    intensities_ws.append(['1', '2', '3', '4', '5', '16', '19', '20', '21'])
    intensities_ws.append(['6', '7', '8', '9', '10', '17', '22', '23', '24'])
    intensities_ws.append(
        ['11', '12', '13', '14', '15', '18', '25', '26', '27'])
    add_anntations_ws(wb)
    wb.save(filename)


def create_diff_xlsx(filename):
    wb = Workbook()
    ws1 = wb.active
    ws1.title = 'something'
    ws1.append([
        'difference', 'ionIdx', 'log2(FC)', 'p-value', 'adj p-value (BH)',
        'other',
    ])
    ws1.append([1, '2', 3, 4, 5, 6])
    ws1.append([1, '2', 3, 4, 5, 6])
    sheet1 = wb.create_sheet('Sheet1')
    sheet1.append(['something'])
    wb.save(filename)


def create_bad_xlsx(filename):
    wb = Workbook()
    ws1 = wb.active
    ws1.title = 'injections'
    ws1.append([
        1, 2, 3, 'difference', 'adj p-value (BH)', 'p-value', 'log2(FC)',
        'ionIdx',
    ])
    ws1.append([4, 5, 6, 10, 11, 12, 13, 18])
    ws1.append([7, 8, 9, 14, 15, 16, 17, 19])
    ws2 = wb.create_sheet('ions')
    ws2.append([
        'ionIdx', 'ionActive', 'ionQualTIC', 'Top annotation ids',
        'Top annotation formula', 'Top annotation ion', 'Top annotation name',
        'Top annotation score', 'difference', 'adj p-value (BH)', 'p-value',
        'log2(FC)',
    ])
    ws2.append([
        '1', '2', '3', '6', '7', '8', '9', '14', '17', '18', '19', '20'])
    ws2.append([
        '4', '5', '10', '11', '12', '13', '15', '16', '21', '22', '23', '24'])
    ws3 = wb.create_sheet('intensities1')
    ws3.append([
        'ionIdx', 'ionMz', 'ionActive', '1/some', '2/data', 'difference',
        'adj p-value (BH)', 'p-value', 'log2(FC)'])
    ws3.append(['1', '2', '3', '4', '5', '6', '7', '8', '9'])
    add_anntations_ws(wb)
    wb.save(filename)


MOCK_JSON_DATA = {
    "label": "test_label",
    "data": [
        {'1': 'mock1_1', '2': 'mock1_2'},
        {'1': 'mock2_1', '2': 'mock2_2'},
        {'1': 'mock3_1', '2': 'mock3_2'},
    ],
    "workflow": "test_workflow",
    "vars": [
        {
            "id": "GROUPS",
            "value": ("{ 'value1_1','dsPlateInj';"
                      "'1','dsPlateInj','value2_3';"
                      "'1','dsPlateInj','value3_3','value3_4';"
                      "'value4_1','dsPlateInj','value4_3','value4_4',"
                      "'value4_5' }")
        }
    ]
}


class MockDSAPIHelperWithDownload(MockDSAPIHelper):
    def download_file(self, filename, **kwargs):
        if kwargs['client_file_ext'] == '.xlsx':
            create_good_xlsx(filename)
        elif kwargs['client_file_ext'] == '.csv':
            with open(filename, 'w') as f:
                f.write(','.join(
                    [f'Col{num}' for num in range(QC_HEADER_COLUMNS_COUNT)]))
                f.write('\n)')
                f.write(','.join(
                    [f'tesst{num}' for num in range(QC_HEADER_COLUMNS_COUNT)]))
        elif kwargs['client_file_ext'].lower() == '.json':
            with open(filename, 'w') as f:
                f.write(dumps(MOCK_JSON_DATA))


class MockDSAPIHelperWithDiffs(MockDSAPIHelper):
    def download_file(self, filename, **kwargs):
        if kwargs['client_file_ext'] == '.xlsx':
            create_diff_xlsx(filename)
        elif kwargs['client_file_ext'].lower() == '.json':
            with open(filename, 'w') as f:
                f.write(dumps(MOCK_JSON_DATA))


class MockDSAPIHelperWithBadDownload(MockDSAPIHelper):
    def download_file(self, filename, **kwargs):
        if kwargs['client_file_ext'] == '.xlsx':
            create_bad_xlsx(filename)
        elif kwargs['client_file_ext'] == '.csv':
            with open(filename, 'w') as f:
                f.write(','.join(
                    [f'Col{num}' for num in range(QC_HEADER_COLUMNS_COUNT)]))
                f.write('\n)')
                f.write(','.join(
                    [f'tesst{num}' for num in range(QC_HEADER_COLUMNS_COUNT)]))
        elif kwargs['client_file_ext'].lower() == '.json':
            with open(filename, 'w') as f:
                f.write(dumps(MOCK_JSON_DATA))


class MockDSAPIHelperWithNoFilesFound(MockDSAPIHelper):
    def download_file(self, filename, **kwargs):
        raise FileNotFoundError


class MockPostgresHookForDiff(MockPostgresHookWithSetResponse):
    def __init__(self, postgres_conn_id=None, autocommit=False):
        super(MockPostgresHookForDiff, self).__init__(
            response=[(1, "1"), (2, '2')])


@patch('fiatof_xlsx_dag.TransactPostgresHook',
       new=MockPostgresHookForDiff)
@patch('fiatof_xlsx_dag.load_dataframe')
class ParseXlsxTestCase(TestCase):
    @patch('fiatof_xlsx_dag.get_ds_helper',
           return_value=MockDSAPIHelperWithDownload())
    def test_xlsx_parse_success(self, mock_ds, mock_load_df):
        folder_name = '20200113T111213 Bio-666_Testing'
        mock_processingout = {'return_value': [
            (13, folder_name, 'Bio-666_Testing.JSON',),
        ]}
        mock_task = MockTask(
            'get_new_fiatof_processed_files', mock_processingout)
        result = parse_files(task_instance=mock_task, parse_diff=False)
        self.assertEqual(result, 'process_fiatof_files')
        self.assertEqual(
            mock_task.xcom_pull(key='output'),
            '*:white_check_mark: Success*: `20200113T111213 Bio-666_Testing`')
        self.assertEqual(6, mock_load_df.call_count)
        json_df = mock_load_df.call_args_list[0][0][0]
        for key in ['json_id', 'samplefield_id', 'value', 'row_id']:
            self.assertIn(key, json_df)
        self.assertEqual(len(MOCK_JSON_DATA['data'][0]) * 3, json_df.shape[0])

        raw_ion_df = mock_load_df.call_args_list[1][0][0]
        self.assertIn('processingout_id', raw_ion_df)
        self.assertEqual(13, raw_ion_df['processingout_id'][0])
        self.assertEqual(13, raw_ion_df['processingout_id'][1])
        self.assertIn('json_id', raw_ion_df)
        self.assertEqual(1, raw_ion_df['json_id'][0])
        self.assertEqual(1, raw_ion_df['json_id'][1])
        self.assertIn('ionidx', raw_ion_df)
        self.assertEqual(1, raw_ion_df['ionidx'][0])
        self.assertEqual(4, raw_ion_df['ionidx'][1])
        self.assertIn('ionactive', raw_ion_df)
        self.assertEqual(2, raw_ion_df['ionactive'][0])
        self.assertEqual(5, raw_ion_df['ionactive'][1])
        self.assertIn('ionqualtic', raw_ion_df)
        self.assertEqual(3, raw_ion_df['ionqualtic'][0])
        self.assertEqual(0.0, raw_ion_df['ionqualtic'][1])

        raw_intensity_df = mock_load_df.call_args_list[2][0][0]
        self.assertIn('processingout_id', raw_intensity_df)
        self.assertEqual(13, raw_intensity_df['processingout_id'][0])
        self.assertEqual(13, raw_intensity_df['processingout_id'][1])
        self.assertIn('json_id', raw_intensity_df)
        self.assertEqual(1, raw_intensity_df['json_id'][0])
        self.assertEqual(1, raw_intensity_df['json_id'][1])
        self.assertIn('ionidx', raw_intensity_df)
        self.assertEqual(1, raw_intensity_df['ionidx'][0])
        self.assertEqual(6, raw_intensity_df['ionidx'][1])
        self.assertEqual(11, raw_intensity_df['ionidx'][2])
        self.assertIn('dsidx', raw_intensity_df)
        self.assertEqual(1, raw_intensity_df['dsidx'][0])
        self.assertEqual(1, raw_intensity_df['dsidx'][1])
        self.assertEqual(1, raw_intensity_df['dsidx'][2])
        self.assertEqual(2, raw_intensity_df['dsidx'][3])
        self.assertEqual(2, raw_intensity_df['dsidx'][4])
        self.assertEqual(2, raw_intensity_df['dsidx'][5])
        self.assertIn('intensity', raw_intensity_df)
        self.assertEqual(4, raw_intensity_df['intensity'][0])
        self.assertEqual(9, raw_intensity_df['intensity'][1])
        self.assertEqual(14, raw_intensity_df['intensity'][2])
        self.assertEqual(5, raw_intensity_df['intensity'][3])
        self.assertEqual(10, raw_intensity_df['intensity'][4])
        self.assertEqual(15, raw_intensity_df['intensity'][5])

        ion_df = mock_load_df.call_args_list[3][0][0]
        for column in [
                'Top annotation ids', 'Top annotation formula',
                'Top annotation ion', 'Top annotation name']:
            self.assertNotIn(column, ion_df)
        self.assertEqual(1, ion_df['ionidx'][0])
        self.assertEqual(4, ion_df['ionidx'][1])
        self.assertEqual(2, ion_df['ionactive'][0])
        self.assertEqual(5, ion_df['ionactive'][1])
        self.assertEqual(6, ion_df['topannotationscore'][0])
        self.assertEqual(7, ion_df['topannotationscore'][1])

        annotation_df = mock_load_df.call_args_list[4][0][0]
        for column in [
                'ionMz', 'ionActive', 'Unnamed: 10', 'ionQualTIC',
                'ionAverageInt', 'desc', 'label (bona fide)']:
            self.assertNotIn(column, annotation_df)
        self.assertEqual(1, annotation_df['ionidx'][0])
        self.assertEqual(2, annotation_df['score'][0])
        self.assertIn('label', annotation_df)
        self.assertIn('withspace', annotation_df)
        self.assertIn('some_ids', annotation_df)
        self.assertIn('other_ids', annotation_df)

        intensities_df = mock_load_df.call_args_list[5][0][0]
        self.assertNotIn('ionMz', intensities_df)
        self.assertNotIn('ionActive', intensities_df)
        self.assertNotIn('1/some', intensities_df)
        self.assertIn('ionidx', intensities_df)
        self.assertIn('dsidx', intensities_df)
        self.assertIn('intensity', intensities_df)
        self.assertEqual((18, 5), intensities_df.shape)
        self.assertIn('processingout_id', intensities_df)
        self.assertIn('json_id', intensities_df)
        self.assertEqual(1, mock_ds.call_count)

    @patch('fiatof_xlsx_dag.get_ds_helper',
           return_value=MockDSAPIHelperWithDiffs())
    def test_diff_parse(self, mock_ds, mock_load_df):
        folder_name = '20200120T200220 Bio-667_Testing'
        mock_processingout = {'return_value': [
            (14, folder_name, 'Bio-667_Testing.JSON',),
        ]}
        mock_task = MockTask(
            'get_new_fiatof_processed_files', mock_processingout)
        result = parse_files(
            task_instance=mock_task, parse_raw=False, parse_curated=False)
        self.assertEqual(result, 'process_fiatof_files')
        self.assertEqual(
            mock_task.xcom_pull(key='output'),
            '*:white_check_mark: Success*: `20200120T200220 Bio-667_Testing`')
        self.assertEqual(2, mock_load_df.call_count)
        diff_df = mock_load_df.call_args_list[1][0][0]
        self.assertEqual('met_diff', mock_load_df.call_args_list[1][0][2])
        self.assertEqual((1, 6), diff_df.shape)
        for col in [
                'ionidx', 'logfc', 'pval', 'adjpval', 'processingout_id',
                'jsondiff_id']:
            self.assertIn(col, diff_df)
        self.assertEqual(2, diff_df['ionidx'][0])
        self.assertEqual(1, mock_ds.call_count)

    @patch('fiatof_xlsx_dag.get_ds_helper',
           return_value=MockDSAPIHelperWithBadDownload())
    def test_xlsx_parse_fail(self, mock_ds, mock_load_df):
        folder_name = '20200113T111213 Bio-666_Testing'
        mock_processingout = {'return_value': [
            (13, folder_name, 'Bio-666_Testing.json',),
        ]}
        mock_task = MockTask(
            'get_new_fiatof_processed_files', mock_processingout)
        result = parse_files(task_instance=mock_task)
        self.assertEqual(result, 'process_fiatof_files')
        self.assertEqual(
            mock_task.xcom_pull(key='output'),
            ('*:x: Error*: `20200113T111213 Bio-666_Testing` '
             '(Samples count mismatch (2 vs. 3))'))
        self.assertEqual(8, mock_load_df.call_count)
        self.assertEqual(1, mock_ds.call_count)

    @patch('fiatof_xlsx_dag.get_ds_helper',
           return_value=MockDSAPIHelper())
    def test_xlsx_parse_no_data(self, mk_ds, mk_load_df):
        mock_task = MockTask(
            'get_new_fiatof_processed_files', {'return_value': []})
        result = parse_files(task_instance=mock_task)
        self.assertEqual(result, 'finish')
        self.assertTrue(mk_ds.called)
        self.assertFalse(mk_load_df.called)


class MissedKeysTestCase(TestCase):
    def test_check_keys(self):
        test_list = ['MandatoryValue1', 'TheOtherValue']
        mocked_hook = MockPostgresHookWithSetResponse(
            response=[(item,) for item in test_list])
        mocked_hook.response = test_list
        test_dict = {key: 'test' for key in test_list}
        result = get_missed_keys([test_dict], test_list)
        self.assertEqual(set(), result)
        self.assertFalse(result)
        test_dict['extra'] = 'value'
        result = get_missed_keys([test_dict], test_list)
        self.assertEqual(set(), result)
        self.assertFalse(result)
        test_dict.pop(test_list[0])


@patch('fiatof_xlsx_dag.TransactPostgresHook',
       new=MockPostgresHookWithResponse)
class JSONFileParseTestCase(TestCase):
    def test_json_file_absent(self):
        with self.assertLogs(level='WARNING') as logs:
            result1, result2 = parse_json(
                1, '2', '3', MockDSAPIHelperWithNoFilesFound(), dict(),
                MockPostgresHook())
        self.assertIsNone(result1)
        self.assertEqual(0, result2)
        self.assertIn('JSON file not found', logs.records[0].getMessage())

    @patch('fiatof_xlsx_dag.load_dataframe')
    def test_json_file_parsed(self, mk_df):
        result1, result2 = parse_json(
            4, '5', 'some.json', MockDSAPIHelperWithDownload(), {},
            MockPostgresHookWithSetResponse())
        self.assertEqual('1', result1)
        self.assertEqual(MOCK_JSON_DATA, result2)
        self.assertFalse(mk_df.called)
        result3 = parse_jsonsample(
            4, result1, MOCK_JSON_DATA['data'], MOCK_JSON_DATA['vars'],
            MockPostgresHookWithSetResponse())
        self.assertEqual(3, result3)
        self.assertEqual(1, mk_df.call_count)
        df = mk_df.call_args_list[0][0][0]
        self.assertEqual((6, 4), df.shape)
        correct_row_ids = []
        for row_num in range(1, len(MOCK_JSON_DATA['data']) + 1):
            correct_row_ids.extend([row_num] * len(MOCK_JSON_DATA['data'][0]))
        self.assertEqual(correct_row_ids, list(df['row_id']))


class GetJsonDiffIdTestCase(TestCase):
    def test_json_diff_id_requested_by_combined(self):
        result = get_json_diff_id(
            MockPostgresHookWithSetResponse(response=[(3,)]), 4,
            combined='tstcmbnd')
        self.assertEqual(3, result)
        result = get_json_diff_id(
            MockPostgresHookWithResponse(),
            4, combined='tstcmbnd', denominator=2)
        self.assertEqual(1, result)

    def test_json_diff_id_requested_by_numerator_denominator(self):
        result = get_json_diff_id(
            MockPostgresHookWithSetResponse(response=[(5,)]),
            8, numerator=9, denominator=10)
        self.assertEqual(5, result)

    def test_json_diff_id_requested_incorrectly(self):
        result = get_json_diff_id(MockPostgresHookWithResponse(), 4)
        self.assertIsNone(result)
        result = get_json_diff_id(
            MockPostgresHookWithResponse(), 4, numerator=1)
        self.assertIsNone(result)
        result = get_json_diff_id(
            MockPostgresHookWithResponse(), 4, denominator=2)
        self.assertIsNone(result)


class ProcessIntensitiesTestCase(TestCase):
    def test_intensities_processed(self):
        source_df = DataFrame(
            [[1, 2, 3.3, 21, 24], [4, 5, 6.6, 22, 25], [7, 8, 9.9, 23, 26]],
            columns=[
                'ionMz', 'ionActive', 'ionIdx', '50/something', '60/more'])
        ion_qc_df = DataFrame([[11.11, 12.12], [13.13, 14.14], [15.15, 16.16]])
        result_df = process_raw_intensities(
            source_df, None, ion_qc_df=ion_qc_df, processingout_id=30,
            json_id=40, samples_count_matches=True)
        for col_name in ['ionidx', 'dsidx', 'intensity', 'backfilled']:
            self.assertIn(col_name, list(result_df))
        self.assertEqual((6, 6), result_df.shape)
        self.assertEqual([3, 6, 9, 3, 6, 9], list(result_df['ionidx']))
        self.assertEqual(list(range(21, 27)), list(result_df['intensity']))
        self.assertEqual(list(range(11, 17)), list(result_df['backfilled']))
