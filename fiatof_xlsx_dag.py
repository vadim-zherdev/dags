from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator,
)
from airflow.models import DAG
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
from darwinsync_api_helper import FileNotFoundError
from json import load
import logging
from numpy import int64
from os import path, listdir
from pandas import read_csv, DataFrame
from re import match
from tempfile import TemporaryDirectory
from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.slack_alert import task_fail_slack_alert
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import (
    xls_to_csvs,
    get_ds_helper,
    load_dataframe,
    clean,
    get_slack_conn
)

DB_CONNECTION_ID = 'informatics_db'
WORKLIST_ID = 999_999
FIA_VERSION = 'v0.1'
QC_FILE_NAME = 'fiaexp_ionQC.csv'
SUCCESS_STATUS = 'Success'
COMMIT_EVERY = 100
QC_HEADER_COLUMNS_COUNT = 6

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='fiatof_xlsx_parse',
    default_args=args,
    schedule_interval='@hourly',
    catchup=False
)

SET_FILE_STATUS_QUERY = '''
    UPDATE
        fia.processingout
    SET
        load_status = %(status)s
    WHERE
        processingout_id = %(processingout_id)s
'''


def add_to_audit(file_id, action, result):
    """Insert a record to the audit table"""
    hook = TransactPostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    hook.insert_rows(
        'stg_infodw.fia_processingout_audit',
        [(file_id, action, result)],
        target_fields=['processingout_id', 'action_name', 'action_result'],
        commit_every=COMMIT_EVERY)


def set_status(processingout_id, status):
    """Sets status of a file

    Args:
        processingout_id: row ID
        status: text of the status to be set
    """
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    hook.run(
        SET_FILE_STATUS_QUERY,
        parameters={'processingout_id': processingout_id, 'status': status})


def report_file_status(report, file_name, message):
    """
    Set text message about given file for Slack report
    Args:
        report: source dict to add/update the message
        file_name: the key for the dict
        message: message to add/update
    """
    if file_name not in report:
        report[file_name] = message
        return
    if message == SUCCESS_STATUS:
        return
    report[file_name] = (
        message if report[file_name] == SUCCESS_STATUS
        else report[file_name] + ', ' + message)


def process_failure(processingout_id, file_name, report, action_name, message):
    """
    Do everything needed in case of failure: log a warning, add row to the
    audit table, add a key to the Slack report dict, set status to 'failed'
    Args:
        processingout_id: ID from processingout table
        file_name: name to be included in the logs
        report: Slack report dict
        action_name: name to be included into the audit table
        message: error description to be included into log and Slack report
    """
    logging.warning(f'{message} for {file_name}')
    add_to_audit(processingout_id, action_name, message)
    report_file_status(report, file_name, message)
    set_status(processingout_id, 'failed')


def import_csv(filename, hook, tablename, schema, processor=None, **kwargs):
    """
    Read the csv-file into pandas DataFrame and load it into the table
    using the hook provided.
    Args:
        filename: csv-file to import
        hook: AirFlow database hook to use
        tablename: table to insert the data
        schema: schema of the table
        processor: callable that processes the DataFrame before loading
            must accept the dataframe, the hook and **kwargs
            must return the processed DataFrame or None
        **kwargs:
            arbitrary arguments for the processor
    Returns:
        boolean success flag
    """
    df = read_csv(filename)
    logging.debug(f'{df.shape[0]} lines imported from {filename}')
    if callable(processor):
        df = processor(df, hook, **kwargs)
    if df is None:
        return False
    load_dataframe(
        df, hook, tablename, schema, reorder_columns=True,
        exclude_columns=[
            'annotation_id',
            'ion_id',
            'intensities_id',
            'met_diff_id',
        ],
        cast_empty_to_nan=kwargs.get('cast_empty_to_nan', True))
    return True


def process_ions(df, hook, json_id=None):
    """
    Processes ions DataFrame before loading into fia.ion table.
    To be called from import_csv function (see above)
    Args:
        df: the DataFrame to be processed
        hook: AirFlow database hook, not used here, only for common signature
        **kwargs: arbitrary arguments, not used, for signature only
    Returns:
        Processed DataFrame
    """
    df.drop([
        'Top annotation ids',
        'Top annotation formula',
        'Top annotation ion',
        'Top annotation name'],
        axis='columns', inplace=True)
    for column in ['ionIdx', 'ionActive', 'Top annotation score']:
        df[column] = df[column].astype(int64)
    df.rename(mapper=lambda x: x.lower().replace(' ', ''),
              axis='columns', inplace=True)
    df['json_id'] = json_id
    return df


def process_raw_ions(df, hook, json_id=None, processingout_id=None):
    """
    Processes raw ions DataFrame before loading into fia.raw_ion table.
    To be called from import_csv function (see above)
    Args:
        df: the DataFrame to be processed
        hook: AirFlow database hook, used for the table clean-up
        json_id, processingout_id - values to be inserted along
    Returns:
        Processed DataFrame
    """
    df['json_id'] = json_id
    df['processingout_id'] = processingout_id
    df['ionIdx'] = df['ionIdx'].astype(int64)
    df['ionActive'] = df['ionActive'].astype(int64)
    df['ionQualTIC'] = df['ionQualTIC'].fillna(0.0)
    logging.info("Loading raw ions")
    df.rename(
        mapper=lambda x: x.lower(), axis='columns', inplace=True)
    return df


def process_annotations(df, hook, **kwargs):
    """
    Processes annotations DataFrame before loading into fia.annotation table.
    To be called from import_csv function (see above)
    Args:
        df: the DataFrame to be processed
        hook: AirFlow database hook, not used here, only for common signature
        **kwargs: list of additional fixed-value columns to add
    Returns:
        Processed DataFrame
    """
    cols_to_drop = [
        'ionMz', 'ionActive', 'Unnamed: 10', 'ionQualTIC', 'ionAverageInt',
        'desc']
    df.drop(list(set(list(df)) & set(cols_to_drop)), axis=1, inplace=True)
    for key, value in kwargs.items():
        df[key] = value
    df['ionIdx'] = df['ionIdx'].astype(int64)
    df['score'] = df['score'].astype(int64)
    df.rename({'label (bona fide)': 'label'}, axis='columns', inplace=True)
    df.rename(mapper=lambda x: x.lower().replace(' ', ''),
              axis='columns', inplace=True)
    df.rename(
        {column: column[:-3] + '_ids'
         for column in list(df) if column.endswith('ids')},
        axis='columns', inplace=True)
    cols_to_fillna = ['kegg_ids', 'other_ids']
    df.loc[:, cols_to_fillna] = df.loc[:, cols_to_fillna].fillna(value='')
    return df


def process_intensities(df, hook, json_id=None, processingout_id=None):
    """
    Processes intensities DataFrame before loading into fia.intensities table.
    To be called from import_csv function (see above)
    Args:
        df: the DataFrame to be processed
        hook: AirFlow database hook, not used here, only for common signature
        json_id, processingout_id: values to be inserted along
    Returns:
        Processed DataFrame
    """
    df.drop(['ionMz', 'ionActive'], axis=1, inplace=True)
    df['ionIdx'] = df['ionIdx'].astype(int64)
    df.rename(
        mapper=lambda x: int(x.split('/')[0]) if '/' in x else x,
        axis='columns', inplace=True)
    df = df.melt(id_vars=['ionIdx'], var_name='dsidx', value_name='intensity')
    df.rename({'ionIdx': 'ionidx'}, axis='columns', inplace=True)
    df['processingout_id'] = processingout_id
    df['json_id'] = json_id
    return df


def process_raw_intensities(
        df, hook, processingout_id=None, intensities_count=None,
        action_name=None, json_id=None, ion_qc_df=None,
        samples_count_matches=None):
    """
    Processes raw intensities DataFrame before loading
    into fia.raw_intensities table.
    To be called from import_csv function (see above)
    Args:
        df: the DataFrame to be processed
        hook: AirFlow database hook, not used here, only for common signature
        json_id, processingout_id: values to be inserted along
        intensities_count: value to check the count with
        action_name: string to put into log records
    Returns:
        Processed DataFrame
    """
    df.drop(['ionMz', 'ionActive'], axis=1, inplace=True)
    add_backfill = samples_count_matches and ion_qc_df.shape[0] == df.shape[0]
    logging.info("Number of ion rows and backfill rows "
                 f"{'match' if add_backfill else 'mismatch'}")
    df['ionIdx'] = df['ionIdx'].astype(int64)
    df.rename(
        mapper=lambda x: int(x.split('/')[0]) if '/' in x else x,
        axis='columns',
        inplace=True)
    df = df.melt(
        id_vars=['ionIdx'],
        var_name='dsidx',
        value_name='intensity')
    if add_backfill:
        ion_qc_df_stacked = ion_qc_df.stack().astype(
            int).to_frame().reset_index(drop=True)
        ion_qc_df_stacked.columns = ['backfilled']
        df = df.merge(ion_qc_df_stacked, left_index=True, right_index=True)
    df.rename(
        {'ionIdx': 'ionidx'}, axis='columns', inplace=True)
    df['processingout_id'] = processingout_id
    df['json_id'] = json_id
    return df


MANDATORY_KEYS_FIELDS = [
    'samplefield_id',
    'samplefield_name',
    'required',
]
GET_MANDATORY_KEYS_QUERY = f'''
    SELECT
        {','.join(MANDATORY_KEYS_FIELDS)}
    FROM
        fia.samplefield
    WHERE
        required
'''
MANDATORY_KEY_ID_POS = MANDATORY_KEYS_FIELDS.index('samplefield_id')
MANDATORY_KEY_NAME_POS = MANDATORY_KEYS_FIELDS.index('samplefield_name')
MANDATORY_KEY_REQUIRED_POS = MANDATORY_KEYS_FIELDS.index('required')


def get_missed_keys(source_list, mandatory_keys):
    """
    Looks through the list of dicts to find the absence of mandatory keys
    provided by the pattern agrument
    Args:
        source_list: iterable of dicts to look through
        mandatory_keys: iterable of the mandatory keys

    Returns:
        Set of mandatory keys that weren't found
    """
    all_data_keys = set()
    for item in source_list:
        all_data_keys = all_data_keys.union(item.keys())
    return set(mandatory_keys) - all_data_keys


def load_jsondiff(json_source, mandatory_keys, json_id, hook):
    """
    Load JSON content into fia.jsondiff table
    Args:
        json_source: JSON to be loaded
        json_id: value for the table
        hook: database hook to use while inserting
    """
    diff_records = []
    for item in json_source[0]["value"].split(';'):
        if item[0].startswith('{'):
            item = item[1:]
        if item[-1].endswith('}'):
            item = item[:-1]
        record = item.split(',')
        if 3 <= len(record) <= 4:
            field_id = [
                key[MANDATORY_KEY_ID_POS]
                for key in mandatory_keys
                if key[MANDATORY_KEY_NAME_POS] == clean(record[0])][0]
            diff_records.append((
                json_id,
                record[3].strip("'") if len(record) > 3 else None,
                field_id, clean(record[1]), clean(record[2])))
    if diff_records:
        hook.insert_rows(
            'fia.jsondiff',
            diff_records,
            target_fields=[
                'json_id', 'label', 'samplefield_id', 'numerator',
                'denominator'],
            commit_every=COMMIT_EVERY,
        )
        logging.info(f'Differential variables loaded for id {json_id}')
    else:
        logging.info(
            f'No differential valuables found for id {json_id}')


INSERT_JSON_QUERY = '''
    INSERT INTO
        fia."json" (
            worklist_id,
            filename,
            label,
            fiaworkflow,
            fiaversion,
            processingout_id)
    VALUES (
        %(worklist_id)s,
        %(filename)s,
        %(label)s,
        %(fiaworkflow)s,
        %(fiaversion)s,
        %(processingout_id)s)
    ON CONFLICT (upper(filename)) DO UPDATE
    SET
        worklist_id = %(worklist_id)s,
        label = %(label)s,
        fiaworkflow = %(fiaworkflow)s,
        fiaversion = %(fiaversion)s,
        processingout_id = %(processingout_id)s
    RETURNING
        json_id
'''


def parse_json(
        processingout_id, file_folder, json_file_name, ds_api_helper, report,
        hook):
    """
    Download JSON-file from BioBright and load its contents into
    fia.json and fia.jsonsample tables.
    Preliminary checks apply to assure the content is correct.
    Args:
        processingout_id: from fia.processingout table
        file_folder: folder to dowlnoad from
        json_file_name: file to download
        ds_api_helper: DSAPIHelper instance to request BioBright
        report: dict to add values for Slack report
        hook: database hook to use for inserting into the tables

    Returns:
        json_id from fia.json table or None in case of failure
    """
    with TemporaryDirectory() as tmp_dir_name:
        tmp_file_name = path.join(tmp_dir_name, 'tmp.json')
        bb_file_path = (f'{file_folder}/{json_file_name}')
        bb_file_ext = path.splitext(json_file_name)[1]
        try:
            ds_api_helper.download_file(
                tmp_file_name,
                client_path=bb_file_path,
                client_file_ext=bb_file_ext)
        except FileNotFoundError:
            process_failure(
                processingout_id, file_folder, report, 'Parse JSON',
                'JSON file not found')
            return None, 0
        with open(tmp_file_name, encoding="utf8", errors='ignore') as f:
            json_data = load(f, strict=False)
    mandatory_keys = hook.get_records(GET_MANDATORY_KEYS_QUERY)
    missed_keys = get_missed_keys(
        json_data['data'],
        [item[MANDATORY_KEY_NAME_POS] for item in mandatory_keys])
    if missed_keys:
        process_failure(
            processingout_id, file_folder, report, 'Parse JSON',
            f'Missed keys: {missed_keys}')
        return None, 0
    else:
        logging.info(f'All fields matched for {file_folder}')
    json_id = hook.get_first(
        INSERT_JSON_QUERY,
        parameters={
            'worklist_id': WORKLIST_ID,
            'filename': json_file_name,
            'label': json_data['label'],
            'fiaworkflow': json_data['workflow'],
            'fiaversion': FIA_VERSION,
            'processingout_id': processingout_id,
        }
    )[0]
    logging.info(f'Added/updated json {json_id}')
    return json_id, json_data


def parse_jsonsample(processingout_id, json_id, json_data, json_vars, hook):
    """
    Load jsonsamples and jsondiff tables from JSON downloaded before.
    Parameters
    ----------
    processingout_id - rom fia.processingout table
    json_id - for foreign keys columns
    json_data - data for jsonsample table
    json_vars - data for jsondiff table (optional)
    hook - database hook to use for inserting into the tables

    Returns
    -------
    Samples count to be compared with backoff later.
    """
    mandatory_keys = hook.get_records(GET_MANDATORY_KEYS_QUERY)
    records = []
    for row in json_data:
        record = []
        for key, value in row.items():
            field_id = [mandatory_rec[MANDATORY_KEY_ID_POS]
                        for mandatory_rec in mandatory_keys
                        if mandatory_rec[MANDATORY_KEY_NAME_POS] == key][0]
            record.append([field_id, value])
        records.append(record)
    flat_records = [[item[0], item[1], row_num + 1]
                    for row_num, row in enumerate(records) for item in row]
    df = DataFrame(flat_records, columns=['samplefield_id', 'value', 'row_id'])
    df['json_id'] = json_id
    load_dataframe(
        df, hook, 'jsonsample', 'fia', reorder_columns=True,
        exclude_columns=['jsonsample_id'])
    logging.info(f'fia.jsonsample populated for id {json_id}')
    if json_vars:
        logging.info('Adding differential variables')
        load_jsondiff(json_vars, mandatory_keys, json_id, hook)
    add_to_audit(processingout_id, 'Parse JSON', SUCCESS_STATUS)
    return len(records)


def parse_raw_data(
        processingout_id, json_id, file_folder, experiment_name,
        json_samples_count, report, ds_api_helper, hook):
    """
    Download GMET raw results from BioBright and load them into fia.raw_ion
    and fia.raw_intensities tables
    Args:
        processingout_id: from fia.processingout table
        json_id: to keep connections between the tables
        file_folder: folder to dowlnoad from
        experiment_name: name of the experiment (starting from "Bio-..."
            to request its files
        ds_api_helper: DSAPIHelper instance to request BioBright
        report: dict to add values for Slack report
        hook: database hook to use for inserting into the tables

    Returns: boolean success flag
    """
    with TemporaryDirectory() as tmp_dir_name:
        tmp_file_name = path.join(tmp_dir_name, 'tmp.xlsx')
        ds_api_helper.download_file(
            tmp_file_name,
            client_path=f'{file_folder}/{experiment_name} DATA RAW.xlsx',
            client_file_ext='.xlsx')
        # TODO: import only needed sheets
        xls_to_csvs(tmp_file_name, tmp_dir_name)
        injections_df = read_csv(
            path.join(tmp_dir_name, 'injections.csv'))
        logging.debug(
            f'{injections_df.shape[0]} injections imported '
            f'for file {file_folder}')
        if injections_df.shape[0] == json_samples_count:
            logging.debug(
                'Samples count matches for JSON '
                f'{json_id}: {json_samples_count}')
        else:
            process_failure(
                processingout_id, file_folder, report, 'Parse raw data',
                'Samples count mismatch ('
                f'{injections_df.shape[0]} vs. {json_samples_count})')
            return False
        tmp_file_name = path.join(tmp_dir_name, 'tmp.csv')
        try:
            ds_api_helper.download_file(
                tmp_file_name,
                client_path=f'{file_folder}/{experiment_name}/{QC_FILE_NAME}',
                client_file_ext='.csv')
            ion_qc_df = read_csv(tmp_file_name)
            samples_count, intensities_count = ion_qc_df.shape
            samples_count_matches = injections_df.shape[0] == samples_count
        except FileNotFoundError:
            intensities_count = 0
            samples_count_matches = False
            ion_qc_df = DataFrame(columns=range(QC_HEADER_COLUMNS_COUNT))
        message = 'matches' if samples_count_matches else "mismatch"
        log_method = (logging.info if samples_count_matches
                      else logging.warning)
        message = ('Number of samples of raw data and backfill csv '
                   f'{message} for json {json_id}')
        log_method(message)
        add_to_audit(processingout_id, 'Parse raw data', message)
        ions_csv_filename = path.join(tmp_dir_name, 'ions.csv')
        import_success = True
        import_success = import_success and import_csv(
            ions_csv_filename, hook, 'raw_ion', 'fia',
            processor=process_raw_ions,
            processingout_id=processingout_id, json_id=json_id)
        add_to_audit(processingout_id, 'Parse raw data', 'ions loaded')
        logging.info('Loading intensities')
        intensities_sheets = [file for file in listdir(tmp_dir_name)
                              if match('^intensities[0-9]+.csv$', file)]
        ion_qc_df.drop(
            ion_qc_df.columns[range(QC_HEADER_COLUMNS_COUNT)],
            axis=1, inplace=True)
        ion_qc_df = ion_qc_df.transpose()
        for intensity_sheet in intensities_sheets:
            logging.info(
                f'Processing {intensity_sheet} for json {json_id}')
            import_success = import_success and import_csv(
                path.join(tmp_dir_name, intensity_sheet),
                hook, 'raw_intensities', 'fia',
                processor=process_raw_intensities,
                intensities_count=intensities_count,
                processingout_id=processingout_id, json_id=json_id,
                action_name='Parse raw data', ion_qc_df=ion_qc_df,
                samples_count_matches=samples_count_matches)
        if import_success:
            message = 'Raw data loaded'
            report[file_folder] = SUCCESS_STATUS
            set_status(processingout_id, message)
            add_to_audit(processingout_id, 'Raw data import', message)
            logging.info(f'{message} for {file_folder}')
        else:
            process_failure(
                processingout_id, file_folder, report, 'Parse raw data',
                'Data import error')
    return import_success


def parse_curated_data(
        processingout_id, json_id, experiment_name, file_folder, report,
        ds_api_helper, hook):
    """
    Download GMET curated results from BioBright and load them
    into fia.annotation and fia.intensities tables
    Args:
        processingout_id: from fia.processingout table
        json_id: to keep connections between the tables
        experiment_name: name of the experiment (starting from "Bio-..."
            to request its files
        file_folder: folder to dowlnoad from
        report: dict to add values for Slack report
        ds_api_helper: DSAPIHelper instance to request BioBright
        hook: database hook to use for inserting into the tables

    Returns: boolean success flag
    """
    with TemporaryDirectory() as tmp_dir_name:
        tmp_file_name = path.join(tmp_dir_name, 'tmp.xlsx')
        ds_api_helper.download_file(
            tmp_file_name,
            client_path=f'{file_folder}/{experiment_name} DATA CURATED.xlsx',
            client_file_ext='.xlsx')
        xls_to_csvs(tmp_file_name, tmp_dir_name)
        logging.info('Loading ions')
        import_csv(
            path.join(tmp_dir_name, 'ions.csv'), hook, 'ion', 'fia',
            processor=process_ions, json_id=json_id)
        logging.info('Loading annotation')
        import_csv(
            path.join(tmp_dir_name, 'annotation.csv'), hook, 'annotation',
            'fia', processor=process_annotations,
            processingout_id=processingout_id, json_id=json_id,
            cast_empty_to_nan=False)
        logging.info('Loading intensities')
        intensities_sheets = [file for file in listdir(tmp_dir_name)
                              if match('^intensities[0-9]+.csv$', file)]
        import_success = True
        for intensity_sheet in intensities_sheets:
            logging.info(
                f'Processing {intensity_sheet} for json {json_id}')
            import_success = (import_success and import_csv(
                path.join(tmp_dir_name, intensity_sheet), hook,
                'intensities', 'fia', processor=process_intensities,
                json_id=json_id, processingout_id=processingout_id))
    if import_success:
        message = 'Curated data loaded'
        log_method = logging.info
        report_file_status(report, file_folder, SUCCESS_STATUS)
        set_status(processingout_id, message)
    else:
        message = 'Curated data parsing error'
        log_method = logging.warning
        report_file_status(report, file_folder, message)
        set_status(processingout_id, 'failed')
    log_method(f'{message} for {file_folder}')
    add_to_audit(processingout_id, 'Parse curated data', message)
    return import_success


GET_DIFF_ID_BT_COMBINED = '''
    SELECT
        jsondiff_id
    FROM
        fia.jsondiff_v
    WHERE
        json_id = %(json_id)s
        AND combined = %(combined)s
'''
GET_DIFF_ID_BY_NUMERATOR_DENOMINATOR = '''
    SELECT
        jsondiff_id
    FROM
        fia.jsondiff
    WHERE
        json_id = %(json_id)s
        AND numerator = %(numerator)s
        AND denominator = %(denominator)
'''


def get_json_diff_id(
        hook, json_id, numerator=None, denominator=None, combined=None):
    """
    Retrieves from db json_diff_id value required for diffs parsing.
    Queries either by 'combined' field of by 'numerator' and 'denominator'
    Args:
        hook: the hook to use when requesting the db
        json_id: required for the request
        numerator: required together with numberator arg
        denominator: required together with numerator arg
        combined: required if numerator or denominator is not provided

    Returns:
        json_diff_id. If not found or wrong args set passed, returns None
    """
    if combined:
        record = hook.get_first(
            GET_DIFF_ID_BT_COMBINED,
            parameters={'json_id': json_id, 'combined': combined})
    elif numerator and denominator:
        record = hook.get_first(
            GET_DIFF_ID_BY_NUMERATOR_DENOMINATOR,
            parameters={
                'json_id': json_id,
                'numerator': numerator, 'denominator': denominator}
        )
    else:
        return
    return record[0] if record else None


def process_diff(df, hook, **kwargs):
    """
    Processes diffs DataFrame before loading into fia.met_diff table.
    To be called from import_csv function (see above)
    Args:
        df: the DataFrame to be processed
        hook: AirFlow database hook, not used here, only for common signature
        json_id, processingout_id: values to be inserted along
    Returns:
        Processed DataFrame
    """
    json_diff_id = get_json_diff_id(
        hook, kwargs['json_id'], combined=df.loc[0, 'difference'])
    result_df = df[['ionIdx', 'log2(FC)', 'p-value', 'adj p-value (BH)']]
    result_df.loc[:, 'ionIdx'] = result_df['ionIdx'].astype(int64)
    result_df.rename(
        columns={
            'ionIdx': 'ionidx',
            'log2(FC)': 'logfc',
            'p-value': 'pval',
            'adj p-value (BH)': 'adjpval'
        },
        inplace=True)
    result_df.loc[:, 'processingout_id'] = kwargs['processingout_id']
    result_df.loc[:, 'jsondiff_id'] = json_diff_id
    result_df.drop_duplicates(inplace=True)
    return result_df


def parse_diff_data(
        processingout_id, experiment_name, file_folder, json_id, report,
        ds_api_helper, hook):
    """
    Download GMET diff results from BioBright and load them into fia.met_diff
    OR fia.cm_met_diff tables
    Args:
        processingout_id: from fia.processingout table
        json_id: to keep connections between the tables
        experiment_name: name of the experiment (starting from "Bio-..."
            to request its files
        ds_api_helper: DSAPIHelper instance to request BioBright
        report: dict to add values for Slack report
        hook: database hook to use for inserting into the tables

    Returns: boolean success flag
    """
    with TemporaryDirectory() as tmp_dir_name:
        tmp_file_name = path.join(tmp_dir_name, 'tmp.xlsx')
        crn_diff = False
        try:
            ds_api_helper.download_file(
                tmp_file_name,
                client_path=f'{file_folder}/{experiment_name} DIFF DATA.xlsx',
                client_file_ext='.xlsx')
        except FileNotFoundError:
            crn_diff = True
            try:
                ds_api_helper.download_file(
                    tmp_file_name,
                    client_path=f'{experiment_name} CRN DIFF Data.xlsx',
                    client_file_ext='.xlsx')
            except FileNotFoundError:
                process_failure(
                    processingout_id, experiment_name, report, 'Parse diff',
                    'File not found')
                return
        xls_to_csvs(tmp_file_name)
        sheet_names = [
            f for f in listdir(tmp_dir_name)
            if f.endswith('.csv') and f != 'Sheet1.csv'
        ]
        import_success = True
        for sheet_name in sheet_names:
            logging.info(f'Processing {sheet_name}')
            import_success = import_success and import_csv(path.join(
                tmp_dir_name, sheet_name), hook,
                'crn_met_diff' if crn_diff else 'met_diff', 'fia',
                processor=process_diff, processingout_id=processingout_id,
                json_id=json_id, crn_diff=crn_diff)
    if import_success:
        message = 'Diff data loaded'
        log_method = logging.info
        report_file_status(report, file_folder, SUCCESS_STATUS)
        set_status(processingout_id, message)
    else:
        message = 'Diff data parsing error'
        log_method = logging.warning
        report_file_status(report, file_folder, message)
        set_status(processingout_id, 'failed')
    log_method(f'{message} for {file_folder}')
    add_to_audit(processingout_id, 'Parse diff data', message)
    return import_success


def parse_files(
        parse_raw=True, parse_curated=True, parse_diff=True, **context):
    """
    Download from BioBright files found by the first DAGs task,
    parse the files and insert into fia tables
    Args:
        **context:

    Returns:
        next task name: Slack messaging if results obtained or finish
        if nothing is done
    """
    task_instance = context['task_instance']
    files_data = task_instance.xcom_pull(
        task_ids='get_new_fiatof_processed_files')
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    ds_api_helper = get_ds_helper(conn_id='biobright')
    result = {}
    for file_data in files_data:
        processingout_id = file_data[GRAB_FILES_ID_POS]
        file_folder = file_data[GRAB_FILES_FOLDER_POS]
        json_file_name = file_data[GRAB_FILES_JSON_POS]
        logging.debug(f'Processing file {json_file_name}')
        experiment_name = file_folder[file_folder.find(' Bio-') + 1:]
        json_id, json_data = parse_json(
            processingout_id, file_folder, json_file_name, ds_api_helper,
            result, hook)
        if not json_id:
            continue
        logging.debug(f'Cleaning up data for JSON {json_id}')
        hook.run(f'SELECT fia.json_cleanup(%(json_id)s);',
                 parameters={'json_id': json_id}, autocommit=True)
        jsondiff_samples_count = parse_jsonsample(
            processingout_id, json_id, json_data['data'],
            json_data.get('vars'), hook)
        if parse_raw:
            parse_raw_data(
                    processingout_id, json_id, file_folder, experiment_name,
                    jsondiff_samples_count, result, ds_api_helper, hook)
        if parse_curated:
            parse_curated_data(
                processingout_id, json_id, experiment_name, file_folder,
                result, ds_api_helper, hook)
        if parse_diff:
            parse_diff_data(
                processingout_id, experiment_name, file_folder, json_id,
                result, ds_api_helper, hook)
    result = '\n'.join(['*{result}*: `{filename}`{error}'.format(
        result=(f':white_check_mark: {SUCCESS_STATUS}'
                if result == SUCCESS_STATUS else ':x: Error'),
        filename=filename,
        error=f' ({result})' if result != SUCCESS_STATUS else '')
        for filename, result in result.items()])
    task_instance.xcom_push(key='output', value=result)
    return 'process_fiatof_files' if result else 'finish'


INVALIDATED_STATUS = 'invalidated'
GRAB_FILES_COLUMNS = [
    'processingout_id', 'processingout_foldername', 'json_filename']
GRAB_FILES_QUERY = f'''
    UPDATE
        fia.processingout
    SET
        load_status = 'Started'
    WHERE
        COALESCE(load_status, %(invalidated_status)s) = %(invalidated_status)s
    RETURNING
        {','.join(GRAB_FILES_COLUMNS)}
        '''
GRAB_FILES_ID_POS = GRAB_FILES_COLUMNS.index('processingout_id')
GRAB_FILES_FOLDER_POS = GRAB_FILES_COLUMNS.index('processingout_foldername')
GRAB_FILES_JSON_POS = GRAB_FILES_COLUMNS.index('json_filename')

if __name__.startswith('unusual_prefix'):

    get_new_processed_files_task = PostgresOutputOperator(
        task_id='get_new_fiatof_processed_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql=GRAB_FILES_QUERY,
        parameters={'invalidated_status': INVALIDATED_STATUS},
        autocommit=True,
        dag=dag,
    )

    parse_files_task = BranchPythonOperator(
        task_id='parse_fiatof_files',
        python_callable=parse_files,
        provide_context=True,
        dag=dag,
    )

    process_files_task = PostgresOutputOperator(
        task_id='process_fiatof_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT filename, output FROM stg_infodw.fia_process_files()',
        autocommit=True,
        dag=dag,
    )

    slack_conn = get_slack_conn(f'{dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: FIATOF raw files were loaded and processed. "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull("
                "task_ids='parse_fiatof_files', key='output') }}"),
        username='Airflow',
        dag=dag,
    )

    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )

    get_new_processed_files_task >> parse_files_task >> process_files_task >>\
        slack_report_task >> finish_task
    get_new_processed_files_task >> parse_files_task >> finish_task
