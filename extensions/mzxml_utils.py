import logging
from os import path
from platform import system
from subprocess import run
from extensions.utils import egnytize
from egnyte.exc import (  # noqa F401
    NotFound, InsufficientPermissions, NotAuthorized, EgnyteError
)


MSCONVERT_ARGS_TEMPLATE = (
    '--mzXML --filter "peakPicking true [1,1]" --filter "polarity {polarity}" '
    '"/raw/{file_name}" -o "/raw/" --outfile "{outfile}"'
)
LINUX_DOCKER_COMMAND_TEMPLATE = (
    'docker run -v {conversion_folder}:/raw -e WINEDEBUG=-all '
    '--rm chambm/pwiz-skyline-i-agree-to-the-vendor-licenses wine msconvert '
    '{msconvert_args}'
)  # TODO: move to Airflow variable
WINDOWS_COMMAND_TEMPLATE = (
    '"c:\\Program Files\\ProteoWizard\\ProteoWizard 3.0.19311.2045238a9\\'
    'msconvert.exe" {msconvert_args}'
)
COMMAND_TEMPLATES = {
    'Windows': WINDOWS_COMMAND_TEMPLATE,
    'Linux': LINUX_DOCKER_COMMAND_TEMPLATE,
    'Darwin': LINUX_DOCKER_COMMAND_TEMPLATE,  # MacOS
}
COMMAND_TEMPLATE = COMMAND_TEMPLATES[system()]


SAPIO_PATH_MARKER = '/Sapio/'


class DownloadError(Exception):
    """Error downloading file"""
    pass


class BBFileDownloader:
    """Context manager for downloading file from given BB S3 URL"""
    def __init__(self, s3_url, file_id, ds_helper):
        self.s3_url = s3_url
        self.file_id = file_id
        self.ds_helper = ds_helper

    def __enter__(self):
        self.response = self.ds_helper.get_s3_file(self.s3_url, self.file_id)
        if self.response is None:
            raise DownloadError()
        return self

    def read(self):
        return self.response.content if self.response is not None else b''

    def iter_content(self, chunk_size=16384):
        return self.response.iter_content(chunk_size) \
            if self.response is not None else b''

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.response is not None:
            self.response.close()


class BBFile:
    """BioBright wrapper for downloading files with
       Egnyte-compatible interface
    """
    def __init__(self, name, path, s3_url, file_id, ds_helper):
        self.name = name
        self.path = path
        self.s3_url = s3_url
        self.file_id = file_id
        self.ds_helper = ds_helper

    def download(self):
        return BBFileDownloader(self.s3_url, self.file_id, self.ds_helper)


def get_files_from_bb_by_exp(exp_path, ds_helper, extension, sub_path=''):
    """Return list of file objects found on given BB path"""
    if SAPIO_PATH_MARKER in exp_path:
        exp_path = exp_path[exp_path.find(SAPIO_PATH_MARKER):]
    files_data, files_included = ds_helper.get_files_index(
        client_path=exp_path.replace('/', '\\\\') + sub_path,
        client_file_ext=extension, and_included=True,
        remove_obsolete_versions=True)
    return [
        BBFile(file_data['attributes']['client_file_name'],
               egnytize(file_data['attributes']['client_path']),
               next(include_item['attributes']['s3_link']
                    for include_item in files_included
                    if include_item['id'] == file_data['id']),
               file_data['id'],
               ds_helper)
        for file_data in files_data]


def get_files_from_egnyte_by_exp(exp_path, egnyte_client,
                                 extension, sub_path=''):
    """Return list of file objects found on given Egnyte path"""
    root_folder = egnyte_client.folder(exp_path + sub_path)
    try:
        root_folder.list()
        result = [
            f for f in root_folder.files
            if path.splitext(f.name)[1] == extension
        ]
        folders = root_folder.folders
        while folders:
            child_folders = []
            for folder in folders:
                folder.list()
                result += [
                    f for f in folder.files
                    if path.splitext(f.name)[1] == extension
                ]
                child_folders += folder.folders
            folders = child_folders
    except NotFound:
        logging.warning(f'Path "{exp_path}" not found on Egnyte, skipped')
        return []
    except InsufficientPermissions:
        logging.error(f'Egnyte insufficient permissions')
        return []
    except NotAuthorized:
        logging.error(f'Egnyte authorization failed, exiting')
        return []
    return result


def call_msconvert(full_path, polarity, raise_exceptions=False):
    """Call msconvert for converting certain single raw file
       and return the name of the converted mzXML file
       Arguments:
           - full path to the file
           - polarity to be specified for the conversion process

        Return the converted file name or None in case of failure
    """
    file_path, file_name = path.split(full_path)
    file_name_only, file_ext = path.splitext(file_name)
    outfile = f'{file_name_only}_{polarity[:3]}'
    command = COMMAND_TEMPLATE.format(
        conversion_folder=file_path,
        msconvert_args=MSCONVERT_ARGS_TEMPLATE.format(
            file_name=file_name, polarity=polarity,
            outfile=outfile)
    )
    result = run(command, text=True, capture_output=True, shell=True)
    log_message = (
        f'Command: "{command}", return_code {result.returncode}, '
        f'Output: "{result.stdout}", errors: "{result.stderr}'
    )
    if raise_exceptions:
        result.check_returncode()
    if result.returncode:
        logging.error(log_message)
        converted_file_name = None
    else:
        logging.debug(log_message)
        converted_file_name = f'{outfile}.mzXML'
    return converted_file_name
