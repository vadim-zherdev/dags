from datetime import datetime
import logging
from math import sqrt

from numpy import nonzero
from scipy import io as spio
from statistics import median


# used throughout processing for
# binning and mass accuracy assumptions; default=2
QC_ACCURACY = 2
# Fractional minimum intensity filter relative to
# the summed TIC; default=0.001'
QC_INTENSITY_FILTER = 0.001


class SummedSpectrum:

    def __init__(self, filename):

        self.tmp_filename = filename

        self.spectrum = dict()
        self.intensity_filter = QC_INTENSITY_FILTER

        self.start_time_stamp = None
        self.fiamat_tic = int()
        self.low_mz = 50.
        self.high_mz = 1050.
        self.mode = str()
        self.num_centroids = int()

        self.v = list()
        self.scaled_v = list()
        self.normalized_scaled_v = list()
        self.normalized_v = list()
        self.nz_array = list()

        self.summed_tic = float()
        self.signal_to_noise = float()
        self.precision = QC_ACCURACY

    def _set_mode(self, polarity):
        self.mode = {'+': 'pos', '-': 'neg'}.get(polarity, 'unk')

    def _sort_spectrum_by_mz(self):
        self.spectrum = {
            float(k): v for k, v in self.spectrum.items()}

    def _filter_by_intensity(self):
        max_ = max(self.spectrum.values())
        min_ = max_ * self.intensity_filter

        self.spectrum = {k: v for k, v in self.spectrum.items() if v >= min_}
        logging.info(f'Peak filtering: {len(self.spectrum)} to'
                     f' {len(self.spectrum)} peaks, minAb={int(min_)}'
                     f' at {self.intensity_filter * 100}% of base peak')

    def _calculate_signal_to_noise(self):
        mx = max(self.spectrum.values())
        med = median(self.spectrum.values())
        self.signal_to_noise = round(mx / med, 2)

    def _sum_tic(self):
        self.summed_tic = sum(self.spectrum.values())

    def _get_centroids(self):
        self.num_centroids = len(self.spectrum)

    def _vectorize(self):

        def bin_spectrum(s, precision):
            spectr = dict()
            for mz, ab in s.items():
                binned_mz = round(mz, precision)
                if binned_mz not in spectr:
                    spectr[binned_mz] = ab
                else:
                    spectr[binned_mz] += ab
            return spectr

        i = self.low_mz

        # Adjusted per change below for binning.
        mz_step = round(0.1 ** self.precision, self.precision)
        # PAR 11/18/2019 Will increase size of vectors 10X.

        # Reduce dimensions for dot product calculations
        # Changing this to bin at acc, will change dp's!
        spectrum = bin_spectrum(self.spectrum, self.precision)
        # PAR 11/18/2019

        while i < self.high_mz:
            # filtered_spectrum must be binned at the same precision
            if i in spectrum:
                # vectorize non-zero abundances
                self.v.append(spectrum[i])
                # square root scaling
                self.scaled_v.append(sqrt(spectrum[i]))
            else:
                self.v.append(0)
                self.scaled_v.append(0)
            i += mz_step

            # fix floating-point problems
            i = round(i, self.precision)

        # Create an additional list of
        # nonzero indices; speeds up dp calc
        self.nz_array = nonzero(self.v)[0]

    def _normalize(self):
        """
        TIC-based normalization
        :return:
        """
        ssv = sum(self.scaled_v)
        self.normalized_scaled_v = [ab / ssv for ab in self.scaled_v]

    def from_fiamat(self):
        """
        This function adds compatibility with .fiamat
        data with the fia_spectrum class
        :param self:
        :return:
        """

        # load content of matlab file
        fia = spio.loadmat(
            self.tmp_filename,
            struct_as_record=False,
            squeeze_me=True
        )

        self.spectrum = dict(fia['data'].cent)
        self._sort_spectrum_by_mz()
        self._filter_by_intensity()

        self.start_time_stamp = datetime.strptime(
            fia['data'].acquisitionDate,
            "%m/%d/%Y %H:%M:%S"
        )
        self.fiamat_tic = fia['data'].tic
        self._sum_tic()

        self.low_mz = fia['data'].mzMin
        self.high_mz = fia['data'].mzMax
        self._set_mode(fia['data'].polarity)

        self._calculate_signal_to_noise()
        self._get_centroids()

        self._vectorize()
        self._normalize()


def open_fiamat_file(filepath):
    """
    Open fiamat file and parse its contents

    :param filepath: full filepath of Fiamat file
    :return: spectrum object
    """

    spectrum = SummedSpectrum(filepath)
    spectrum.from_fiamat()
    return spectrum
