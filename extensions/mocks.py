import re

from darwinsync_api_helper import FileNotFoundError


class MockCursor:
    def __init__(self, response=None):
        self.copy_from_call_count = 0
        self.response = response
        self.execute_log = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def copy_from(self, buffer, table, columns=None):
        self.copy_from_call_count += 1
        self.copy_from_buffer = buffer
        self.copy_from_table = table

    def execute(self, query, parameters=None):
        self.execute_log.append(query)

    def fetchone(self):
        return self.response


class MockConnection:

    def __init__(self, response=None):
        self.response = response

    def cursor(self):
        self.cur = MockCursor(response=self.response)
        return self.cur

    def commit(self):
        pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class MockPostgresHook:
    def __init__(self, postgres_conn_id=None, autocommit=False):
        pass

    def get_conn(self, response=None):
        self.conn = MockConnection(response=getattr(self, 'response', None))
        return self.conn

    def set_autocommit(self, conn, value):
        pass

    def get_records(self, *args, **kwargs):
        return []

    def run(self, *args, **kwargs):
        pass


class MockPostgresHookWithResponse(MockPostgresHook):
    def __init__(self, postgres_conn_id=None, autocommit=False):
        self.response = [(1, 2, 3, 4, 5)]
        self.pattern_response = dict()
        super(MockPostgresHookWithResponse, self).__init__()

    def run(self, query, *args, **kwargs):
        pass

    def get_records(self, query, *args, **kwargs):
        for pattern, response in self.pattern_response.items():
            if re.search(pattern, query):
                return response
        return self.response

    def get_first(self, query, *args, **kwargs):
        for pattern, response in self.pattern_response.items():
            if re.search(pattern, query, re.DOTALL):
                return response[0]
        return self.response[0]

    def insert_rows(self, query, *args, **kwargs):
        pass


class MockPostgresHookWithSetResponse(MockPostgresHookWithResponse):
    def __init__(self, postgres_conn_id=None, autocommit=False, response=None):
        self.response = response if response else [
            ('1', '1', True), ('2', '2', False)]
        self.pattern_response = dict()
        super(MockPostgresHookWithResponse, self).__init__()


class MockSapioAPIHelper:
    def __init__(
            self, data_types=None, report=None, data_records=None,
            data_records_parents=None):
        self.data_types = data_types
        self.report = report
        self.data_records = data_records
        self.data_records_parents = data_records_parents
        self.next_value = 0

    def get_data_types(self):
        return self.data_types

    @property
    def next_int(self):
        self.next_value += 1
        return self.next_value

    def get_report(self, datatype_names, condition=None, page_size=None,
                   page_number=None):
        if self.report:
            return self.report
        datatypes = datatype_names.split(',')
        result = {
            'results': [[self.next_int for _ in range(len(datatypes))]],
            'columns': [{
                'datatype_name': datatype.split('.')[0],
                'datafield_name': datatype.split('.')[1]
            } for datatype in datatypes]
        }
        return result

    def get_data_records(self, data_type_name):
        return self.data_records

    def get_data_records_parents(self, *args, **kwargs):
        return self.data_records_parents


class MockAirflowTaskInstance:
    def __init__(self, xcom_value):
        self.xcom_value = xcom_value

    def xcom_pull(self, task_ids=None, key=None):
        return self.xcom_value[task_ids][key]

    def xcom_push(self, key, value):
        self.xcom_value.update({key: value})


class MockEgnyteFileDownload:
    def __init__(self, content=None):
        self.content = content
        self.exausted = False
        self.position = 0

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def read(self, *args, decode_content=False):
        if hasattr(self.content, 'read'):
            return self.content.read(decode_content=decode_content)
        elif decode_content:
            return self.content.decode('UTF-8')
        else:
            return self.content

    def __iter__(self):
        return self.content


class MockEgnyteFile:
    name = 'test_file_name.csv'
    path = 'some_path/' + name

    def __init__(self, content=None):
        self.content = content

    def download(self):
        return MockEgnyteFileDownload(content=self.content)


class MockEgnyteFolder:
    def __init__(self, content=None):
        self.files = [MockEgnyteFile(content=content)]
        self.folders = []

    def list(self):
        pass


class MockEgnyte:
    def __init__(self, folder_content=None, file_content=None):
        self._folder = MockEgnyteFolder(content=folder_content)
        self._file = MockEgnyteFile(content=file_content)
        self.folder_content = folder_content
        self.file_content = file_content

    def folder(self, path):
        return self._folder

    def file(self, *args):
        return self._file


class MockDSAPIHelper:
    def __init__(self, mock_data=None, mock_file_content=None, **kwargs):
        self.kwargs = None
        self.mock_data = mock_data
        self.mock_file_content = mock_file_content

    def get_files_index(self, **kwargs):
        self.kwargs = kwargs
        return self.mock_data[0].copy(), self.mock_data[1].copy()

    def get_file(self, **kwargs):
        if not self.mock_file_content:
            raise FileNotFoundError
        return MockEgnyteFileDownload(content=self.mock_file_content)
