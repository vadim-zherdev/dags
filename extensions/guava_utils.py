"""Useful methods for parsing GUAVA files."""
import logging

from darwinsync_api_helper import FileNotFoundError
from pandas import DataFrame

from egnyte.exc import NotFound
from .utils import load_dataframe, get_ds_helper, get_egnyte_client

GUAVA_FILES_QUERY = f'''
    SELECT
        id,
        experiment_id,
        experiment_name,
        plate_id,
        src_filename,
        result_stage,
        load_status
    FROM
        stg_infodw.guava_start_files()
'''
GUAVA_HEADER_SIZE = 6
NEEDED_COLUMNS = ['row_letter', 'column_number', 'result_value']
GUAVA_RESULT_TYPES = ['ANALYZED_DATA', 'RAW_DATA']


def set_guava_file_status(guava_file_id, status, db_hook):
    """Set given status to given GUAVA file."""
    db_hook.run(
        '''
            UPDATE
                stg_infodw.guava_files
            SET
                load_status=%(status)s
            WHERE
                id = %(id)s
        ''',
        parameters=dict(status=status, id=guava_file_id),
    )


def add_to_audit(file_id, action_name, action_result, db_hook):
    """Write a row to GUAVA audit table."""
    db_hook.run(
        '''
            INSERT INTO stg_infodw.guava_file_audit
            (
                guava_file_id,
                action_name,
                action_result
            )
            VALUES
            (
                %(file_id)s,
                %(action_name)s,
                %(action_result)s
            )
        ''',
        parameters=dict(
            file_id=file_id,
            action_name=action_name,
            action_result=action_result,
        )
    )


def report_result(
        file_id, src_filename, message, status, report, db_hook,
        success_status):
    """All necessary actions for file loading completion."""
    add_to_audit(file_id, 'Loading file', message, db_hook)
    if status == success_status:
        report[src_filename] = success_status
        set_guava_file_status(file_id, 'loaded', db_hook)
        logging.info(f'File {src_filename} loaded successfully')
    else:
        report[src_filename] = message
        set_guava_file_status(file_id, status, db_hook)
        logging.warning(f'{message} for file {src_filename}')


def parse_guava_results(files_data, db_hook, success_status, check_bb=True):
    """Download GUAVA result files, parse and load into the DB table"""
    report = {}
    ds_helper = get_ds_helper()
    egnyte_client = get_egnyte_client()
    for (file_id, experiment_id, experiment_name, plate_id, src_filename,
         result_stage, load_status) in files_data:
        buffer = None
        if check_bb:
            bb_path = src_filename[
                      src_filename.find('/Sapio/') + 1:].replace('/', '\\\\')
            try:
                buffer = ds_helper.get_file(client_path=bb_path)
            except FileNotFoundError:
                logging.warning(
                    f'File {src_filename} not found on BioBright')
        if not buffer:
            try:
                buffer = [line.decode('ISO-8859-1') for line
                          in egnyte_client.file(src_filename).download()]
            except NotFound:
                message = 'Not found on Egnyte'
                report[src_filename] = message
                set_guava_file_status(file_id, 'failed', db_hook)
                add_to_audit(file_id, 'Loading file', message, db_hook)
                continue
        header_skipped = False
        header = []
        csv_data = []
        for line in buffer:
            if not line:
                continue
            if not header_skipped:
                if 0 <= line.find('Sample') <= 1:
                    header_skipped = True
                    header = line.split(',')
                continue
            csv_data.append(line.split(',')[:len(header)])
        if not (header and csv_data):
            report_result(file_id, src_filename, 'Data not found', 'failed',
                          report, db_hook, success_status)
            continue
        df = DataFrame(csv_data, columns=header)
        try:
            df['row_letter'] = df[' Sample ID'].str[1]
            df['column_number'] = df[' Sample ID'].str[2:].astype(int)
        except (ValueError, AttributeError):
            report_result(file_id, src_filename, 'Incorrect format', 'failed',
                          report, db_hook, success_status)
            continue
        df = df.iloc[:, ~df.columns.duplicated()].rename(
            columns={' Viable': 'result_value'})
        df.drop(
            columns=[col_name for col_name in df
                     if col_name not in NEEDED_COLUMNS],
            inplace=True)
        df['guava_file_id'] = file_id
        result_type = next((
            guava_result_type for guava_result_type in GUAVA_RESULT_TYPES
            if guava_result_type in src_filename),
            None)
        df['result_type'] = result_type
        load_dataframe(
            df, db_hook, 'guava_file_contents', 'stg_infodw',
            reorder_columns=True)
        report_result(file_id, src_filename, 'Loaded successfully',
                      success_status, report, db_hook, success_status)
    return report
