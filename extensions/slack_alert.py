from airflow.contrib.hooks.slack_webhook_hook import SlackWebhookHook

from extensions.utils import get_slack_conn


def send_slack_message(message, dag_id):
    """
    Sends arbitrary message to Slack using AirFlow connection with given ID
    Args:
        message: str, message to send
        dag_id: str, dag name for creating AirFlow connection ID

    Returns:
        Slack response (whatever it is)
    """

    slack_conn = get_slack_conn(f'{dag_id}_slack')

    slack_hook = SlackWebhookHook(
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=message,
        username='Airflow'
    )
    return slack_hook.execute()


def task_fail_slack_alert(context):
    """
    Retrieves information about current task instance and sends a report
    to Slack using send_slack_message method.
    Args:
        context: AirFlow DAG's context, usually passed by
            provide_context=True argument

    Returns:
        Slack response (whatever it is)
    """
    task_instance = context['ti']
    slack_msg = (
        ':shrug: Task Failed.\n*Task*: {task}\n*Dag*: {dag}\n'
        '*Execution Time*: {exec_date}\n*Log Url*: {log_url}'.format(
            task=task_instance.task_id, dag=task_instance.dag_id,
            exec_date=context['execution_date'],
            log_url=task_instance.log_url))
    return send_slack_message(slack_msg, task_instance.dag_id)


def prepare_report(raw_report, success_status, max_files):
    """Prepare short and full versions of Slack report."""
    summary = 'total: {total}, failed: {failed}'.format(
        total=len(raw_report),
        failed=sum(1 for v in raw_report.values()
                   if v != success_status))
    # Compress information about OK files if needed
    if len(raw_report) > max_files:
        ok_count = sum(1 for v in raw_report.values()
                       if v == success_status)
        raw_report = dict(filter(lambda i: i[1] != success_status,
                                 raw_report.items()))
        raw_report[f'{ok_count} files (filenames hidden)'] = \
            success_status
    full_report = '\n'.join(['*{result}*: `{filename}`{error}'.format(
        result=result if result == success_status else 'Error',
        filename=filename,
        error=f' ({result})' if result != success_status else '')
        for filename, result in raw_report.items()])
    return summary, full_report
