"""Functions used when importing Sapio data into the database."""
from datetime import date, timedelta
from io import StringIO
import logging
from operator import itemgetter
from re import sub, compile, IGNORECASE

from numpy import str
from pandas import DataFrame, read_csv
from sqlalchemy.exc import ProgrammingError

from .utils import get_sapio_helper, get_egnyte_client, load_dataframe

SCHEMA = 'stg_infodw'
TABLE_PREFIX = 'sapio_'
SAPIO_CHUNK_SIZE = 100_000
RHE_PLATE_WELL_TABLE_NAME = TABLE_PREFIX + 'rhe_plate_well'
RHE_EXP_TABLE_NAME = TABLE_PREFIX + 'rhe_exp'
DEFAULT_DAYS_DELTA = 10
SAPIO_DATE_FORMAT = '%m/%d/%Y'

TABLE_NAMES_QUERY = '''
    SELECT
        DISTINCT table_name
    FROM
        information_schema.tables
    WHERE
        table_schema = %(schema)s
        AND table_type = 'BASE TABLE'
        AND table_name like %(table_prefix)s
'''
SAPIO_DATAFIELD_TYPES = {
    0: 'bool',
    1: 'float',
    2: 'Int8',
    3: 'Int32',
    4: 'Int16',
    5: 'Int64',
    6: str,
    7: 'Int64',
    # 8: ?
    # 9: ?
    10: str,
    11: str,
    12: str,
    13: str,
    # 14: ?
    # 15: ?
    16: 'Int64',
}


def populate_sapio_tables(
        db_hook, success_status, debug=False, tables_data=None):
    """
    Get all available data types from Sapio API and import into existing
    tables with the same names. Skip if the table does not exist.
    Args:
        db_hook: AirFlow database hook to use
        success_status: value to put into returned dict for successfully
            processed tables
        debug: insert into the tables only one first raw of data
        tables_data: list of one-tuples with the names on the tables.
            If not provided, all tables with the given prefix will be included.
    Returns:
        dict with the tables names as the keys and results and error messages
            as the values.
    """
    sapio_api_helper = get_sapio_helper()
    data_types = sapio_api_helper.get_data_types()
    data_types_ids = {item['datatype'].lower(): item['datatype_id']
                      for item in data_types}
    if not tables_data:
        tables_data = db_hook.get_records(
            TABLE_NAMES_QUERY,
            parameters={'schema': SCHEMA, 'table_prefix': f'{TABLE_PREFIX}%%'})
    result = {}
    for table_record in tables_data:
        table_name = sub(f'^{TABLE_PREFIX}', '', table_record[0].lower())
        data_type_id = data_types_ids.get(table_name)
        if not data_type_id:
            logging.info(f'{table_name} does not exist, skipped')
            continue
        data_type = [item for item in data_types
                     if item['datatype_id'] == data_type_id][0]
        report = sapio_api_helper.get_report(
            data_type['datatype'] + '.%', page_size=SAPIO_CHUNK_SIZE,
            page_number=0)
        if not report:
            result[table_name] = 'Unable to request data from Sapio'
            continue
        columns_data = {
            column['datafield_name'].lower(): SAPIO_DATAFIELD_TYPES[
                column.get('datafield_type')]
            for column in report['columns']}
        total_rows = report['total']
        page_number = 0
        db_hook.run(
            f'TRUNCATE TABLE {SCHEMA}.{TABLE_PREFIX}{table_name}',
            autocommit=True)
        while True:
            df = DataFrame(report['results'], columns=columns_data.keys())
            for column_name, column_data in df.iteritems():
                type_to_cast = columns_data[column_name]
                if (isinstance(type_to_cast, str) and
                        type_to_cast.lower().startswith('int') and
                        column_data.dtype.name.lower().startswith('float')):
                    column_data = column_data.round(0)
                df[column_name] = column_data.astype(columns_data[column_name])
            # TODO: add missing columns
            if not len(df):
                logging.info(f'Empty dataset for {table_name}')
                result[table_name] = 'empty dataset'
                break
            logging.info(
                f'Loading rows {page_number * SAPIO_CHUNK_SIZE} - '
                f'{page_number * SAPIO_CHUNK_SIZE + df.shape[0]} '
                f'of {total_rows} into {table_name}')
            df.loc[:, df.dtypes == bool] = df.loc[:, df.dtypes == bool].astype(
                'int8')
            try:
                if debug:
                    df.drop(df.index[1:], inplace=True)
                load_dataframe(
                    df, db_hook, TABLE_PREFIX + table_name, schema=SCHEMA,
                    reorder_columns=True, cast_empty_to_nan=False)
                result[table_name] = success_status
            except ProgrammingError as e:
                logging.warning(f'Error loading {table_name}: {e}')
                result[table_name] = str(e)
            if page_number >= total_rows // SAPIO_CHUNK_SIZE:
                break
            page_number += 1
            report = sapio_api_helper.get_report(
                data_type['datatype'] + '.%', page_size=SAPIO_CHUNK_SIZE,
                page_number=page_number)
    return result


EXEMPLARCONFIG_QUERY = f'''
    SELECT  platedesignerprimarytypes
    FROM    {SCHEMA}.{TABLE_PREFIX}exemplarconfig
'''

PLATE_WELL_DATA_TYPES = [
        'Sample.OnPlateId', 'Sample.OtherSampleId', 'Sample.RecordId',
        'WellElement.WellElementDataType', 'WellElement.WellElementSubType',
        'WellElement.Layer', 'WellElement.Amount', 'WellElement.UnitOfMeasure',
        'WellElement.Notes', 'WellElement.TimePoint', 'Plate.PlateId',
        'WellElement.RecordId',
    ]
WELL_ELEMENT_DATA_TYPE_POS = PLATE_WELL_DATA_TYPES.index(
    'WellElement.WellElementDataType')
WELL_ELEMENT_SUBTYPE_POS = PLATE_WELL_DATA_TYPES.index(
    'WellElement.WellElementSubType')
WELL_ELEMENT_RECORD_ID_POS = PLATE_WELL_DATA_TYPES.index(
    'WellElement.RecordId')
PLATE_WELL_COLUMNS_NAMES = [
    'on_plate_id', 'sample_name', 'sample_record_id', 'wellelement_datatype',
    'wellelement_subtype', 'wellelement_layer', 'wellelement_amount',
    'wellelement_uom', 'wellelement_notes', 'wellelement_timepoint',
    'wellelement_record_id', 'data_type_subtype_record_id', 'plate_id',
    'plate_row', 'plate_column', 'Plate Nbr',
]
ON_PLATE_ID_POS = PLATE_WELL_COLUMNS_NAMES.index('on_plate_id')
PLATE_ROW_POS = PLATE_WELL_COLUMNS_NAMES.index('plate_row')
PLATE_COLUMN_POS = PLATE_WELL_COLUMNS_NAMES.index('plate_column')
PLATE_NBR_POS = PLATE_WELL_COLUMNS_NAMES.index('Plate Nbr')
RHE_PLATE_WELL_TMP_TABLE_NAME = RHE_PLATE_WELL_TABLE_NAME + '_tmp'


def populate_rhe_plate_well(
        db_hook, success_status, sapio_report_days_delta=None):
    """
    Collect data from Sapio and populate '...rhe_plate_well' table
    Args:
        db_hook: hook to use with the database
        success_status: value to put into returned dict in case of success
    """
    exemplar_config = db_hook.get_first(EXEMPLARCONFIG_QUERY)
    plate_types = str(exemplar_config[0]).split(',')
    logging.info(f'Collecting data for plate types {plate_types}')
    sapio_api_helper = get_sapio_helper()
    layer_data_types = {
        plate_type: {
            report_row[0]: report_row[1]
            for report_row in sapio_api_helper.get_report(
                '{plate_type}.DataRecordName, {plate_type}.RecordId'.format(
                    plate_type=plate_type),
                condition=plate_type + '.RecordId > 0')['results']}
        for plate_type in plate_types}
    logging.info('Requesting wells data')
    min_data = (date.today() - timedelta(
        days=int(sapio_report_days_delta or DEFAULT_DAYS_DELTA))).strftime(
        SAPIO_DATE_FORMAT)
    report = sapio_api_helper.get_report(
        ','.join(PLATE_WELL_DATA_TYPES),
        condition="Sample.OnPlateId <> '' AND "
                  f"[WellElement.VeloxLastModifiedDate > {min_data} OR "
                  f"WellElement.DateCreated > {min_data}]")
    pattern = compile(r'(P-\d+)_([A-H])(\d+)', IGNORECASE)
    samples_list = []
    well_element_record_ids = set()
    logging.info(f"Processing {len(report['results'])} rows of wells data")
    for report_row in report['results']:
        well_element_record_id = report_row[WELL_ELEMENT_RECORD_ID_POS]
        if well_element_record_id in well_element_record_ids:
            continue
        well_element_record_ids.add(well_element_record_id)
        sample = report_row[:10]
        sample.extend([
            well_element_record_id,
            layer_data_types.get(
                report_row[WELL_ELEMENT_DATA_TYPE_POS], {}).get(
                report_row[WELL_ELEMENT_SUBTYPE_POS], 0)])
        match = pattern.match(sample[ON_PLATE_ID_POS])
        sample.extend([
            match.group(1), match.group(2), int(match.group(3)),
            int(match.group(1)[2:])] if match else [None] * 4)
        samples_list.append(sample)
    samples_list.sort(key=itemgetter(
        PLATE_NBR_POS, PLATE_ROW_POS, PLATE_COLUMN_POS))
    samples_list = [item[:-1] for item in samples_list]
    samples_df = DataFrame(samples_list, columns=PLATE_WELL_COLUMNS_NAMES[:-1])
    with db_hook.get_conn() as conn:
        with conn.cursor() as cursor:
            cursor.execute(f'''
                CREATE TEMPORARY TABLE {RHE_PLATE_WELL_TMP_TABLE_NAME}
                ON COMMIT DROP
                AS TABLE {SCHEMA}.{RHE_PLATE_WELL_TABLE_NAME}
                WITH NO DATA;
            ''')
            cursor.execute('''
                SELECT
                    table_schema
                FROM
                    information_schema.TABLES
                WHERE
                    table_name = %(table_name)s
            ''', {'table_name': RHE_PLATE_WELL_TMP_TABLE_NAME})
            schema = cursor.fetchone()[0]
            logging.debug(
                f'Temporary table {schema}.{RHE_PLATE_WELL_TMP_TABLE_NAME} '
                'created')
            buffer = StringIO()
            samples_df.to_csv(
                buffer, index=False, sep='\t', header=False, na_rep=r'\N')
            buffer.seek(0)
            cursor.copy_from(
                buffer, RHE_PLATE_WELL_TMP_TABLE_NAME,
                columns=list(samples_df))
            logging.debug(
                f'Temporary table {schema}.{RHE_PLATE_WELL_TMP_TABLE_NAME} '
                'populated')
            cursor.execute(
                'SELECT sapio_load_rhe_plate_well '
                'FROM stg_infodw.sapio_load_rhe_plate_well()')
            result = cursor.fetchone()[0]
            logging.info(result)
    return {RHE_PLATE_WELL_TABLE_NAME: success_status}


def get_experiment_properties_df(sapio_api_helper, datatype_names):
    """
    Requests Sapio API for given experiment properties and returns
        pandas DataFrame containing the result.
    Parameters
    ----------
    sapio_api_helper - API helper to use while communicating with Sapio
    datatype_names - experiment properties to ask for

    Returns
    -------
        pandas.DataFrame with the properties from Sapio
    """
    exp_studies_report = sapio_api_helper.get_report(
        'ELNExperiment.RecordId,' + datatype_names,
        condition='ELNExperiment.RecordId > 0')
    result_df = DataFrame(
        exp_studies_report['results'],
        columns=[column['datatype_name'] + '.' + column['datafield_name']
                 for column in exp_studies_report['columns']])
    return result_df


def populate_rhe_exp(db_hook, success_status):
    """
    Fills in 'rhe_exp' table with data retrieved from Sapio by API
    Parameters
    ----------
    db_hook -  database hook to use while inserting the data
    success_status - string to include into the returning value

    Returns
    -------
    {RHE_EXP_TABLE_NAME: success status}
    """
    sapio_api_helper = get_sapio_helper()
    exp_records = sapio_api_helper.get_data_records('ELNExperiment')
    exp_columns = list(exp_records[0]['fields'].keys())
    exp_columns.sort()
    flat_exp_records = [[exp_record['fields'].get(exp_column)
                         for exp_column in exp_columns]
                        for exp_record in exp_records]
    exp_records_df = DataFrame(flat_exp_records, columns=exp_columns)
    exp_records_df.rename(
        columns={'RecordId': 'ELNExperiment.RecordId'}, inplace=True)
    foreign_exp_properties = [
        'Study.RecordId,Study.StudyId',
        'Project.RecordId,Project.ProjectId',
        'VeloxDepartment.RecordId,VeloxDepartment.DepartmentName',
        'VeloxLocation.RecordId,VeloxLocation.LocationName',
    ]
    for exp_properties in foreign_exp_properties:
        exp_property_df = get_experiment_properties_df(
            sapio_api_helper, exp_properties)
        exp_records_df = exp_records_df.merge(
            exp_property_df, how='left', on='ELNExperiment.RecordId')
    velox_location_ids = list(
        exp_property_df['VeloxLocation.RecordId'].unique())
    exp_parent_report = sapio_api_helper.get_data_records_parents(
        'VeloxLocation', velox_location_ids, parent_data_type='Directory')
    location_parents = [
        (item[0]['record_id'], item[0]['fields']['DirectoryName'])
        for item in exp_parent_report]
    location_parents = [
        (item[0], item[1][0], item[1][1])
        for item in zip(velox_location_ids, location_parents)]
    location_parents_df = DataFrame(location_parents, columns=[
        'VeloxLocation.RecordId',
        'Directory.RecordId',
        'Directory.DirectoryName'])
    exp_records_df = exp_records_df.merge(
        location_parents_df, how='left', on='VeloxLocation.RecordId')
    columns_to_aggregate = [
        'Directory.DirectoryName', 'VeloxLocation.LocationName',
        'VeloxDepartment.DepartmentName', 'Project.ProjectId', 'Study.StudyId'
    ]
    exp_records_df[columns_to_aggregate] = exp_records_df[
        columns_to_aggregate].fillna('').astype(str)
    exp_records_df['parent_path'] = exp_records_df[columns_to_aggregate].agg(
        '/'.join, axis=1)
    exp_records_df['parent_path'].replace(
        to_replace=r'\/{2,}', value='/', inplace=True, regex=True)
    exp_records_df.drop(columns=columns_to_aggregate, inplace=True)
    redundant_columns = [
        column for column in exp_records_df
        if column.startswith('ELNExperiment.DataTypeId_')]
    exp_records_df.drop(columns=redundant_columns, inplace=True)
    exp_records_df.rename(
        columns=lambda x: x.lower().replace('.', '_'), inplace=True)
    exp_records_df.rename(columns={
        'study_recordid': 'fk_study_recordid',
        'project_recordid': 'fk_project_recordid',
        'veloxdepartment_recordid': 'fk_veloxdepartment_recordid',
        'veloxlocation_recordid': 'fk_veloxlocation_recordid',
        'directory_recordid': 'fk_directory_recordid',
        'elnexperiment_recordid': 'recordid',
    }, inplace=True)
    float_columns_to_round = [
        'fk_study_recordid',
        'fk_project_recordid',
        'fk_veloxdepartment_recordid',
    ]
    exp_records_df[float_columns_to_round] = exp_records_df[
        float_columns_to_round].round(0).astype('Int64')
    db_hook.run(f'truncate table {SCHEMA}.{TABLE_PREFIX}rhe_exp')
    load_dataframe(
        exp_records_df, db_hook, TABLE_PREFIX + 'rhe_exp', SCHEMA,
        reorder_columns=True)
    return {RHE_EXP_TABLE_NAME: success_status}


EGNYTE_METADATA_FOLDER = 'Metadata-Exports'
EGNYTE_FILES_DATA = [
    ('rhe_experiments_plates', '/Experiments_Plates/Experiments_Plates.tsv'),
    ('rhe_nb_exp_sig', '/Experiments_Signings.tsv'),
    ('rhe_nb_exp_role', '/Experiments_Roles.tsv'),
    ('rhe_nb', '/NbExp_Details.tsv'),
    ('rhe_nb_instru', '/NbExp_Instruments.tsv'),
    ('rhe_nb_cp_plate', '/NbExp_Cp_Plates.tsv'),
    ('rhe_msd_2_guava_mapping', '/MSD_2_Guava_Plate_Mapping.tsv'),
]


def populate_rhe_tables_from_egnyte(db_hook, success_status):
    """
    Populate '..._rhe_...' tables with data from files downloaded from Egnyte.

    Parameters
    ----------
    db_hook - database hook to use when inserting data
    success_status - string to include into the returning value

    Returns
    -------
    Dict with the tables names as keys and success_status as values
    """
    sapio_api_helper = get_sapio_helper()
    egnyte_client = get_egnyte_client()
    report = sapio_api_helper.get_report(
        'ClientConfigurations.EgnyteFTPRootPath',
        condition='ClientConfigurations.RecordId > 0')
    root_egnyte_path = report['results'][0][0]
    result = {}
    for table_name, file_path in EGNYTE_FILES_DATA:
        full_table_name = TABLE_PREFIX + table_name
        db_hook.run(
            f'truncate table {SCHEMA}.{full_table_name}')
        with egnyte_client.file(root_egnyte_path + EGNYTE_METADATA_FOLDER
                                + file_path).download() as buffer:
            df = read_csv(buffer, sep='\t')
            load_dataframe(df, db_hook, full_table_name, SCHEMA)
        result[full_table_name] = success_status
    return result
