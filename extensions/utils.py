from airflow import settings
from airflow.hooks.base_hook import BaseHook
from airflow.models import Variable
from csv import writer, QUOTE_ALL
from datetime import datetime
from io import StringIO
from json import dumps
import logging

from airflow.models import Connection
from numpy import nan
from openpyxl import load_workbook
from os import path, makedirs
from re import search, sub
from requests import get

from darwinsync_api_helper import DSAPIHelper
from sapio_api_helper import SapioAPIHelper
from genedata_api_helper import GDAPIHelper
from egnyte import EgnyteClient


EGNYTE_PATH_START = 'Shared'


def get_ds_helper(conn_id='biobright'):
    """Return DSAPIHelper instance using AirFlow connection properties"""

    def get_bb_token():
        connection = BaseHook.get_connection(conn_id)
        extra = connection.extra_dejson
        return extra.get('token')

    def save_bb_token(token):
        connection = BaseHook.get_connection(conn_id)
        extra = connection.extra_dejson
        extra.update({'token': token})
        connection.set_extra(dumps(extra))

    connection = BaseHook.get_connection(conn_id)
    return DSAPIHelper(
        root_url=connection.host,
        client_id=connection.extra_dejson['client_id'],
        username=connection.login,
        password=connection.password,
        get_token_cb=get_bb_token,
        set_token_cb=save_bb_token,
    )


def get_sapio_helper(conn_id='sapio'):
    """Return an instance of SapioAPIHelper using AirFlow connection data"""
    connection = BaseHook.get_connection(conn_id)
    return SapioAPIHelper(
        root_url=connection.host,
        username=connection.login,
        password=connection.password,
        guid=connection.extra_dejson['guid'],
    )


def get_genedata_helper(conn_id='genedata'):
    """Return an instance of GDAPIHelper using AirFlow connection data"""
    connection = BaseHook.get_connection(conn_id)
    return GDAPIHelper(
        root_url=connection.host,
        username=connection.login,
        password=connection.password
    )


def get_egnyte_client():
    """Return an instance of EgnyteClient using Airflow variables"""
    return EgnyteClient({
        "domain": Variable.get('egnyte_domain',
                               default_var='rheosrx.egnyte.com'),
        "access_token": Variable.get('egnyte_token',
                                     default_var='no_token'),
    })


def download_file(url, fullpath):
    """Download a file from given URL to given path

    Return boolean success flag
    """
    filepath, filename = path.split(fullpath)
    makedirs(filepath, exist_ok=True)
    with get(url, allow_redirects=True, stream=True) as r:
        if r.status_code >= 400:
            logging.warning(
                f'Unable to download "{url}": status={r.status_code}, '
                f'reason={r.reason}')
            return False
        with open(fullpath, 'wb+') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
    return True


EXP_NAME_SEARCH_PATTERN = r'[\\\/](Bio-[0-9]+[^\\\/]*)[\\\/]'


def get_experiment_name(file_path):
    """Return experiment part of the file path"""
    match = search(EXP_NAME_SEARCH_PATTERN, file_path)
    return match.group(1) if match else None


def camel_to_snake(source):
    """Return string converted from camelCase to snake_case

    If list passed, traverse it and convert each item.
    Other types (besides list and str) return unchanged.
    """
    if isinstance(source, list):
        return [camel_to_snake(item) for item in source]
    if not isinstance(source, str):
        return source
    s1 = sub('(.)([A-Z][a-z]+)', r'\1_\2', source)
    return sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def egnytize(filepath):
    """
    Converts a filepath to Egnyte standard by removing the drive letter,
    removing the beginning of the path to "Shared" fragment and replace
    backslashes (and double backslashes) with single forward slashes.
    Args:
        filepath: source path

    Returns:
        converted path
    """
    forward_slashed_filepath = sub(r'\\{1,2}', '/', filepath)
    start_position = forward_slashed_filepath.find(f'/{EGNYTE_PATH_START}/')
    if start_position == -1:
        start_position = forward_slashed_filepath.find(':') + 1
    return forward_slashed_filepath[start_position:]


def xls_to_csvs(xls_file, target_dir=None):
    """
    Converts xlsx file into several csv viles, one per each sheet.
    Args:
        xls_file: full path to source xlsx
        target_dir: path to the folder for csv files,
            same as the source by default

    """
    wb = load_workbook(xls_file, read_only=True, data_only=True)
    sheet_names = wb.sheetnames
    logging.debug(f"Processing file {xls_file}")
    for sheet_name in sheet_names:
        logging.debug(f'exporting "{sheet_name}" sheet')
        sh = wb[sheet_name]
        with open(path.join(
                target_dir or path.split(xls_file)[0], sheet_name + '.csv'),
                'w') as csv_file:
            wr = writer(csv_file, quoting=QUOTE_ALL)
            for row in sh.rows:
                wr.writerow([cell.value for cell in row])


TABLE_COLUMNS_QUERY = '''
    SELECT
        column_name
    FROM
        information_schema."columns"
    WHERE
        table_schema = %(schema)s
        AND table_name = %(table)s
    ORDER BY
        ordinal_position
'''


def load_dataframe(
        df, hook, table_name, schema, reorder_columns=False,
        exclude_columns=None, cast_empty_to_nan=True):
    """
    Load pandas DataFrame into given table using given hook
    Parameters
    ----------
    df - the DataFrame to be loaded
    hook - database hook to be used
    table_name -  the name of the table to load to
    schema - the schema the table belongs to
    reorder_columns - optional boolean: whether to reorder the df columns
        to conform the table or they are guaranteed to be ordered in advance
    exclude_columns - optional list of the columns to skip when inserting.
        Usually when some columns are filled in by sequence or other default.
    """
    if reorder_columns or exclude_columns:
        columns_data = hook.get_records(
            TABLE_COLUMNS_QUERY,
            parameters={'schema': schema, 'table': table_name})
        if reorder_columns:
            columns = [record[0] for record in columns_data]
            for column in columns:
                if column not in df:
                    df[column] = nan
            df = df[columns]
        if exclude_columns:
            df.drop(columns=exclude_columns, inplace=True, errors='ignore')
    df.loc[:, df.dtypes == object] = df.loc[:, df.dtypes == object].replace(
        {'\\n': '', '\\\\': '\\\\\\\\', r'^None$': nan},
        regex=True)
    if cast_empty_to_nan:
        df.loc[:, df.dtypes == object] = df.loc[
            :, df.dtypes == object].replace({r'^\s*$': nan}, regex=True)
    logging.debug(f'Inserting {df.shape[0]} records into {table_name}')
    start = datetime.now().timestamp()
    buffer = StringIO()
    df.to_csv(buffer, index=False, sep='\t', header=False, na_rep=r'\N')
    buffer.seek(0)
    conn = hook.get_conn()
    with conn.cursor() as cursor:
        cursor.copy_from(
            buffer, f'{schema}.{table_name}',
            columns=list(df) if exclude_columns else None)
    conn.commit()
    logging.debug(f'Inserted in {int(datetime.now().timestamp() - start)} sec')


def clean(text):
    """
    Clean the text from quotes, spaces, slashes and other special characters
    Args:
        text: source text to be cleaned

    Returns:
        cleaned text
    """
    if not isinstance(text, str):
        return text
    return text.replace('"', '').replace('\'', '').strip(' ').replace(
        '\r', '').replace('\n', '').replace('\t', '').replace('      ', '')


def get_slack_conn(conn_id,
                   default_connection_id='slack'):
    """
    Return the name of existing connection
    :return:
    """

    session = settings.Session()
    slack_conn = session.query(Connection).filter(
        Connection.conn_id == conn_id).first()
    if slack_conn:
        logging.debug(f'Connection {conn_id} was found and will be used')
        return slack_conn
    else:
        logging.debug(f'Connection {conn_id} was not found.'
                      f' Message will be send in default'
                      f' {default_connection_id} connection')
        return BaseHook.get_connection(default_connection_id)
