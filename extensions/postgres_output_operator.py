from airflow.operators.postgres_operator import PostgresOperator
from airflow.utils.decorators import apply_defaults

from .transact_postgres_hook import TransactPostgresHook


class PostgresOutputOperator(PostgresOperator):
    """
    Executes sql code in a specific Postgres database
    and returns produced recordset (to be pushed in XCom)
    """

    @apply_defaults
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def execute(self, context):
        self.log.info('Executing: %s', self.sql)
        hook = TransactPostgresHook(postgres_conn_id=self.postgres_conn_id,
                                    schema=self.database, autocommit=True)
        records = hook.get_records(self.sql, parameters=self.parameters)
        for output in hook.conn.notices:
            self.log.info(output)
        return records
