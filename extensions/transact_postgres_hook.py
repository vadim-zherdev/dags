from airflow.hooks.postgres_hook import PostgresHook


class TransactPostgresHook(PostgresHook):
    def __init__(self,  *args, autocommit=False, **kwargs):
        self.autocommit = autocommit
        super(TransactPostgresHook, self).__init__(*args, **kwargs)

    def get_records(self, sql, parameters=None):
        """
        Executes the sql and returns a set of records.
        If the sql applies changes (like INSERT... RETURNING),
        the changes are commited if specified at instantiation

        :param sql: the sql statement to be executed (str) or a list of
            sql statements to execute
        :type sql: str or list
        :param parameters: The parameters to render the SQL query with.
        :type parameters: mapping or iterable
        """
        with self.get_conn() as conn:
            conn.autocommit = self.autocommit
            with conn.cursor() as cur:
                cur.execute(sql, vars=parameters)
                return cur.fetchall()

    def get_first(self, sql, parameters=None):
        """
        Executes the sql and returns the first resulting row..
        If the sql applies changes (like INSERT... RETURNING),
        the changes are commited if specified at instantiation

        :param sql: the sql statement to be executed (str) or a list of
            sql statements to execute
        :type sql: str or list
        :param parameters: The parameters to render the SQL query with.
        :type parameters: mapping or iterable
        """
        with self.get_conn() as conn:
            conn.autocommit = self.autocommit
            with conn.cursor() as cur:
                cur.execute(sql, vars=parameters)
                return cur.fetchone()
