from os import path, makedirs
from pathlib import Path
from unittest import TestCase
from unittest.mock import patch
from re import sub
from egnyte.exc import NotFound

from mzxml_convert_dag import (
    convert_files,
    EXP_PATH_PREFIX,
    get_exp_name,
    get_new_experiments,
    get_new_files,
    INSTRU_NAME,
    SUCCESS_STATUS,
)
from extensions.mzxml_utils import (
    get_files_from_bb_by_exp,
    SAPIO_PATH_MARKER
)
from extensions.mocks import (
    MockPostgresHook,
    MockPostgresHookWithResponse,
    MockEgnyte,
    MockDSAPIHelper,
)


MOCK_PATH_PREFIX = EXP_PATH_PREFIX.replace("\\", "")
MOCK_EXP_PREFIX = f'{MOCK_PATH_PREFIX}Testing/Bio-666_/{INSTRU_NAME}/'

mock_bb_data = (
    [  # data
        {
            'attributes': {
                'client_path': (
                        MOCK_EXP_PREFIX + 'p1\\123.raw'),
                'client_file_name': '123.raw',
                'client_updated_at': '2019-10-11T09:08:07.654Z',
                # 'client'
            },
            'id': 1,
        },
        {
            'attributes': {
                'client_path': (
                        MOCK_EXP_PREFIX + 'p2\\456.raw'),
                'client_file_name': '456.raw',
                'client_updated_at': '2018-10-11T09:08:07.654Z',
            },
            'id': 3,
        },
    ],
    [  # included
        {
            'id': 1,
            'attributes': {
                'client_file_name': '123.raw',
                's3_link': 's3link1',
            }
        },
        {
            'id': 2,
            'attributes': {
                'client_file_name': '123.raw',
                's3_link': 's3link2',
            }
        },
        {
            'id': 3,
            'attributes': {
                's3_link': 's3link3',
                'client_file_name': '456.raw',
            }
        },
    ]
)


class MockBinaryStream:
    def iter_content(self, chunk_size):
        return b''

    def close(self):
        pass


class MockDSAPIHelperWithDownload(MockDSAPIHelper):
    def download_s3_file(self, s3_url, fullpath, file_id):
        return create_raw_file(s3_url, fullpath)

    def get_s3_file(self, s3_url, file_id):
        return MockBinaryStream()


class MockDSAPIHelperSingleFile(MockDSAPIHelperWithDownload):
    def get_files_index(self, **kwargs):
        return mock_bb_data[0][:1].copy(), mock_bb_data[1].copy()


class MockMissingEgnyteFile:
    def __init__(self, path):
        self.path = path

    def download(self):
        raise NotFound()


class MockkBBFile:
    def __init__(self, path, content):
        self.path = path
        self.content = content

    def download(self):
        return MockBBFileDownloader(self.content)


class MockBBFileDownloader:
    def __init__(self, content=None):
        self.content = content

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def read(self, *args):
        return self.content.read() if hasattr(
            self.content, 'read') else self.content

    def __iter__(self):
        return self.content


class MockRun:
    def __init__(self):
        self.returncode = 0
        self.stdout = 'Test output'
        self.stderr = 'Test errors'


class MockTask:
    def __init__(self, exps_data):
        self.exps_data = exps_data

    def xcom_pull(self, key=None, **kwargs):
        if isinstance(self.exps_data, dict):
            result = self.exps_data.get(key, self.exps_data)
        else:
            result = self.exps_data
        return result

    def xcom_push(self, key, value):
        self.exps_data = {key: value}


class MockPostgresHookWithFilesResponse(MockPostgresHookWithResponse):
    def __init__(self, postgres_conn_id=None, autocommit=False):
        super(MockPostgresHookWithFilesResponse, self).__init__()
        self.response = [(
            14,
            'Bio-123',
            '2001-01-01',
            'SomeWorklistName.csv',
            'T-666',
            'T',
            666,
            MOCK_EXP_PREFIX + 'p1/123.raw',
            'testing',
            'shmesting',
            15,
            'biobright',
        )]


class MockPostgresHookWithBadFilesResponse(MockPostgresHookWithFilesResponse):
    def __init__(self, postgres_conn_id=None, autocommit=False,
                 good_filename=True):
        super(MockPostgresHookWithBadFilesResponse, self).__init__()
        self.pattern_response[r'UPDATE\s+stg_infodw\.qe_files.+RETURNING'] = \
            [(
                16,
                'T-777',
            )]
        self.response = [(
            16,
            'Bio-345',
            '2001-01-01',
            'SomeWorklistName.csv',
            'T-777',
            'T',
            777,
            'blahblah',
            'testing',
            'shmesting',
            17,
            'biobright',
        )]


class MockRunResult:
    returncode = 0
    stdout = 'mocked test stdout'
    stderr = 'mocked test stderr'


mock_csv = (b'Header\n1,File Name,Sample ID,Position,Other,Path\n'
            b'4,P-12345_Y66_and_more,P-12345_X13_more,test:T666,'
            b'other,testpath/')


def create_raw_file(s3link, raw_file_name):
    raw_file_path, raw_file_name_only = path.split(raw_file_name)
    makedirs(raw_file_path, exist_ok=True)
    Path(raw_file_name).touch()
    return True


class MockResponse:
    def __init__(self):
        self.status_code = 200
        self.content = mock_csv


class MZXMLConvertTestCase(TestCase):
    @patch('mzxml_convert_dag.TransactPostgresHook', new=MockPostgresHook)
    def test_no_new_experiments_processed(self):
        mk_task = MockTask({})
        with self.assertLogs(level='DEBUG') as logs:
            result = get_new_experiments(task_instance=mk_task)
        self.assertIn('No new experiments found', logs.records[0].getMessage())
        self.assertEqual(result, 'finish')

    @patch('mzxml_convert_dag.TransactPostgresHook',
           new=MockPostgresHookWithResponse)
    def test_some_new_experiments_processed(self):
        mk_task = MockTask({})
        with self.assertLogs(level='INFO') as logs:
            result = get_new_experiments(task_instance=mk_task)
        self.assertIn('Found 1 new experiment', logs.records[0].getMessage())
        self.assertEqual(result, 'get_new_qe_files')
        self.assertEqual(
            mk_task.exps_data, {'exps_data': [(1, 2, 3, 4, 5)]})

    @patch('mzxml_convert_dag.get_files_from_bb_by_exp')
    @patch('mzxml_convert_dag.add_to_audit')
    @patch('mzxml_convert_dag.get_egnyte_client')
    @patch('mzxml_convert_dag.get_ds_helper')
    @patch('mzxml_convert_dag.TransactPostgresHook',
           new=MockPostgresHookWithResponse)
    def test_worklists_processed(self, mk_get_bb_client, mk_get_egnyte_client,
                                 mk_audit, mk_get_bb):
        mk_get_bb_client.return_value = MockDSAPIHelper()
        mk_get_egnyte_client.return_value = MockEgnyte(folder_content=mock_csv)
        mk_task = MockTask([
            (1, 'testname1', 3, 'testpath1', 'teststatus1'),
            (5, 'testname2', 7, 'testpath2', 'teststatus2'),
        ])
        mk_get_bb.return_value = []
        with self.assertLogs(level='DEBUG') as logs:
            get_new_files(task_instance=mk_task)
        self.assertEqual(
            logs.records[0].getMessage(), 'Processing experiment "testpath1"')
        self.assertEqual(mk_audit.call_count, 2)
        self.assertTrue(mk_get_bb_client.called)
        self.assertTrue(mk_get_egnyte_client.called)
        self.assertTrue(mk_get_bb.call_count, 2)

        mk_get_bb.return_value = [MockkBBFile('testfile1.csv',
                                              content=mock_csv)]
        with self.assertLogs(level='DEBUG') as logs:
            get_new_files(task_instance=mk_task)
        self.assertEqual(
            logs.records[0].getMessage(), 'Processing experiment "testpath1"')
        self.assertEqual(mk_audit.call_count, 4)
        self.assertEqual(mk_get_bb.call_count, 4)

    @patch('mzxml_convert_dag.get_files_from_egnyte_by_exp')
    @patch('mzxml_convert_dag.upload_to_egnyte')
    @patch('extensions.mzxml_utils.run', return_value=MockRunResult())
    @patch(
        'mzxml_convert_dag.TransactPostgresHook',
        new=MockPostgresHookWithFilesResponse)
    @patch('mzxml_convert_dag.get_ds_helper',
           return_value=MockDSAPIHelperSingleFile())
    @patch('mzxml_convert_dag.get_egnyte_client')
    def test_files_converted(self, mk_get_egnyte_client, mk_conn,
                             mk_run, mk_upload, mk_get_egnyte):
        with self.assertLogs(level='INFO') as logs:
            result = convert_files()
        self.assertIn(
            '1 files found for experiment 14', logs.records[0].getMessage())
        self.assertIn('123.raw" processed', logs.records[1].getMessage())
        self.assertIn(MOCK_EXP_PREFIX + 'p1/123.raw', result)
        self.assertEqual(
            result[MOCK_EXP_PREFIX + 'p1/123.raw'], SUCCESS_STATUS)
        self.assertTrue(mk_conn.called)
        self.assertTrue(mk_get_egnyte_client.called)
        self.assertTrue(mk_run.called)
        self.assertTrue(mk_upload.called)
        self.assertFalse(mk_get_egnyte.called)

    @patch('mzxml_convert_dag.get_files_from_egnyte_by_exp')
    @patch('mzxml_convert_dag.upload_to_egnyte')
    @patch('extensions.mzxml_utils.run')
    @patch(
        'mzxml_convert_dag.TransactPostgresHook',
        new=MockPostgresHookWithBadFilesResponse)
    @patch('mzxml_convert_dag.get_ds_helper',
           return_value=MockDSAPIHelperWithDownload)
    @patch('mzxml_convert_dag.get_egnyte_client')
    def test_missing_files_skipped(self, mk_get_egnyte_client, mk_conn,
                                   mk_run, mk_upload, mk_get_egnyte):
        mk_get_egnyte.return_value = [MockMissingEgnyteFile('test.raw')]
        with self.assertLogs(level='INFO') as logs:
            convert_files()
        self.assertIn(
            'Experiment path could not be extracted for',
            logs.records[0].getMessage())
        self.assertIn(
            'Experiment "16" marked as failed',
            logs.records[1].getMessage())
        self.assertTrue(mk_get_egnyte_client.called)
        self.assertTrue(mk_conn.called)
        self.assertFalse(mk_run.called)
        self.assertFalse(mk_upload.called)
        self.assertFalse(mk_get_egnyte.called)

    def test_get_exp_name(self):
        self.assertIsNotNone(get_exp_name(MOCK_EXP_PREFIX))
        alt_exp_prefix = '?' + MOCK_EXP_PREFIX[1:]
        self.assertIsNotNone(get_exp_name(alt_exp_prefix))
        bad_exp_prefix = sub(r'Bio-\d+', 'Bio-ABC', MOCK_EXP_PREFIX)
        self.assertIsNone(get_exp_name(bad_exp_prefix))


class BioBrightRequestTestCase(TestCase):
    def test_bb_path_converted(self):
        mk_api_helper = MockDSAPIHelper(mock_data=mock_bb_data)
        result = get_files_from_bb_by_exp(
            f'any_beginning{SAPIO_PATH_MARKER}my_funny_exp/',
            mk_api_helper, '.csv', 'WORKLIST\\\\')
        self.assertEqual(
            SAPIO_PATH_MARKER.replace(
                '/', '\\\\') + 'my_funny_exp\\\\WORKLIST\\\\',
            mk_api_helper.kwargs['client_path']
        )
        self.assertEqual(2, len(result))
        self.assertEqual(result[0].path, MOCK_EXP_PREFIX + 'p1/123.raw')
        self.assertEqual(result[1].path, MOCK_EXP_PREFIX + 'p2/456.raw')
