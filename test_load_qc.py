import numpy as np
import logging
from unittest import TestCase
from unittest.mock import patch
from datetime import datetime

from extensions.fiamat_utils import (
    QC_INTENSITY_FILTER,
    QC_ACCURACY
)
from load_qc_dag import (
    extract_files,
    new_files_on_bb_check,
    merge_output
)

ID_TEST_1 = 'f45d'
ID_TEST_2 = 'g656'
ID_TEST_3 = 'sed3'
ON_PLATE_ID_TEST_1 = 'P-1153_A1'
ON_PLATE_ID_TEST_2 = 'P-666_B8'
FILENAME_TEST_1_1 = f'{ON_PLATE_ID_TEST_1}_r1_NEGb.fiamat'
FILENAME_TEST_1_2 = f'{ON_PLATE_ID_TEST_1}_r1_NEGa.fiamat'
FILENAME_TEST_2_1 = f'{ON_PLATE_ID_TEST_2}_r1_NEGb.fiamat'
FILENAME_TEST_BAD = f'aaa.fiamat'

task_dict = {
    'extract_files': {
        f'/test/{FILENAME_TEST_1_1}': 'OK',
        f'/test/{FILENAME_TEST_1_2}': 'OK',
        f'/test/{FILENAME_TEST_2_1}': 'OK',
    },
    'process_files': [(f'/test/{FILENAME_TEST_1_1}', 'Calculation error',)],
}


class MockPostgresHook:

    is_used_get_first = False

    def __init__(self, postgres_conn_id=None, first=None, autocommit=False):
        pass

    def get_first(self, query, *args, **kwargs):
        if query.find('MAX') > -1:
            return [datetime(year=2012, month=1, day=10)]
        elif query.find('invalidated') > -1:
            return (0,)
        elif query.find('src_filename') > -1 \
                and query.find('file_created_at') > -1:
            return (13,
                    datetime(year=2017, month=1, day=1))
        elif query.find('INSERT INTO'):
            return (66,)

        elif not self.is_used_get_first:
            self.is_used_get_first = True
            return ()
        else:
            return (13,)

    def get_records(self, query, *args, **kwargs):
        if query.find('invalidated') > -1:
            return []
        else:
            return [(ON_PLATE_ID_TEST_1,
                     'Bio 1234',
                     'some_sample',
                     'hella'),
                    (ON_PLATE_ID_TEST_2,
                     'Bio 5678',
                     'some_sample2',
                     'lalala')
                    ]

    def insert_rows(self, *args, **kwargs):
        pass

    def run(self, *args, **kwargs):
        pass


class MockPostgresHookEmpty(MockPostgresHook):
    def get_first(self, *args, **kwargs):
        return (0,)


class MockDSAPIHelper:
    def __init__(self, **kwargs):
        pass

    def get_files_index(self, **kwargs):
        if 'and_included' in kwargs:
            files = [{'id': ID_TEST_1,
                      'attributes':
                          {'created_at': '2020-01-14T01:34:04.457Z',
                           'updated_at': '2020-01-14T01:34:08.541Z',
                           'client_path': f'/test/{FILENAME_TEST_1_1}',
                           'client_file_name': f'{FILENAME_TEST_1_1}'}},
                     {'id': ID_TEST_2,
                      'attributes':
                          {'created_at': '2019-10-14T01:34:04.457Z',
                           'updated_at': '2019-10-14T01:34:08.541Z',
                           'client_path': f'/test/{FILENAME_TEST_1_2}',
                           'client_file_name': f'{FILENAME_TEST_1_2}'}},
                     {'id': ID_TEST_3,
                      'attributes':
                          {'created_at': '2012-01-14T01:34:04.457Z',
                           'updated_at': '2012-01-14T01:34:08.541Z',
                           'client_path': f'/test/{FILENAME_TEST_2_1}',
                           'client_file_name': f'{FILENAME_TEST_2_1}'}}
                     ]

            files_included = [{'id': 'bad',
                               'attributes':
                                   {'computer_id': '8'}},
                              {'id': ID_TEST_1,
                               'attributes':
                                   {'s3_link': 'http://tets/test/rhebfs'}},
                              {'id': ID_TEST_2,
                               'attributes':
                                   {'s3_link': 'http://tets/test/khtfdx'}},
                              {'id': ID_TEST_3,
                               'attributes':
                                   {'s3_link': 'http://tets/test/rget'}}]
            return files, files_included
        elif 'remove_obsolete_versions' in kwargs:
            files = [{'id': ID_TEST_1,
                      'attributes':
                          {'client_created_at': '2020-01-14T01:34:04.457Z'}},
                     {'id': ID_TEST_2,
                      'attributes':
                          {'client_created_at': '2019-10-14T01:34:04.457Z'}},
                     {'id': ID_TEST_3,
                      'attributes':
                          {'client_created_at': '2012-01-14T01:34:04.457Z'}}
                     ]
            return files

    def get_files_count(self, **kwargs):
        return 40

    def download_s3_file(self, s3_url, fullpath, file_id):
        return True


class MockDSAPIHelperEmpty(MockDSAPIHelper):
    def get_files_count(self, **kwargs):
        return 0


class MockTaskInstance:
    def xcom_pull(self, task_ids=None, key=None):
        return task_dict.get(task_ids)

    def xcom_push(self, key, value):
        pass


class MockSpectrum:
    def __init__(self, **kwargs):
        self.tmp_filename = 'some_tmp_filename'

        self.spectrum = {4: 56, 9: 7656, 67: 45, 666: 6434677}
        self.start_time_stamp = datetime(
            year=2019, month=1, day=10)

        self.intensity_filter = QC_INTENSITY_FILTER

        self.fiamat_tic = 1000
        self.low_mz = 50.
        self.high_mz = 1050.
        self.mode = 'pos'
        self.num_centroids = 4

        self.v = [1, 1, 1, 1]
        self.scaled_v = [1, 1, 1, 1]
        self.normalized_scaled_v = [1, 1, 1, 1]
        self.normalized_v = [1, 1, 1, 1]
        self.nz_array = np.array([1, 1, 1, 1])

        self.summed_tic = 0.89
        self.signal_to_noise = 0.3
        self.precision = QC_ACCURACY


@patch('load_qc_dag.get_ds_helper')
class DAGTestCase(TestCase):
    def setUp(self):
        logging.basicConfig(level='DEBUG')

    @patch('load_qc_dag.PostgresHook', new=MockPostgresHook)
    @patch('load_qc_dag.TransactPostgresHook', new=MockPostgresHook)
    @patch('load_qc_dag.open_fiamat_file', return_value=MockSpectrum())
    def test_extract_files(self, o_f_f, g_d_h):

        g_d_h.return_value = MockDSAPIHelper()
        with self.assertLogs(level='DEBUG') as logs:
            extract_files()
        self.assertTrue(g_d_h.called)

        self.assertEqual(len(logs.records), 21)
        self.assertIn(
            'Will be loading only files after '
            '(and included) 2012-01-10 00:00:00',
            logs.records[0].getMessage())
        self.assertIn(
            'Get metadata by on_plate_id',
            logs.records[1].getMessage())
        self.assertEqual(
            f'File /test/{FILENAME_TEST_1_1} was rewritten in BioBright at'
            f' 2020-01-14 01:34:04.457000 and'
            f' so it must be reloaded into the database.',
            logs.records[2].getMessage())
        self.assertEqual(
            f'Cleaning up previous data for /test/{FILENAME_TEST_1_1}',
            logs.records[3].getMessage())
        self.assertEqual(
            logs.records[4].getMessage(),
            f'Update info about /test/{FILENAME_TEST_1_1}')
        self.assertEqual(
            f'Starting loading file: /test/{FILENAME_TEST_1_1}',
            logs.records[5].getMessage())
        self.assertRegex(
            logs.records[6].getMessage(),
            r'Temp file .+ was downloaded')
        self.assertEqual(
            f'Trying to open file /test/{FILENAME_TEST_1_1}',
            logs.records[7].getMessage())
        self.assertEqual(
            f'Starting to write data into database',
            logs.records[8].getMessage())
        self.assertEqual(
            f'Fiamat data from file /test/{FILENAME_TEST_1_1}'
            f' was successfully loaded into database',
            logs.records[9].getMessage())
        self.assertEqual(
            f'Finished loading file with id=13',
            logs.records[10].getMessage())
        self.assertEqual(
            f'File /test/{FILENAME_TEST_1_2} was rewritten in '
            f'BioBright at 2019-10-14 01:34:04.457000'
            f' and so it must be reloaded into the database.',
            logs.records[11].getMessage())
        self.assertEqual(
            f'Cleaning up previous data for /test/{FILENAME_TEST_1_2}',
            logs.records[12].getMessage())
        self.assertEqual(
            f'Update info about /test/{FILENAME_TEST_1_2}',
            logs.records[13].getMessage())
        self.assertEqual(
            f'Starting loading file: /test/{FILENAME_TEST_1_2}',
            logs.records[14].getMessage())
        self.assertEqual(
            f'Trying to open file /test/{FILENAME_TEST_1_2}',
            logs.records[16].getMessage())

    @patch('load_qc_dag.PostgresHook', new=MockPostgresHook)
    def test_new_files_check(self, g_d_h):

        g_d_h.return_value = MockDSAPIHelper()
        with self.assertLogs(level='DEBUG') as logs:
            result = new_files_on_bb_check()
            self.assertEqual(result, 'extract_files')
            self.assertTrue(g_d_h.called)

            self.assertEqual(len(logs.records), 1)
            self.assertIn(
                'Found 39 new files. Moving to extract step.',
                logs.records[0].getMessage())

    @patch('load_qc_dag.PostgresHook', new=MockPostgresHook)
    def test_new_files_check_empty(self, g_d_h):

        g_d_h.return_value = MockDSAPIHelperEmpty()
        with self.assertLogs(level='DEBUG') as logs:
            result = new_files_on_bb_check()
            self.assertEqual(result, 'finish')
            self.assertTrue(g_d_h.called)

            self.assertEqual(len(logs.records), 1)
            self.assertIn(
                'No new files found. Moving to finish.',
                logs.records[0].getMessage())

    def test_merge_output(self, g_d_h):
        self.assertEqual(
            merge_output(ti=MockTaskInstance()),
            f"*Error*: `/test/{FILENAME_TEST_1_1}` (Calculation error)\n"
            f"*OK*: `/test/{FILENAME_TEST_1_2}`\n"
            f"*OK*: `/test/{FILENAME_TEST_2_1}`")
