from openpyxl import Workbook
from os import path, listdir
from tempfile import TemporaryDirectory
from unittest import TestCase

from pandas import DataFrame

from extensions.utils import (
    camel_to_snake, get_experiment_name, egnytize, EGNYTE_PATH_START,
    xls_to_csvs, load_dataframe,)
from extensions.mocks import MockPostgresHook


class CaseConverterTestCase(TestCase):
    def test_string_converted(self):
        result = camel_to_snake('CamelCase')
        self.assertEqual(result, 'camel_case')
        result = camel_to_snake('CamelCamelCase')
        self.assertEqual(result, 'camel_camel_case')
        result = camel_to_snake('Camel2Camel2Case')
        self.assertEqual(result, 'camel2_camel2_case')
        result = camel_to_snake('getHTTPResponseCode')
        self.assertEqual(result, 'get_http_response_code')
        result = camel_to_snake('get2HTTPResponseCode')
        self.assertEqual(result, 'get2_http_response_code')
        result = camel_to_snake('HTTPResponseCode')
        self.assertEqual(result, 'http_response_code')
        result = camel_to_snake('HTTPResponseCodeXYZ')
        self.assertEqual(result, 'http_response_code_xyz')

    def test_list_converted(self):
        result = camel_to_snake(['CamelCase', 'get2HTTPResponseCode'])
        self.assertCountEqual(
            result, ['camel_case', 'get2_http_response_code'])

    def test_other_types_unchanged(self):
        result = camel_to_snake(13)
        self.assertEqual(result, 13)
        result = camel_to_snake(False)
        self.assertEqual(result, False)


class ExperimentExtractionTestCase(TestCase):
    def test_experiment_extracted(self):
        result = get_experiment_name(
            'L:\\Sapio\\Platform\\Optimization\\2019-10\\'
            'Bio-446_QE_Source_Settings_Optimization\\QE01\\ANALYZED_DATA\\'
            'Bio-442_Ext_Buffer_POS.csv')
        self.assertEqual(result, 'Bio-446_QE_Source_Settings_Optimization')
        result = get_experiment_name(
            r'blahblahblah\\'
            r'Bio-666_testing_experiment_extraction\\ABC01\\something_else')
        self.assertEqual(result, 'Bio-666_testing_experiment_extraction')
        result = get_experiment_name(
            r'blah\\Stereo-666_testing_experiment_extraction\\something_else')
        self.assertIsNone(result)
        result = get_experiment_name(
            'L:/Sapio/Platform/Optimization/2019-10/'
            'Bio-446_QE_Source_Settings_Optimization/QE01/ANALYZED_DATA/'
            'Bio-442_Ext_Buffer_POS.csv')
        self.assertEqual(result, 'Bio-446_QE_Source_Settings_Optimization')
        result = get_experiment_name(
            r'L:\\Sapio\\Platform\\Optimization\\2019-10\\'
            r'Bio-446_QE_Source_Settings_Optimization\\QE01\\ANALYZED_DATA\\'
            r'Bio-442_Ext_Buffer_POS.csv')
        self.assertEqual(result, 'Bio-446_QE_Source_Settings_Optimization')


class EgnytizationTestCase(TestCase):
    def test_egnytization(self):
        result = egnytize(
            f't:\\back\\slashes\\{EGNYTE_PATH_START}\\the\\rest')
        self.assertEqual(
            f'/{EGNYTE_PATH_START}/the/rest', result)
        result = egnytize('t:\\back\\slashes\\no\\start')
        self.assertEqual(f'/back/slashes/no/start', result)
        result = egnytize(f't:/forward/slashes/{EGNYTE_PATH_START}/the/rest')
        self.assertEqual(f'/{EGNYTE_PATH_START}/the/rest', result)
        result = egnytize(
            f'bad:\\back\\slashes\\{EGNYTE_PATH_START}\\the\\rest')
        self.assertEqual(f'/{EGNYTE_PATH_START}/the/rest', result)
        result = egnytize(
            f'bad:\\back\\slashes\\no\\start')
        self.assertEqual('/back/slashes/no/start', result)
        result = egnytize(
            f's3:\\Amazon\\path\\{EGNYTE_PATH_START}\\the\\rest')
        self.assertEqual(f'/{EGNYTE_PATH_START}/the/rest', result)
        result = egnytize(f's3:\\Amazon\\path\\no\\start')
        self.assertEqual('/Amazon/path/no/start', result)


class XlsToCsvsTestCase(TestCase):
    def setUp(self):
        self.wb = Workbook()
        ws1 = self.wb.active
        ws1.title = 'TestPage1'
        ws1.append([1, 2, 3])
        ws1.append([4, 5, 6])
        ws2 = self.wb.create_sheet('TestPage2')
        ws2.append(['A', 'B', 'C'])
        ws2.append(['D', 'E', 'F'])

    def test_xlsx_to_csvs(self):
        with TemporaryDirectory() as tmp_dir:
            xlsx_file = path.join(tmp_dir, 'tmp.xlsx')
            self.wb.save(xlsx_file)
            xls_to_csvs(xlsx_file)
            file_list = listdir(tmp_dir)
            self.assertIn('TestPage1.csv', file_list)
            self.assertIn('TestPage2.csv', file_list)
            with open(path.join(tmp_dir, 'TestPage1.csv'), 'r') as f:
                self.assertEqual(f.readline(), '"1","2","3"\n')
                self.assertEqual(f.readline(), '"4","5","6"\n')
            with open(path.join(tmp_dir, 'TestPage2.csv'), 'r') as f:
                self.assertEqual(f.readline(), '"A","B","C"\n')
                self.assertEqual(f.readline(), '"D","E","F"\n')


class LoadDataFrameTestCase(TestCase):
    def test_dataframe_clean(self):
        mock_hook = MockPostgresHook()
        df = DataFrame([
            ['1', '2'],
            [3, 'None'],
            ['4', 'c:\\'],
            ['', ' '],
            ['a\nb', 'c\\d']],
            columns=['a', 'b'])
        load_dataframe(df, mock_hook, 'test_table', 'test_schema')
        self.assertEqual(1, mock_hook.conn.cur.copy_from_call_count)
        buf = mock_hook.conn.cur.copy_from_buffer.readline()
        self.assertEqual('1\t2\n', buf)
        buf = mock_hook.conn.cur.copy_from_buffer.readline()
        self.assertEqual('3\t\\N\n', buf)
        buf = mock_hook.conn.cur.copy_from_buffer.readline()
        self.assertEqual('4\tc:\\\\\n', buf)
        buf = mock_hook.conn.cur.copy_from_buffer.readline()
        self.assertEqual('\\N\t\\N\n', buf)
        buf = mock_hook.conn.cur.copy_from_buffer.readline()
        self.assertEqual('ab\tc\\\\d\n', buf)
