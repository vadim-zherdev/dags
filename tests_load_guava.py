from io import StringIO, BytesIO
from unittest import TestCase
from unittest.mock import patch

from extensions.guava_utils import parse_guava_results
from extensions.mocks import MockPostgresHook, MockEgnyte, MockDSAPIHelper


MOCK_EGNYTE_FILE_CONTENT = (
    b'Some header\n'
    b' Sample No., Sample ID, Viable,something else, Viable\n'
    b'21, X22,23,24,25'
)


class MockEgnyteForGuava(MockEgnyte):
    def __init__(self):
        self.file_content = MOCK_EGNYTE_FILE_CONTENT
        super(MockEgnyteForGuava, self).__init__(
            file_content=BytesIO(self.file_content))


class GuavaTablesPopulationTestCase(TestCase):
    def setUp(self):
        self.mock_files_data = [
            (
                123,
                456,
                'Bio-666 My Funny Experiment',
                'P-666',
                '/Some/Long/Path/To/My/ANALYZED_DATA/P-666.csv',
                'ANALYZED_DATA',
                'started',
            ),
        ]
        self.six_line_header_file_content = (
            '1st header line\n'
            '2nd header line\n'
            '3rd header line\n'
            '4th header line\n'
            '5th header line\n'
            ',,,6th header line\n'
            'Sample No., Sample ID, Viable,something else, Viable\n'
            '1, Z13,14,15,20')
        self.five_line_header_file_content = (
            '1st header line\n'
            '2nd header line\n'
            '3rd header line\n'
            '4th header line\n'
            ',,,5th header line\n'
            'Sample No., Sample ID, Viable,something else, Viable\n'
            '2, Y16,17,18,19')

    @patch('extensions.guava_utils.get_egnyte_client')
    @patch('extensions.guava_utils.load_dataframe')
    @patch('extensions.guava_utils.get_ds_helper')
    def test_guava_tables_populated(self, mk_bb, mk_load, mk_egnyte):
        mk_bb.return_value = MockDSAPIHelper(
            mock_file_content=StringIO(self.six_line_header_file_content))
        mk_egnyte.return_value = MockEgnyteForGuava()
        result = parse_guava_results(
            self.mock_files_data, MockPostgresHook(), 'okidoki')
        self.assertEqual(1, mk_load.call_count)
        self.assertEqual({self.mock_files_data[0][4]: 'okidoki'}, result)
        df = mk_load.call_args[0][0]
        self.assertEqual((1, 5), df.shape)
        self.assertCountEqual([
                'guava_file_id',
                'row_letter',
                'column_number',
                'result_value',
                'result_type'
            ],
            list(df))
        df.reset_index(inplace=True)
        self.assertEqual(df['guava_file_id'][0], 123)
        self.assertEqual(df['row_letter'][0], 'Z')
        self.assertEqual(df['column_number'][0], 13)
        self.assertEqual(df['result_value'][0], '14')
        self.assertEqual(df['result_type'][0], 'ANALYZED_DATA')
        self.assertEqual(1, mk_bb.call_count)
        self.assertTrue(mk_egnyte.called)

        mk_bb.return_value = MockDSAPIHelper(
            mock_file_content=StringIO(self.five_line_header_file_content))
        result = parse_guava_results(
            self.mock_files_data, MockPostgresHook(), 'okidoki2')
        self.assertEqual(2, mk_load.call_count)
        self.assertEqual({self.mock_files_data[0][4]: 'okidoki2'}, result)
        df = mk_load.call_args[0][0]
        self.assertEqual((1, 5), df.shape)
        self.assertCountEqual([
                'guava_file_id',
                'row_letter',
                'column_number',
                'result_value',
                'result_type'
            ],
            list(df))
        df.reset_index(inplace=True)
        self.assertEqual(df['guava_file_id'][0], 123)
        self.assertEqual(df['row_letter'][0], 'Y')
        self.assertEqual(df['column_number'][0], 16)
        self.assertEqual(df['result_value'][0], '17')
        self.assertEqual(df['result_type'][0], 'ANALYZED_DATA')
        self.assertEqual(2, mk_bb.call_count)
        self.assertTrue(mk_egnyte.called)

        parse_guava_results(
            self.mock_files_data, MockPostgresHook(), 'ok', check_bb=False)
        self.assertEqual(3, mk_bb.call_count)
        self.assertTrue(mk_egnyte.called)
        self.assertEqual(3, mk_load.call_count)
        df = mk_load.call_args[0][0]
        self.assertEqual((1, 5), df.shape)
        self.assertCountEqual([
                'guava_file_id',
                'row_letter',
                'column_number',
                'result_value',
                'result_type'
            ],
            list(df))
        self.assertEqual(df['guava_file_id'][0], 123)
        self.assertEqual(df['row_letter'][0], 'X')
        self.assertEqual(df['column_number'][0], 22)
        self.assertEqual(df['result_value'][0], '23')
        self.assertEqual(df['result_type'][0], 'ANALYZED_DATA')
