import logging
import re
from datetime import datetime

from airflow import DAG
from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator)
from airflow.hooks.postgres_hook import PostgresHook
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import (
    BranchPythonOperator, PythonOperator)
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule
from darwinsync_api_helper import DSAPIHelper
from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import get_ds_helper, egnytize, get_slack_conn
from extensions.slack_alert import task_fail_slack_alert
from extensions.fiamat_utils import open_fiamat_file
from scipy.io.matlab.miobase import MatReadError
from requests.exceptions import ConnectionError
from tempfile import NamedTemporaryFile

DB_CONNECTION_ID = 'informatics_db'
DB_QC_FILES_TABLE = 'stg_infodw.qc_files'
DB_QC_AUDIT_TABLE = 'stg_infodw.qc_file_audit'
DB_QC_CONTENT_TABLE = 'stg_infodw.qc_file_contents'
DB_QC_METADATA_VIEW = 'stg_infodw.qc_metadata_v'
AUDIT_TARGET_FIELDS = ['qc_file_id', 'action_name', 'action_result']
OK_OUTPUT_STATUS = 'OK'
COMPRESS_OUTPUT_FILES_COUNT = 30

LAST_LOADING_DATE = datetime(year=1970, month=1, day=1)
DOTD_DATE_REQUEST_BATCH_THRESHOLD = 30

logging.basicConfig(level=logging.DEBUG)

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='load_qc',
    default_args=args,
    schedule_interval='*/15 * * * *',  # every 15 minutes
    catchup=False,
    max_active_runs=1
)


class DownloadFileError(Exception):
    pass


def get_last_loading_date():
    """
    Returns the max date in the file_created_at of stg_infodw.qc_files table

    :return: datetime
    """
    # Get the last loading date in database
    hook = PostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    last_loading_date = hook.get_first(
        f'''
            SELECT
                MAX(file_created_at)
            FROM {DB_QC_FILES_TABLE}
        ''')[0]

    if not last_loading_date:
        # if table qc_files is empty set earliest date
        last_loading_date = LAST_LOADING_DATE

    return last_loading_date


def new_files_on_bb_check():
    """Check if there are files on BB after last loading date in database"""
    threshold_date = get_last_loading_date()

    # Get file count in BB since threshold date
    extractor = get_ds_helper(conn_id='biobright')
    bb_files_count = extractor.get_files_count(
        client_file_ext='.fiamat',
        client_path='s3://gmet-rheos/converter-out/Shared/Lab Data/',
        created_at_start=threshold_date)

    hook = PostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    invalidated_count = hook.get_first(
        f'''
            SELECT
                COUNT(*)
            FROM {DB_QC_FILES_TABLE}
            WHERE
                load_status = 'invalidated'
        ''')[0]

    if bb_files_count > 1 or invalidated_count > 0:
        new_files_count = bb_files_count - 1 + invalidated_count
        logging.info(
            f'Found {new_files_count} new files. Moving to extract step.')
        return 'extract_files'
    else:
        logging.info('No new files found. Moving to finish.')
        return 'finish'


def get_date_from_entry(entry, field_name):
    """
    Return the right format of date and time
    :param entry: HTTP response data
    :param field_name: typen of date (uodated/created/etc)
    :return: datetime object
    """
    date = entry['attributes'][field_name]
    return datetime.strptime(date, "%Y-%m-%dT%H:%M:%S.%fZ")


def load_fiamat_data(hook, file_id, spectrum):
    """
    Inserting fiamat file data into database
    :param hook: AirFlow database hook
    :param file_id: QC file identifier
    :param spectrum: Spectrum object
    """

    columns = ['qc_file_id', 'num_centroids',
               'nz_array', 'normalized_scaled_vector',
               'spectrum_ab_array',
               'spectrum_mz_array',
               'mz_min', 'mz_max',
               'mode', 'signal_to_noise',
               'fiamat_tic', 'summed_tic',
               'acq_date']
    row = (file_id, spectrum.num_centroids,
           spectrum.nz_array.tolist(),
           spectrum.normalized_scaled_v,
           list(spectrum.spectrum.values()),
           list(spectrum.spectrum.keys()),
           spectrum.low_mz, spectrum.high_mz,
           spectrum.mode, spectrum.signal_to_noise,
           spectrum.fiamat_tic, spectrum.summed_tic,
           spectrum.start_time_stamp)

    hook.insert_rows(
        DB_QC_CONTENT_TABLE,
        [row],
        target_fields=columns)


def get_metadata(hook, on_plate_ids):
    """
    Get metadata info from database
    for entries with pointed on_plate_id
    :param hook: AirFlow database hook
    :param on_plate_ids: set/array of on_plate_id
    :return: dictionary on_plate_id -> metadata
    """

    logging.debug(f'Get metadata by on_plate_id: [{on_plate_ids}]')
    metadata = hook.get_records(
        f'''
            SELECT
                on_plate_id,
                experiment_name,
                sample,
                cell_type
            FROM {DB_QC_METADATA_VIEW}
            WHERE
                on_plate_id IN %s
        ''',
        parameters=(tuple(on_plate_ids),)
    )
    m_dict = {m[0]: m[1:] for m in metadata}
    return m_dict


def trunc_filename(filename, tail):
    return filename.replace('a.fiamat', tail) \
        .replace('b.fiamat', tail).replace('.fiamat', tail)


def get_dotd_dates(extractor: DSAPIHelper):
    dotd_data = extractor.get_files_index(
        client_path='\\\\Shared\\\\Lab Data\\\\%\\\\P-%.d'
                    '\\\\AcqData\\\\sample_info.xml',
        remove_obsolete_versions=True)
    dotd_re = re.compile(
        r'\\(P\-\d+_[A-Z]\d+(_r\d+)?_(neg|pos)(a|b)?)\.d\\',
        re.IGNORECASE)
    dotd_date_dict = {}
    for dotd_file in dotd_data:
        match = dotd_re.search(dotd_file['attributes']['client_path'])
        if match:
            dotd_date_dict[match.group(1)] = \
                get_date_from_entry(dotd_file, 'client_created_at')
    return dotd_date_dict


def get_dotd_date(extractor: DSAPIHelper, filename):
    dotd_pathname = trunc_filename(filename, '.d')
    dotd_data = extractor.get_files_index(client_path=dotd_pathname,
                                          remove_obsolete_versions=True)
    return get_date_from_entry(dotd_data[0], 'client_created_at')


def update_old_file_info(hook, entry, dotd_acq_date, file_id):
    """Update fields in table stg_infodw.qc_files."""
    hook.run(
        f'''
            UPDATE {DB_QC_FILES_TABLE}
            SET
                file_created_at = %s,
                file_updated_at = %s,
                dotd_acq_date = %s
            WHERE
                id = %s
        ''',
        parameters=(entry['created_at'],
                    entry['updated_at'],
                    dotd_acq_date,
                    file_id))


def insert_new_file_info(hook, entry, dotd_acq_date, metadata):
    """Insert info about new fiamat file into database."""
    query_result = hook.get_first(f"""
        INSERT INTO {DB_QC_FILES_TABLE}
        (
            file_created_at,
            file_updated_at,
            dotd_acq_date,
            src_filename,
            load_status,
            experiment_name,
            sample,
            cell_type,
            plate_id,
            row_letter,
            column_number,
            replicate,
            data_source
        )
        VALUES
        (
            %(file_created_at)s,
            %(file_updated_at)s,
            %(dotd_acq_date)s,
            %(src_filename)s,
            'started',
            %(experiment_name)s,
            %(sample)s,
            %(cell_type)s,
            %(plate_id)s,
            %(row_letter)s,
            %(column_number)s,
            %(replicate)s,
            'biobright'
        )
        RETURNING
            id
    """, parameters={
        'file_created_at': entry['created_at'],
        'file_updated_at': entry['updated_at'],
        'dotd_acq_date': dotd_acq_date,
        'src_filename': entry['src_filename'],
        'experiment_name': metadata[0],
        'sample': metadata[1],
        'cell_type': metadata[2],
        'plate_id': entry['plate_id'],
        'row_letter': entry['row_letter'],
        'column_number': entry['column_number'],
        'replicate': entry['replicate']
    })
    return query_result[0]


def extract_files():
    """Extract files from BB, parse them and load into database"""

    extractor = get_ds_helper(conn_id='biobright')

    last_loading_date = get_last_loading_date()
    logging.debug(
        f'Will be loading only files after (and included) {last_loading_date}')

    files_data, files_included = extractor.get_files_index(
        client_file_ext='.fiamat',
        client_path='s3://gmet-rheos/converter-out/Shared/Lab Data/',
        created_at_start=last_loading_date,
        and_included=True,
        remove_obsolete_versions=True,
        sort='created_at'
    )

    # Extract main attributes
    entries = [{'id': file_data['id'],
                'client_path': file_data['attributes']['client_path'],
                'filename': file_data['attributes']['client_file_name'],
                'created_at': get_date_from_entry(file_data, 'created_at'),
                'updated_at': get_date_from_entry(file_data, 'updated_at'),
                'url': next(item['attributes']['s3_link']
                            for item in files_included
                            if item['id'] == file_data['id'])
                } for file_data in files_data]

    # Append to entries array from invalidated files
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    invalidated_files = hook.get_records(
        f'''
            SELECT
                id,
                src_filename,
                file_created_at
            FROM {DB_QC_FILES_TABLE}
            WHERE
                load_status = 'invalidated'
        '''
    )
    for file_row in invalidated_files:
        files_data, files_included = extractor.get_files_index(
            client_path=file_row[1],
            and_included=True,
            remove_obsolete_versions=True
        )
        files_data = [file_data for file_data in files_data
                      if file_row[1] in file_data['attributes']['client_path']]
        entries.extend([{
            'id': file_data['id'],
            'record_id': file_row[0],
            'invalidated': True,
            'old_created_at': file_row[2],
            'client_path': file_data['attributes']['client_path'],
            'filename': file_data['attributes']['client_file_name'],
            'created_at': get_date_from_entry(file_data, 'created_at'),
            'updated_at': get_date_from_entry(file_data, 'updated_at'),
            'url': next(item['attributes']['s3_link']
                        for item in files_included
                        if item['id'] == file_data['id'])
            } for file_data in files_data])

    # Get .d dates with batch method if threshold exceeded
    dotd_date_dict = None
    if len(entries) > DOTD_DATE_REQUEST_BATCH_THRESHOLD:
        dotd_date_dict = get_dotd_dates(extractor)

    # Parse filename and fill attributes
    task_output = dict()
    filename_re = re.compile(
        r'((P\-\d+)_([A-Z])(\d+))(_r\d+)?_(neg|pos)(a|b)?\.fiamat',
        re.IGNORECASE)
    for entry in entries:
        entry['src_filename'] = egnytize(entry['client_path'])
        match = filename_re.match(entry['filename'])
        if not match:
            task_output[entry['src_filename']] = 'Filename could not be parsed'
            logging.warning(
                f"Filename {entry['filename']} could not be parsed")
            continue
        entry['plate_id'] = match.group(2).upper()
        entry['row_letter'] = match.group(3).upper()
        entry['column_number'] = int(match.group(4))
        entry['on_plate_id'] = match.group(1).upper()
        entry['replicate'] = match.group(7).lower() \
            if match.group(7) else 'unknown'
        if dotd_date_dict:
            trunc = trunc_filename(entry['filename'], '')
            if trunc in dotd_date_dict:
                entry['dotd_acq_date'] = dotd_date_dict[trunc]

    # Filter out entries which were not parsed
    entries = [entry for entry in entries if entry['on_plate_id']]

    #  Get metadata for pulled on_plate_id(s)
    on_plate_ids = set([entry['on_plate_id'] for entry in entries])
    metadata_dict = get_metadata(hook, on_plate_ids)

    for entry in entries:
        src_filename = entry['src_filename']
        dotd_acq_date = entry['dotd_acq_date'] if 'dotd_acq_date' in entry \
            else get_dotd_date(extractor, entry['filename'])

        # Check if this file already exists in database
        record_id = entry['record_id'] if 'record_id' in entry else None

        metadata = metadata_dict.get(entry['on_plate_id'], 'impossible value')
        if metadata == 'impossible value':
            logging.error(f'File {src_filename} has no metadata')
            continue

        old_file_created_at = entry['old_created_at'] \
            if 'old_created_at' in entry else None
        if not record_id:
            existing_row = hook.get_first(
                f'''
                    SELECT
                        id,
                        file_created_at
                    FROM {DB_QC_FILES_TABLE}
                    WHERE
                        src_filename = %s
                ''', parameters=(src_filename,))
            if existing_row:
                record_id, old_file_created_at = existing_row

        if record_id and ('invalidated' in entry or
                          old_file_created_at < entry['created_at']):
            # We will reload all data with new created_at value
            logging.debug(
                f"File {src_filename} was rewritten in BioBright at"
                f" {entry['created_at']} and so it must be reloaded"
                f" into the database.")
            logging.debug(f'Cleaning up previous data for {src_filename}')
            hook.run('SELECT stg_infodw.qc_cleanup_file(%s)',
                     parameters=(src_filename,))
            logging.debug(f'Update info about {src_filename}')
            update_old_file_info(hook, entry, dotd_acq_date, record_id)
            hook.insert_rows(
                DB_QC_AUDIT_TABLE,
                [(record_id, 'Update entity record', 'Success')],
                target_fields=AUDIT_TARGET_FIELDS)

        elif record_id and old_file_created_at >= entry['created_at']:
            # We will skip all not changed data
            logging.info(
                f'File {src_filename} which was updated at'
                f" {entry['updated_at']} will be skipped because"
                f' it was loaded previously')
            continue

        else:
            # If the file isn't in database we will load it
            logging.debug(f'Inserting {src_filename} data into db')
            record_id = insert_new_file_info(
                hook, entry, dotd_acq_date, metadata)
            hook.insert_rows(
                DB_QC_AUDIT_TABLE,
                [(record_id, 'Insert entity record', 'Success')],
                target_fields=AUDIT_TARGET_FIELDS)

        logging.debug(f'Starting loading file: {src_filename}')
        try:
            with NamedTemporaryFile() as tmp_file:
                downloaded = extractor.download_s3_file(entry['url'],
                                                        tmp_file.name,
                                                        entry['id'])
                if not downloaded:
                    raise DownloadFileError(
                        f'Unable to download file {src_filename}')
                logging.debug(f'Temp file {tmp_file.name} was downloaded')
                logging.debug(f'Trying to open file {src_filename}')
                spectrum = open_fiamat_file(tmp_file.name)
        except (ConnectionError, DownloadFileError, MatReadError) as error:
            logging.error(
                'Exception occurred while opening'
                f' mat file {src_filename}: {error}')
            hook.insert_rows(
                DB_QC_AUDIT_TABLE,
                [(record_id, 'Download and parse file', f'Error: {error}')],
                target_fields=AUDIT_TARGET_FIELDS)
            hook.run(
                f'''
                    UPDATE {DB_QC_FILES_TABLE}
                    SET
                        load_status = %s
                    WHERE
                        id = %s
                ''',
                parameters=('failed', record_id))
            task_output[src_filename] = str(error)
            continue

        hook.insert_rows(
            DB_QC_AUDIT_TABLE,
            [(record_id, 'Download and parse file', 'Success')],
            target_fields=AUDIT_TARGET_FIELDS)

        logging.debug('Starting to write data into database')
        try:
            load_fiamat_data(hook, record_id, spectrum)
        except Exception as ex:
            logging.error('Fail to load fiamat data into database')
            hook.insert_rows(
                DB_QC_AUDIT_TABLE,
                [(record_id, 'Load file contents into database',
                  f'Error: {ex}')],
                target_fields=AUDIT_TARGET_FIELDS)
            hook.run(
                f'''
                    UPDATE {DB_QC_FILES_TABLE}
                    SET
                        load_status = %s
                    WHERE
                        id = %s
                ''',
                parameters=('failed', record_id))
            task_output[src_filename] = str(ex)
            continue

        logging.debug(f'Fiamat data from file {src_filename} was'
                      ' successfully loaded into database')
        hook.insert_rows(
            DB_QC_AUDIT_TABLE,
            [(record_id, 'Load file contents into database', 'Success')],
            target_fields=AUDIT_TARGET_FIELDS)
        hook.run(
            f'''
                UPDATE {DB_QC_FILES_TABLE}
                SET
                    load_status = %s
                WHERE
                    id = %s
            ''',
            parameters=('loaded', record_id))

        logging.info(f'Finished loading file with id={record_id}')
        task_output[src_filename] = 'OK'

    return task_output


def merge_output(**kwargs):
    """
    Merge output of extract and process stages
    and prepare it for showing in Slack
    """
    extract_files_output = kwargs['ti'].xcom_pull(task_ids='extract_files')
    process_files_output = kwargs['ti'].xcom_pull(task_ids='process_files')
    merged_output = extract_files_output
    merged_output.update({row[0]: row[1] for row in process_files_output})
    kwargs['ti'].xcom_push(key='summary',
                           value='total: {total}, failed: {failed}'.format(
                               total=len(merged_output),
                               failed=sum(1 for v in merged_output.values()
                                          if v != OK_OUTPUT_STATUS)))
    # Compress information about OK files if needed
    if len(merged_output) > COMPRESS_OUTPUT_FILES_COUNT:
        ok_count = sum(1 for v in merged_output.values()
                       if v == OK_OUTPUT_STATUS)
        merged_output = dict(filter(lambda i: i[1] != OK_OUTPUT_STATUS,
                                    merged_output.items()))
        merged_output[f'{ok_count} files (filenames hidden)'] = \
            OK_OUTPUT_STATUS
    return '\n'.join(['*{result}*: `{filename}`{error}'.format(
        result=result if result == OK_OUTPUT_STATUS else 'Error',
        filename=filename,
        error=f' ({result})' if result != OK_OUTPUT_STATUS else '')
        for filename, result in merged_output.items()])


if __name__.startswith('unusual_prefix'):
    check_new_files_task = BranchPythonOperator(
        task_id='check_new_files_on_bb',
        python_callable=new_files_on_bb_check,
        dag=dag,
    )
    extract_files_task = PythonOperator(
        task_id='extract_files',
        python_callable=extract_files,
        dag=dag
    )
    process_files_task = PostgresOutputOperator(
        task_id='process_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT filename, output FROM stg_infodw.qc_process_files()',
        dag=dag
    )
    merge_output_task = PythonOperator(
        task_id='merge_output',
        python_callable=merge_output,
        provide_context=True,
        dag=dag
    )
    slack_conn = get_slack_conn(f'{dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: QC files were loaded and parsed "
            "({{ task_instance.xcom_pull(task_ids='merge_output',"
                "key='summary') }}). "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull(task_ids='merge_output') }}"),
        username='Airflow',
        dag=dag
    )
    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )

    check_new_files_task >> finish_task
    check_new_files_task >> extract_files_task >> process_files_task >> \
        merge_output_task >> slack_report_task >> finish_task


if __name__ == "__main__":
    extract_files()
