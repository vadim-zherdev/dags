import logging
from io import TextIOBase
import pandas as pd
from pandas.errors import ParserError
import re
from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator
)
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import (
    BranchPythonOperator, PythonOperator
)
from airflow.utils.trigger_rule import TriggerRule
from airflow.utils.dates import days_ago
from airflow.utils.db import provide_session

from darwinsync_api_helper import FileNotFoundError
from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.slack_alert import task_fail_slack_alert
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import get_slack_conn, get_ds_helper

DB_CONNECTION_ID = 'informatics_db'
DB_FORTESSA_FILES_TABLE = 'stg_infodw.fortessa_files'
DB_FORTESSA_AUDIT_TABLE = 'stg_infodw.fortessa_file_audit'
DB_FORTESSA_FILE_COLUMNS = [
    'id',
    'experiment_id',
    'experiment_name',
    'plate_id',
    'src_filename',
    'result_stage'
]
DB_FORTESSA_GRAB_FILES_QUERY = f'''
    SELECT
        {', '.join(DB_FORTESSA_FILE_COLUMNS)}
    FROM stg_infodw.fortessa_start_files()
'''
DB_FORTESSA_FILE_ID_POS = DB_FORTESSA_FILE_COLUMNS.index('id')
DB_FORTESSA_FILE_SRC_FILENAME_POS = DB_FORTESSA_FILE_COLUMNS.index(
    'src_filename')
DB_FORTESSA_FILE_PLATE_POS = DB_FORTESSA_FILE_COLUMNS.index('plate_id')

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='load_fortessa',
    default_args=args,
    schedule_interval='@hourly',
    catchup=False
)


def new_files_check():
    """Check for new FORTESSA files in SAPIO tables"""
    hook = PostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    new_files_count = hook.get_first(
        '''
        SELECT
            COUNT(*)
        FROM
        (
            SELECT 1 FROM stg_infodw.fortessa_missing_files_v
            UNION ALL
            SELECT 1
            FROM stg_infodw.fortessa_files
            WHERE
                load_status = 'invalidated'
                AND result_stage = 'ANALYZED_DATA'
        ) sq
        ''')[0]
    if new_files_count > 0:
        logging.info(
            f'Found {new_files_count} new files. Moving to load step.')
        return 'create_files'
    else:
        logging.info('No new files found. Moving to finish.')
        return 'finish'


def parse_file(input: TextIOBase, file_id):
    """
    The func for parsing of Fortessa csv files and transform it
    into the dataframe in database table format
    Return the output dataframe

    :param
    input:
        csv file
    file_id: int

    :return:
        pandas.DataFrame
    """

    df_input_file = pd.read_csv(input, header=0)

    # validate input file
    good_line_criteria = re.compile(
        r'_A[0-9]+_|_B[0-9]+_|_C[0-9]+_|_D[0-9]+_|_E[0-9]+_')
    good_line_count = len(list(
        filter(lambda x: good_line_criteria.search(str(x)),
               df_input_file.iloc[:, 0])))
    if good_line_count <= 2:
        raise ParserError(
            'File does not seem to have well positions specified')

    # rename first column from empty name to sample
    df_input_file.rename(
        columns={df_input_file.columns[0]: 'sample'},
        inplace=True
    )

    # drop fully empty columns
    df_input_file.dropna(axis=1, how="all", inplace=True)

    # detect empty sample
    if df_input_file['sample'].hasnans:
        raise ParserError('File contains empty samples')

    # melt
    fortessa_df = pd.melt(
        df_input_file,
        id_vars='sample',
        value_vars=list(df_input_file.columns[1:]),
        var_name='result_field',
        value_name='result_value'
    ).set_index(['sample']).reset_index()

    # detect values with percent symbol
    if not pd.to_numeric(fortessa_df['result_value'],
                         errors='coerce').notnull().all():
        raise ParserError(
            'Result values are not valid numeric values')

    fortessa_df['fortessa_file_id'] = file_id
    return fortessa_df


@provide_session
def extract_files(session=None):
    """Extract files from BB, parse them and load into database"""
    extractor = get_ds_helper()

    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    file_rows = hook.get_records(DB_FORTESSA_GRAB_FILES_QUERY)
    logging.debug(f'{len(file_rows)} files found for processing')

    task_output = dict()
    for file_row in file_rows:
        # get record identifier
        record_id = file_row[DB_FORTESSA_FILE_ID_POS]
        logging.debug(f'Started loading file with id={record_id}')

        # parse and validate
        filepath = file_row[DB_FORTESSA_FILE_SRC_FILENAME_POS]
        bb_filepath = filepath[filepath.find('/Sapio'):].replace('/', '\\\\')
        try:
            file_contents = extractor.get_file(client_path=bb_filepath)
            parse_result = parse_file(file_contents, record_id)
        except (FileNotFoundError, ParserError) as error:
            logging.warning(f'Error parsing file {filepath}: {error}')
            hook.insert_rows(
                DB_FORTESSA_AUDIT_TABLE,
                [(record_id, 'Parse file contents', f'Error: {error}')],
                target_fields=[
                    'fortessa_file_id', 'action_name', 'action_result'
                ])
            hook.run(
                f'''
                    UPDATE {DB_FORTESSA_FILES_TABLE}
                    SET load_status = %s
                    WHERE id = %s
                ''',
                parameters=('failed', record_id))
            task_output[filepath] = str(error)
            continue

        logging.debug(f'Successfully parsed file {filepath}')
        hook.insert_rows(
            'stg_infodw.fortessa_file_audit',
            [(record_id, 'Parse file contents', 'Success')],
            target_fields=['fortessa_file_id', 'action_name', 'action_result'])

        # fill data
        sqlalchemy_engine = hook.get_sqlalchemy_engine()
        if sqlalchemy_engine:
            parse_result.to_sql(
                'fortessa_file_contents', sqlalchemy_engine,
                schema='stg_infodw', if_exists='append',
                index=False, method='multi')
        else:
            logging.warning('Saving to database skipped')
            session.rollback()

        # log success
        hook.run(
            f'''
                UPDATE {DB_FORTESSA_FILES_TABLE}
                SET load_status = %s
                WHERE id = %s
            ''',
            parameters=('loaded', record_id))
        hook.insert_rows(
            'stg_infodw.fortessa_file_audit',
            [(record_id, 'Fill data', 'Success')],
            target_fields=['fortessa_file_id', 'action_name', 'action_result'])
        logging.info(f'Finished loading file with id={record_id}')
        task_output[filepath] = 'OK'

    return task_output


def merge_output(**kwargs):
    """
    Merge output of extract and process stages
    and prepare it for showing in Slack
    """
    extract_files_output = kwargs['ti'].xcom_pull(task_ids='extract_files')
    process_files_output = kwargs['ti'].xcom_pull(task_ids='process_files')
    merged_output = extract_files_output
    merged_output.update({row[0]: row[1] for row in process_files_output})
    kwargs['ti'].xcom_push(key='summary',
                           value='total: {total}, failed: {failed}'.format(
                               total=len(merged_output),
                               failed=sum(1 for v in merged_output.values()
                                          if v != 'OK')))
    return '\n'.join(['*{result}*: `{filename}`{error}'.format(
        result=result if result == 'OK' else 'Error',
        filename=filename,
        error=f' ({result})' if result != 'OK' else '')
        for filename, result in merged_output.items()])


if __name__.startswith('unusual_prefix'):
    check_new_files_task = BranchPythonOperator(
        task_id='check_new_files',
        python_callable=new_files_check,
        dag=dag,
    )
    create_files_task = PostgresOperator(
        task_id='create_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT stg_infodw.fortessa_create_files(src := \'biobright\')',
        dag=dag
    )
    extract_files_task = PythonOperator(
        task_id='extract_files',
        python_callable=extract_files,
        dag=dag,
    )
    process_files_task = PostgresOutputOperator(
        task_id='process_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT filename, output FROM stg_infodw.fortessa_process_files()',
        dag=dag
    )
    merge_output_task = PythonOperator(
        task_id='merge_output',
        python_callable=merge_output,
        provide_context=True,
        dag=dag
    )
    slack_conn = get_slack_conn(f'{dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: FORTESSA files were loaded and processed "
            "({{ task_instance.xcom_pull(task_ids='merge_output',"
            "key='summary') }}). "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull(task_ids='merge_output') }}"),
        username='Airflow',
        dag=dag
    )
    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )
    check_new_files_task >> finish_task
    check_new_files_task >> create_files_task >> extract_files_task >> \
        process_files_task >> merge_output_task >> slack_report_task >> \
        finish_task


if __name__ == "__main__":
    extract_files()
