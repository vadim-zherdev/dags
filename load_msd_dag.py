import logging

from airflow.models import DAG
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator,
)
from airflow.hooks.postgres_hook import PostgresHook
from airflow.utils.db import provide_session
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule

from pandas import read_csv
from os import path

from darwinsync_api_helper import FileNotFoundError
from msd_parser import MsdParser, MsdParseError
from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.slack_alert import task_fail_slack_alert
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import get_slack_conn, get_ds_helper

DB_CONNECTION_ID = 'informatics_db'
DB_MSD_FILES_TABLE = 'stg_infodw.msd_files'
DB_MSD_FILE_COLUMNS = [
    'id',
    'experiment_id',
    'experiment_name',
    'plate_id',
    'src_filename',
    'result_stage'
]
DB_MSD_GRAB_FILES_QUERY = f'''
    SELECT
        {', '.join(DB_MSD_FILE_COLUMNS)}
    FROM stg_infodw.msd_start_files()
'''
DB_MSD_FILE_ID_POS = DB_MSD_FILE_COLUMNS.index('id')
DB_MSD_FILE_SRC_FILENAME_POS = DB_MSD_FILE_COLUMNS.index('src_filename')
DB_MSD_FILE_PLATE_POS = DB_MSD_FILE_COLUMNS.index('plate_id')

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='load_msd',
    default_args=args,
    schedule_interval='@hourly',
    catchup=False
)


def new_files_check():
    """Check for new MSD files in SAPIO tables"""
    hook = PostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    new_files_count = hook.get_first(
        '''
        SELECT
            COUNT(*)
        FROM
        (
            SELECT 1 FROM stg_infodw.msd_missing_files_v
            UNION ALL
            SELECT 1
            FROM stg_infodw.msd_files
            WHERE
                load_status = 'invalidated'
        ) sq
        ''')[0]
    if new_files_count > 0:
        logging.info(
            f'Found {new_files_count} new files. Moving to load step.')
        return 'create_files'
    else:
        logging.info('No new files found. Moving to finish.')
        return 'finish'


@provide_session
def extract_files(session=None, **kwargs):
    """Extract files from BB, parse them and load into database"""
    extractor = get_ds_helper()
    parser = MsdParser()
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    file_rows = hook.get_records(DB_MSD_GRAB_FILES_QUERY)
    logging.debug(f'{len(file_rows)} files found for processing')

    task_output = dict()
    plate_spot_number = dict()
    for file_row in file_rows:
        # get record identifier
        record_id = file_row[DB_MSD_FILE_ID_POS]
        logging.debug(f'Started loading file with id={record_id}')

        # parse and validate
        filepath = file_row[DB_MSD_FILE_SRC_FILENAME_POS]
        bb_filepath = filepath[filepath.find('/Sapio'):].replace('/', '\\\\')
        try:
            file_contents = extractor.get_file(client_path=bb_filepath)
            parse_result = parser.parse(file_contents)
            if not parse_result.ok:
                for error in parse_result.errors:
                    logging.warning(f'Error parsing file {filepath}: {error}')
                    task_output[filepath] = error
                hook.insert_rows(
                    'stg_infodw.msd_file_audit',
                    [(record_id, 'Parse file contents', f'Error: {error}')
                     for error in parse_result.errors],
                    target_fields=[
                        'msd_file_id', 'action_name', 'action_result'])
                hook.run(
                    f'''
                        UPDATE {DB_MSD_FILES_TABLE}
                        SET load_status = %s
                        WHERE id = %s
                    ''',
                    parameters=('failed', record_id))
                continue
        except (FileNotFoundError, MsdParseError) as error:
            logging.warning(
                f'Exception occurred while parsing file {filepath}: {error}')
            hook.insert_rows(
                'stg_infodw.msd_file_audit',
                [(record_id, 'Parse file contents', f'Exception: {error}')],
                target_fields=['msd_file_id', 'action_name', 'action_result'])
            hook.run(
                f'''
                    UPDATE {DB_MSD_FILES_TABLE}
                    SET load_status = %s
                    WHERE id = %s
                ''',
                parameters=('failed', record_id))
            task_output[filepath] = str(error)
            continue

        logging.debug(f'Successfully parsed file {filepath}')
        hook.insert_rows(
            'stg_infodw.msd_file_audit',
            [(record_id, 'Parse file contents', 'Success')],
            target_fields=['msd_file_id', 'action_name', 'action_result'])

        # save spot number
        plate_id = file_row[DB_MSD_FILE_PLATE_POS]
        plate_spot_number[plate_id] = int(parse_result.metadata.get(
            'Spots Per Well', len(parse_result.data)))

        # fill metadata
        hook.insert_rows(
            'stg_infodw.msd_file_metadata',
            [(record_id, key, value)
             for key, value in parse_result.metadata.items()],
            target_fields=['msd_file_id', 'meta_name', 'meta_value'])
        logging.debug(f'Successfully filled metadata for file id={record_id}')
        hook.insert_rows(
            'stg_infodw.msd_file_audit',
            [(record_id, 'Fill metadata', 'Success')],
            target_fields=['msd_file_id', 'action_name', 'action_result'])

        # fill data
        sqlalchemy_engine = hook.get_sqlalchemy_engine()
        for spot, df in enumerate(parse_result.data, start=1):
            target_df = df.unstack().reset_index(name='result_value')
            target_df.rename(
                columns={'level_0': 'column_number', 'level_1': 'row_letter'},
                inplace=True)
            target_df['msd_file_id'] = record_id
            target_df['spot_number'] = spot
            if sqlalchemy_engine:
                target_df.to_sql(
                    'msd_file_contents', sqlalchemy_engine,
                    schema='stg_infodw', if_exists='append', index=False,
                    method='multi')
            else:
                logging.warning('Saving to database skipped')
                session.rollback()

        # log success
        hook.run(
            f'''
                UPDATE {DB_MSD_FILES_TABLE}
                SET load_status = %s
                WHERE id = %s
            ''',
            parameters=('loaded', record_id))
        hook.insert_rows(
            'stg_infodw.msd_file_audit',
            [(record_id, 'Fill data', 'Success')],
            target_fields=['msd_file_id', 'action_name', 'action_result'])
        logging.info(f'Finished loading file with id={record_id}')
        task_output[filepath] = 'OK'

    kwargs['ti'].xcom_push(key='plate_spot_number', value=plate_spot_number)
    kwargs['ti'].xcom_push(key='output', value=task_output)


def fill_kits(**kwargs):
    """Fill MSD kits for parsed plates based on their spot number"""
    plate_spot_number = kwargs['ti'].xcom_pull(task_ids='extract_files',
                                               key='plate_spot_number')
    if len(plate_spot_number) == 0:
        logging.info('No MSD kits to write.')
        return

    base_path = path.dirname(path.abspath(__file__))
    csv_1spot = path.join(base_path, 'static/msd_1_spot_kit.tsv')
    csv_10spot = path.join(base_path, 'static/msd_10_spot_kit.tsv')
    with open(csv_1spot, 'rt', encoding='utf8') as f:
        df_1_spot = read_csv(f, sep='\t', header=0)
    with open(csv_10spot, 'rt', encoding='utf8') as f:
        df_10_spot = read_csv(f, sep='\t', header=0)

    # filter existing plates
    hook = PostgresHook(postgres_conn_id=DB_CONNECTION_ID)
    existing_plates = hook.get_records(
        '''
            SELECT DISTINCT
                plate_id
            FROM stg_infodw.msd_kit
            WHERE plate_id IN %s
        ''',
        parameters=(tuple(plate_spot_number.keys()),))
    for plate in existing_plates:
        del plate_spot_number[plate[0]]

    # fill kits
    sqlalchemy_engine = hook.get_sqlalchemy_engine()
    logging.info(
        'Writing MSD kits to database. '
        f'Count to write: {len(plate_spot_number)}.')
    if sqlalchemy_engine:
        for plate_id, spot_number in plate_spot_number.items():
            if spot_number == 1:
                df_1_spot['plate_id'] = plate_id
                df_1_spot.to_sql(
                    'msd_kit', sqlalchemy_engine,
                    schema='stg_infodw', if_exists='append', index=False,
                    method='multi')
                logging.info(
                    'Successfully written 1-spot kit '
                    f'for plate {plate_id}')
            elif spot_number == 10:
                df_10_spot['plate_id'] = plate_id
                df_10_spot.to_sql(
                    'msd_kit', sqlalchemy_engine,
                    schema='stg_infodw', if_exists='append', index=False,
                    method='multi')
                logging.info(
                    'Successfully written 10-spot kit '
                    f'for plate {plate_id}')
            else:
                logging.warning(
                    f'Unsupported spot number {spot_number} '
                    f'for plate {plate_id}')
    else:
        logging.warning('Saving kits to database skipped')


def merge_output(**kwargs):
    """
    Merge output of extract and process stages
    and prepare it for showing in Slack
    """
    extract_files_output = kwargs['ti'].xcom_pull(task_ids='extract_files',
                                                  key='output')
    process_files_output = kwargs['ti'].xcom_pull(task_ids='process_files')
    merged_output = extract_files_output
    merged_output.update({row[0]: row[1] for row in process_files_output})
    kwargs['ti'].xcom_push(key='summary',
                           value='total: {total}, failed: {failed}'.format(
                               total=len(merged_output),
                               failed=sum(1 for v in merged_output.values()
                                          if v != 'OK')))
    return '\n'.join(['*{result}*: `{filename}`{error}'.format(
        result=result if result == 'OK' else 'Error',
        filename=filename,
        error=f' ({result})' if result != 'OK' else '')
        for filename, result in merged_output.items()])


if __name__.startswith('unusual_prefix'):
    check_new_files_task = BranchPythonOperator(
        task_id='check_new_files',
        python_callable=new_files_check,
        dag=dag,
    )
    create_files_task = PostgresOperator(
        task_id='create_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT stg_infodw.msd_create_files(src := \'biobright\')',
        dag=dag
    )
    extract_files_task = PythonOperator(
        task_id='extract_files',
        python_callable=extract_files,
        provide_context=True,
        dag=dag,
    )
    process_files_task = PostgresOutputOperator(
        task_id='process_files',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT filename, output FROM stg_infodw.msd_process_files()',
        dag=dag
    )
    fill_kits_task = PythonOperator(
        task_id='fill_kits',
        python_callable=fill_kits,
        provide_context=True,
        dag=dag
    )
    refresh_views_task = PostgresOperator(
        task_id='refresh_views',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT prc_infodw.msd_refresh_mvs()',
        dag=dag
    )
    merge_output_task = PythonOperator(
        task_id='merge_output',
        python_callable=merge_output,
        provide_context=True,
        dag=dag
    )
    slack_conn = get_slack_conn(f'{dag.dag_id}_slack')
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: MSD files were loaded and processed "
            "({{ task_instance.xcom_pull(task_ids='merge_output',"
            "key='summary') }}). "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull(task_ids='merge_output') }}"),
        username='Airflow',
        dag=dag
    )
    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )
    check_new_files_task >> finish_task
    check_new_files_task >> create_files_task >> extract_files_task >> \
        process_files_task >> fill_kits_task >> refresh_views_task >> \
        merge_output_task >> slack_report_task >> finish_task

if __name__ == "__main__":
    extract_files()
