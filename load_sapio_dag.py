"""DAG for importing data from Sapio (and apparently from Egnyte)"""
from airflow.contrib.operators.slack_webhook_operator import (
    SlackWebhookOperator,
)
from airflow.models import DAG, Variable
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from airflow.utils.trigger_rule import TriggerRule

from extensions.postgres_output_operator import PostgresOutputOperator
from extensions.sapio_utils import (
    populate_sapio_tables,
    populate_rhe_plate_well,
    populate_rhe_exp,
    populate_rhe_tables_from_egnyte,
    SCHEMA as SAPIO_SCHEMA,
    TABLE_PREFIX as SAPIO_TABLE_PREFIX,
)
from extensions.slack_alert import task_fail_slack_alert
from extensions.transact_postgres_hook import TransactPostgresHook
from extensions.utils import get_slack_conn

SLACK_CONN_ID = 'sapio_slack'
DB_CONNECTION_ID = 'informatics_db'
SUCCESS_STATUS = 'Success'

args = {
    'owner': 'Rheos',
    'start_date': days_ago(1),
    'on_failure_callback': task_fail_slack_alert
}

dag = DAG(
    dag_id='load_sapio',
    default_args=args,
    schedule_interval='5,25,45 * * * *',
    catchup=False
)


def call_instrument_tool(tool, variables=None, **context):
    """
    Call any function and push its return value to xcom.
    Get variables if passed in context and pass them as kwargs.
    Parameters
    ----------
    tool - the function to call. Must accept database hook as first
        positional argument and SUCCESS_STATUS as the second one.
    variables - Airflow variables to get and pass to the tool as kwargs
    context - AirFlow context
    """
    kwargs = {variable: Variable.get(variable, default_var=None)
              for variable in variables or []}
    hook = TransactPostgresHook(
        postgres_conn_id=DB_CONNECTION_ID, autocommit=True)
    output = tool(hook, SUCCESS_STATUS, **kwargs)
    context['task_instance'].xcom_push(key='output', value=output)


def merge_output(**context):
    """
    Merge output of extract and process stages
    and prepare it for showing in Slack
    """
    if not Variable.get('send_sapio_slack_report', default_var=False):
        return 'finish'
    merged_output = context['task_instance'].xcom_pull(
        task_ids='import_sapio_data', key='output')
    for task_id in [
            'import_sapio_plate_well',
            'import_sapio_exp',
            'import_sapio_egnyte_data']:
        merged_output.update(
            context['task_instance'].xcom_pull(
                task_ids=task_id, key='output'))
    fill_sample_lineage_output = context['task_instance'].xcom_pull(
        task_ids='fill_sapio_sample_lineage', key='output')
    merged_output.update({
        f'{SAPIO_SCHEMA}.{SAPIO_TABLE_PREFIX}rhe_sample_lineage':
            SUCCESS_STATUS if fill_sample_lineage_output
            else f'{fill_sample_lineage_output} rows inserted'})
    output = '\n'.join(['*{result}*: `{table_name}`{error}'.format(
        result=result if result == SUCCESS_STATUS else 'Error',
        table_name=table_name,
        error=f' ({result})' if result != SUCCESS_STATUS else '')
        for table_name, result in merged_output.items()])
    context['task_instance'].xcom_push(key='output', value=output)
    return 'slack_report'


if __name__.startswith('unusual_prefix'):
    import_data_task = PythonOperator(
        task_id='import_sapio_data',
        python_callable=call_instrument_tool,
        op_args=(populate_sapio_tables,),
        provide_context=True,
        dag=dag,
    )
    import_plate_well_task = PythonOperator(
        task_id='import_sapio_plate_well',
        python_callable=call_instrument_tool,
        op_args=(populate_rhe_plate_well,),
        op_kwargs={'variables': ['sapio_report_days_delta']},
        provide_context=True,
        dag=dag,
    )
    fill_sample_lineage_task = PostgresOutputOperator(
        task_id='fill_sapio_sample_lineage',
        postgres_conn_id=DB_CONNECTION_ID,
        sql=f'SELECT {SAPIO_SCHEMA}.fill_sample_lineage()',
        dag=dag,
    )
    import_exp_task = PythonOperator(
        task_id='import_sapio_exp',
        python_callable=call_instrument_tool,
        op_args=(populate_rhe_exp,),
        provide_context=True,
        dag=dag,
    )
    import_egnyte_data_task = PythonOperator(
        task_id='import_sapio_egnyte_data',
        python_callable=call_instrument_tool,
        op_args=(populate_rhe_tables_from_egnyte,),
        provide_context=True,
        dag=dag,
    )
    refresh_views_task = PostgresOperator(
        task_id='refresh_views',
        postgres_conn_id=DB_CONNECTION_ID,
        sql='SELECT prc_infodw.sapio_refresh_mvs()',
        dag=dag
    )
    merge_output_task = BranchPythonOperator(
        task_id='merge_output',
        python_callable=merge_output,
        provide_context=True,
        dag=dag
    )
    slack_conn = get_slack_conn(SLACK_CONN_ID)
    slack_report_task = SlackWebhookOperator(
        task_id='slack_report',
        http_conn_id=slack_conn.conn_id,
        webhook_token=slack_conn.password,
        message=(
            ":ok_hand: Sapio data has been imported. "
            "Here are the results:"
            "\n{{ task_instance.xcom_pull("
            "task_ids='merge_output', key='output') }}"),
        username='Airflow',
        dag=dag,
    )
    finish_task = DummyOperator(
        task_id='finish',
        trigger_rule=TriggerRule.NONE_FAILED,
        dag=dag
    )
    import_data_task >> import_plate_well_task >> fill_sample_lineage_task >> \
        import_exp_task >> import_egnyte_data_task >> refresh_views_task >> \
        merge_output_task >> slack_report_task >> finish_task
    merge_output_task >> finish_task
