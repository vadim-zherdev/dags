import logging
from unittest import TestCase
import pandas as pd
from unittest.mock import patch, MagicMock

from pandas.core.dtypes.common import is_datetime64_ns_dtype

from extensions.mocks import (
    MockPostgresHook,
)
from load_genedata_dag import (
    load_qcsessions,
    load_compounds,
    load_plates,
    load_genedata
)

mock_genedata_qcsessions_response = [
    {
        'qcsRefId': 'QCS-3',
        'name': 'MM test',
        'description': 'some description',
        'reported': False,
        'creationDate': 1538510950099,
        'lastUpdate': 1538510950099,
        'creator': {
            'loginName': 'some user name'},
        'public': False,
        'uniqueCompoundIds': 160,
        'uniqueCombinationIds': 0,
        'experiment': {
            'expRefId': 'EXP-3',
            'folderName': '/some/path',
            'name': 'WebAssay',
            'description': 'DRC assay',
            'creator': {
                'loginName': 'some user name'},
            'lastUpdate': 1513960700325,
            'corporateId': '',
            'annotations': {}},
        'layers': [
            {
                'layerIndex': 0,
                'name': 'Lance 665',
                'type': 'MEASURED',
                'normalization': {
                    'id': 'NEUTRAL_CONTROLS_MINUS_BLANKS'}
            },
            {
                'layerIndex': 1,
                'name': 'Lance 615',
                'type': 'MEASURED',
                'activityLabel': 'Activity [%]',
                'annotations': {},
                'normalization': {
                    'id': 'COMPOUNDS'}},
            ]
    },
    {
        'qcsRefId': 'QCS-26',
        'name': '20190403_CD45RO_MSD_AD',
        'description': '',
        'reported': False,
        'creationDate': 1554391050550,
        'lastUpdate': 1554391050550,
        'creator': {'loginName': 'user2'},
        'public': False,
        'experiment':
            {
                'expRefId': 'EXP-23',
                'folderName': '/some/path2',
                'name': 'RH_CD45RO_MSD_384_AD',
                'description': 'Cell type:  CD45RO ... for 24 hr',
                'creator': {
                    'loginName': 'user2'},
                'lastUpdate': 1554388534220,
                'corporateId': '',
                'annotations': {}
            },
        'layers': [
            {
                'layerIndex': 0,
                'name': 'IFN-g',
                'type': 'MEASURED',
                'activityLabel': 'Activity [%]',
                'annotations': {},
                'normalization': {
                    'id': 'TWO_POINT',
                    'parameters': {
                        'centralReferenceType': 'Neutral Control',
                        'scaleReferenceType': 'Inhibitor Control',
                        'Assay_Type': 'Stimulator'}
                },
                'condensing': {
                    'id': 'GD_SMART_FIT',
                    'parameters': {
                        'modelSelection': 'Automatic ',
                        'fitValidityMinNumberOfValidPoints': '6.0'
                    }
                },
                'calculatedFitColumns':
                    [
                        {
                            'id': 'qAC50uM'
                        }
                    ]
            },
            {
                'layerIndex': 1,
                'name': 'IL-1b',
                'type': 'MEASURED',
                'activityLabel': 'Activity [%]',
                'normalization': {
                    'id': 'TWO_POINT',
                    'parameters': {
                        'centralReferenceType': 'Neutral Control',
                        'scaleReferenceType': 'Inhibitor Control',
                        'Assay_Type': 'Stimulator'}
                },
                'condensing': {
                    'id': 'GD_SMART_FIT',
                    'parameters': {
                        'modelSelection': 'Automatic',
                        'allowFixedNHill': 'false',
                        'fitValidityMinNumberOfValidPoints': '6.0'
                    }
                },
                'calculatedFitColumns':
                    [
                        {
                            'id': 'qAC50uM'
                        }
                    ]
            }
        ]
    },
    {
        'qcsRefId': 'QCS-4',
        'name': 'JTR-20180817_HK1_2_DR',
        'description': '',
        'reported': False,
        'creationDate': 1538589751863,
        'lastUpdate': 1538590345679,
        'creator': {
            'loginName': 'administrator'
        },
        'public': True,
        'uniqueCompoundIds': 20,
        'uniqueCombinationIds': 0,
        'experiment': {
            'expRefId': 'EXP-12',
            'folderName': '/some/path3',
            'name': 'HK1_2 ADP-Glo Assay',
            'description': 'DRC assay',
            'creator': {
                'loginName': 'user666'
            },
            'lastUpdate': 1534518773144,
            'corporateId': '',
            'annotations': {}
        },
        'layers':
            [
                {
                    'layerIndex': 0,
                    'name': 'LUM (Crosstalk Corrected)',
                    'subExperimentStats':
                        [
                            {
                                'subExperiment': {
                                    'Enzyme': 'HK1'},
                                'globalStandardDeviation': 111.8258
                            },
                            {
                                'subExperiment': {
                                    'Enzyme': 'HK2'},
                                'globalStandardDeviation': 120.75969
                            }
                        ],
                    'normalization': {
                        'id': 'COMPOUNDS'},
                    'condensing':
                        {
                            'id': 'GD_SMART_FIT',
                            'parameters':
                                {
                                    'modelSelection': 'Automatic',
                                    'allowFixedNHill': 'false',
                                }
                        },
                    'calculatedFitColumns':
                        [
                            {
                                'id': 'curveDiagnostics',
                                'parameters':
                                    {
                                        'maxBellShape': '30.0',
                                        'maxConsecutiveMasks': '2.0',
                                        'maxTotalMasks': '25.0'
                                    }
                            }
                        ]
                }
            ]
    }
]

mock_genedata_compounds_response = [
    {
        'qcsRefId': 'QCS-3',
        'layerIndex': 2,
        'compoundId': 'CP_0000001',
        'compoundIndex': 0,
        'annotations': {},
        'condensing': {
            'validDataPointCount': 30},
        'fitting': {
            'isDummyFitResult': False,
            'isConstantFitResult': False,
            'SELog10AC50': 0.020407498,
            'SES0': 1.0737665,
            'activities': [-49.947186, -12.250469, 5.7841616],
            'masked': [False, False, False],
            'calculatedColumns': {
                'curveDiagnostics-lastActivity': -55.263973},
            'drcPlot': 'drcPlots/Qj1sLS0xNkxvWCOY'
        },
        'concUnit': {
            'baseUnit': 'M', 'log10mult': -6
        },
        'calculatedColumns': {
            'curveDiagnostics-class': 'Model',
        }
    },
    {
        'qcsRefId': 'QCS-4',
        'layerIndex': 0,
        'compoundId': 'REO-1000001-01',
        'compoundIndex': 0,
        'subExperiment': {'Enzyme': 'HK1'},
        'annotations': {},
        'condensing': {
            'validDataPointCount': 18},
        'fitting': {
            'isDummyFitResult': False,
            'activities': [-49.947186, -12.250469, 5.7841616],
            'linearConcentrations': [10.0, 3.3333333, 1.1111112],
            'log10Concentrations': [-5.0, -5.4771214, -5.9542427],
            'masked': [False, False, False],
            'isValid': True,
            'log10UpperConfidenceLimit': -4.98263,
            'description': '',
            'calculatedColumns': {
                'curveDiagnostics-class': 'Model',
                'curveDiagnostics-lastActivity': -55.263973},
            'drcPlot': 'drcPlots/Qj1sLS0xNkxvWCOY'
        },
        'concUnit': {
            'baseUnit': 'M', 'log10mult': -6
        },
        'calculatedColumns': {
            'curveDiagnostics-lastActivity': -55.263973
        }
    },
    {
        'qcsRefId': 'QCS-26',
        'layerIndex': 0,
        'compoundId': 'REO-1000004-01',
        'compoundIndex': 0,
        'annotations': {},
        'condensing': {'validDataPointCount': 16},
        'fitting': {
            'isDummyFitResult': False,
            'activities': [89.217094, 71.90298, 37.32],
            'linearConcentrations': [30.0, 10.0, 3.3333333],
            'log10Concentrations': [-4.5228786, -5.0, -5.4771214],
            'masked': [False, False, False],
            'algorithmVersion': 'custom',
            'description': '',
            'calculatedColumns': {
                'qAC50uM-qAC50uM': 4.4556737},
            'drcPlot': 'drcPlots/Qj1sHS0xNk5vWF1w'
        },
        'concUnit': {
            'baseUnit': 'M',
            'log10mult': -6
        },
        'calculatedColumns': {
            'qAC50uM-qAC50uM': 4.4556737
        }
    }
]

mock_genedata_plates_response = [
    {
        'qcsRefId': 'QCS-4',
        'barcode': 'Plate_001',
        'date': 1534360847000,
        'source': '\\\\EgnyteDrive\\path\\file',
        'layerIndex': 0,
        'plateIndex': 0,
        'runIndex': 0,
        'rowCount': 16,
        'columnCount': 24,
        'wellCount': 384,
        'controlPlate': False,
        'masked': False,
        'properties': {
            'EnVision_Protocol': 'HK1_2 LUM'},
        'annotations': {},
        'wellTypes': {
            'COMPOUND': {
                'mean': 153.05836,
                'median': 0.0,
                'sd': 262.1964,
                'rsd': 111.8258,
                'min': -77.20668,
                'max': 661.95685},
            'NEUTRAL_CONTROL': {
                'mean': 585.2848,
                'median': 564.1521,
                'sd': 64.06792,
                'rsd': 58.45275,
                'min': 501.47488,
                'max': 718.47736
            },
            'BLANK_CONTROL': {
                'mean': -75.55916,
                'median': -77.56589,
                'sd': 5.8615675,
                'rsd': 3.3041153,
                'min': -81.132416,
                'max': -53.221664
            },
            'UNDEFINED': {
                'mean': -99.8994,
                'median': -99.908455,
                'sd': 0.08206312,
                'rsd': 0.046014603,
                'min': -100.0,
                'max': -99.39795
            }
        },
        'wells': {
            'types': ['COMPOUND', 'COMPOUND', 'COMPOUND'],
            'rawValues': [288722.0, 72470.0, 50709.0],
            'correctedValues': [124.001305, -43.77507, -60.658],
            'masked': [False, False, False],
            'tags': [[], [], [], []],
            'maskingReasons': ['', '', 'Manual (well)'],
            'subExperiments': {
                'Enzyme': ['HK1', 'HK1', 'HK1']
            },
            'compoundIds': ['REO-1000001-01', 'REO-1000001-01', None],
            'concentrations': [50.0, 16.666666, 'NaN']
        },
        'subExperimentStats': [
            {
                'subExperiment': {'Enzyme': 'HK1'}
            },
            {
                'subExperiment': {'Enzyme': 'HK2'},
                'sb': 0.0
            }
        ]
    },
    {
        'qcsRefId': 'QCS-3',
        'barcode': 'Plate_0801',
        'date': 1534360847000,
        'source': '\\\\EgnyteDrive\\path\\file2',
        'layerIndex': 0,
        'plateIndex': 0,
        'properties': {
            'EnVision_Protocol': 'HK1_2 LUM'},
        'annotations': {},
        'wellTypes': {
            'COMPOUND': {
                'mean': 153.05836,
                'median': 0.0,
                'sd': 262.1964,
                'rsd': 111.8258,
                'min': -77.20668,
                'max': 661.95685},
            'NEUTRAL_CONTROL': {
                'mean': 585.2848,
                'median': 564.1521,
                'sd': 64.06792,
                'rsd': 58.45275,
                'min': 501.47488,
                'max': 718.47736
            },
            'BLANK_CONTROL': {
                'mean': -75.55916,
                'median': -77.56589,
                'sd': 5.8615675,
                'rsd': 3.3041153,
                'min': -81.132416,
                'max': -53.221664
            },
            'UNDEFINED': {
                'mean': -99.8994,
                'median': -99.908455,
                'sd': 0.08206312,
                'rsd': 0.046014603,
                'min': -100.0,
                'max': -99.39795
            }
        },
        'wells': {
            'types': ['COMPOUND', 'COMPOUND', 'COMPOUND'],
            'rawValues': [288722.0, 72470.0, 50709.0],
            'correctedValues': [124.001305, -43.77507, -60.658],
            'masked': [False, False, False],
            'tags': [[], [], [], []],
            'maskingReasons': ['', '', 'Manual (well)'],
            'subExperiments': {
                'Enzyme': ['HK1', 'HK1', 'HK1']
            },
            'compoundIds': ['REO-1000001-01', 'REO-1000001-01', None],
            'concentrations': [50.0, 16.666666, 'NaN']
        },
        'subExperimentStats': [
            {
                'subExperiment': {'Enzyme': 'HK1'}
            },
            {
                'subExperiment': {'Enzyme': 'HK2'},
                'sb': 0.0
            }
        ]
    },
    {
        'qcsRefId': 'QCS-26',
        'barcode': 'Plate_00991',
        'date': 1534360847000,
        'source': '\\\\EgnyteDrive\\path\\file3',
        'layerIndex': 0,
        'plateIndex': 0,
        'properties': {
            'EnVision_Protocol': 'HK1_2 LUM'},
        'annotations': {},
        'wellTypes': {
            'COMPOUND': {
                'mean': 153.05836,
                'median': 0.0,
                'sd': 262.1964,
                'rsd': 111.8258,
                'min': -77.20668,
                'max': 661.95685},
            'NEUTRAL_CONTROL': {
                'mean': 585.2848,
                'median': 564.1521,
                'sd': 64.06792,
                'rsd': 58.45275,
                'min': 501.47488,
                'max': 718.47736
            },
            'BLANK_CONTROL': {
                'mean': -75.55916,
                'median': -77.56589,
                'sd': 5.8615675,
                'rsd': 3.3041153,
                'min': -81.132416,
                'max': -53.221664
            },
            'UNDEFINED': {
                'mean': -99.8994,
                'median': -99.908455,
                'sd': 0.08206312,
                'rsd': 0.046014603,
                'min': -100.0,
                'max': -99.39795
            }
        },
        'wells': {
            'types': ['COMPOUND', 'COMPOUND', 'COMPOUND'],
            'rawValues': [288722.0, 72470.0, 50709.0],
            'correctedValues': [124.001305, -43.77507, -60.658],
            'masked': [False, False, False],
            'tags': [[], [], [], []],
            'maskingReasons': ['', '', 'Manual (well)'],
            'subExperiments': {
                'Enzyme': ['HK1', 'HK1', 'HK1']
            },
            'compoundIds': ['REO-1000001-01', 'REO-1000001-01', None],
            'concentrations': [50.0, 16.666666, 'NaN']
        },
        'subExperimentStats': [
            {
                'subExperiment': {'Enzyme': 'HK1'}
            },
            {
                'subExperiment': {'Enzyme': 'HK2'},
                'sb': 0.0
            }
        ]
    }
]

qcsession_tables = [
    'gd_experiments',
    'gd_qcsessions',
    'gd_qcsession_annotations',
    'gd_methods',
    'gd_method_parameters',
    'gd_layers',
    'gd_layer_annotations',
    'gd_layer_calculated_fit_columns',
    'gd_layer_sub_experiment_stats',
    'gd_layer_sub_experiment'
]
compounds_tables = [
    'gd_compounds',
    'gd_compound_fitting',
    'gd_compound_fitting_calculated_columns',
    'gd_compound_calculated_columns',
    'gd_compound_sub_experiment',
    'gd_compound_annotations',
    'gd_compound_condensing_scope',
    'gd_compound_fitting_fit_columns',
    'gd_compound_condensing_columns'
]
plates_tables = [
    'gd_plates',
    'gd_plate_well_types',
    'gd_plate_wells',
    'gd_plate_sub_experiment_stats',
    'gd_plate_sub_experiment',
    'gd_plate_sub_experiment_columns',
    'gd_plate_properties',
    'gd_plate_annotations',
    'gd_plate_columns',
    'gd_plate_well_properties',
    'gd_plate_well_sub_experiments',
    'gd_plate_well_condensing_scopes'
]
test_qcsessions = ['QCS-3', 'QCS-26', 'QCS-4']
test_compounds = 'CP_0000001, REO-1000001-01, REO-1000004-01'
test_plates = 'Plate_001, Plate_0801, Plate_00991'


class MockGDAPIHelper:
    def __init__(self, **kwargs):
        self.kwargs = None

    def get_qc_sessions(self, **kwargs):
        self.kwargs = kwargs
        return mock_genedata_qcsessions_response.copy()

    def get_compounds(self, **kwargs):
        self.kwargs = kwargs
        return mock_genedata_compounds_response.copy()

    def get_plates(self, **kwargs):
        self.kwargs = kwargs
        return mock_genedata_plates_response.copy()


class MockSqlAlchemyConnection:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def execute(self, sql):
        pass


class MockSqlAlchemyEngine:
    def begin(self):
        return MockSqlAlchemyConnection()


class MockPostgresHookAlchemy(MockPostgresHook):
    def get_sqlalchemy_engine(self):
        return MockSqlAlchemyEngine()


class DAGTestCase(TestCase):
    def setUp(self):
        logging.basicConfig(level='DEBUG')

    def test_load_qcsessions(self):
        return_value = MockGDAPIHelper()
        with self.assertLogs(level='DEBUG') as logs:
            qcs, tables = load_qcsessions(return_value)
        self.assertEqual(len(logs.records), 1)
        self.assertEqual(
            'All QCSessions data are ready to inserting',
            logs.records[0].getMessage())

        self.assertListEqual(qcs, test_qcsessions)
        self.assertListEqual([x[0] for x in tables], qcsession_tables)
        for t in [x[1] for x in tables]:
            self.assertTrue(isinstance(t, pd.DataFrame))
        for type_ in [x[2] for x in tables]:
            self.assertIsNone(type_)
        # check data converting
        self.assertTrue(is_datetime64_ns_dtype(tables[0][1]['last_update']))
        self.assertTrue(is_datetime64_ns_dtype(tables[1][1]['creation_date']))
        self.assertTrue(is_datetime64_ns_dtype(tables[1][1]['last_update']))

    def test_load_compounds(self):
        return_value = MockGDAPIHelper()

        with self.assertLogs(level='DEBUG') as logs:
            tables = load_compounds(return_value, test_qcsessions)
        self.assertLessEqual(len(logs.records), 7)
        self.assertEqual(
             f'Request compounds for QCSessions {test_qcsessions}',
             logs.records[0].getMessage())
        self.assertEqual(
            f'Loaded compounds: {test_compounds}',
            logs.records[1].getMessage())
        self.assertEqual(
            'Compounds data are ready to insert into database',
            logs.records[len(logs.records) - 1].getMessage())

        self.assertListEqual([x[0] for x in tables], compounds_tables)
        for t in [x[1] for x in tables]:
            self.assertTrue(isinstance(t, pd.DataFrame))
        for type_ in [x[2] for x in tables]:
            self.assertIsNone(type_)

    def test_load_plates(self):
        return_value = MockGDAPIHelper()

        with self.assertLogs(level='DEBUG') as logs:
            tables = load_plates(return_value, test_qcsessions)

        self.assertLessEqual(len(logs.records), 8)
        self.assertEqual(
            f'Request plates for QCSessions {test_qcsessions}',
            logs.records[0].getMessage())
        self.assertEqual(
            f'Next plates (barcode) were downloaded: {test_plates}',
            logs.records[1].getMessage())
        self.assertEqual(
            'Plates data are ready to insert into database',
            logs.records[len(logs.records) - 1].getMessage())

        self.assertListEqual([x[0] for x in tables], plates_tables)
        for t in [x[1] for x in tables]:
            self.assertTrue(isinstance(t, pd.DataFrame))
        for type_ in [x[2] for x in tables]:
            if not isinstance(type_, dict):
                self.assertIsNone(type_)

    @patch('load_genedata_dag.get_genedata_helper')
    @patch('load_genedata_dag.PostgresHook', new=MockPostgresHookAlchemy)
    @patch('pandas.DataFrame.to_sql')
    def test_load_genedata(self, df_to_sql, get_gd_help):
        get_gd_help.return_value = MockGDAPIHelper()
        df_to_sql.return_value = MagicMock()
        with self.assertLogs(level='DEBUG') as logs:
            next_step = load_genedata()
        self.assertTrue(get_gd_help.called)
        self.assertTrue(df_to_sql.called)

        self.assertEqual(
            f'Task successfully done',
            logs.records[len(logs.records) - 1].getMessage())
        self.assertEqual(next_step, 'finish')
