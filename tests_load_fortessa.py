from io import StringIO
import logging
from unittest import TestCase
from unittest.mock import patch

from load_fortessa_dag import (
    extract_files,
    new_files_check,
    merge_output)


class MockPostgresHook:
    def __init__(self, postgres_conn_id=None, first=None, autocommit=False):
        pass

    def get_first(self, query, *args, **kwargs):
        if query.find('COUNT(*)') > -1:
            return (13,)

    def get_records(self, query, *args, **kwargs):
        return [(1, 100500, 'Super experiment', 'P-123', 'somefakefile',
                 'RAW_DATA')]

    def insert_rows(self, *args, **kwargs):
        pass

    def run(self, *args, **kwargs):
        pass

    def get_sqlalchemy_engine(self):
        pass


class MockPostgresHookEmpty(MockPostgresHook):
    def get_first(self, *args, **kwargs):
        return (0,)


class MockDSAPIHelper:
    def __init__(self, **kwargs):
        pass

    def get_file(self, **kwargs):
        return StringIO(
            ',"col1, col1","col 2","col 3","col 4",Col 5,\n'
            'Specimen_001_A3_A3.fcs,55.8,97.2,8.28,90.8,14746,\n'
            'Specimen_001_C8_C8.fcs,6.00,84.9,67.8,79.6,10815,\n'
            'Specimen_001_C9_C9.fcs,6.24,85.5,69.7,81.5,8683,\n'
            'Mean,49.0,84.0,44.5,80.3,32821,\n'
            'SD,18.1,23.4,30.4,22.2,14757,\n'
        )


class MockTaskInstance:
    def xcom_pull(self, task_ids=None, key=None):
        if task_ids == 'extract_files':
            return {
                'file1': 'OK',
                'file2': 'OK',
                'file3': 'Parse error'
            }
        elif task_ids == 'process_files':
            return [('file2', 'Calculation error',)]
        else:
            return None

    def xcom_push(self, key, value):
        pass


@patch('load_fortessa_dag.get_ds_helper', new=MockDSAPIHelper)
class DAGTestCase(TestCase):
    def setUp(self):
        logging.basicConfig(level='DEBUG')

    @patch('load_fortessa_dag.TransactPostgresHook', new=MockPostgresHook)
    def test_extract_files(self):
        with self.assertLogs(level='DEBUG') as logs:
            extract_files()
        self.assertEqual(len(logs.records), 5)
        self.assertIn(
            '1 files found for processing',
            logs.records[0].getMessage())
        self.assertIn(
            'Started loading file with id=1',
            logs.records[1].getMessage())
        self.assertEqual(
            'Successfully parsed file somefakefile',
            logs.records[2].getMessage())
        self.assertEqual(
            'Saving to database skipped',
            logs.records[3].getMessage())
        self.assertEqual(
            'Finished loading file with id=1',
            logs.records[4].getMessage())

    @patch('load_fortessa_dag.PostgresHook', new=MockPostgresHook)
    def test_new_files_check_positive(self):
        with self.assertLogs(level='INFO') as logs:
            self.assertEqual(new_files_check(), 'create_files')
        self.assertEqual(
            logs.records[0].getMessage(),
            'Found 13 new files. Moving to load step.')

    @patch('load_fortessa_dag.PostgresHook', new=MockPostgresHookEmpty)
    def test_new_files_check_negative(self):
        with self.assertLogs(level='INFO') as logs:
            self.assertEqual(new_files_check(), 'finish')
        self.assertEqual(
            logs.records[0].getMessage(),
            'No new files found. Moving to finish.')

    def test_merge_output(self):
        self.assertEqual(merge_output(ti=MockTaskInstance()),
                         "*OK*: `file1`\n"
                         "*Error*: `file2` (Calculation error)\n"
                         "*Error*: `file3` (Parse error)")
